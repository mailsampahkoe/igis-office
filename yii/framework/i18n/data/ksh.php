<?php
/**
 * Locale data for 'ksh'.
 *
 * This file is automatically generated by yiic cldr command.
 *
 * Copyright © 1991-2007 Unicode, Inc. All rights reserved.
 * Distributed under the Terms of Use in http://www.unicode.org/copyright.html.
 *
 * Copyright © 2008-2011 Yii Software LLC (http://www.yiiframework.com/license/)
 */
return array (
  'version' => '6546',
  'numberSymbols' => 
  array (
    'alias' => '',
    'decimal' => '.',
    'group' => ',',
    'list' => ';',
    'percentSign' => '%',
    'plusSign' => '+',
    'minusSign' => '-',
    'exponential' => 'E',
    'perMille' => '‰',
    'infinity' => '∞',
    'nan' => 'NaN',
  ),
  'decimalFormat' => '#,##0.###',
  'scientificFormat' => '#E0',
  'percentFormat' => '#,##0 %',
  'currencyFormat' => '#,##0.00 ¤',
  'currencySymbols' => 
  array (
    'AUD' => 'AU$',
    'BRL' => 'R$',
    'CAD' => 'CA$',
    'CNY' => 'CN¥',
    'EUR' => '€',
    'GBP' => '£',
    'HKD' => 'HK$',
    'ILS' => '₪',
    'INR' => '₹',
    'JPY' => 'JP¥',
    'KRW' => '₩',
    'MXN' => 'MX$',
    'NZD' => 'NZ$',
    'THB' => '฿',
    'TWD' => 'NT$',
    'USD' => 'US$',
    'VND' => '₫',
    'XAF' => 'FCFA',
    'XCD' => 'EC$',
    'XOF' => 'CFA',
    'XPF' => 'CFPF',
  ),
  'monthNames' => 
  array (
    'wide' => 
    array (
      1 => 'Jannewa',
      2 => 'Fäbrowa',
      3 => 'Määz',
      4 => 'Aprell',
      5 => 'Mäi',
      6 => 'Juuni',
      7 => 'Juuli',
      8 => 'Oujoß',
      9 => 'Septämber',
      10 => 'Oktoober',
      11 => 'Novämber',
      12 => 'Dezämber',
    ),
    'abbreviated' => 
    array (
      1 => 'Jan.',
      2 => 'Fäb.',
      3 => 'Mar.',
      4 => 'Apr.',
      5 => 'Mäi',
      6 => 'Jun.',
      7 => 'Jul.',
      8 => 'Oug.',
      9 => 'Säp.',
      10 => 'Okt.',
      11 => 'Nov.',
      12 => 'Dez.',
    ),
  ),
  'monthNamesSA' => 
  array (
    'narrow' => 
    array (
      1 => 'J',
      2 => 'F',
      3 => 'M',
      4 => 'A',
      5 => 'M',
      6 => 'J',
      7 => 'J',
      8 => 'A',
      9 => 'S',
      10 => 'O',
      11 => 'N',
      12 => 'D',
    ),
  ),
  'weekDayNames' => 
  array (
    'wide' => 
    array (
      0 => 'Sunndaach',
      1 => 'Moondaach',
      2 => 'Dinnsdaach',
      3 => 'Metwoch',
      4 => 'Dunnersdaach',
      5 => 'Friidaach',
      6 => 'Samsdaach',
    ),
    'abbreviated' => 
    array (
      0 => 'Su.',
      1 => 'Mo.',
      2 => 'Di.',
      3 => 'Me.',
      4 => 'Du.',
      5 => 'Fr.',
      6 => 'Sa.',
    ),
  ),
  'weekDayNamesSA' => 
  array (
    'narrow' => 
    array (
      0 => 'S',
      1 => 'M',
      2 => 'D',
      3 => 'M',
      4 => 'D',
      5 => 'F',
      6 => 'S',
    ),
  ),
  'eraNames' => 
  array (
    'abbreviated' => 
    array (
      0 => 'v.Ch.',
      1 => 'n.Ch.',
    ),
    'wide' => 
    array (
      0 => 'vür Chrestus',
      1 => 'noh Chrestus',
    ),
    'narrow' => 
    array (
      1 => 'n.Ch.',
    ),
  ),
  'dateFormats' => 
  array (
    'full' => 'EEEE, \'dä\' d. MMMM y',
    'long' => 'd. MMMM y',
    'medium' => 'd. MMM y',
    'short' => 'd. M. yyyy',
  ),
  'timeFormats' => 
  array (
    'full' => 'HH:mm:ss zzzz',
    'long' => 'HH:mm:ss z',
    'medium' => 'HH:mm:ss',
    'short' => 'HH:mm',
  ),
  'dateTimeFormat' => '{1} {0}',
  'amName' => 'Uhr des vormittags',
  'pmName' => 'Uhr des nachmittags',
  'orientation' => 'ltr',
  'languages' => 
  array (
    'ab' => 'Abchaasesch',
    'af' => 'Afrikaans',
    'am' => 'Amhaaresch',
    'ar' => 'Arabescha',
    'as' => 'Aßameesesch',
    'asa' => 'Pare',
    'ay' => 'Aimaresch',
    'az' => 'Asserbaidschaanesch',
    'be' => 'Wiißrußesch',
    'bem' => 'Bemba',
    'bez' => 'Bena',
    'bg' => 'Bulljaaresch',
    'bm' => 'Bambara',
    'bn' => 'Bängjaalesch',
    'bo' => 'Tibeetesch',
    'brx' => 'Boddo',
    'bs' => 'Boßnesch',
    'ca' => 'Kattalanesch',
    'cs' => 'Tschäschesch',
    'cy' => 'Walliisesch',
    'da' => 'Dänesch',
    'de' => 'Deutsch',
    'de_at' => 'Deutsch uß Ößterich',
    'de_ch' => 'Deutsch uß der Schweijz',
    'dv' => 'Divehesch',
    'dz' => 'Butanesch',
    'ebu' => 'Embu',
    'efi' => 'Efik',
    'el' => 'Jriischesch',
    'en' => 'Änglesch',
    'en_au' => 'Änglesch uß Außtraalije',
    'en_ca' => 'Änglesch uß Kannaada',
    'en_gb' => 'Brittesch Änglesch',
    'en_us' => 'Amärreskaanesch Änglesch',
    'eo' => 'Esperanto',
    'es' => 'Schpaanesch',
    'es_419' => 'Schpaanesch uß Lattein-Ammärrika',
    'es_es' => 'Schpaanesch uß Schpaaneje',
    'et' => 'Äßnesch',
    'eu' => 'Baskesch',
    'fa' => 'Pärsesch',
    'fi' => 'Finnesch',
    'fil' => 'Fillipiinesch',
    'fj' => 'Fidschesch',
    'fr' => 'Franzüüsesch',
    'fr_ca' => 'Franzüüsesch uß Kannada',
    'fr_ch' => 'Franzüüsesch uß de Schweijz',
    'ga' => 'Ieresch',
    'gl' => 'Jalliizesch',
    'gn' => 'Juwaraanesch',
    'gsw' => 'Schwitzerdütsch',
    'gu' => 'Gutscharatesch',
    'ha' => 'Haußa',
    'haw' => 'Hauajaanesch',
    'he' => 'Hebrähesch',
    'hi' => 'Hindi',
    'hr' => 'Krowatesch',
    'ht' => 'Ha\'iitesch',
    'hu' => 'Unjarresch',
    'hy' => 'Areenesch',
    'id' => 'Indoneesesch',
    'ig' => 'Igbo',
    'is' => 'Ißländesch',
    'it' => 'Etalljänesch',
    'ja' => 'Japaanesch',
    'jv' => 'Javaanesch',
    'ka' => 'Je\'orjesch',
    'kea' => 'Kapvärdesch',
    'kk' => 'Kassakesch',
    'km' => 'Khmer',
    'kn' => 'Kannada',
    'ko' => 'Korrejaanesch',
    'ks' => 'Kschamieresch',
    'ku' => 'Kurdesch',
    'ky' => 'Kirjiisesch',
    'la' => 'Lateijnesch',
    'lah' => 'de Landa-Schprooche',
    'lb' => 'Luxemborjesch',
    'ln' => 'Lingjalla',
    'lo' => 'Lahootesch',
    'lt' => 'Littouesch',
    'luy' => 'Luyjanesch',
    'lv' => 'Lättesch',
    'mg' => 'Madajaßesch',
    'mi' => 'Maahori',
    'mk' => 'Mazedoonesch',
    'ml' => 'Mallajalam',
    'mn' => 'Mongjolesch',
    'mr' => 'Marraatesch',
    'ms' => 'Mallaijesch',
    'mt' => 'Malteesesch',
    'mul' => '-ongerscheidlijje Schprooche-',
    'my' => 'Burmessesch',
    'nb' => 'Norrweejesch Bokmål',
    'nd' => 'Nood-Ndebele',
    'ne' => 'Nepallessesch',
    'nl' => 'Holländesch',
    'nl_be' => 'Flämesch',
    'nn' => 'Neu-Norrweejesch',
    'no' => 'Norrweejesch',
    'nso' => 'Nood-Sooto',
    'ny' => 'Schi-Schewa',
    'or' => 'Oriija',
    'os' => 'Oßeetesch',
    'pa' => 'Panschaabesch',
    'pl' => 'Pollnesch',
    'ps' => 'Paschtunesch',
    'pt' => 'Pochtojiesesch',
    'pt_br' => 'Brasilljaanesch Pochtojiesesch',
    'pt_pt' => 'Pochtojiisesch uß Pochtojall',
    'qu' => 'Kättschowa',
    'rm' => 'Räto-Romaanesch',
    'rn' => 'K-Rundesch',
    'ro' => 'Rumänesch',
    'rof' => 'Kirombo',
    'ru' => 'Rußßesch',
    'rw' => 'Kinja-Ruandesch',
    'sa' => 'Sanskrit',
    'sah' => 'Jackutesch',
    'sd' => 'Sinndi',
    'se' => 'Nood-Lappländesch',
    'sg' => 'Sangjo',
    'sh' => 'Särbokowatesch',
    'si' => 'Singjaleesesch',
    'sk' => 'ẞlovakesch',
    'sl' => 'ẞloveenesch',
    'sm' => 'Sammohanesch',
    'sn' => 'Schi-Schona',
    'so' => 'Somaalesch',
    'sq' => 'Albaanesch',
    'sr' => 'Särbesch',
    'ss' => 'Si-Swatesch',
    'st' => 'Söd-Sooto',
    'su' => 'Sindaneesesch',
    'sv' => 'Schweedesch',
    'sw' => 'Suaheelesch',
    'swb' => 'Kommooresch',
    'ta' => 'Tamiilesch',
    'te' => 'Telluuju',
    'tet' => 'Tetumsch',
    'tg' => 'Tadschiikesch',
    'th' => 'Tailändesch',
    'ti' => 'Tigrinianesch',
    'tk' => 'Törkmeenesch',
    'tl' => 'Tagalog',
    'tn' => 'Se-Zwaanesch',
    'to' => 'Tongjaanesch',
    'tpi' => 'Took Pisin',
    'tr' => 'Törkesch',
    'ts' => 'Xi-Zongjanesch',
    'ty' => 'Tahitesch',
    'ug' => 'Uj\'juuersch',
    'uk' => 'Ukrainesch',
    'und' => '-onbikannt-',
    'ur' => 'Urdu/Hindi',
    'uz' => 'Ußbeekesch',
    've' => 'Wenda',
    'vi' => 'Vijätnammeesesch',
    'wae' => 'Walserdütsch',
    'wo' => 'Woloff',
    'xh' => 'Isi-Khoosa',
    'yo' => 'Joruuba',
    'yue' => 'Kanton-Schineesesch',
    'zh' => 'Schineesesch',
    'zh_hans' => 'Schineesesch en de eijfacher Schreff',
    'zh_hant' => 'Schineesesch en de tradizjonälle Schreff',
    