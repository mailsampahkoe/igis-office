-- MySQL dump 10.13  Distrib 5.6.41, for Linux (x86_64)
--
-- Host: localhost    Database: igis-office
-- ------------------------------------------------------
-- Server version	5.6.41

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1539636091),('m130524_201442_init',1539636097);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmakses`
--

DROP TABLE IF EXISTS `tmakses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmakses` (
  `aksesid` varchar(20) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `superadmin` tinyint(1) NOT NULL DEFAULT '0',
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`aksesid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmakses`
--

LOCK TABLES `tmakses` WRITE;
/*!40000 ALTER TABLE `tmakses` DISABLE KEYS */;
INSERT INTO `tmakses` VALUES ('0','superadmin','Superadmin',1,'admin','2019-11-01 00:00:00','admin','superadmin','2019-11-01 00:00:00','admin',''),('1','admin','Admin',1,'admin','2018-10-15 00:00:00','admin','admin','2018-10-15 00:00:00','admin','\0'),('10','staf','Staf',0,'admin','2019-11-01 00:00:00','admin','admin','2019-11-01 00:00:00','admin','\0'),('2','kaunit','Kepala Unit',1,'admin','2018-10-15 00:00:00','admin','admin','2018-10-15 00:00:00','admin',''),('3','kasubunit','Kepala Sub Unit',1,'admin','2018-10-15 00:00:00','admin','admin','2018-10-15 00:00:00','admin',''),('4','kabid','Kepala Bidang',1,'admin','2018-10-15 00:00:00','admin','admin','2018-10-15 00:00:00','admin',''),('5','kasubbid','Kepala Sub Bidang',1,'admin','2018-10-15 00:00:00','admin','admin','2018-10-15 00:00:00','admin',''),('6','kabag','Kepala Bagian / UPT',0,'admin','2018-10-15 00:00:00','admin','admin','2018-10-15 00:00:00','admin','\0'),('7','kasubbag','Kepala Sub Bagian / Seksie',0,'admin','2019-11-01 00:00:00','admin','admin','2019-11-01 00:00:00','admin','\0');
/*!40000 ALTER TABLE `tmakses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmbagian`
--

DROP TABLE IF EXISTS `tmbagian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmbagian` (
  `bagianid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`bagianid`),
  KEY `fk_tmbagian_reference_tmunit` (`unitid`),
  CONSTRAINT `fk_tmbagian_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmbagian`
--

LOCK TABLES `tmbagian` WRITE;
/*!40000 ALTER TABLE `tmbagian` DISABLE KEYS */;
INSERT INTO `tmbagian` VALUES ('A73C39EAD9D0548DDAE2',NULL,'01.','Balai Teknik Kesehatan Lingkungan dan Pengendalian Penyakit (BTKLPP)','123','-','','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0');
/*!40000 ALTER TABLE `tmbagian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tminstansi`
--

DROP TABLE IF EXISTS `tminstansi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tminstansi` (
  `instansiid` varchar(20) NOT NULL,
  `namainstansi` varchar(150) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `kodepos` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `jenisinstansi` varchar(50) DEFAULT NULL,
  `jenisdaerah` varchar(1) DEFAULT NULL,
  `kepda` varchar(50) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `namakepda` varchar(50) DEFAULT NULL,
  `logo` varchar(1000) DEFAULT NULL,
  `picpemda` varchar(1000) DEFAULT NULL,
  `namakadis` varchar(100) DEFAULT NULL,
  `nipkadis` varchar(50) DEFAULT NULL,
  `namakasi` varchar(100) DEFAULT NULL,
  `nipkasi` varchar(50) DEFAULT NULL,
  `namakabid` varchar(100) DEFAULT NULL,
  `nipkabid` varchar(50) DEFAULT NULL,
  `penomoran` varchar(50) DEFAULT NULL,
  `penomoran1` varchar(50) DEFAULT NULL,
  `penomoran2` varchar(50) DEFAULT NULL,
  `opadd` varchar(20) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(20) DEFAULT NULL,
  `opedit` varchar(20) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(20) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`instansiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tminstansi`
--

LOCK TABLES `tminstansi` WRITE;
/*!40000 ALTER TABLE `tminstansi` DISABLE KEYS */;
INSERT INTO `tminstansi` VALUES ('','Balai Teknik Kesehatan Lingkungan dan Pengendalian Penyakit - Kelas 1 Batam','Kelurahan Sei Binti Kecamatan Sagulung Kota Batam','0778-8075096','0778-8075097','29434','btklbatam@yahoo.co.id','1','1',NULL,'Batam',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2018-10-26 22:10:52','::1','admin','2018-11-14 09:36:08','43.225.186.122','\0');
/*!40000 ALTER TABLE `tminstansi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmjabatan`
--

DROP TABLE IF EXISTS `tmjabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmjabatan` (
  `jabatanid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `subunitid` varchar(20) DEFAULT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `subbagianid` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `terimadisposisi` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`jabatanid`),
  KEY `fk_tmjabatan_reference_tmunit` (`unitid`),
  KEY `fk_tmjabatan_reference_tmsubunit` (`subunitid`),
  KEY `fk_tmjabatan_reference_tmbagian` (`bagianid`),
  KEY `fk_tmjabatan_reference_tmsubbagian` (`subbagianid`),
  CONSTRAINT `fk_tmjabatan_reference_tmbagian` FOREIGN KEY (`bagianid`) REFERENCES `tmbagian` (`bagianid`),
  CONSTRAINT `fk_tmjabatan_reference_tmsubbagian` FOREIGN KEY (`subbagianid`) REFERENCES `tmsubbagian` (`subbagianid`),
  CONSTRAINT `fk_tmjabatan_reference_tmsubunit` FOREIGN KEY (`subunitid`) REFERENCES `tmsubunit` (`subunitid`),
  CONSTRAINT `fk_tmjabatan_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmjabatan`
--

LOCK TABLES `tmjabatan` WRITE;
/*!40000 ALTER TABLE `tmjabatan` DISABLE KEYS */;
INSERT INTO `tmjabatan` VALUES ('5380303FA54C3B4F0FF0',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613E98','Kepala Sub Bagian TU',5,'Ok','','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','2018-10-31 07:03:26','::1','\0'),('6984A5ADB3C25D599FF9',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA3','Staf ADKL',10,'-','\0','2018','admin','2018-11-07 06:05:52','112.215.174.136','admin','2018-11-14 10:27:29','43.225.186.122','\0'),('88618521FEFE2F296E27',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA2','Kepala Seksi PTL',5,'-','','2018','admin','2018-11-07 06:05:00','112.215.174.136','admin','2018-11-07 06:05:00','112.215.174.136','\0'),('893C493AA48B557CF999',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA1','Staf SE',10,'','\0','2018','admin','2018-11-05 02:16:11','127.0.0.1','admin','2018-11-14 10:27:14','43.225.186.122','\0'),('90CDFE4C8C5E4D546E41',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA3','Kepala Seksi ADKL',5,'-','','2018','admin','2018-11-07 06:05:38','112.215.174.136','admin','2018-11-07 06:05:38','112.215.174.136','\0'),('B2611EFDDC196804D239',NULL,NULL,'A73C39EAD9D0548DDAE2',NULL,'Kepala BTKLPP Kelas I Batam',4,'Ket....','\0','2018','admin','2018-10-23 15:41:22','202.154.185.250','admin','2018-11-14 10:31:23','43.225.186.122','\0'),('B668EC12AEF5D3E8E560',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA2','Staf PTL',10,'-','\0','2018','admin','2018-11-07 06:05:18','112.215.174.136','admin','2018-11-14 10:27:22','43.225.186.122','\0'),('D3BD701257A7063571A1',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613E98','Staf TU',10,'','\0','2018','admin','2018-11-05 02:15:46','127.0.0.1','admin','2018-11-14 10:27:05','43.225.186.122','\0'),('F8265B4659313AC9044F',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA1','Kepala Seksi SE',5,'','','2018','admin','2018-11-05 02:05:18','127.0.0.1','admin','2018-11-05 02:05:18','127.0.0.1','\0');
/*!40000 ALTER TABLE `tmjabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmjenis`
--

DROP TABLE IF EXISTS `tmjenis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmjenis` (
  `jenisid` varchar(20) NOT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`jenisid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmjenis`
--

LOCK TABLES `tmjenis` WRITE;
/*!40000 ALTER TABLE `tmjenis` DISABLE KEYS */;
INSERT INTO `tmjenis` VALUES ('A42FE1083EE3A6359DFC','AA','1233',NULL,'-','2018','admin','2018-10-29 06:16:05','::1','1','2018-10-29 06:16:09','::1',''),('A73C39EAD9D0548DDAA0','AR','Kearsipan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA1','HK','Hukum',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA2','IR','Informatika',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA3','KH','Kemahasiswaan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA4','KM','Komunikasi Publik',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA5','KN','Kekayaan Negara',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA6','KP','Kepegawaian',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA7','KR','Kerumahtanggaan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA8','KS','Kerja Sama',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAA9','KU','Keuangan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAB1','OT','Organisasi dan Tatalaksana',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAB2','PP','Pendidikan dan Pengajaran',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAB3','PR','Perencanaan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAB4','PS','Pengawasan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAB5','UM','Umum',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC0','AD','Analisis Determinan Kesehatan',NULL,'--','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','2018-11-13 20:05:34','112.215.245.137','\0'),('A73C39EAD9D0548DDAC1','DG','Perencanaan dan Pendayagunaan SDM Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC2','DL','Pelatihan Sumber Daya Manusia Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC3','DM','Peningkatan Mutu Sumber Daya Manusia Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC4','DP','Pendidikan Sumber Daya Manusia Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC5','FK','Pengawasan Alat Kesehatan dan Perbekalan Rumah Tangga',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC6','FO','Tata Kelola Obat Publik dan Perbekalan Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC7','FP','Produksi dan Distribusi Kefarmasian',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC8','FR','Penilaian Alat Kesehatan dan Perbekalan Rumah Tangga',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAC9','FY','Pelayanan Kefarmasian',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD0','GM','Gizi Masyarakat',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD1','HJ','Kesehatan Haji',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD2','JP','Pembiayaan dan Jaminan Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD3','KG','Kesehatan Keluarga',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD4','KI','Konsil Kedokteran Indonesia',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD5','KJ','Kesehatan Jiwa',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD6','KK','Penanggulangan Krisis',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD7','KL','Kesehatan Lingkungan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD8','KO','Kesehatan Kerja dan Olahraga',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAD9','LB','Penelitian dan Pengembangan Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE0','PK','Promosi Kesehatan dan Pemberdayaan Masyarakat',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE1','PM','Pencegahan dan Pengendalian Penyakit Menular Langsung',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE2','PV','Pencegahan dan Pengendalian Penyakit Tular Vektor Zoonotik',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE3','SR','Surveilans dan Karantina Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE4','TL','Pengembangan Teknologi Laboratorium',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE5','TM','Pencegahan dan Pengendalian Penyakit Tidak Menular',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE6','YK','Fasilitas Pelayanan Kesehatan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE7','YM','Mutu dan Akreditasi Pelayanan Masyarakat',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE8','YP','Pelayanan Kesehatan Primer',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAE9','YR','Pelayanan Kesehatan Rujukan',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('A73C39EAD9D0548DDAF0','YT','Pelayanan Kesehatan Tradisional',NULL,'-','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0');
/*!40000 ALTER TABLE `tmjenis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmmenu`
--

DROP TABLE IF EXISTS `tmmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmmenu` (
  `menuid` varchar(20) NOT NULL,
  `modulid` varchar(20) DEFAULT NULL,
  `parent` varchar(50) DEFAULT NULL,
  `menu` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `ishide` varchar(50) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`menuid`),
  KEY `tmmenu_modulid_fkey` (`modulid`),
  CONSTRAINT `tmmenu_modulid_fkey` FOREIGN KEY (`modulid`) REFERENCES `tmmodul` (`modulid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmmenu`
--

LOCK TABLES `tmmenu` WRITE;
/*!40000 ALTER TABLE `tmmenu` DISABLE KEYS */;
INSERT INTO `tmmenu` VALUES ('1',NULL,'Instansi','Instansi','instansi',1,'0',1),('101',NULL,'Surat Masuk','Surat Masuk','suratmasuk/admin',101,'0',2),('102',NULL,'Surat Masuk','Disposisi Kepala','disposisikabag/admin',102,'0',2),('103',NULL,'Surat Masuk','Disposisi Kasubbag','disposisikasubbag/admin',103,'0',2),('104',NULL,'Surat Masuk','Pelaksanaan','pelaksanaan/admin',104,'0',2),('105',NULL,'Surat Keluar','Surat Keluar','suratkeluar/admin',105,'0',2),('106',NULL,'Surat Keluar','Verifikasi Kasubbag','verifikasikasubbag/admin',106,'0',2),('107',NULL,'Surat Keluar','Verifikasi Kepala','verifikasikabag/admin',107,'0',2),('108',NULL,'Surat Keluar','Pengiriman','pengiriman/admin',108,'0',2),('2',NULL,'Tahun','Tahun','tahun/admin',2,'0',1),('201',NULL,'Laporan Rekap','Laporan Rekapitulasi','laporan',201,'0',3),('3',NULL,'Jenis','Jenis','jenis/admin',3,'0',1),('301',NULL,'Pengkodean','Pengkodean','kode/admin',301,'1',4),('302',NULL,'User','Pengaturan User','user/admin',302,'0',4),('303',NULL,'User','Ubah Password','user/password',303,'0',4),('304',NULL,'Pengumuman','Pengumuman','pengumuman/admin',4,'0',4),('5',NULL,'Bagian','Bagian','bagian/admin',5,'0',1),('6',NULL,'Bagian','Sub Bagian','subbagian/admin',6,'0',1),('7',NULL,'Pegawai','Pegawai','pegawai/admin',7,'0',1),('8',NULL,'Jabatan','Jabatan','jabatan/admin',8,'0',1),('9',NULL,'Perintah','Perintah','perintah/admin',9,'0',1);
/*!40000 ALTER TABLE `tmmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmmodul`
--

DROP TABLE IF EXISTS `tmmodul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmmodul` (
  `modulid` varchar(20) NOT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL,
  `nourut` int(11) DEFAULT NULL,
  `hidden` smallint(6) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`modulid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmmodul`
--

LOCK TABLES `tmmodul` WRITE;
/*!40000 ALTER TABLE `tmmodul` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmmodul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmpegawai`
--

DROP TABLE IF EXISTS `tmpegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmpegawai` (
  `pegawaiid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `subunitid` varchar(20) DEFAULT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `subbagianid` varchar(20) DEFAULT NULL,
  `jabatanid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `gelardepan` varchar(30) DEFAULT NULL,
  `gelarbelakang1` varchar(30) DEFAULT NULL,
  `gelarbelakang2` varchar(30) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tanggallahir` date DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `jeniskelamin` smallint(6) DEFAULT NULL,
  `jeniskepegawaian` smallint(6) DEFAULT NULL,
  `statusaktif` smallint(6) DEFAULT NULL,
  `statuspernikahan` smallint(6) DEFAULT NULL,
  `nohp` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`pegawaiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmpegawai`
--

LOCK TABLES `tmpegawai` WRITE;
/*!40000 ALTER TABLE `tmpegawai` DISABLE KEYS */;
INSERT INTO `tmpegawai` VALUES ('255600DE37D616370204',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA1','F8265B4659313AC9044F','197301232001121001','Rencana','S.Si','M.Kes','','',NULL,'',0,NULL,1,1,'081372894521','rentar.tarigan73@gmail.com',NULL,NULL,'admin','2018-11-13 20:22:37','112.215.245.137','admin','2018-11-14 10:41:50','43.225.186.122','\0'),('5041F58B668743AAA2DC',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA2','88618521FEFE2F296E27','196207071989031002','Supranoto','S.Sos','','','',NULL,'',0,NULL,1,1,'08127643568','pran_btklbtm@yahoo.com',NULL,NULL,'admin','2018-11-14 10:44:25','43.225.186.122','admin','2018-11-14 10:44:25','43.225.186.122','\0'),('6D6451EF5AAE3038EF73',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA3','6984A5ADB3C25D599FF9','','Dedi Sartomi','','','','',NULL,'',0,NULL,1,0,'081364924142','dedisartomi17@gmail.com',NULL,NULL,'admin','2018-11-14 10:47:54','43.225.186.122','admin','2018-11-14 10:47:54','43.225.186.122','\0'),('7251590CAA0BC772BC59',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA3','90CDFE4C8C5E4D546E41','197711152006041001','Zulhirdan Siregar','S.T.','','','',NULL,'',0,NULL,1,1,'085206348884','zul1511dan@gmail.com',NULL,NULL,'admin','2018-11-14 10:46:58','43.225.186.122','admin','2018-11-14 10:46:58','43.225.186.122','\0'),('7C7691F2A499DB759F9B',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613E98','D3BD701257A7063571A1','000000000','Admin','-','-','-','Batam','2018-11-07','Batam',0,NULL,1,0,'67687','admin@xxxxxxxx.com',NULL,NULL,'admin','0000-00-00 00:00:00','127.0.0.1','admin','2018-11-07 06:02:10','112.215.174.136','\0'),('7CB3E4CEA034FFA4FD76',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA2','B668EC12AEF5D3E8E560','','Haryo Seto Wicaksono, S.Si','','','','',NULL,'',0,NULL,1,0,'08563482922','hsetowicaksono@gmail.com',NULL,NULL,'admin','2018-11-14 10:45:49','43.225.186.122','admin','2018-11-14 10:45:49','43.225.186.122','\0'),('90B8BAA47F9F3838D085',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613E98','5380303FA54C3B4F0FF0','197505132001121002','Ismail','S.T.','M.Sc','','',NULL,'',0,NULL,1,1,'081536071472','ismail.btklpp@gmail.com',NULL,NULL,'admin','2018-11-13 20:20:19','112.215.245.137','admin','2018-11-14 10:34:00','43.225.186.122','\0'),('BC289275C34A1F2C61F3',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613EA1','893C493AA48B557CF999','','Aan Oktavia Yuman Lubis','SKM','','','',NULL,'',0,NULL,1,1,'085271207206','aanoktaviayumanlubis@gmail.com',NULL,NULL,'admin','2018-11-13 20:23:05','112.215.245.137','admin','2018-11-14 10:42:57','43.225.186.122','\0'),('C8C78126069666B06C1D',NULL,NULL,'A73C39EAD9D0548DDAE2',NULL,'B2611EFDDC196804D239','196405122000031001','Slamet Mulsiswanto','','','','',NULL,'',0,NULL,1,1,'08111987291','ariska2@yahoo.com',NULL,NULL,'admin','2018-11-13 20:20:59','112.215.245.137','admin','2018-11-14 10:31:46','43.225.186.122','\0'),('D0468AB6C3CDDF4B83EE',NULL,NULL,'A73C39EAD9D0548DDAE2','AD4D71EAF74CFF613E98','D3BD701257A7063571A1','','Lusiana Andayani','','','','',NULL,'',1,NULL,1,0,'085668269596','lusiana.la69@gmail.com',NULL,NULL,'admin','2018-11-13 20:21:39','112.215.245.137','admin','2018-11-14 10:40:09','43.225.186.122','\0');
/*!40000 ALTER TABLE `tmpegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmpegawaijabatan`
--

DROP TABLE IF EXISTS `tmpegawaijabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmpegawaijabatan` (
  `pegawaijabatanid` varchar(20) NOT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `jabatan` smallint(6) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusaktif` smallint(6) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`pegawaijabatanid`),
  KEY `fk_tmpegawa_reference_tmpegawa` (`pegawaiid`),
  CONSTRAINT `fk_tmpegawa_reference_tmpegawa` FOREIGN KEY (`pegawaiid`) REFERENCES `tmpegawai` (`pegawaiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmpegawaijabatan`
--

LOCK TABLES `tmpegawaijabatan` WRITE;
/*!40000 ALTER TABLE `tmpegawaijabatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmpegawaijabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmperintah`
--

DROP TABLE IF EXISTS `tmperintah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmperintah` (
  `perintahid` varchar(20) NOT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`perintahid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmperintah`
--

LOCK TABLES `tmperintah` WRITE;
/*!40000 ALTER TABLE `tmperintah` DISABLE KEYS */;
INSERT INTO `tmperintah` VALUES ('10',10,'Arsip','-','','2018','admin','2018-10-29 00:00:00','admin','admin','2018-10-29 00:00:00','admin','\0'),('201EF2C49B036ECCB91E',1,'Mohon Saran / Tanggapan','test ...','','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('508E4BBAB1F73B7E28BC',2,'Mohon Konsep Jawaban','--','','2018','admin','2018-10-25 05:53:55','::1','admin','2018-10-25 05:53:55','::1','\0'),('51CC713A295451D15C62',3,'Untuk Diketahui','test ...','','2018','admin','0000-00-00 00:00:00','127.0.0.1','1','0000-00-00 00:00:00','127.0.0.1','\0'),('6',6,'Untuk Dipergunakan','-','','2018','admin','2018-10-29 00:00:00','admin','admin','2018-10-29 00:00:00','admin','\0'),('7',7,'Dibicarakan Dengan Saya','-','','2018','admin','2018-10-29 00:00:00','admin','admin','2018-10-29 00:00:00','admin','\0'),('7F897739A13968C8A889',4,'Untuk Tindak Lanjut','test ...','','2018','admin','0000-00-00 00:00:00','127.0.0.1','1','0000-00-00 00:00:00','127.0.0.1','\0'),('8',8,'Teruskan Ke Staf','-','','2018','admin','2018-10-29 00:00:00','admin','admin','2018-10-29 00:00:00','admin','\0'),('9',9,'Fotocopy','-','','2018','admin','2018-10-29 00:00:00','admin','admin','2018-10-29 00:00:00','admin','\0'),('F8EC291302EB9FE6B658',5,'Untuk Dipertimbangkan','-','','2018','admin','2018-10-25 05:54:05','::1','admin','2018-10-25 05:54:05','::1','\0');
/*!40000 ALTER TABLE `tmperintah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmsubbagian`
--

DROP TABLE IF EXISTS `tmsubbagian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmsubbagian` (
  `subbagianid` varchar(20) NOT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`subbagianid`),
  KEY `fk_tmsubbag_reference_tmbagian` (`bagianid`),
  CONSTRAINT `fk_tmsubbag_reference_tmbagian` FOREIGN KEY (`bagianid`) REFERENCES `tmbagian` (`bagianid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmsubbagian`
--

LOCK TABLES `tmsubbagian` WRITE;
/*!40000 ALTER TABLE `tmsubbagian` DISABLE KEYS */;
INSERT INTO `tmsubbagian` VALUES ('AD4D71EAF74CFF613E98','A73C39EAD9D0548DDAE2','01.','Sub Bagian TU','alamat - ','keterangan - ','2018','admin','0000-00-00 00:00:00','127.0.0.1','admin','0000-00-00 00:00:00','127.0.0.1','\0'),('AD4D71EAF74CFF613EA1','A73C39EAD9D0548DDAE2','02.','Seksi SE','-','-','2018','Admin','2018-10-29 00:00:00','Admin','Admin','2018-10-29 00:00:00','Admin','\0'),('AD4D71EAF74CFF613EA2','A73C39EAD9D0548DDAE2','03.','Seksi PTL','-','-','2018','Admin','2018-10-29 00:00:00','Admin','Admin','2018-10-29 00:00:00','Admin','\0'),('AD4D71EAF74CFF613EA3','A73C39EAD9D0548DDAE2','04.','Seksi ADKL','-','-','2018','Admin','2018-10-29 00:00:00','Admin','Admin','2018-10-29 00:00:00','Admin','\0'),('AD4D71EAF74CFF613EA4','A73C39EAD9D0548DDAE2','05.','Instalasi Lab','-','-','2018','Admin','2018-10-29 00:00:00','Admin','Admin','2018-10-29 00:00:00','Admin','\0'),('AD4D71EAF74CFF613EA5','A73C39EAD9D0548DDAE2','06.','PK Dipa','-','-','2018','Admin','2018-10-29 00:00:00','Admin','Admin','2018-10-29 00:00:00','Admin','\0');
/*!40000 ALTER TABLE `tmsubbagian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmsubunit`
--

DROP TABLE IF EXISTS `tmsubunit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmsubunit` (
  `subunitid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`subunitid`),
  KEY `fk_tmsubuni_reference_tmunit` (`unitid`),
  CONSTRAINT `fk_tmsubuni_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmsubunit`
--

LOCK TABLES `tmsubunit` WRITE;
/*!40000 ALTER TABLE `tmsubunit` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmsubunit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmtahun`
--

DROP TABLE IF EXISTS `tmtahun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmtahun` (
  `tahunid` varchar(20) NOT NULL,
  `kode` varchar(4) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`tahunid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmtahun`
--

LOCK TABLES `tmtahun` WRITE;
/*!40000 ALTER TABLE `tmtahun` DISABLE KEYS */;
INSERT INTO `tmtahun` VALUES ('280190DC8904C4C6C86F','2019','2019','Tahun Berjalan',NULL,'admin','2018-11-13 20:03:04','112.215.245.137','admin','2018-11-13 20:03:04','112.215.245.137','\0'),('8B88B7FECCA39CA6957B','2018','2018','Tahun 2018',NULL,'admin','2018-11-04 21:18:56','127.0.0.1','admin','2018-11-04 21:18:56','127.0.0.1','\0');
/*!40000 ALTER TABLE `tmtahun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmunit`
--

DROP TABLE IF EXISTS `tmunit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmunit` (
  `unitid` varchar(20) NOT NULL,
  `urusanid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`unitid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmunit`
--

LOCK TABLES `tmunit` WRITE;
/*!40000 ALTER TABLE `tmunit` DISABLE KEYS */;
INSERT INTO `tmunit` VALUES ('1',NULL,'1','test',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
/*!40000 ALTER TABLE `tmunit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmuser`
--

DROP TABLE IF EXISTS `tmuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmuser` (
  `userid` varchar(20) NOT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(254) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `grup` smallint(6) DEFAULT NULL,
  `aksesid` varchar(20) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `nohp` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  `jeniskelamin` smallint(6) DEFAULT '0',
  `alamat` varchar(254) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `filenik` varchar(250) DEFAULT NULL,
  `npwp` varchar(30) DEFAULT NULL,
  `filenpwp` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`userid`),
  KEY `fk_tmakses_reference_tmuser` (`aksesid`),
  KEY `fk_tmuser_reference_tmpegawa` (`pegawaiid`),
  CONSTRAINT `fk_tmakses_reference_tmuser` FOREIGN KEY (`aksesid`) REFERENCES `tmakses` (`aksesid`),
  CONSTRAINT `fk_tmuser_reference_tmpegawa` FOREIGN KEY (`pegawaiid`) REFERENCES `tmpegawai` (`pegawaiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmuser`
--

LOCK TABLES `tmuser` WRITE;
/*!40000 ALTER TABLE `tmuser` DISABLE KEYS */;
INSERT INTO `tmuser` VALUES ('02D0E58BBEB60D981B25','90B8BAA47F9F3838D085','kasubagtu','202cb962ac59075b964b07152d234b70',NULL,NULL,NULL,'7','Kasubag TU','','','Koala.jpg',1,'2018','admin','2018-11-13 20:28:14','112.215.245.137','admin','2018-11-13 20:28:14','112.215.245.137','\0',0,NULL,NULL,NULL,NULL,NULL),('0E9E258821187AC50C7C','255600DE37D616370204','kasise','202cb962ac59075b964b07152d234b70',NULL,NULL,NULL,'7','Kasi SE','','','Jellyfish.jpg',1,'2018','admin','2018-11-13 20:29:39','112.215.245.137','admin','2018-11-13 20:29:39','112.215.245.137','\0',0,NULL,NULL,NULL,NULL,NULL),('1','7C7691F2A499DB759F9B','admin','e10adc3949ba59abbe56e057f20f883e','202cb962ac59075b964b07152d234b70','',1,'1','Administrator','0811','kuclukb@gmail.com','admin.jpg',1,'2018','admin','2018-10-15 00:00:00','admin','admin','2018-10-15 00:00:00','admin','\0',1,'Batam',NULL,NULL,NULL,NULL),('14BA7C8E1FCA4AAD5DA5','D0468AB6C3CDDF4B83EE','lusi','81dc9bdb52d04dc20036dbd8313ed055',NULL,NULL,NULL,'10','Staf TU','','',NULL,1,'2018','admin','2018-11-13 20:30:31','112.215.245.137','admin','2018-11-14 11:04:49','43.225.186.122','\0',0,NULL,NULL,NULL,NULL,NULL),('BEB05BB066DE40636325','7CB3E4CEA034FFA4FD76','haryo','202cb962ac59075b964b07152d234b70',NULL,NULL,NULL,'10','Staf PTL','','',NULL,1,'2018','admin','2018-11-14 11:09:53','43.225.186.122','admin','2018-11-14 11:10:25','43.225.186.122','\0',0,NULL,NULL,NULL,NULL,NULL),('FA7E7B8ADE7E9956323D','C8C78126069666B06C1D','Kepala','202cb962ac59075b964b07152d234b70',NULL,NULL,NULL,'6','Kepala','','',NULL,1,'2018','admin','2018-11-13 20:25:36','112.215.245.137','admin','2018-11-13 20:25:36','112.215.245.137','\0',0,NULL,NULL,NULL,NULL,NULL),('FD8570C4461EC3038842','BC289275C34A1F2C61F3','stafse','202cb962ac59075b964b07152d234b70',NULL,NULL,NULL,'10','Staf SE','','','Penguins.jpg',1,'2018','admin','2018-11-13 20:28:50','112.215.245.137','admin','2018-11-13 20:28:50','112.215.245.137','\0',0,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tmuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traksesmenu`
--

DROP TABLE IF EXISTS `traksesmenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traksesmenu` (
  `aksesmenuid` varchar(20) NOT NULL,
  `aksesid` varchar(20) DEFAULT NULL,
  `menuid` varchar(20) DEFAULT NULL,
  `accessview` tinyint(1) DEFAULT '0',
  `accessadd` tinyint(1) DEFAULT '0',
  `accessedit` tinyint(1) DEFAULT '0',
  `accessdel` tinyint(1) DEFAULT '0',
  `accessprint` tinyint(1) DEFAULT '0',
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`aksesmenuid`),
  KEY `fk_tmakses_reference_traksesm` (`aksesid`),
  KEY `fk_tmmenu_reference_traksesm` (`menuid`),
  CONSTRAINT `fk_tmakses_reference_traksesm` FOREIGN KEY (`aksesid`) REFERENCES `tmakses` (`aksesid`),
  CONSTRAINT `fk_tmmenu_reference_traksesm` FOREIGN KEY (`menuid`) REFERENCES `tmmenu` (`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traksesmenu`
--

LOCK TABLES `traksesmenu` WRITE;
/*!40000 ALTER TABLE `traksesmenu` DISABLE KEYS */;
INSERT INTO `traksesmenu` VALUES ('00000000000000060001','6','1',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060002','6','2',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060003','6','3',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060005','6','5',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060006','6','6',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060007','6','7',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060008','6','8',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060009','6','9',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060101','6','101',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060102','6','102',1,1,1,1,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060103','6','103',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060104','6','104',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060105','6','105',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060106','6','106',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060107','6','107',1,1,1,1,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060108','6','108',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060201','6','201',1,0,0,0,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060301','6','301',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060302','6','302',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060303','6','303',1,0,1,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000060304','6','304',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070001','7','1',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070002','7','2',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070003','7','3',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070005','7','5',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070006','7','6',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070007','7','7',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070008','7','8',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070009','7','9',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070101','7','101',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070102','7','102',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070103','7','103',1,1,1,1,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070104','7','104',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070105','7','105',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070106','7','106',1,1,1,1,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070107','7','107',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070108','7','108',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070201','7','201',1,0,0,0,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070301','7','301',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070302','7','302',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070303','7','303',1,0,1,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000070304','7','304',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100001','10','1',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100002','10','2',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100003','10','3',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100005','10','5',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100006','10','6',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100007','10','7',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100008','10','8',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100009','10','9',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100101','10','101',1,1,1,1,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100102','10','102',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100103','10','103',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100104','10','104',1,1,1,1,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100105','10','105',1,1,1,1,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100106','10','106',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100107','10','107',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100108','10','108',1,1,1,1,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100201','10','201',1,0,0,0,1,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100301','10','301',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100302','10','302',1,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100303','10','303',1,0,1,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0'),('00000000000000100304','10','304',0,0,0,0,0,'admin','2018-11-03 00:00:00','admin','admin','2018-11-03 00:00:00','admin','\0');
/*!40000 ALTER TABLE `traksesmenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `traksesmodul`
--

DROP TABLE IF EXISTS `traksesmodul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traksesmodul` (
  `aksesmodulid` varchar(20) NOT NULL,
  `aksesid` varchar(20) DEFAULT NULL,
  `modulid` varchar(20) DEFAULT NULL,
  `accessview` bit(1) DEFAULT NULL,
  `accessadd` bit(1) DEFAULT NULL,
  `accessedit` bit(1) DEFAULT NULL,
  `accessdel` bit(1) DEFAULT NULL,
  `accessprint` bit(1) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`aksesmodulid`),
  KEY `fk_tmmodul_reference_traksesm` (`modulid`),
  CONSTRAINT `fk_tmmodul_reference_traksesm` FOREIGN KEY (`modulid`) REFERENCES `tmmodul` (`modulid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traksesmodul`
--

LOCK TABLES `traksesmodul` WRITE;
/*!40000 ALTER TABLE `traksesmodul` DISABLE KEYS */;
/*!40000 ALTER TABLE `traksesmodul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tsberita`
--

DROP TABLE IF EXISTS `tsberita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsberita` (
  `beritaid` varchar(20) NOT NULL,
  `judul` varchar(150) DEFAULT NULL,
  `keterangan` text,
  `tglberita` datetime DEFAULT NULL,
  `flag` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `img` varchar(30) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`beritaid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tsberita`
--

LOCK TABLES `tsberita` WRITE;
/*!40000 ALTER TABLE `tsberita` DISABLE KEYS */;
INSERT INTO `tsberita` VALUES ('1','Menkes: Persiapkan Riskesdas 2018 Secara Matang','-','2018-01-29 00:00:00',1,1,'slide_101.jpg',2,'http://www.depkes.go.id/article/view/18013000002/menkes-persiapkan-riskesdas-2018-secara-matang.html','admin','2018-09-18 00:00:00','pc','admin','2018-09-18 00:00:00','pc','\0'),('2','Pembiayaan Mikro Jadi Solusi Mudah Permodalan Nelayan','','2018-06-06 00:00:00',1,1,'slide_102.jpg',2,'http://www.depkes.go.id/article/view/18060700001/pembiayaan-mikro-jadi-solusi-mudah-permodalan-nelayan.html','admin','2018-09-18 00:00:00','pc','admin','2018-09-18 00:00:00','pc','\0'),('3','Rakerkesnas 2018, Kemenkes Percepat Atasi 3 Masalah Kesehatan','-','2018-03-05 00:00:00',1,1,'slide_103.jpg',2,'http://www.depkes.go.id/article/view/18030700005/rakerkesnas-2018-kemenkes-percepat-atasi-3-masalah-kesehatan.html','admin','2018-09-18 00:00:00','pc','admin','2018-09-18 00:00:00','pc','\0'),('4','Indonesia Jadi Center of Excelent: Momentum Baru Bagi Negara-negara Islam dalam Pengembangan Vaksin dan Produk Bioteknologi','-','2018-05-14 00:00:00',1,1,'slide_104.jpg',2,'http://www.depkes.go.id/article/view/18051500002/indonesia-jadi-center-of-excelent-momentum-baru-bagi-negara-negara-islam-dalam-pengembangan-vaksin-d.html','admin','2018-09-18 00:00:00','pc','admin','2018-09-18 00:00:00','pc','\0');
/*!40000 ALTER TABLE `tsberita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tsemailverifikasi`
--

DROP TABLE IF EXISTS `tsemailverifikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsemailverifikasi` (
  `emailverifikasiid` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `tglgenerate` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`emailverifikasiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tsemailverifikasi`
--

LOCK TABLES `tsemailverifikasi` WRITE;
/*!40000 ALTER TABLE `tsemailverifikasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `tsemailverifikasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tsnotifikasi`
--

DROP TABLE IF EXISTS `tsnotifikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsnotifikasi` (
  `notifikasiid` varchar(20) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `tglnotifikasi` datetime DEFAULT NULL,
  `controller` varchar(30) DEFAULT NULL,
  `action` varchar(30) DEFAULT NULL,
  `primarykey` varchar(20) DEFAULT NULL,
  `transaksiid` varchar(20) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  `jenistransaksi` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`notifikasiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tsnotifikasi`
--

LOCK TABLES `tsnotifikasi` WRITE;
/*!40000 ALTER TABLE `tsnotifikasi` DISABLE KEYS */;
INSERT INTO `tsnotifikasi` VALUES ('06717B77F1FCD3B3562D','Surat masuk telah selesai','Surat masuk dengan nomor 1 telah selesai','2018-11-13 20:57:27','suratmasuk','admin','suratmasukid','12E5FE65D1AD55AF6239','stafse','2018-11-13 20:57:27','112.215.245.137','stafse','2018-11-13 20:57:27','112.215.245.137','\0',NULL),('0E156B6ED6EB24C72EE9','Surat Keluar Verifikasi Kasubbag','Surat keluar  telah diverifikasi oleh Kasubbag','2018-11-13 21:06:12','verifikasikasubbag','posting','suratkeluarid','192A84D5D6724B311131','stafse','2018-11-13 21:06:12','112.215.245.137','stafse','2018-11-13 21:06:12','112.215.245.137','\0',NULL),('181CA577F44A83D605FE','Surat Masuk Disposisi Kepala','Surat masuk dengan nomor 1 telah didisposisi oleh Kepala','2018-11-14 07:28:30','disposisikasubbag','posting','suratmasukid','920FBBD093E9E391C004','Kepala','2018-11-14 07:28:30','112.215.174.41','Kepala','2018-11-14 07:28:30','112.215.174.41','\0',NULL),('1B469B674D93497BA012','Surat masuk telah diposting','Surat masuk dengan nomor 1 telah diposting','2018-11-14 07:25:42','suratmasuk','admin','suratmasukid','920FBBD093E9E391C004','staftu','2018-11-14 07:25:42','112.215.174.41','staftu','2018-11-14 07:25:42','112.215.174.41','\0',NULL),('6AECEDFDE96B8B94235C','Surat Masuk Disposisi Kepala','Surat masuk dengan nomor 1 telah didisposisi oleh Kepala','2018-11-14 07:28:30','disposisikasubbag','posting','suratmasukid','920FBBD093E9E391C004','Kepala','2018-11-14 07:28:30','112.215.174.41','Kepala','2018-11-14 07:28:30','112.215.174.41','\0',NULL),('6BBC109F9A11E4A7493D','Surat Keluar Verifikasi Kasubbag','Surat keluar  telah diverifikasi oleh Kasubbag','2018-11-14 07:08:15','verifikasikasubbag','posting','suratkeluarid','0D59D17FBF88B3A7F1DC','staftu','2018-11-14 07:08:15','112.215.174.41','staftu','2018-11-14 07:08:15','112.215.174.41','\0',NULL),('73718B88C168A7F6F113','Surat Masuk Disposisi Kepala','Surat masuk dengan nomor 521 telah didisposisi oleh Kepala','2018-11-14 10:58:08','disposisikasubbag','posting','suratmasukid','0F8C0E453BEF8F120729','Kepala','2018-11-14 10:58:08','43.225.186.122','Kepala','2018-11-14 10:58:08','43.225.186.122','\0',NULL),('78C5167FFBA2F379190D','Surat masuk telah diposting','Surat masuk dengan nomor 521 telah diposting','2018-11-14 10:56:50','disposisikabag','admin','suratmasukid','0F8C0E453BEF8F120729','stafse','2018-11-14 10:56:50','43.225.186.122','stafse','2018-11-14 10:56:50','43.225.186.122','\0',NULL),('80305CC931B8F5A2802D','Surat keluar telah diposting','Surat keluar telah diposting','2018-11-13 21:07:57','verifikasikabag','admin','suratmasukid','192A84D5D6724B311131','kasise','2018-11-13 21:07:57','112.215.245.137','kasise','2018-11-13 21:07:57','112.215.245.137','\0',NULL),('8F1A62BBB54DCC42EBD9','Surat masuk telah diposting','Surat masuk dengan nomor 1 telah diposting','2018-11-13 20:36:58','suratmasuk','admin','suratmasukid','12E5FE65D1AD55AF6239','staftu','2018-11-13 20:36:58','112.215.245.137','staftu','2018-11-13 20:36:58','112.215.245.137','\0',NULL),('93B8B0C4D342A7EED364','Surat Masuk Disposisi Kasubbag/Kasi','Surat masuk dengan nomor 1 telah didisposisi oleh Kasubbag/Kasi','2018-11-14 07:41:49','pelaksanaan','admin','suratmasukid','920FBBD093E9E391C004','kasubagtu','2018-11-14 07:41:49','112.215.174.41','kasubagtu','2018-11-14 07:41:49','112.215.174.41','\0',NULL),('98AE5A18AB621E5DF573','Surat Keluar Verifikasi Kasubbag','Surat keluar  telah diverifikasi oleh Kasubbag','2018-11-14 07:08:15','verifikasikasubbag','posting','suratkeluarid','0D59D17FBF88B3A7F1DC','staftu','2018-11-14 07:08:15','112.215.174.41','staftu','2018-11-14 07:08:15','112.215.174.41','\0',NULL),('BE04BDED8A3DB5221469','Surat keluar telah diverifikasi kepala','Surat keluar telah diverifikasi oleh Kepala','2018-11-13 21:12:09','pengiriman','admin','suratmasukid','192A84D5D6724B311131','Kepala','2018-11-13 21:12:09','112.215.245.137','Kepala','2018-11-13 21:12:09','112.215.245.137','\0',NULL),('D290DD43432D709D3661','Surat Keluar Verifikasi Kasubbag','Surat keluar  telah diverifikasi oleh Kasubbag','2018-11-13 21:06:12','verifikasikasubbag','posting','suratkeluarid','192A84D5D6724B311131','stafse','2018-11-13 21:06:12','112.215.245.137','stafse','2018-11-13 21:06:12','112.215.245.137','\0',NULL),('E1990221D3D52855F2C6','Surat Masuk Disposisi Kasubbag/Kasi','Surat masuk dengan nomor 1 telah didisposisi oleh Kasubbag/Kasi','2018-11-14 07:41:49','pelaksanaan','admin','suratmasukid','920FBBD093E9E391C004','kasubagtu','2018-11-14 07:41:49','112.215.174.41','kasubagtu','2018-11-14 07:41:49','112.215.174.41','\0',NULL),('E1ED29C59BF1317EBD03','Surat masuk telah selesai','Surat masuk dengan nomor 521 telah selesai','2018-11-14 11:05:35','suratmasuk','admin','suratmasukid','0F8C0E453BEF8F120729','lusi','2018-11-14 11:05:35','43.225.186.122','lusi','2018-11-14 11:05:35','43.225.186.122','\0',NULL),('E2A563E1C543CCFFCA9A','Surat Masuk Disposisi Kasubbag/Kasi','Surat masuk dengan nomor 521 telah didisposisi oleh Kasubbag/Kasi','2018-11-14 10:59:47','pelaksanaan','admin','suratmasukid','0F8C0E453BEF8F120729','kasubagtu','2018-11-14 10:59:47','43.225.186.122','kasubagtu','2018-11-14 10:59:47','43.225.186.122','\0',NULL);
/*!40000 ALTER TABLE `tsnotifikasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tsnotifikasiuser`
--

DROP TABLE IF EXISTS `tsnotifikasiuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tsnotifikasiuser` (
  `notifikasiuserid` varchar(20) NOT NULL,
  `notifikasiid` varchar(20) DEFAULT NULL,
  `userid` varchar(20) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`notifikasiuserid`),
  KEY `fk_tsnotifi_reference_tmuser` (`userid`),
  KEY `fk_tsnotifi_reference_tsnotifi` (`notifikasiid`),
  CONSTRAINT `fk_tsnotifi_reference_tmuser` FOREIGN KEY (`userid`) REFERENCES `tmuser` (`userid`),
  CONSTRAINT `fk_tsnotifi_reference_tsnotifi` FOREIGN KEY (`notifikasiid`) REFERENCES `tsnotifikasi` (`notifikasiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tsnotifikasiuser`
--

LOCK TABLES `tsnotifikasiuser` WRITE;
/*!40000 ALTER TABLE `tsnotifikasiuser` DISABLE KEYS */;
INSERT INTO `tsnotifikasiuser` VALUES ('0AA9F109B67355B52933','BE04BDED8A3DB5221469','FD8570C4461EC3038842',0,'stafse','2018-11-13 21:12:52','112.215.245.137','stafse','2018-11-13 21:12:52','112.215.245.137','\0'),('1DB1E9E64A69F3F3BAC5','1B469B674D93497BA012','FA7E7B8ADE7E9956323D',0,'Kepala','2018-11-14 07:26:01','112.215.174.41','Kepala','2018-11-14 07:26:01','112.215.174.41','\0'),('2703664E8A608D8ADC66','06717B77F1FCD3B3562D','14BA7C8E1FCA4AAD5DA5',0,'staftu','2018-11-13 21:30:28','112.215.245.137','staftu','2018-11-13 21:30:28','112.215.245.137','\0'),('2CAD9818E344851B0544','E1ED29C59BF1317EBD03','FD8570C4461EC3038842',1,'lusi','2018-11-14 11:05:35','43.225.186.122','lusi','2018-11-14 11:05:35','43.225.186.122','\0'),('31564BC998F512A2C1E3','8F1A62BBB54DCC42EBD9','FA7E7B8ADE7E9956323D',0,'Kepala','2018-11-14 07:26:07','112.215.174.41','Kepala','2018-11-14 07:26:07','112.215.174.41','\0'),('5F4D4055C5D831DC26A0','6BBC109F9A11E4A7493D','02D0E58BBEB60D981B25',0,'kasubagtu','2018-11-14 07:21:01','112.215.174.41','kasubagtu','2018-11-14 07:21:01','112.215.174.41','\0'),('7B7448FACB71A21CF608','80305CC931B8F5A2802D','FA7E7B8ADE7E9956323D',0,'Kepala','2018-11-13 21:08:24','112.215.245.137','Kepala','2018-11-13 21:08:24','112.215.245.137','\0'),('7D12ADDE06F95E910EC2','73718B88C168A7F6F113','02D0E58BBEB60D981B25',1,'Kepala','2018-11-14 10:58:08','43.225.186.122','Kepala','2018-11-14 10:58:08','43.225.186.122','\0'),('84F6089B6F2824D20380','78C5167FFBA2F379190D','FA7E7B8ADE7E9956323D',1,'stafse','2018-11-14 10:56:50','43.225.186.122','stafse','2018-11-14 10:56:50','43.225.186.122','\0'),('9343098ED608DEC418C6','D290DD43432D709D3661','0E9E258821187AC50C7C',0,'kasise','2018-11-14 07:29:29','112.215.174.41','kasise','2018-11-14 07:29:29','112.215.174.41','\0'),('9C816340CF83FF6E0B0B','93B8B0C4D342A7EED364','14BA7C8E1FCA4AAD5DA5',0,'staftu','2018-11-14 07:52:12','112.215.174.41','staftu','2018-11-14 07:52:12','112.215.174.41','\0'),('A543DD57B348C66463B5','6AECEDFDE96B8B94235C','02D0E58BBEB60D981B25',0,'kasubagtu','2018-11-14 07:28:52','112.215.174.41','kasubagtu','2018-11-14 07:28:52','112.215.174.41','\0'),('B895B45C9889195A6A0A','0E156B6ED6EB24C72EE9','02D0E58BBEB60D981B25',0,'kasubagtu','2018-11-14 07:21:05','112.215.174.41','kasubagtu','2018-11-14 07:21:05','112.215.174.41','\0'),('E41EA7DB13B58A1206DE','98AE5A18AB621E5DF573','0E9E258821187AC50C7C',0,'kasise','2018-11-14 07:29:34','112.215.174.41','kasise','2018-11-14 07:29:34','112.215.174.41','\0'),('E90F7A3A3679C648F4B2','E1990221D3D52855F2C6','FD8570C4461EC3038842',1,'kasubagtu','2018-11-14 07:41:49','112.215.174.41','kasubagtu','2018-11-14 07:41:49','112.215.174.41','\0'),('F8F3212FFB2B7487B915','E2A563E1C543CCFFCA9A','14BA7C8E1FCA4AAD5DA5',1,'kasubagtu','2018-11-14 10:59:47','43.225.186.122','kasubagtu','2018-11-14 10:59:47','43.225.186.122','\0'),('F9E3CF78D2763E329E99','181CA577F44A83D605FE','0E9E258821187AC50C7C',0,'kasise','2018-11-14 07:29:36','112.215.174.41','kasise','2018-11-14 07:29:36','112.215.174.41','\0');
/*!40000 ALTER TABLE `tsnotifikasiuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tspengumuman`
--

DROP TABLE IF EXISTS `tspengumuman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tspengumuman` (
  `pengumumanid` varchar(20) NOT NULL,
  `pengirim` varchar(150) DEFAULT NULL,
  `judul` varchar(150) DEFAULT NULL,
  `keterangan` text,
  `tglpengumuman` datetime DEFAULT NULL,
  `flag` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `img` varchar(30) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`pengumumanid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tspengumuman`
--

LOCK TABLES `tspengumuman` WRITE;
/*!40000 ALTER TABLE `tspengumuman` DISABLE KEYS */;
INSERT INTO `tspengumuman` VALUES ('1','Kemenkes RI','Test Sistem','Kepada seluruh Staf untuk melakukan test Sistem','2018-09-18 00:00:00',1,1,'slide_11.jpg','admin','2018-09-18 00:00:00','pc','admin','2018-09-18 00:00:00','pc','\0'),('1E5044CF679DCA0D4C24','Kemenkes RI','Pengumuman','Pengumuman pengumuman pengumuman pengumuman pengumuman pengumuman pengumuman pengumuman','2018-11-07 00:00:00',1,1,NULL,'admin','2018-11-06 21:58:35','127.0.0.1','admin','2018-11-06 21:58:35','127.0.0.1','\0'),('2','Kemenkes','Penginputan Sistem','Diharapkan kepada seluruh Staf untuk melakukan pengarsipan surat melalui sistem. Ok..','2018-09-18 00:00:00',1,1,'slide_12.jpg','admin','2018-09-18 00:00:00','pc','admin','2018-11-05 00:54:22','127.0.0.1','\0'),('2193D508B9FFF6389F77','Sub Bagian TU','Pemakaian Aplikasi','Segera','2018-11-13 00:00:00',1,1,NULL,'admin','2018-11-13 20:17:02','112.215.245.137','admin','2018-11-13 20:17:02','112.215.245.137','\0'),('6564D681362360C17E8A','Kemenkes','Test','Test Test Test Test Test Test Test Test Test Test Test Test ','2018-11-05 00:00:00',1,1,NULL,'admin','2018-11-05 00:55:31','127.0.0.1','admin','2018-11-05 01:00:55','127.0.0.1','\0');
/*!40000 ALTER TABLE `tspengumuman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tspesan`
--

DROP TABLE IF EXISTS `tspesan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tspesan` (
  `pesanid` varchar(20) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `tglpesan` datetime DEFAULT NULL,
  `flag` smallint(6) DEFAULT NULL,
  `controller` varchar(30) DEFAULT NULL,
  `action` varchar(30) DEFAULT NULL,
  `transaksiid` varchar(20) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`pesanid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tspesan`
--

LOCK TABLES `tspesan` WRITE;
/*!40000 ALTER TABLE `tspesan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tspesan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tspesanuser`
--

DROP TABLE IF EXISTS `tspesanuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tspesanuser` (
  `pesanuserid` varchar(20) NOT NULL,
  `pesanid` varchar(20) DEFAULT NULL,
  `userid` varchar(20) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`pesanuserid`),
  KEY `fk_tspesanu_reference_tmuser` (`userid`),
  KEY `fk_tspesanu_reference_tspesan` (`pesanid`),
  CONSTRAINT `fk_tspesanu_reference_tmuser` FOREIGN KEY (`userid`) REFERENCES `tmuser` (`userid`),
  CONSTRAINT `fk_tspesanu_reference_tspesan` FOREIGN KEY (`pesanid`) REFERENCES `tspesan` (`pesanid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tspesanuser`
--

LOCK TABLES `tspesanuser` WRITE;
/*!40000 ALTER TABLE `tspesanuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `tspesanuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratkeluar`
--

DROP TABLE IF EXISTS `ttsuratkeluar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratkeluar` (
  `suratkeluarid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `subunitid` varchar(20) DEFAULT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `subbagianid` varchar(20) DEFAULT NULL,
  `pejabatid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `indexkode1` varchar(20) DEFAULT NULL,
  `indexkode2` varchar(20) DEFAULT NULL,
  `indexbulan` varchar(20) DEFAULT NULL,
  `indexnomor` varchar(20) DEFAULT NULL,
  `indextahun` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `klasifikasi` varchar(20) DEFAULT NULL,
  `tglpenyerahan` date DEFAULT NULL,
  `nosurat` varchar(100) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `kepada` varchar(254) DEFAULT NULL,
  `perihal` varchar(254) DEFAULT NULL,
  `kirimvia` varchar(100) DEFAULT NULL,
  `tglkirim` date DEFAULT NULL,
  `jamkirim` varchar(20) DEFAULT NULL,
  `paraf` bit(1) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratkeluarid`),
  KEY `fk_ttsuratkeluar_reference_tmunit` (`unitid`),
  KEY `fk_ttsuratkeluar_reference_tmsubunit` (`subunitid`),
  KEY `fk_ttsuratkeluar_reference_tmbagian` (`bagianid`),
  KEY `fk_ttsuratkeluar_reference_tmsubbagian` (`subbagianid`),
  CONSTRAINT `fk_ttsuratkeluar_reference_tmbagian` FOREIGN KEY (`bagianid`) REFERENCES `tmbagian` (`bagianid`),
  CONSTRAINT `fk_ttsuratkeluar_reference_tmsubbagian` FOREIGN KEY (`subbagianid`) REFERENCES `tmsubbagian` (`subbagianid`),
  CONSTRAINT `fk_ttsuratkeluar_reference_tmsubunit` FOREIGN KEY (`subunitid`) REFERENCES `tmsubunit` (`subunitid`),
  CONSTRAINT `fk_ttsuratkeluar_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratkeluar`
--

LOCK TABLES `ttsuratkeluar` WRITE;
/*!40000 ALTER TABLE `ttsuratkeluar` DISABLE KEYS */;
INSERT INTO `ttsuratkeluar` VALUES ('0D59D17FBF88B3A7F1DC',NULL,NULL,NULL,NULL,'C8C78126069666B06C1D','D0468AB6C3CDDF4B83EE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0002','2018-11-14','Test ttttt',NULL,NULL,NULL,NULL,NULL,1,'ket','2018','staftu','2018-11-14 07:08:02','112.215.174.41','staftu','2018-11-14 07:08:15','112.215.174.41','\0'),('192A84D5D6724B311131',NULL,NULL,NULL,NULL,'C8C78126069666B06C1D','BC289275C34A1F2C61F3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1212','2018-11-13','Jalalsolusindo',NULL,NULL,'2018-11-14',NULL,NULL,100,'Suntik Rabies','2018','stafse','2018-11-13 21:05:28','112.215.245.137','stafse','2018-11-13 21:13:15','112.215.245.137','\0');
/*!40000 ALTER TABLE `ttsuratkeluar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratkeluardetail`
--

DROP TABLE IF EXISTS `ttsuratkeluardetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratkeluardetail` (
  `suratkeluardetailid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `jenisid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `indekkode1` varchar(20) DEFAULT NULL,
  `indekkode2` varchar(20) DEFAULT NULL,
  `indeknomor` varchar(20) DEFAULT NULL,
  `indekbulan` varchar(20) DEFAULT NULL,
  `indektahun` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `klasifikasi` varchar(20) DEFAULT NULL,
  `perihal` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `paraf` bit(1) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratkeluardetailid`),
  KEY `fk_ttsuratkeluardetail_reference_ttsuratkeluar` (`suratkeluarid`),
  KEY `fk_ttsuratkeluardetail_reference_tmjenis` (`jenisid`),
  CONSTRAINT `fk_ttsuratkeluardetail_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratkeluardetail`
--

LOCK TABLES `ttsuratkeluardetail` WRITE;
/*!40000 ALTER TABLE `ttsuratkeluardetail` DISABLE KEYS */;
INSERT INTO `ttsuratkeluardetail` VALUES ('2B82201405E91DE2F92C','192A84D5D6724B311131','A73C39EAD9D0548DDAD0','-','','1231','1','1231','2018','2018-11-13','segera','Gizi Masyarakat','',NULL,NULL,'2018','stafse','2018-11-13 21:05:28','112.215.245.137','stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('577FBAF285193F14FB22','192A84D5D6724B311131','A73C39EAD9D0548DDAD2','-','','1232','1','1232','2018','2018-11-13','rahasia','Biaya Suntik Rabies','',NULL,NULL,'2018','stafse','2018-11-13 21:05:28','112.215.245.137','stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('D69527ECFECD2C96AA89','0D59D17FBF88B3A7F1DC','A73C39EAD9D0548DDAC4','-','DP','02.03','1','09','2018','2018-11-14','rahasia','Test','',NULL,NULL,'2018','staftu','2018-11-14 07:08:02','112.215.174.41','staftu','2018-11-14 07:08:02','112.215.174.41','\0');
/*!40000 ALTER TABLE `ttsuratkeluardetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratkeluarfiles`
--

DROP TABLE IF EXISTS `ttsuratkeluarfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratkeluarfiles` (
  `suratkeluarfilesid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `files` varchar(254) DEFAULT NULL,
  `filename` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratkeluarfilesid`),
  KEY `fk_ttsuratkeluarfiles_reference_ttsuratkeluar` (`suratkeluarid`),
  CONSTRAINT `fk_ttsuratkeluarfiles_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratkeluarfiles`
--

LOCK TABLES `ttsuratkeluarfiles` WRITE;
/*!40000 ALTER TABLE `ttsuratkeluarfiles` DISABLE KEYS */;
INSERT INTO `ttsuratkeluarfiles` VALUES ('1F7896A28CA457BC7007','0D59D17FBF88B3A7F1DC',2,'0F0E8F5C00F05F6F4EC3.jpg','Hydrangeas.jpg','','2018',NULL,NULL,NULL,'staftu','2018-11-14 07:08:02','112.215.174.41','\0'),('47555BBCD0D3FA6A7E18','192A84D5D6724B311131',1,'52225E91080BFC819D38.jpg','Desert.jpg','','2018',NULL,NULL,NULL,'stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('4E18FC929B7FEEE5FDE1','192A84D5D6724B311131',2,'1A3208D77E6FDAF6A1F7.jpg','Chrysanthemum.jpg','','2018',NULL,NULL,NULL,'stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('782F5D52A64410C40DAC','192A84D5D6724B311131',4,'F3666BC7EEBC1D0153C8.jpg','Hydrangeas.jpg','','2018',NULL,NULL,NULL,'stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('930B5C54AC4F9C1C7207','192A84D5D6724B311131',5,'2873DF2D9CAAE1C7D4BB.jpg','Lighthouse.jpg','','2018',NULL,NULL,NULL,'stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('CB4976886249601E8779','192A84D5D6724B311131',3,'A4DA12441DCE35D04F4C.jpg','Jellyfish.jpg','','2018',NULL,NULL,NULL,'stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('D85FBEC83B196B10BF1A','0D59D17FBF88B3A7F1DC',1,'26046565978E1D38D987.jpg','Desert.jpg','','2018',NULL,NULL,NULL,'staftu','2018-11-14 07:08:02','112.215.174.41','\0'),('DF0ABB6BF2872D83802B','192A84D5D6724B311131',6,'5596693318C835BD2DBB.jpg','Koala.jpg','','2018',NULL,NULL,NULL,'stafse','2018-11-13 21:05:28','112.215.245.137','\0');
/*!40000 ALTER TABLE `ttsuratkeluarfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratkeluarhistory`
--

DROP TABLE IF EXISTS `ttsuratkeluarhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratkeluarhistory` (
  `suratkeluarhistoryid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `tglverifikasi` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `catatan` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratkeluarhistoryid`),
  KEY `fk_ttsuratkeluarhistory_reference_ttsuratkeluar` (`suratkeluarid`),
  KEY `fk_ttsuratkeluarpegawai_reference_tmpegawai` (`pegawaiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratkeluarhistory`
--

LOCK TABLES `ttsuratkeluarhistory` WRITE;
/*!40000 ALTER TABLE `ttsuratkeluarhistory` DISABLE KEYS */;
INSERT INTO `ttsuratkeluarhistory` VALUES ('64DD0F452B134634A4D9','192A84D5D6724B311131','BC289275C34A1F2C61F3',0,'2018-11-13 21:13:15',100,'-','',NULL,'stafse','2018-11-13 21:13:15','112.215.245.137','stafse','2018-11-13 21:13:15','112.215.245.137','\0'),('7EB2220C0BE204072229','192A84D5D6724B311131','255600DE37D616370204',0,'2018-11-13 21:07:57',2,'-','',NULL,'kasise','2018-11-13 21:07:57','112.215.245.137','kasise','2018-11-13 21:07:57','112.215.245.137','\0'),('B189F6998B45E6133BB6','192A84D5D6724B311131','C8C78126069666B06C1D',0,'2018-11-13 21:12:09',3,'-','',NULL,'Kepala','2018-11-13 21:12:09','112.215.245.137','Kepala','2018-11-13 21:12:09','112.215.245.137','\0'),('DCAD292CB1A8F64FBCA8','192A84D5D6724B311131','BC289275C34A1F2C61F3',0,'2018-11-13 21:06:12',1,'-','',NULL,'stafse','2018-11-13 21:06:12','112.215.245.137','stafse','2018-11-13 21:06:12','112.215.245.137','\0'),('E65D6EFF8187E7FFEBAF','0D59D17FBF88B3A7F1DC','D0468AB6C3CDDF4B83EE',0,'2018-11-14 07:08:15',1,'-','',NULL,'staftu','2018-11-14 07:08:15','112.215.174.41','staftu','2018-11-14 07:08:15','112.215.174.41','\0');
/*!40000 ALTER TABLE `ttsuratkeluarhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratkeluarjabatan`
--

DROP TABLE IF EXISTS `ttsuratkeluarjabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratkeluarjabatan` (
  `suratkeluarjabatanid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `jabatanid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` tinyint(1) DEFAULT '0',
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratkeluarjabatanid`),
  KEY `fk_ttsuratkeluarjabatan_reference_ttsuratkeluar` (`suratkeluarid`),
  KEY `fk_ttsuratkeluarjabatan_reference_tmjabatan` (`jabatanid`),
  CONSTRAINT `fk_ttsuratkeluarjabatan_reference_tmjabatan` FOREIGN KEY (`jabatanid`) REFERENCES `tmjabatan` (`jabatanid`),
  CONSTRAINT `fk_ttsuratkeluarjabatan_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratkeluarjabatan`
--

LOCK TABLES `ttsuratkeluarjabatan` WRITE;
/*!40000 ALTER TABLE `ttsuratkeluarjabatan` DISABLE KEYS */;
INSERT INTO `ttsuratkeluarjabatan` VALUES ('8176A82765E550B4B98A','0D59D17FBF88B3A7F1DC','5380303FA54C3B4F0FF0',NULL,'Kepala Sub Bagian TU',0,'2018',NULL,NULL,NULL,'staftu','2018-11-14 07:08:02','112.215.174.41','\0'),('8E1EDA50045B60D1D148','0D59D17FBF88B3A7F1DC','F8265B4659313AC9044F',NULL,'Kepala Seksi SE',0,'2018',NULL,NULL,NULL,'staftu','2018-11-14 07:08:02','112.215.174.41','\0'),('B385A040EE0B605D4423','192A84D5D6724B311131','F8265B4659313AC9044F',NULL,'Kepala Seksi SE',1,'2018',NULL,NULL,NULL,'kasise','2018-11-13 21:07:51','112.215.245.137','\0'),('FE1A7A344834BAAA53F4','192A84D5D6724B311131','5380303FA54C3B4F0FF0',NULL,'Kepala Sub Bagian TU',1,'2018',NULL,NULL,NULL,'kasubagtu','2018-11-13 21:07:04','112.215.245.137','\0');
/*!40000 ALTER TABLE `ttsuratkeluarjabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratkeluarlampiran`
--

DROP TABLE IF EXISTS `ttsuratkeluarlampiran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratkeluarlampiran` (
  `suratkeluarlampiranid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `nourut` int(11) DEFAULT NULL,
  `lampiran` varchar(150) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratkeluarlampiranid`),
  KEY `fk_ttsuratkeluarlampiran_reference_ttsuratkeluar` (`suratkeluarid`),
  CONSTRAINT `fk_ttsuratkeluarlampiran_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratkeluarlampiran`
--

LOCK TABLES `ttsuratkeluarlampiran` WRITE;
/*!40000 ALTER TABLE `ttsuratkeluarlampiran` DISABLE KEYS */;
INSERT INTO `ttsuratkeluarlampiran` VALUES ('2A0E8A610296ECB65EF6','192A84D5D6724B311131',2,'Lampiran2',NULL,'\0','2018','stafse','2018-11-13 21:05:28','112.215.245.137','stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('42B3E9824280CD5FA149','192A84D5D6724B311131',1,'Lampiran1',NULL,'\0','2018','stafse','2018-11-13 21:05:28','112.215.245.137','stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('4BE20430CDEF0D9CECD8','0D59D17FBF88B3A7F1DC',1,'Test Lampiran',NULL,'\0','2018','staftu','2018-11-14 07:08:02','112.215.174.41','staftu','2018-11-14 07:08:02','112.215.174.41','\0');
/*!40000 ALTER TABLE `ttsuratkeluarlampiran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratkeluarpegawai`
--

DROP TABLE IF EXISTS `ttsuratkeluarpegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratkeluarpegawai` (
  `suratkeluarpegawaiid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `catatan` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` tinyint(1) DEFAULT '0',
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratkeluarpegawaiid`),
  KEY `fk_ttsuratkeluarpegawai_reference_ttsuratkeluar` (`suratkeluarid`),
  KEY `fk_ttsuratkeluarpegawai_reference_tmpegawai` (`pegawaiid`),
  CONSTRAINT `fk_ttsuratkeluarpegawai_reference_tmpegawai` FOREIGN KEY (`pegawaiid`) REFERENCES `tmpegawai` (`pegawaiid`),
  CONSTRAINT `fk_ttsuratkeluarpegawai_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratkeluarpegawai`
--

LOCK TABLES `ttsuratkeluarpegawai` WRITE;
/*!40000 ALTER TABLE `ttsuratkeluarpegawai` DISABLE KEYS */;
/*!40000 ALTER TABLE `ttsuratkeluarpegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratkeluartembusan`
--

DROP TABLE IF EXISTS `ttsuratkeluartembusan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratkeluartembusan` (
  `suratkeluartembusanid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `nourut` int(11) DEFAULT NULL,
  `tembusan` varchar(150) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratkeluartembusanid`),
  KEY `fk_ttsuratkeluartembusan_reference_ttsuratkeluar` (`suratkeluarid`),
  CONSTRAINT `fk_ttsuratkeluartembusan_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratkeluartembusan`
--

LOCK TABLES `ttsuratkeluartembusan` WRITE;
/*!40000 ALTER TABLE `ttsuratkeluartembusan` DISABLE KEYS */;
INSERT INTO `ttsuratkeluartembusan` VALUES ('08E9B65B84CFDD9CD224','192A84D5D6724B311131',1,'Tembusan1','',NULL,'2018','stafse','2018-11-13 21:05:28','112.215.245.137','stafse','2018-11-13 21:05:28','112.215.245.137','\0'),('50C8CA63CC2AD37AA1C6','0D59D17FBF88B3A7F1DC',1,'Test Tembusan','',NULL,'2018','staftu','2018-11-14 07:08:02','112.215.174.41','staftu','2018-11-14 07:08:02','112.215.174.41','\0'),('F95028B68D27C6F07E71','192A84D5D6724B311131',2,'Tembusan2','',NULL,'2018','stafse','2018-11-13 21:05:28','112.215.245.137','stafse','2018-11-13 21:05:28','112.215.245.137','\0');
/*!40000 ALTER TABLE `ttsuratkeluartembusan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratmasuk`
--

DROP TABLE IF EXISTS `ttsuratmasuk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratmasuk` (
  `suratmasukid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `subunitid` varchar(20) DEFAULT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `subbagianid` varchar(20) DEFAULT NULL,
  `pejabatid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `jenisid` varchar(20) DEFAULT NULL,
  `indexkode1` varchar(20) DEFAULT NULL,
  `indexkode2` varchar(20) DEFAULT NULL,
  `indexbulan` varchar(20) DEFAULT NULL,
  `indexnomor` varchar(20) DEFAULT NULL,
  `indextahun` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `klasifikasi` varchar(20) DEFAULT NULL,
  `tglpenyerahan` date DEFAULT NULL,
  `nosurat` varchar(100) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `asal` varchar(254) DEFAULT NULL,
  `perihal` varchar(254) DEFAULT NULL,
  `tglpelaksanaan` date DEFAULT NULL,
  `catatanpelaksanaan` varchar(254) DEFAULT NULL,
  `kembalipada` varchar(100) DEFAULT NULL,
  `tglkembali` date DEFAULT NULL,
  `tglterima` date DEFAULT NULL,
  `jamkembali` varchar(20) DEFAULT NULL,
  `jamterima` varchar(20) DEFAULT NULL,
  `paraf` bit(1) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratmasukid`),
  KEY `fk_ttsuratmasuk_reference_tmunit` (`unitid`),
  KEY `fk_ttsuratmasuk_reference_tmsubunit` (`subunitid`),
  KEY `fk_ttsuratmasuk_reference_tmbagian` (`bagianid`),
  KEY `fk_ttsuratmasuk_reference_tmsubbagian` (`subbagianid`),
  KEY `fk_ttsuratmasuk_reference_tmjenis` (`jenisid`),
  CONSTRAINT `fk_ttsuratmasuk_reference_tmbagian` FOREIGN KEY (`bagianid`) REFERENCES `tmbagian` (`bagianid`),
  CONSTRAINT `fk_ttsuratmasuk_reference_tmsubbagian` FOREIGN KEY (`subbagianid`) REFERENCES `tmsubbagian` (`subbagianid`),
  CONSTRAINT `fk_ttsuratmasuk_reference_tmsubunit` FOREIGN KEY (`subunitid`) REFERENCES `tmsubunit` (`subunitid`),
  CONSTRAINT `fk_ttsuratmasuk_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratmasuk`
--

LOCK TABLES `ttsuratmasuk` WRITE;
/*!40000 ALTER TABLE `ttsuratmasuk` DISABLE KEYS */;
INSERT INTO `ttsuratmasuk` VALUES ('0F8C0E453BEF8F120729',NULL,NULL,NULL,NULL,'C8C78126069666B06C1D','BC289275C34A1F2C61F3','A73C39EAD9D0548DDAA0','','01.03','1','521','2018','PM','biasa','2018-11-08','PM.03.03/5/2801/2018','2018-11-08','Direktur P2PML','Pertemuan Akselerasi Pencapaian Eradikasi Frambusia',NULL,'','Kearsipan Subbag. TU','2018-11-14','2018-11-14',NULL,NULL,NULL,100,'','2018','stafse','2018-11-14 10:55:48','43.225.186.122','lusi','2018-11-14 11:05:35','43.225.186.122','\0'),('12E5FE65D1AD55AF6239',NULL,NULL,NULL,NULL,'C8C78126069666B06C1D','D0468AB6C3CDDF4B83EE','A73C39EAD9D0548DDAC2','DL','01.1','07','1','2018','-','segera','2018-11-14','1111','2018-11-13','PT. Makmur Jaya','Perihal ijin vaksinasi','2018-11-11','Tgl 15/11/2018','Kearsipan Subbag. TU','2018-11-14','2018-11-13',NULL,NULL,NULL,100,'-','2018','staftu','2018-11-13 20:35:34','112.215.245.137','stafse','2018-11-13 20:57:27','112.215.245.137','\0'),('3BFB2E93404FF6DBACAA',NULL,NULL,NULL,NULL,'','7C7691F2A499DB759F9B','A73C39EAD9D0548DDAA0','AR','123','141','1','2018','','biasa','2018-11-07','HK.01.03/12333/2018','2018-11-05','DINKES KARIMUN','TESTING',NULL,NULL,'Kearsipan Subbag. TU','2018-11-10',NULL,NULL,NULL,NULL,1,'TESTINGJUGA','2018','admin','2018-11-07 08:34:46','43.225.186.122','admin','2018-11-07 08:40:16','43.225.186.122','\0'),('7D0AFFFAD982919440AF',NULL,NULL,NULL,NULL,'','7C7691F2A499DB759F9B','A73C39EAD9D0548DDAC2','DL','01.02','1','1','2018','12','segera','2018-10-24','0001','2018-10-24','PT. 3Digit','555',NULL,NULL,'Kearsipan Subbag. TU','2018-11-20',NULL,NULL,NULL,NULL,0,'Yy66','2018','admin','2018-11-12 18:32:53','203.78.119.199','admin','2018-11-12 18:32:53','203.78.119.199','\0'),('920FBBD093E9E391C004',NULL,NULL,NULL,NULL,'C8C78126069666B06C1D','D0468AB6C3CDDF4B83EE','A73C39EAD9D0548DDAC0','AD','09.08','07','1','2018','-','rahasia','2018-11-14','0009','2018-11-14','PT. bla bla mmmmmmmmm','Hal. jjjjj','2018-11-14','test','Kearsipan Subbag. TU','2018-11-14','2018-11-14',NULL,NULL,NULL,7,'ket','2018','staftu','2018-11-14 07:22:20','112.215.174.41','staftu','2018-11-14 07:55:59','112.215.174.41','\0');
/*!40000 ALTER TABLE `ttsuratmasuk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratmasukfiles`
--

DROP TABLE IF EXISTS `ttsuratmasukfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratmasukfiles` (
  `suratmasukfilesid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `files` varchar(254) DEFAULT NULL,
  `filename` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratmasukfilesid`),
  KEY `fk_ttsuratmasukfiles_reference_ttsuratmasuk` (`suratmasukid`),
  CONSTRAINT `fk_ttsuratmasukfiles_reference_ttsuratmasuk` FOREIGN KEY (`suratmasukid`) REFERENCES `ttsuratmasuk` (`suratmasukid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratmasukfiles`
--

LOCK TABLES `ttsuratmasukfiles` WRITE;
/*!40000 ALTER TABLE `ttsuratmasukfiles` DISABLE KEYS */;
INSERT INTO `ttsuratmasukfiles` VALUES ('059F8885E57BBF53F798','920FBBD093E9E391C004',2,'5559A2860A6280A1A141.jpg','Tulips.jpg','','2018',NULL,NULL,NULL,'staftu','2018-11-14 07:22:40','112.215.174.41','\0'),('1BEA3817F279B77DB4C7','7D0AFFFAD982919440AF',1,'76A9EAF38EFED3CD5FE1.jpg','chart.jpg','','2018',NULL,NULL,NULL,'admin','2018-11-12 18:32:53','203.78.119.199','\0'),('607FC21B982E5581F4AF','3BFB2E93404FF6DBACAA',1,'BD74FAA7951679E3F5D4.pdf','Doc1.pdf','','2018',NULL,NULL,NULL,'admin','2018-11-07 08:34:46','43.225.186.122','\0'),('68BE8FA5B5740E1339E5','12E5FE65D1AD55AF6239',1,'473F2D4CB98E3EB63550.jpg','Chrysanthemum.jpg','','2018',NULL,NULL,NULL,'staftu','2018-11-13 20:35:34','112.215.245.137','\0'),('CFA7DC05E5FC474DFB30','12E5FE65D1AD55AF6239',2,'A1D9AFA738F13975E0D3.jpg','Lighthouse.jpg','','2018',NULL,NULL,NULL,'staftu','2018-11-13 20:35:34','112.215.245.137','\0'),('EE7B2682A172099855F1','920FBBD093E9E391C004',1,'1C7968327B2328FB77EE.jpg','Jellyfish.jpg','','2018',NULL,NULL,NULL,'staftu','2018-11-14 07:22:40','112.215.174.41','\0');
/*!40000 ALTER TABLE `ttsuratmasukfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratmasukhistory`
--

DROP TABLE IF EXISTS `ttsuratmasukhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratmasukhistory` (
  `suratmasukhistoryid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `tglverifikasi` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `catatan` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratmasukhistoryid`),
  KEY `fk_ttsuratmasukhistory_reference_ttsuratmasuk` (`suratmasukid`),
  KEY `fk_ttsuratmasukpegawai_reference_tmpegawai` (`pegawaiid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratmasukhistory`
--

LOCK TABLES `ttsuratmasukhistory` WRITE;
/*!40000 ALTER TABLE `ttsuratmasukhistory` DISABLE KEYS */;
INSERT INTO `ttsuratmasukhistory` VALUES ('09426F386F098E7DBA3A','920FBBD093E9E391C004','255600DE37D616370204',0,'2018-11-14 07:30:15',7,'-','',NULL,'kasise','2018-11-14 07:30:15','112.215.174.41','kasise','2018-11-14 07:30:15','112.215.174.41','\0'),('0BA528114A64D73FC965','920FBBD093E9E391C004','D0468AB6C3CDDF4B83EE',0,'2018-11-14 07:25:42',1,'-','',NULL,'staftu','2018-11-14 07:25:42','112.215.174.41','staftu','2018-11-14 07:25:42','112.215.174.41','\0'),('0F87D2D5D2F031414337','12E5FE65D1AD55AF6239','BC289275C34A1F2C61F3',0,'2018-11-13 20:57:27',100,'-','',NULL,'stafse','2018-11-13 20:57:27','112.215.245.137','stafse','2018-11-13 20:57:27','112.215.245.137','\0'),('2209605E1F42B293E660','3BFB2E93404FF6DBACAA','7C7691F2A499DB759F9B',0,'2018-11-07 08:35:19',1,'-','',NULL,'admin','2018-11-07 08:35:19','43.225.186.122','admin','2018-11-07 08:35:19','43.225.186.122','\0'),('5336CD05836CE3124C9B','12E5FE65D1AD55AF6239','D0468AB6C3CDDF4B83EE',0,'2018-11-13 20:36:58',1,'-','',NULL,'staftu','2018-11-13 20:36:58','112.215.245.137','staftu','2018-11-13 20:36:58','112.215.245.137','\0'),('63B40355FECAB2350834','12E5FE65D1AD55AF6239','255600DE37D616370204',0,'2018-11-13 20:52:53',7,'-','',NULL,'kasise','2018-11-13 20:52:53','112.215.245.137','kasise','2018-11-13 20:52:53','112.215.245.137','\0'),('7E30B749FFB5EEDC789B','920FBBD093E9E391C004','90B8BAA47F9F3838D085',0,'2018-11-14 07:36:03',7,'-','',NULL,'kasubagtu','2018-11-14 07:36:03','112.215.174.41','kasubagtu','2018-11-14 07:36:03','112.215.174.41','\0'),('B1E31BE5C854EE70BD62','12E5FE65D1AD55AF6239','C8C78126069666B06C1D',0,'2018-11-13 20:42:30',6,'-','',NULL,'Kepala','2018-11-13 20:42:30','112.215.245.137','Kepala','2018-11-13 20:42:30','112.215.245.137','\0'),('CD10D47CAB08AD667121','0F8C0E453BEF8F120729','90B8BAA47F9F3838D085',0,'2018-11-14 10:59:47',7,'-','',NULL,'kasubagtu','2018-11-14 10:59:47','43.225.186.122','kasubagtu','2018-11-14 10:59:47','43.225.186.122','\0'),('D177AEFF63C3CC1DEA72','0F8C0E453BEF8F120729','C8C78126069666B06C1D',0,'2018-11-14 10:58:08',6,'-','',NULL,'Kepala','2018-11-14 10:58:08','43.225.186.122','Kepala','2018-11-14 10:58:08','43.225.186.122','\0'),('DB304C575162BE84DD5E','0F8C0E453BEF8F120729','BC289275C34A1F2C61F3',0,'2018-11-14 10:56:50',1,'-','',NULL,'stafse','2018-11-14 10:56:50','43.225.186.122','stafse','2018-11-14 10:56:50','43.225.186.122','\0'),('E749F15959862E542EE9','0F8C0E453BEF8F120729','D0468AB6C3CDDF4B83EE',0,'2018-11-14 11:05:35',100,'-','',NULL,'lusi','2018-11-14 11:05:35','43.225.186.122','lusi','2018-11-14 11:05:35','43.225.186.122','\0'),('EAC68104399875908004','920FBBD093E9E391C004','90B8BAA47F9F3838D085',0,'2018-11-14 07:41:49',7,'-','',NULL,'kasubagtu','2018-11-14 07:41:49','112.215.174.41','kasubagtu','2018-11-14 07:41:49','112.215.174.41','\0'),('F5EAD41A53B4E8068BDA','920FBBD093E9E391C004','C8C78126069666B06C1D',0,'2018-11-14 07:28:30',6,'-','',NULL,'Kepala','2018-11-14 07:28:30','112.215.174.41','Kepala','2018-11-14 07:28:30','112.215.174.41','\0');
/*!40000 ALTER TABLE `ttsuratmasukhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratmasukjabatan`
--

DROP TABLE IF EXISTS `ttsuratmasukjabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratmasukjabatan` (
  `suratmasukjabatanid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `jabatanid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` tinyint(1) DEFAULT '0',
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratmasukjabatanid`),
  KEY `fk_ttsuratmasukjabatan_reference_ttsuratmasuk` (`suratmasukid`),
  KEY `fk_ttsuratmasukjabatan_reference_tmjabatan` (`jabatanid`),
  CONSTRAINT `fk_ttsuratmasukjabatan_reference_tmjabatan` FOREIGN KEY (`jabatanid`) REFERENCES `tmjabatan` (`jabatanid`),
  CONSTRAINT `fk_ttsuratmasukjabatan_reference_ttsuratmasuk` FOREIGN KEY (`suratmasukid`) REFERENCES `ttsuratmasuk` (`suratmasukid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratmasukjabatan`
--

LOCK TABLES `ttsuratmasukjabatan` WRITE;
/*!40000 ALTER TABLE `ttsuratmasukjabatan` DISABLE KEYS */;
INSERT INTO `ttsuratmasukjabatan` VALUES ('17A9741CBBC5D26E932D','920FBBD093E9E391C004','5380303FA54C3B4F0FF0',NULL,'Kepala Sub Bagian TU',1,'2018',NULL,NULL,NULL,'kasubagtu','2018-11-14 07:29:12','112.215.174.41','\0'),('19AD1261BDBDB9B66FF3','3BFB2E93404FF6DBACAA','88618521FEFE2F296E27',NULL,'Kepala Seksi PTL',0,'2018',NULL,NULL,NULL,'admin','2018-11-07 08:40:16','43.225.186.122','\0'),('1FE1FD1A3E09565EE415','920FBBD093E9E391C004','F8265B4659313AC9044F',NULL,'Kepala Seksi SE',1,'2018',NULL,NULL,NULL,'kasise','2018-11-14 07:29:55','112.215.174.41','\0'),('25244E2E1EE725F441DE','3BFB2E93404FF6DBACAA','F8265B4659313AC9044F',NULL,'Kepala Seksi SE',0,'2018',NULL,NULL,NULL,'admin','2018-11-07 08:40:16','43.225.186.122','\0'),('3E2BA8A82B16BD83FF4B','0F8C0E453BEF8F120729','5380303FA54C3B4F0FF0',NULL,'Kepala Sub Bagian TU',1,'2018',NULL,NULL,NULL,'kasubagtu','2018-11-14 10:59:39','43.225.186.122','\0'),('6C56EB26B8848873734F','3BFB2E93404FF6DBACAA','5380303FA54C3B4F0FF0',NULL,'Kepala Sub Bagian TU',0,'2018',NULL,NULL,NULL,'admin','2018-11-07 08:40:16','43.225.186.122','\0'),('8177DEF6EB766322FD1B','12E5FE65D1AD55AF6239','F8265B4659313AC9044F',NULL,'Kepala Seksi SE',1,'2018',NULL,NULL,NULL,'kasise','2018-11-13 20:52:30','112.215.245.137','\0'),('834050FB02C91FDEB024','3BFB2E93404FF6DBACAA','90CDFE4C8C5E4D546E41',NULL,'Kepala Seksi ADKL',0,'2018',NULL,NULL,NULL,'admin','2018-11-07 08:40:16','43.225.186.122','\0'),('ADBAA12BE4CFE078C361','12E5FE65D1AD55AF6239','5380303FA54C3B4F0FF0',NULL,'Kepala Sub Bagian TU',1,'2018',NULL,NULL,NULL,'kasubagtu','2018-11-13 20:47:46','112.215.245.137','\0');
/*!40000 ALTER TABLE `ttsuratmasukjabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratmasukpegawai`
--

DROP TABLE IF EXISTS `ttsuratmasukpegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratmasukpegawai` (
  `suratmasukpegawaiid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `disposisi` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` tinyint(1) DEFAULT '0',
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratmasukpegawaiid`),
  KEY `fk_ttsuratmasukpegawai_reference_ttsuratmasuk` (`suratmasukid`),
  KEY `fk_ttsuratmasukpegawai_reference_tmpegawai` (`pegawaiid`),
  CONSTRAINT `fk_ttsuratmasukpegawai_reference_tmpegawai` FOREIGN KEY (`pegawaiid`) REFERENCES `tmpegawai` (`pegawaiid`),
  CONSTRAINT `fk_ttsuratmasukpegawai_reference_ttsuratmasuk` FOREIGN KEY (`suratmasukid`) REFERENCES `ttsuratmasuk` (`suratmasukid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratmasukpegawai`
--

LOCK TABLES `ttsuratmasukpegawai` WRITE;
/*!40000 ALTER TABLE `ttsuratmasukpegawai` DISABLE KEYS */;
INSERT INTO `ttsuratmasukpegawai` VALUES ('5DD74EF6519158B17203','0F8C0E453BEF8F120729','D0468AB6C3CDDF4B83EE','fotokopi','',1,'2018','kasubagtu','2018-11-14 10:59:47','43.225.186.122','lusi','2018-11-14 11:05:27','43.225.186.122','\0'),('62F1D77E101899E48BB6','920FBBD093E9E391C004','BC289275C34A1F2C61F3','test','',0,'2018','kasubagtu','2018-11-14 07:41:49','112.215.174.41','kasubagtu','2018-11-14 07:41:49','112.215.174.41','\0'),('9BD886210EB9FFEB319E','12E5FE65D1AD55AF6239','BC289275C34A1F2C61F3','dilaksanakan segera','',1,'2018','kasise','2018-11-13 20:52:53','112.215.245.137','stafse','2018-11-13 20:56:03','112.215.245.137','\0'),('9EE34D73F9B1F939E2CA','12E5FE65D1AD55AF6239','D0468AB6C3CDDF4B83EE','Laksanakan segera','',1,'2018','kasise','2018-11-13 20:52:53','112.215.245.137','staftu','2018-11-13 20:54:38','112.215.245.137','\0'),('BD9A38545D269967928E','920FBBD093E9E391C004','D0468AB6C3CDDF4B83EE','TEST','',1,'2018','kasubagtu','2018-11-14 07:41:49','112.215.174.41','staftu','2018-11-14 07:55:59','112.215.174.41','\0');
/*!40000 ALTER TABLE `ttsuratmasukpegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ttsuratmasukperintah`
--

DROP TABLE IF EXISTS `ttsuratmasukperintah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ttsuratmasukperintah` (
  `suratmasukperintahid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `perintahid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `perintah` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`suratmasukperintahid`),
  KEY `fk_ttsuratmasukperintah_reference_ttsuratmasuk` (`suratmasukid`),
  KEY `fk_ttsuratmasukperintah_reference_tmperintah` (`perintahid`),
  CONSTRAINT `fk_ttsuratmasukperintah_reference_tmperintah` FOREIGN KEY (`perintahid`) REFERENCES `tmperintah` (`perintahid`),
  CONSTRAINT `fk_ttsuratmasukperintah_reference_ttsuratmasuk` FOREIGN KEY (`suratmasukid`) REFERENCES `ttsuratmasuk` (`suratmasukid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ttsuratmasukperintah`
--

LOCK TABLES `ttsuratmasukperintah` WRITE;
/*!40000 ALTER TABLE `ttsuratmasukperintah` DISABLE KEYS */;
INSERT INTO `ttsuratmasukperintah` VALUES ('098E60D669F4404D70D5','0F8C0E453BEF8F120729','7F897739A13968C8A889',1,'Untuk Tindak Lanjut','','2018','Kepala','2018-11-14 10:58:08','43.225.186.122','Kepala','2018-11-14 10:58:08','43.225.186.122','\0'),('12BF0B2D8079D096C060','3BFB2E93404FF6DBACAA',NULL,1,'SEGERA DI TINJUT. UTNUK STAFF YANG AKAN MENJALANKAN.','','2018',NULL,NULL,NULL,'admin','2018-11-07 08:40:16','43.225.186.122','\0'),('8579F206B6462CF50F76','920FBBD093E9E391C004','7',2,'Dibicarakan Dengan Saya','','2018',NULL,NULL,NULL,'Kepala','2018-11-14 07:28:30','112.215.174.41','\0'),('A0DFB293A1D1C977DC8E','12E5FE65D1AD55AF6239',NULL,2,'Tes','','2018','Kepala','2018-11-13 20:42:30','112.215.245.137','Kepala','2018-11-13 20:42:30','112.215.245.137','\0'),('AA8DBAD1110E09F00046','12E5FE65D1AD55AF6239','51CC713A295451D15C62',1,'Untuk Diketahui','','2018','Kepala','2018-11-13 20:42:30','112.215.245.137','Kepala','2018-11-13 20:42:30','112.215.245.137','\0'),('B9F2359DE69958B36862','920FBBD093E9E391C004','10',1,'Arsip','','2018',NULL,NULL,NULL,'Kepala','2018-11-14 07:28:30','112.215.174.41','\0');
/*!40000 ALTER TABLE `ttsuratmasukperintah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','123456','$2y$13$9m68xruYj3Rxq3MJ/KEL5ezE9RaSwA9Dq6baFSa2cUeKDXBsxRC6q','123456','kuclukb@gmail.com',1,0,0,'');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-15 15:53:41
