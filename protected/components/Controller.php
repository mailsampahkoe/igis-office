<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
//require Yii::getPathOfAlias('application.extensions') . '/PHPMailer/PHPMailer.php';
//require Yii::getPathOfAlias('application.extensions') . '/PHPMailer/SMTP.php';
//require Yii::getPathOfAlias('application.extensions') . '/PHPMailer/Exception.php';

class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
	
    function getFilterTahun() {
        $filter = " HAVING Tahun IN ('" . (Yii::app()->user->getTahun()) . "','" . (Yii::app()->user->getTahun() - 1) . "',                '" . (Yii::app()->user->getTahun() - 2) . "',
                '" . (Yii::app()->user->getTahun() - 3) . "','" . (Yii::app()->user->getTahun() - 4) . "'                     
                 )";
        return $filter;
    }

    public function ExportToJSONDataSource($data, $total = null) {
        if (is_null($total))
            $total = count($data);
        // untuk extjs 4 tidak perlu mengirim total record ($total)
        //return sprintf('{results:%s,rows:%s}', $total, CJSON::encode($data));
        return CJSON::encode($data);
    }
	
	public function getLogAddDataInfo()
    {
		$op=Yii::app()->user->name;
		return array(
            'opadd' => $op,
            'tgladd' => $this->getCurrentDatetime(), //$this->standard_date('DATE_KOE',time()),
			'pcadd' => Yii::app()->user->getIP(),
			'opedit' => $op,
            'tgledit' => $this->getCurrentDatetime(), //$this->standard_date('DATE_KOE',time()), //$this->standard_date('DATE_RFC822',time()),
			'pcedit' => Yii::app()->user->getIP(),
			'dlt'	=> '0'
        );
    }
	
	public function getLogEditDataInfo()
    {
		$op=Yii::app()->user->name;
		return array(
            'opedit' => $op,
            'tgledit' => $this->getCurrentDatetime(), //$this->standard_date('DATE_KOE',time()), //$this->standard_date('DATE_RFC822',time()),
			'pcedit' => Yii::app()->user->getIP(),
			'dlt'	=> '0'
        );
    }
	
	public function getLogDeleteDataInfo()
    {
		$op=Yii::app()->user->id;
		return array(
            'opedit' => $op,
            'tgledit' => $this->getCurrentDatetime(), //$this->standard_date('DATE_KOE',time()), //$this->standard_date('DATE_RFC822',time()),
			'pcedit' => Yii::app()->user->getIP(),
			'dlt'	=> '1'
        );
    }
    
    public function getCurrentDatetime() {
		if (enumVar::DBMS == 'postgres') {
			return $this->standard_date('DATE_RFC822',time());
		}
		else {
			return $this->standard_date('DATE_KOE',time());
		}
	}
	
	public function standard_date($fmt = 'DATE_RFC822', $time = '')
	{
		date_default_timezone_set("Asia/Jakarta");
		$formats = array(
						'DATE_SMALL'	=>	'%m/%d/%Y',
						'DATE_ATOM'		=>	'%Y-%m-%dT%H:%i:%s%Q',
						'DATE_COOKIE'	=>	'%l, %d-%M-%y %H:%i:%s UTC',
						'DATE_ISO8601'	=>	'%Y-%m-%dT%H:%i:%s%O',
						'DATE_RFC822'	=>	'%D, %d %M %y %H:%i:%s %O',
						'DATE_RFC850'	=>	'%l, %d-%M-%y %H:%m:%i UTC',
						'DATE_RFC1036'	=>	'%D, %d %M %y %H:%i:%s %O',
						'DATE_RFC1123'	=>	'%D, %d %M %Y %H:%i:%s %O',
						'DATE_RSS'		=>	'%D, %d %M %Y %H:%i:%s %O',
						'DATE_W3C'		=>	'%Y-%m-%dT%H:%i:%s%Q',
						'DATE_KOE'		=>	'%Y-%m-%d %H:%i:%s'
						);

		if ( ! isset($formats[$fmt]))
		{
			return FALSE;
		}
	
		return $this->mdate($formats[$fmt], $time);
	}
	
	public function mdate($datestr = '', $time = '')
	{
		if ($datestr == '')
			return '';
	
		if ($time == '')
			$time = now();
		
		$datestr = str_replace('%\\', '', preg_replace("/([a-z]+?){1}/i", "\\\\\\1", $datestr));
		return date($datestr, $time);
	}
		
	//Untuk merubah format tanggal dari EXTJS(dd/mm/yyyy) menjadi tanggal Indonesia dd MM YYYY	
	public function dateIndonesia($tanggal){
		return substr($tanggal, 0, 2).' '.$this->getNmBulan(substr($tanggal, 3, 2)).' '.substr($tanggal, 6, 4); 	
	}
	
	public function datetimeFormat($tanggal){
		return date('Y-m-d',strtotime($tanggal));
	}

    public function konversiBit($bool) {
        return $bool ? '1' : '0';
    }

    public function konversiBitString($bool) {
        return ($bool == 'true') ? '1' : '0';
    }

    public function konversiCheckbox($aktif) {
        return $aktif == 'on' ? '1' : '0';
    }

    public function konversiCheckboxStatus($aktif) {
        return $aktif == 'on' ? '01' : '00';
    }
	
	function getIP() {
        $ip;
        if (getenv("HTTP_CLIENT_IP"))
        $ip = getenv("HTTP_CLIENT_IP");
        else if(getenv("HTTP_X_FORWARDED_FOR"))
        $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if(getenv("REMOTE_ADDR"))
        $ip = getenv("REMOTE_ADDR");
        else
        $ip = "UNKNOWN";
        //return $ip;
		return $_SERVER["REMOTE_ADDR"] ;
    }
	
	public function getNmBulan($bulan) {
        if ($bulan == '1')
            return 'Januari';
        else if ($bulan == '2')
            return 'Februari';
        else if ($bulan == '3')
            return 'Maret';
        else if ($bulan == '4')
            return 'April';
        else if ($bulan == '5')
            return 'Mei';
        else if ($bulan == '6')
            return 'Juni';
        else if ($bulan == '7')
            return 'Juli';
        else if ($bulan == '8')
            return 'Agustus';
        else if ($bulan == '9')
            return 'September';
        else if ($bulan == '10')
            return 'Oktober';
        else if ($bulan == '11')
            return 'November';
        else
            return 'Desember';
    }
	
	
    public function getDirBase() {
        return Yii::getPathOfAlias('webroot');
    }
	
    public function getAPIBase() {
        return 'https://simapatda.batam.go.id/dev-simapatda/';
    }
	
	public function getWebapi($url,$dataparam,$method="POST"){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$params = http_build_query($dataparam);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	
	
    public function CetakPDF($report, $params, $namafile, $isEmptySource = false) {
//===================================================================================
//EXPORT TO PDF
//===================================================================================
        require_once("Report/java/Java.inc");

        $jasperReportsLib = $this->getJasperReportLib();
        $handle = @opendir($jasperReportsLib);

        $java_library_path = "";
        while (($new_item = readdir($handle)) !== false) {
            $java_library_path .= 'file:' . $jasperReportsLib . '/' . $new_item . ';';
        }

        $jasperConnectString = $this->getJasperConString();
        java_require($java_library_path);
        $Conn = new Java("org.altic.jasperReports.JdbcConnection");
        $Conn->setDriver("org.postgresql.Driver");
        $Conn->setConnectString($jasperConnectString);
        $Conn->setUser($this->getUserDB());
        $Conn->setPassword($this->getPassDB());

        $fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
        $emptyDataSource = new Java("net.sf.jasperreports.engine.JREmptyDataSource");
        $jasperPrint = $fillManager->fillReport($report, $params, !$isEmptySource ? $Conn->getConnection() : $emptyDataSource);
        $outputPath = realpath(".") . "/" . ".pdf";
        $exportManager = new JavaClass("net.sf.jasperreports.engine.JasperExportManager");
        $exportManager->exportReportToPdfFile($jasperPrint, $outputPath);
        header("Content-Type: application/force-download\n");
        header("Content-Disposition: attachment; filename=$namafile.pdf");
        readfile($outputPath);
    }

    public function CetakExcel($report, $params, $namafile, $isEmptySource = false) {
        require_once("Report/java/Java.inc");

        $jasperReportsLib = $this->getJasperReportLib();
        $handle = @opendir($jasperReportsLib);

        $java_library_path = "";
        while (($new_item = readdir($handle)) !== false) {
            $java_library_path .= 'file:' . $jasperReportsLib . '/' . $new_item . ';';
        }

        $jasperConnectString = $this->getJasperConString();
        java_require($java_library_path);
        $Conn = new Java("org.altic.jasperReports.JdbcConnection");
        $Conn->setDriver("org.postgresql.Driver");
        $Conn->setConnectString($jasperConnectString);
        $Conn->setUser($this->getUserDB());
        $Conn->setPassword($this->getPassDB());

        $fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
//===================================================================================
//EXPORT TO EXCEL
//===================================================================================
        $emptyDataSource = new Java("net.sf.jasperreports.engine.JREmptyDataSource");
        $jasperPrint = $fillManager->fillReport($report, $params, !$isEmptySource ? $Conn->getConnection() : $emptyDataSource);
        $outputPath = realpath(".") . "/" . ".xls";
        $exporter = new java("net.sf.jasperreports.engine.export.JRXlsExporter");
//$exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_IGNORE_GRAPHICS, java("java.lang.Boolean")->FALSE);
        $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_DETECT_CELL_TYPE, java("java.lang.Boolean")->TRUE);
        $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, java("java.lang.Boolean")->TRUE);
        $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
        $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
        header("Content-Type: application/force-download\n");
        header("Content-Disposition: attachment; filename=$namafile.xls");
        $exporter->exportReport();
        readfile($outputPath);
    }

    public function CetakDoc($report, $params, $namafile, $isEmptySource = false) {
        require_once("Report/java/Java.inc");

        $jasperReportsLib = $this->getJasperReportLib();
        $handle = @opendir($jasperReportsLib);

        $java_library_path = "";
        while (($new_item = readdir($handle)) !== false) {
            $java_library_path .= 'file:' . $jasperReportsLib . '/' . $new_item . ';';
        }

        $jasperConnectString = $this->getJasperConString();
        java_require($java_library_path);
        $Conn = new Java("org.altic.jasperReports.JdbcConnection");
        $Conn->setDriver("org.postgresql.Driver");
        $Conn->setConnectString($jasperConnectString);
        $Conn->setUser($this->getUserDB());
        $Conn->setPassword($this->getPassDB());

        $fillManager = new JavaClass("net.sf.jasperreports.engine.JasperFillManager");
//===================================================================================
//EXPORT TO EXCEL
//===================================================================================
        $emptyDataSource = new Java("net.sf.jasperreports.engine.JREmptyDataSource");
        $jasperPrint = $fillManager->fillReport($report, $params, !$isEmptySource ? $Conn->getConnection() : $emptyDataSource);
        $outputPath = realpath(".") . "/" . ".doc";

        $exporter = new java("net.sf.jasperreports.engine.export.ooxml.JRDocxExporter");
//$exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_IGNORE_GRAPHICS, java("java.lang.Boolean")->FALSE);
// $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_DETECT_CELL_TYPE, java("java.lang.Boolean")->TRUE);
// $exporter->setParameter(java("net.sf.jasperreports.engine.export.JRXlsExporterParameter")->IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, java("java.lang.Boolean")->TRUE);
        $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->JASPER_PRINT, $jasperPrint);
        $exporter->setParameter(java("net.sf.jasperreports.engine.JRExporterParameter")->OUTPUT_FILE_NAME, $outputPath);
        header("Content-Type: application/force-download\n");
        header("Content-Disposition: attachment; filename=$namafile.doc");
        $exporter->exportReport();
        readfile($outputPath);
    }

	  public function getJasperReportLib() {
        $path_java = "/usr/java/jdk1.7.0_09/jre/lib/ext";
        return $path_java;
    }
	
	 public function getUserDB() {
        return "postgres";
    }

    public function getPassDB() {
        return "123456";
    }

    public function getNameDB() {
        return "igis-office";
    }

    public function getHost() {
        return "localhost";
    }

    public function getJasperConString() {
        return 'jdbc:postgresql://' . $this->getHost() . ':5432/' . $this->getNameDB();
    }
    
    public function datePostgres($tanggal) {
        $result = null;
        $result = substr($tanggal, 6, 4) . '/' . substr($tanggal, 3, 2) . '/' . substr($tanggal, 0, 2);
        if ($result == "//") {
            return null;
        }
        return $result;
    }

    public function datePHP($tanggal) {
        $result = null;
        $result = substr($tanggal, 6, 4) . '-' . substr($tanggal, 3, 2) . '-' . substr($tanggal, 0, 2);
        if ($result == "--") {
            return null;
        }
        return $result;
    }

    public function dateIndo($tanggal) {
        $result = null;
        $result = substr($tanggal, 8, 2) . '-' . substr($tanggal, 5, 2) . '-' . substr($tanggal, 0, 4);
        if ($result == "--") {
            return null;
        }
        return $result;
    }

    public function dateTransferPostgres($tanggal) {
        return substr($tanggal, 6, 4) . '/' . substr($tanggal, 3, 2) . '/' . substr($tanggal, 0, 2);
    }
	
	protected function beforeAction($action)
   	{
       	if($action->id != "uploader")
       	{
          	//do some stuff here
       	}
	    if(Yii::app()->user->isGuest) {
			//Yii::app()->user->setTahun(date("Y"));
      
    	} else {
    		
		}

       return parent::beforeAction($action);
   }
	
	private function assignrandvalue($num) {
	    // accepts 1 - 36
	    switch($num) {
	        case "1"  : $rand_value = "a"; break;
	        case "2"  : $rand_value = "b"; break;
	        case "3"  : $rand_value = "c"; break;
	        case "4"  : $rand_value = "d"; break;
	        case "5"  : $rand_value = "e"; break;
	        case "6"  : $rand_value = "f"; break;
	        case "7"  : $rand_value = "g"; break;
	        case "8"  : $rand_value = "h"; break;
	        case "9"  : $rand_value = "i"; break;
	        case "10" : $rand_value = "j"; break;
	        case "11" : $rand_value = "k"; break;
	        case "12" : $rand_value = "l"; break;
	        case "13" : $rand_value = "m"; break;
	        case "14" : $rand_value = "n"; break;
	        case "15" : $rand_value = "o"; break;
	        case "16" : $rand_value = "p"; break;
	        case "17" : $rand_value = "q"; break;
	        case "18" : $rand_value = "r"; break;
	        case "19" : $rand_value = "s"; break;
	        case "20" : $rand_value = "t"; break;
	        case "21" : $rand_value = "u"; break;
	        case "22" : $rand_value = "v"; break;
	        case "23" : $rand_value = "w"; break;
	        case "24" : $rand_value = "x"; break;
	        case "25" : $rand_value = "y"; break;
	        case "26" : $rand_value = "z"; break;
	        case "27" : $rand_value = "0"; break;
	        case "28" : $rand_value = "1"; break;
	        case "29" : $rand_value = "2"; break;
	        case "30" : $rand_value = "3"; break;
	        case "31" : $rand_value = "4"; break;
	        case "32" : $rand_value = "5"; break;
	        case "33" : $rand_value = "6"; break;
	        case "34" : $rand_value = "7"; break;
	        case "35" : $rand_value = "8"; break;
	        case "36" : $rand_value = "9"; break;
	    }
	    return $rand_value;
	}

	public function getrandalphanumeric($length) {
	    if ($length>0) {
	        $rand_id="";
	        for ($i=1; $i<=$length; $i++) {
	            mt_srand((double)microtime() * 1000000);
	            $num = mt_rand(1,36);
	            $rand_id .= $this->assignrandvalue($num);
	        }
	    }
	    return $rand_id;
	}

	public function getrandnumbers($length) {
	    if ($length>0) {
	        $rand_id="";
	        for($i=1; $i<=$length; $i++) {
	            mt_srand((double)microtime() * 1000000);
	            $num = mt_rand(27,36);
	            $rand_id .= $this->assignrandvalue($num);
	        }
	    }
	    return $rand_id;
	}

	public function getrandletters($length) {
	    if ($length>0) {
	        $rand_id="";
	        for($i=1; $i<=$length; $i++) {
	            mt_srand((double)microtime() * 1000000);
	            $num = mt_rand(1,26);
	            $rand_id .= $this->assignrandvalue($num);
	        }
	    }
	    return $rand_id;
	}
	
	public function sendmail($to, $subject, $message) {
    	$mail = new PHPMailer\PHPMailer\PHPMailer();
		$mail->IsSMTP();
    	$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
	    $mail->SMTPSecure = "tls";
	    $mail->Port = 587;
		$mail->Username = 'admin@gmail.com';
		$mail->Password = 'czgmaklxyswuynpf';
		$mail->SetFrom('admin@gmail.com', 'Admin');
		$mail->Subject = $subject;
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message);
		$recipients = explode(',',$to);

		foreach($recipients as $recipient) {
			if (filter_var($recipient, FILTER_VALIDATE_EMAIL)) {    
				$mail->AddAddress(trim($recipient, " "), trim($recipient, " "));
			}
		}
		return $mail->Send();
		
		//echo 'sent';
	}
	
	public function sendmailwithattachment($to, $subject, $message, $file, $file_name) {
    	$mail = new PHPMailer\PHPMailer\PHPMailer();
		$mail->IsSMTP();
    	$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
	    $mail->SMTPSecure = "tls";
	    $mail->Port = 587;
		$mail->Username = 'admin@gmail.com';
		$mail->Password = 'czgmaklxyswuynpf';
		$mail->SetFrom('admin@gmail.com', 'Admin');
		$mail->Subject = $subject;
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message);
		$recipients = explode(',',$to);

		foreach($recipients as $recipient) {
			if (filter_var($recipient, FILTER_VALIDATE_EMAIL)) {    
				$mail->AddAddress(trim($recipient, " "), trim($recipient, " "));
			}
		}
		if ($file != '') {
			if ($file_name == '') $file_name = 'attachment';
	        $mail->AddAttachment( $file, $file_name );
		}
		
		return $mail->Send();
		
		//echo 'sent';
	}
	
	public function createlog($category, $group, $username, $modulename, $methodname, $log) {
		$file = '';
		$key = md5(session_id().'-'.$_SERVER['REMOTE_ADDR'].'-'.$username);
		
	    $current_date = date("Y-m-d H:i:s");
	    $current_date_file = date("Ymd");
		
        if (!is_resource($file)) {
	        $filename = ROOT . DS . 'resources'. DS . 'logs'. DS .'log_'.$username.'_'.$current_date_file.'_'.$key.'.txt';
	        $file = fopen($filename, 'a') or exit("Can't open $filename!");
        }
        $result = '';
	    try {
		    $log  = $category."-".$group." ".$_SERVER['REMOTE_ADDR'].'::'.$current_date."::".$username."::".$modulename."-".$methodname."::".$log.PHP_EOL;
	        fwrite($file, $log);
	        fclose($file);
			$result = 'succeed';
			
			if (strpos(strtolower($log), 'failed') !== false) {
			    try {
			        $filename2 = ROOT . DS . 'resources'. DS . 'logs'. DS .'log_systemfailed_'.$current_date_file.'_'.$key.'.txt';
			        $file = fopen($filename2, 'a') or exit("Can't open $filename2!");
				    
				    $log .= 'Filename : '.$filename.PHP_EOL;
			        fwrite($file, $log);
			        fclose($file);
				}
				catch (Exception $e) {
				}
			}
		}
		catch (Exception $e) {
			$result = 'failed-'.$e->getMessage();
		}
		catch (RuntimeException $e) {
		    $result .= $e->getMessage();
		}
	}
   	
	public function saveNotificationSubbagian($transid, $bagianid, $subbagianid, $level, $judul, $keterangan, $controller, $action, $primarykey) {
		$sql = "SELECT a.userid
				FROM tmuser a 
				inner join tmpegawai b on b.pegawaiid = a.pegawaiid and b.dlt = '0'
				inner join tmjabatan c on c.jabatanid = b.jabatanid and c.dlt = '0'
				where a.dlt='0' 
				and b.bagianid = '".$bagianid."'
				and coalesce(b.subbagianid, '') = '".$subbagianid."'
				and c.level = '".$level."'
				";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		//echo $sql;
		if (count($rows) > 0) {
			$modelnotif = new Notifikasi;
			$modelnotif->notifikasiid = Globals::newID("tsnotifikasi", "notifikasiid");
			$modelnotif->tglnotifikasi = $this->getCurrentDatetime();
			$modelnotif->judul = $judul;
			$modelnotif->keterangan = $keterangan;
			$modelnotif->attributes = array_merge($modelnotif->attributes,$this->getLogAddDataInfo());
			$modelnotif->controller = $controller;
			$modelnotif->action = $action;
			$modelnotif->primarykey = $primarykey;
			$modelnotif->transaksiid = $transid;
            if ($modelnotif->save()) {
		        foreach ($rows as $row) {
					$modelnotifuser = new Notifikasiuser;
					$modelnotifuser->notifikasiuserid = Globals::newID("tsnotifikasiuser", "notifikasiuserid");
					$modelnotifuser->notifikasiid = $modelnotif->notifikasiid;
					$modelnotifuser->userid = $row['userid'];
					$modelnotifuser->status = '1';
					$modelnotifuser->attributes = array_merge($modelnotifuser->attributes,$this->getLogAddDataInfo());
					
					$modelnotifuser->save();
		        }
			}
		}
	}
   	
	public function saveNotificationPegawai($transid, $pegawaiid, $judul, $keterangan, $controller, $action, $primarykey) {
		$sql = "SELECT a.userid
				FROM tmuser a 
				where a.dlt='0' 
				and a.pegawaiid = '".$pegawaiid."'
				";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		//echo $sql;
		if (count($rows) > 0) {
			$modelnotif = new Notifikasi;
			$modelnotif->notifikasiid = Globals::newID("tsnotifikasi", "notifikasiid");
			$modelnotif->tglnotifikasi = $this->getCurrentDatetime();
			$modelnotif->judul = $judul;
			$modelnotif->keterangan = $keterangan;
			$modelnotif->attributes = array_merge($modelnotif->attributes,$this->getLogAddDataInfo());
			$modelnotif->controller = $controller;
			$modelnotif->action = $action;
			$modelnotif->primarykey = $primarykey;
			$modelnotif->transaksiid = $transid;
            if ($modelnotif->save()) {
		        foreach ($rows as $row) {
					$modelnotifuser = new Notifikasiuser;
					$modelnotifuser->notifikasiuserid = Globals::newID("tsnotifikasiuser", "notifikasiuserid");
					$modelnotifuser->notifikasiid = $modelnotif->notifikasiid;
					$modelnotifuser->userid = $row['userid'];
					$modelnotifuser->status = '1';
					$modelnotifuser->attributes = array_merge($modelnotifuser->attributes,$this->getLogAddDataInfo());
					
					$modelnotifuser->save();
		        }
			}
		}
	}
	
	public function getNotification() {
		$sql = "select a.notifikasiid, a.judul, a.keterangan, a.tglnotifikasi, b.status, a.controller, a.action 
			from tsnotifikasi a
			inner join tsnotifikasiuser b on b.notifikasiid = a.notifikasiid and b.dlt = '0' and userid = '".Yii::app()->user->getuser('id')."' and coalesce(b.status, '0') = '1'
			where a.dlt='0'
			order by tglnotifikasi desc ";
		$datanotifikasi = Yii::app()->db->createCommand($sql)->queryAll();
		$sql = "select a.pesanid, a.judul, a.keterangan, a.tglpesan, b.status, a.controller, a.action 
			from tspesan a
			inner join tspesanuser b on b.pesanid = a.pesanid and b.dlt = '0' and userid = '".Yii::app()->user->getuser('id')."' and coalesce(b.status, '0') = '1' 
			where a.dlt='0'
			order by tglpesan desc ";
		$datapesan = Yii::app()->db->createCommand($sql)->queryAll();
		
		$hasil = array('datapesan'=>$datapesan);
		$hasil = array_merge($hasil, array('datanotifikasi'=>$datanotifikasi));
		return $hasil;
	}
   	
	public function saveHistorySuratmasuk($suratmasukid, $pegawaiid, $status, $catatan, $keterangan) {
		$model = new Suratmasukhistory;
		$model->suratmasukhistoryid = Globals::newID("ttsuratmasukhistory", "suratmasukhistoryid");
		$model->suratmasukid = $suratmasukid;
		$model->pegawaiid = $pegawaiid;
		$model->tglverifikasi = $this->getCurrentDatetime();
		$model->nourut = '0';
		$model->status = $status;
		$model->catatan = $catatan;
		$model->keterangan = $keterangan;
		$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
        return $model->save();
	}
   	
	public function saveHistorySuratkeluar($suratkeluarid, $pegawaiid, $status, $catatan, $keterangan) {
		$model = new Suratkeluarhistory;
		$model->suratkeluarhistoryid = Globals::newID("ttsuratkeluarhistory", "suratkeluarhistoryid");
		$model->suratkeluarid = $suratkeluarid;
		$model->pegawaiid = $pegawaiid;
		$model->tglverifikasi = $this->getCurrentDatetime();
		$model->nourut = '0';
		$model->status = $status;
		$model->catatan = $catatan;
		$model->keterangan = $keterangan;
		$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
        return $model->save();
	}
}
