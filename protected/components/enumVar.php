<?php

class enumVar {
	const BCINMAIN = false;
	const DBMS_MYSQL = 'mysql';
	const DBMS_POSTGRES = 'postgres';
	const DBMS = 'mysql';
	
    const JENIS_SKPD_SKPD = 0;
	const JENIS_SKPD_BLUD = 1;
	
    const JENISKEL_LAKILAKI = 0;
	const JENISPER_PEREMPUAN = 1;
	
	public function listJenisKel($name="id") {
		if ($name=="id")
			return array(self::JENISKEL_LAKILAKI, self::JENISPER_PEREMPUAN);
		else
			return array("Laki-laki","Perempuan");
	}
	
	//Jabatan
	const JABATAN_PPKD = 1;
	const JABATAN_PPTK = 2;
	const JABATAN_BENDAHARA_PENGELUARAN = 3;
	const JABATAN_KA_SKPD = 4;
	const JABATAN_KUASA_BUD = 5;
	const JABATAN_BUD = 6;
	const JABATAN_PENGGUNA_ANGGARAN = 7;
	const JABATAN_KUASA_PENGGUNA_ANGGARAN = 8;
	const JABATAN_PPK_SKPD = 9;
	const JABATAN_BENDAHARA_PEMBANTU = 10;
	const JABATAN_BENDAHARA_PENERIMAAN = 11;
	const JABATAN_STAFF_PPTK = 12;
	const JABATAN_BENDAHARA_PENGELUARAN_PEMBANTU = 13;
	const JABATAN_PEMBANTU_BENDAHARA_PENGELUARAN = 14;
	const JABATAN_PEMBANTU_BENDAHARA_PENGELUARAN_PEMBANTU = 15;
	const JABATAN_STAF_BUD = 16;

	
	public function listJabatan($name="id") {
		if ($name=="id")
			return array(self::JABATAN_PPKD, self::JABATAN_PPTK, self::JABATAN_BENDAHARA_PENGELUARAN, self::JABATAN_KA_SKPD, self::JABATAN_KUASA_BUD, self::JABATAN_BUD, self::JABATAN_PENGGUNA_ANGGARAN, self::JABATAN_KUASA_PENGGUNA_ANGGARAN, self::JABATAN_PPK_SKPD, self::JABATAN_BENDAHARA_PEMBANTU, self::JABATAN_BENDAHARA_PENERIMAAN, self::JABATAN_STAFF_PPTK, self::JABATAN_BENDAHARA_PENGELUARAN_PEMBANTU, self::JABATAN_PEMBANTU_BENDAHARA_PENGELUARAN, self::JABATAN_PEMBANTU_BENDAHARA_PENGELUARAN_PEMBANTU, self::JABATAN_STAF_BUD);
		else
			return array("PPKD","PPTK","Bendahara Pengeluaran","Ka. SKPD","Kuasa BUD","BUD","Pengguna Anggaran","Kuasa Pengguna Anggaran","PPK-SKPD","Bendahara Pembantu","Bendahara Penerimaan","Staff PPTK","Bendahara Pengeluaran Pembantu","Pembantu Bendahara Pengeluaran","Pembantu Bendahara Pengeluaran Pembantu","Staf BUD");
	}
	
    const JENISUSAHA_PT = 0;
    const JENISUSAHA_CV = 1;
    const JENISUSAHA_KOPERASI = 2;
    const JENISUSAHA_UD = 3;
    const JENISUSAHA_YAYASAN = 4;
    const JENISUSAHA_TOKO = 5;
    const JENISUSAHA_LAINNYA = 6;
	
	const STEP_I_DRAFT = 0;
	const STEP_I_POSTED = 1;
	const STEP_I_KAUNIT_VERIFIED = 2;
	const STEP_I_KASUBUNIT_VERIFIED = 3;
	const STEP_I_KABID_VERIFIED = 4;
	const STEP_I_KASUBBID_VERIFIED = 5;
	const STEP_I_KABAG_VERIFIED = 6;
	const STEP_I_KASUBBAG_VERIFIED =7;
	const STEP_I_FINISHED = 100;
	const STEP_I_CANCELLED = 101;
	const STEP_I_DENIED = 102;
	
	const STEP_I_DESC_DRAFT = "Draft";
	const STEP_I_DESC_POSTED = "Telah Diposting";
	const STEP_I_DESC_KAUNIT_VERIFIED = "Disposisi Kepala Unit";
	const STEP_I_DESC_KASUBUNIT_VERIFIED = "Disposisi Kepala Unit";
	const STEP_I_DESC_KABID_VERIFIED = "Disposisi Kepala Bidang";
	const STEP_I_DESC_KASUBBID_VERIFIED = "Disposisi Kepala Sub Bidang";
	const STEP_I_DESC_KABAG_VERIFIED = "Disposisi Kepala Bagian / UPT";
	const STEP_I_DESC_KASUBBAG_VERIFIED ="Disposisi Kepala Sub Bagian / Seksie";
	const STEP_I_DESC_FINISHED = "Selesai";
	const STEP_I_DESC_CANCELLED = "Dibatalkan";
	const STEP_I_DESC_DENIED = "Ditolak";
	
	const STEP_O_DRAFT = 0;
	const STEP_O_POSTED = 1;
	const STEP_O_KASUBBAG_VERIFIED = 2;
	const STEP_O_KABAG_VERIFIED = 3;
	const STEP_O_KASUBBID_VERIFIED = 4;
	const STEP_O_KABID_VERIFIED = 5;
	const STEP_O_KASUBUNIT_VERIFIED = 6;
	const STEP_O_KAUNIT_VERIFIED = 7;
	const STEP_O_FINISHED = 100;
	const STEP_O_CANCELLED = 101;
	const STEP_O_DENIED = 102;
	
	const STEP_O_DESC_DRAFT = "Draft";
	const STEP_O_DESC_POSTED = "Telah Diposting";
	const STEP_O_DESC_KAUNIT_VERIFIED = "Verifikasi Kepala Unit";
	const STEP_O_DESC_KASUBUNIT_VERIFIED = "Verifikasi Kepala Unit";
	const STEP_O_DESC_KABID_VERIFIED = "Verifikasi Kepala Bidang";
	const STEP_O_DESC_KASUBBID_VERIFIED = "Verifikasi Kepala Sub Bidang";
	const STEP_O_DESC_KABAG_VERIFIED = "Verifikasi Kepala Bagian / UPT";
	const STEP_O_DESC_KASUBBAG_VERIFIED ="Verifikasi Kepala Sub Bagian / Seksie";
	const STEP_O_DESC_FINISHED = "Selesai";
	const STEP_O_DESC_CANCELLED = "Dibatalkan";
	const STEP_O_DESC_DENIED = "Ditolak";
	
	const USE_UNIT = false;
	const USE_SUBUNIT = false;
	const USE_BID = false;
	const USE_SUBBID = false;
	const USE_BAG = true;
	const USE_SUBBAG = true;
	
	public function listStepIn($name="id") {
		if ($name=="id")
			return array(self::STEP_I_DRAFT, self::STEP_I_POSTED, self::STEP_I_KABAG_VERIFIED, self::STEP_I_KASUBBAG_VERIFIED, self::STEP_I_FINISHED);
		else
			return array(self::STEP_I_DESC_DRAFT, self::STEP_I_DESC_POSTED, self::STEP_I_DESC_KABAG_VERIFIED, self::STEP_I_DESC_KASUBBAG_VERIFIED, self::STEP_I_DESC_FINISHED);
	}
	
	public function getStoreStepIn() {
		$dataids = self::listStepIn();
		$datanames = self::listStepIn("name");
		$result = array();
		for($i = 0; $i<count($dataids); $i++) {
			$result = array_merge($result, array('id' => $dataids[$i], 'text' => $datanames[$i]));
			//echo "{\"id\":\"".$dataids[$i]."\",\"text\":\"".$datanames[$i]."\"},";
		}
		
		return json_encode($result);
	}
	
	//step : 0 -> back, 1 -> next, option : 0 -> in, 1 -> out
	public function getStep($current=0, $step=1, $option=0) {
		$result = self::STEP_I_DRAFT;
		if ($option == 1) {
			switch($current) {
				case self::STEP_O_DRAFT:
					if ($step == 1) {
						$result = self::STEP_O_POSTED;
					}
					break;
				case self::STEP_O_POSTED:
					if ($step == 1) {
						if (self::USE_SUBBAG) {
							$result = self::STEP_O_KASUBBAG_VERIFIED;
						}
						elseif (self::USE_BAG) {
							$result = self::STEP_O_KABAG_VERIFIED;
						}
						elseif (self::USE_SUBBID) {
							$result = self::STEP_O_KASUBBID_VERIFIED;
						}
						elseif (self::USE_BID) {
							$result = self::STEP_O_KABID_VERIFIED;
						}
						elseif (self::USE_SUBUNIT) {
							$result = self::STEP_O_KASUBUNIT_VERIFIED;
						}
						else {
							$result = self::STEP_O_KAUNIT_VERIFIED;
						}
					}
					else {
						$result = self::STEP_O_DRAFT;
					}
					break;
				case self::STEP_O_KASUBBAG_VERIFIED:
					if ($step == 1) {
						if (self::USE_BAG) {
							$result = self::STEP_O_KABAG_VERIFIED;
						}
						elseif (self::USE_SUBBID) {
							$result = self::STEP_O_KASUBBID_VERIFIED;
						}
						elseif (self::USE_BID) {
							$result = self::STEP_O_KABID_VERIFIED;
						}
						elseif (self::USE_SUBUNIT) {
							$result = self::STEP_O_KASUBUNIT_VERIFIED;
						}
						elseif (self::USE_UNIT) {
							$result = self::STEP_O_KAUNIT_VERIFIED;
						}
						else {
							$result = self::STEP_O_FINISHED;
						}
					}
					else {
						$result = self::STEP_O_POSTED;
					}
					break;
				case self::STEP_O_KABAG_VERIFIED:
					if ($step == 1) {
						if (self::USE_SUBBID) {
							$result = self::STEP_O_KASUBBID_VERIFIED;
						}
						elseif (self::USE_BID) {
							$result = self::STEP_O_KABID_VERIFIED;
						}
						elseif (self::USE_SUBUNIT) {
							$result = self::STEP_O_KASUBUNIT_VERIFIED;
						}
						elseif (self::USE_UNIT) {
							$result = self::STEP_O_KAUNIT_VERIFIED;
						}
						else {
							$result = self::STEP_O_FINISHED;
						}
					}
					else {
						if (self::USE_SUBBAG) {
							$result = self::STEP_O_KASUBBAG_VERIFIED;
						}
						else {
							$result = self::STEP_O_POSTED;
						}
					}
					break;
				case self::STEP_O_FINISHED:
					if ($step == 1) {
						$result = self::STEP_O_FINISHED;
					}
					else {
						if (self::USE_UNIT) {
							$result = self::STEP_O_KAUNIT_VERIFIED;
						}
						elseif (self::USE_SUBUNIT) {
							$result = self::STEP_O_KASUBUNIT_VERIFIED;
						}
						elseif (self::USE_BID) {
							$result = self::STEP_O_KABID_VERIFIED;
						}
						elseif (self::USE_SUBBID) {
							$result = self::STEP_O_KASUBBID_VERIFIED;
						}
						elseif (self::USE_BAG) {
							$result = self::STEP_O_KABAG_VERIFIED;
						}
						elseif (self::USE_SUBBAG) {
							$result = self::STEP_O_KASUBBAG_VERIFIED;
						}
						else {
							$result = self::STEP_O_POSTED;
						}
					}
					break;
			}
		}
		else {
			switch($current) {
				case self::STEP_I_DRAFT:
					if ($step == 1) {
						$result = self::STEP_I_POSTED;
					}
					break;
				case self::STEP_I_POSTED:
					if ($step == 1) {
						if (self::USE_UNIT) {
							$result = self::STEP_I_KAUNIT_VERIFIED;
						}
						elseif (self::USE_SUBUNIT) {
							$result = self::STEP_I_KASUBUNIT_VERIFIED;
						}
						elseif (self::USE_BID) {
							$result = self::STEP_I_KABID_VERIFIED;
						}
						elseif (self::USE_SUBBID) {
							$result = self::STEP_I_KASUBBID_VERIFIED;
						}
						elseif (self::USE_BAG) {
							$result = self::STEP_I_KABAG_VERIFIED;
						}
						else {
							$result = self::STEP_I_KASUBBAG_VERIFIED;
						}
					}
					else {
						$result = self::STEP_I_DRAFT;
					}
					break;
				case self::STEP_I_KABAG_VERIFIED:
					if ($step == 1) {
						$result = self::STEP_I_KASUBBAG_VERIFIED;
					}
					else {
						if (self::USE_SUBBID) {
							$result = self::STEP_I_KASUBBID_VERIFIED;
						}
						elseif (self::USE_BID) {
							$result = self::STEP_I_KABID_VERIFIED;
						}
						elseif (self::USE_SUBUNIT) {
							$result = self::STEP_I_KASUBUNIT_VERIFIED;
						}
						elseif (self::USE_UNIT) {
							$result = self::STEP_I_KAUNIT_VERIFIED;
						}
						else {
							$result = self::STEP_I_POSTED;
						}
					}
					break;
				case self::STEP_I_KASUBBAG_VERIFIED:
					if ($step == 1) {
						$result = self::STEP_I_FINISHED;
					}
					else {
						if (self::USE_BAG) {
							$result = self::STEP_I_KABAG_VERIFIED;
						}
						elseif (self::USE_SUBBID) {
							$result = self::STEP_I_KASUBBID_VERIFIED;
						}
						elseif (self::USE_BID) {
							$result = self::STEP_I_KABID_VERIFIED;
						}
						elseif (self::USE_SUBUNIT) {
							$result = self::STEP_I_KASUBUNIT_VERIFIED;
						}
						elseif (self::USE_UNIT) {
							$result = self::STEP_I_KAUNIT_VERIFIED;
						}
						else {
							$result = self::STEP_I_POSTED;
						}
					}
					break;
				case self::STEP_I_FINISHED:
					if ($step == 1) {
						$result = self::STEP_I_FINISHED;
					}
					else {
						if (self::USE_SUBBAG) {
							$result = self::STEP_I_KASUBBAG_VERIFIED;
						}
						elseif (self::USE_BAG) {
							$result = self::STEP_I_KABAG_VERIFIED;
						}
						elseif (self::USE_SUBBID) {
							$result = self::STEP_I_KASUBBID_VERIFIED;
						}
						elseif (self::USE_BID) {
							$result = self::STEP_I_KABID_VERIFIED;
						}
						elseif (self::USE_SUBUNIT) {
							$result = self::STEP_I_KASUBUNIT_VERIFIED;
						}
						elseif (self::USE_UNIT) {
							$result = self::STEP_I_KAUNIT_VERIFIED;
						}
						else {
							$result = self::STEP_I_POSTED;
						}
					}
					break;
			}
		}
		
		return $result;
	}
}

?>