<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Globals extends CApplicationComponent {

    public static function newID($TableName, $Primary) {
        $isAda = TRUE;
        $jumlahSama = 0;
        $guidText = "";
        while ($isAda) {
            $s = strtoupper(md5(uniqid(rand(), true)));
            $guidText = substr($s, 0, 8) . '' .
                    substr($s, 8, 2) . '' .
                    substr($s, 12, 2) . '' .
                    substr($s, 16, 4) . '' .
                    substr($s, 20, 4);
            if (Globals::isDataExist("select count(*) as count from " . $TableName . " where " . $Primary . "='" . $guidText . "'")) {
                $isAda = TRUE;
                $jumlahSama = $jumlahSama + 1;
            } else {
                $isAda = FALSE;
            }
        }
        return $guidText;
    }

    public static function isDataExist($sql) {
        $recCount = "0";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result) {
            $recCount = $recCount + $result['count'];
        }
        if ($recCount == "0" || $recCount == null || $recCount == 'null') {
            return false;
        } else {
            return true;
        }
    }
    
        public function cryptPassword($password) {
        $sql = "select crypt('$password',gen_salt('md5'))";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        $result = $rows[0];
        $hasilcrypt = $result['crypt'];
        return $hasilcrypt;
    }

    public static function findByRef($table, $field, $where = "") {
        $sql = "SELECT $field as result FROM $table WHERE $field is not null $where LIMIT 1";
		
		
        $output = "";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result) {
            $output = $result['result'];
        }
        return $output;
    }

    public static function GetNextNo($table, $field, $where = "") {
        try {
            $sql = "select ifnull(max(cast($field as UNSIGNED))+1,1) as kode from $table where $field is not null $where";
            $nextKode = "1";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextKode = $result['kode'];
            }

            while (strlen($nextKode) < 4) {
                $nextKode = "0" . $nextKode;
            }

            return $nextKode . "";
        } catch (Exception $e) {
            return "0001";
        }
    }

    public static function dateMysql($date) {
        try {

            $result = substr($date, 6, 4) . "-" . substr($date, 3, 2) . "-" . substr($date, 0, 2);

            return $result;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function dateRevMysql($date) {
        try {

            $result = substr($date, 8, 2) . "/" . substr($date, 5, 2) . "/" . substr($date, 0, 4);

            return $result;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function dateIndonesia($date) {
        try {

            $result = substr($date, 8, 2) . " " . Globals::bulan(substr($date, 5, 2)) . " " . substr($date, 0, 4);

            return $result;
        } catch (Exception $e) {
            return null;
        }
    }

    public static function bulan($i) {
        $i = intval($i);
        $data = array(
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        return $data[$i - 1];
    }

    public static function AdminLogging($action) {
        $model = new AdminLog();
        $model->action = $action;
        $model->user_id = Yii::app()->user->id;
        $model->time = new CDbExpression('NOW()');
        $model->ip = $_SERVER['REMOTE_ADDR'];
        $model->save();
    }

}

?>