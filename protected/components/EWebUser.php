<?php

class EWebUser extends CWebUser {

    protected $_model;

    function isSuperadmin() {
        $modeluser = $this->loadUser();
        if (is_null($modeluser)) {
			return false;
		}
        $model = Akses::model()->findByAttributes(array('aksesid' => $modeluser->aksesid));
		if (!is_null($model)) {
			if ($model->superadmin == '1') {
				return true;
			}
			else {
				return false;
			}
		}else{
			return false;
		}
		
		return;
        
        $user = $this->loadUser();
        if ($user)
            return $user->aksesid == 1;
        return false;
    }
    
	function getInstansi($type) {
        $hasil = "";
        $sql = "select " . $type . " as hasil from tbminstansi";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($rows as $hsl) {
            $hasil = $hsl['hasil'];
        }
        return $hasil;
    }
	
	function getsetting($field){
		$model = Instansi::model()->findByAttributes(array('dlt' => "0"));
		if (!is_null($model)) {
			return $model->$field;
		}else{
			return null;
		}
	}
	
    function getuser($param) {
        $user = $this->loadUser();
		if (is_null($user)) return null;
        if ($param == 'roleid') {
            return $user->aksesid;
        } else if ($param == 'id') {
            return $user->userid;
        } else if ($param == 'username') {
            return $user->username;
        } else if ($param == 'nama') {
            return $user->nama;
        } else if ($param == 'status') {
            return $user->status;
        } else if ($param == 'photo') {
            return $user->photo;
        } else if ($param == 'picture') {
            return Yii::app()->theme->baseUrl.'/../../foto/'.$user->picture;
        } else if ($param == 'filenik') {
            return Yii::app()->theme->baseUrl.'/../../foto/'.$user->filenik;
        } else if ($param == 'filenpwp') {
            return Yii::app()->theme->baseUrl.'/../../foto/'.$user->filenpwp;
        } else if ($param == "role") {
            return Role::model()->findByPk($user->aksesid)->role;
        } else if ($param == 'pegawaiid') {
            return $user->pegawaiid;
        }
    }
	
    function getinfouser($param) {
        $pegawaiid = $this->getuser('pegawaiid');
		$pegawai = Pegawai::model()->findByPk($pegawaiid);
		
		if (is_null($pegawai)) return null;
		if ($param == 'pegawaiid') {
            return $pegawai->pegawaiid;
        } else if ($param == 'nama') {
            return $pegawai->nama;
        } else if ($param == 'nip') {
            return $pegawai->kode;
        } else if ($param == 'jabatanid') {
            return $pegawai->jabatanid;
        } else if ($param == 'unitid') {
            return $pegawai->unitid;
        } else if ($param == 'subunitid') {
            return $pegawai->subunitid;
        } else if ($param == 'bagianid') {
            return $pegawai->bagianid;
        } else if ($param == 'subbagianid') {
            return $pegawai->subbagianid;
        }
    }

    function menuislocked($menuid) {
		$model = Lockmenu::model()->findByAttributes(array('menuid' => $menuid, "thang" => Yii::app()->user->getTahun(), "dlt" => "0"));
		if (is_null($model)) return false;
		if ($model->islock=="1") return true;
		else return false;
	}
	
	function getprivileges($param, $menu) {
		$aksesid = '';
		$user = User::model()->findByAttributes(array('userid' => Yii::app()->user->id));
		if (!isset($user->aksesid)) {
			//$this->redirect(array('site/login'));
			header("Location: index.php?r=site/login");
			die();
			return;
		}
		$aksesid = $user->aksesid;
		$aksesmenu = Aksesmenu::model()->findByAttributes(array('aksesid' => $aksesid, 'menuid' => $menu));
		if (is_null($aksesmenu)) return false;
		//return $aksesmenu->view."xx".$aksesmenu->usermenuid;
        if ($param == 'view') {
            if ($aksesmenu->accessview == '1') {
                return true;
            }
        } else if ($param == 'create') {
            if ($aksesmenu->accessadd == '1') {
                return true;
            }
        } else if ($param == 'edit') {
            if ($aksesmenu->accessedit == '1') {
                return true;
            }
        } else if ($param == 'del') {
            if ($aksesmenu->accessdel == '1') {
                return true;
            }
        } else if ($param == 'print') {
            if ($aksesmenu->accessprint == '1') {
                return true;
            }
        }
        return false;
    }

    function setTahun($tahun) {
        Yii::app()->user->setState('gvartahun', $tahun);
        Yii::app()->user->setState('gvarbulan', date("n"));
    }

    function getTahun() {
        return Yii::app()->user->getState('gvartahun');
    }

    
	function setIP($ip){
		Yii::app()->user->setState('gvarip',$ip);
	}
	
	function getIP(){
		return Yii::app()->user->getState('gvarip');
	}

    protected function beforeLogout() {
        parent::beforeLogout();
        // Globals::AdminLogging("login:site:" . Yii::app()->user->id . "");
        //   $sql = "DELETE FROM tuseronline WHERE userid='" . Yii::app()->user->id . "'";
        //  Yii::app()->db->createCommand($sql)->execute();
        return true;
    }

    // Load user model.
    protected function loadUser() {
        if ($this->_model === null) {
            $this->_model = User::model()->findByPk(Yii::app()->user->id);
        }
        return $this->_model;
    }

    protected function loadUserAkses() {
        if ($this->_model === null) {
            $this->_model = User::model()->findByPk(Yii::app()->user->id);
        }
        $modelAkses = Akses::model()->findByPk($this->_model->aksesid);
        
        return $modelAkses;
    }

}
