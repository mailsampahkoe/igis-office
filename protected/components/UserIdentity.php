<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	const ERROR_USERNOTACTIVE=101;
	const ERROR_USERREJECTED=102;
	private $_id;
       
        public function authenticate()
	{
            $username=strtolower($this->username);
            $user=User::model()->find('LOWER(username)=? and dlt=0',array($username));
            if($user===null){
                $this->errorCode=self::ERROR_USERNAME_INVALID;}
            elseif(!$user->validatePassword($this->username,($this->password))){
                $this->errorCode=self::ERROR_PASSWORD_INVALID;}
            else
            {
                $this->_id=$user->userid;
                $this->username=$user->username;
                $this->errorCode=self::ERROR_NONE;
            }
            return $this->errorCode==self::ERROR_NONE;
	}
       
    public function auth()
	{
        $username=strtolower($this->username);
        $user=User::model()->find('LOWER(username)=? and dlt=0',array($username));
        if($user===null){
            $this->errorCode=self::ERROR_USERNAME_INVALID;}
        elseif(!$user->validatePassword($this->username,($this->password))){
            $this->errorCode=self::ERROR_PASSWORD_INVALID;}
        else
        {
        	if ($user->status == '1') {
	            $this->_id=$user->userid;
	            $this->username=$user->username;
	            $this->errorCode=self::ERROR_NONE;
			}
			elseif ($user->status == '0') {
				$this->errorCode=self::ERROR_USERNOTACTIVE;
			}
			else {
				$this->errorCode=self::ERROR_USERREJECTED;
			}
        }
        return $this->errorCode==self::ERROR_NONE;
	}

	public function getId()
	{
            return $this->_id;
	}
}