<?php
/* @var $this PegawaiController */
/* @var $model Pegawai */

$this->breadcrumbs = array(
    'PEGAWAI' => array('admin'),
    'TAMBAH PEGAWAI',
);
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">TAMBAH PEGAWAI</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('pegawai/admin'); ?>">DATA PEGAWAI</a></li>
                <li class="breadcrumb-item active">TAMBAH PEGAWAI</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'strjab' => $strjab)); ?>