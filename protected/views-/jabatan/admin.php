<?php
/* @var $this JabatanController */
/* @var $model Jabatan */

Yii::import('ext.select2.Select2');
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data Jabatan</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Data Jabatan</li>
            </ol>
			<?php if (Yii::app()->user->getprivileges("create", "8") || Yii::app()->user->isSuperadmin()) { ?>
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('jabatan/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tambah Jabatan</button>
			<?php } ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">	
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
	<div class="form-group" id="" style="padding-right:15px;">
		<div class="row">
			<div class="col-lg-6">
			    <?php 
					echo Select2::activeDropDownList($model, 'bagianid', array('' => ''), array(
                    'prompt' => 'PILIH BAGIAN',
					'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px;'
                    ));
				?>
			</div>
        	<div class="col-lg-6">
			    <?php 
					echo Select2::activeDropDownList($model, 'subbagianid', array('' => ''), array(
                    'prompt' => 'PILIH SUB BAGIAN',
					'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px;'
                    ));
				?>
            </div>
        </div>
        <br>
		<div class="row">
			<div class="col-lg-12"> 
                <?php echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
        </div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'jabatan-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('jabatan/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => 'Unit Kerja',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Bagian',
                'name' => 'bagianship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['bagiankode']." ".$data['bagiannama'];
		        },
            ),
            array(
                'header' => 'Sub Bagian',
                'name' => 'subbagianship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['subbagiankode']." ".$data['subbagiannama'];
		        },
            ),
            array(
                'header' => 'Jabatan',
                'name' => 'jabatanship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['nama'];
		        },
            ),
            array(
                'template' => '{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/jabatan/update", array("jabatanid"=>$data["jabatanid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "8") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/jabatan/delete", array("jabatanid"=>$data["jabatanid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "8") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
    function loadBagian() {
        $.ajax({
            url: "<?php echo CController::createUrl('jabatan/loadbagian') ?>",
            type: 'POST',
            data: {isall:'true'},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val('');
                $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').html(data);
				if ("<?php echo $model->bagianid; ?>"!="")
                	$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val', "<?php echo $model->bagianid; ?>");
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	function tambahData() {
        js:document.location.href="<?php echo CController::createUrl('jabatan/create') ?>/bagianid/"+$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val();
    }
	$(document).ready(function () {
        loadBagian();
		$('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				$.fn.yiiGridView.update('jabatan-grid', {
					//data: $(this).serialize()
					data: {searchtext:$('#searchtext').val(),filterbagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val()},
				});
				return false;
			}
		});
		$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').change(function(e){
			$.fn.yiiGridView.update('jabatan-grid', {
				//data: $(this).serialize()
				data: {searchtext:$('#searchtext').val(),filterbagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val()},
			});
			return false;
		});
    });
</script>