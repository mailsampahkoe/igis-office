<?php
/* @var $this JabatanController */
/* @var $model Jabatan */

?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Tambah Jabatan</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('jabatan/admin'); ?>">Data Jabatan</a></li>
                <li class="breadcrumb-item active">Tambah Jabatan</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>