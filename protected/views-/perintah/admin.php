<?php
/* @var $this PerintahController */
/* @var $model Perintah */

Yii::import('ext.select2.Select2');
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data Perintah</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Data Perintah</li>
            </ol>
			<?php if (Yii::app()->user->getprivileges("create", "9") || Yii::app()->user->isSuperadmin()) { ?>
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('perintah/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tambah Perintah</button>
			<?php } ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
        
		
    </div>
<div class="row">	
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
	<div class="form-group" id="" style="padding-right:0px;">
        <div class="row">
			<div class="col-lg-12"> 
                <?php echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
        </div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'perintah-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('perintah/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => '',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Nomor Urut',
                'name' => 'nourutship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['nourut'];
		        },
            ),
            array(
                'header' => 'Nama Perintah Disposisi',
                'name' => 'namaship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['nama'];
		        },
            ),
            array(
                'template' => '{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/perintah/update", array("perintahid"=>$data["perintahid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "9") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/perintah/delete", array("perintahid"=>$data["perintahid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "9") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
    function tambahData() {
        js:document.location.href="<?php echo CController::createUrl('perintah/create') ?>";
    }
	$(document).ready(function () {
        $('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				$.fn.yiiGridView.update('perintah-grid', {
					data: {searchtext:$('#searchtext').val()},
				});
				return false;
			}
		});
    });
</script>