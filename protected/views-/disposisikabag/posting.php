<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<?php
/* @var $this DisposisiController */
/* @var $model Suratmasuk */

$this->breadcrumbs = array(
    'Disposisi Surat Masuk' => array('admin'),
    'Disposisi Surat Masuk',
);
?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css"/>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>

<link type="text/css" href="<?php echo $baseUrl; ?>/assets/js/jquery-datatables-checkboxes-1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $baseUrl; ?>/assets/js/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/rg-1.0.2/sl-1.2.4/datatables.min.css"/>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/rg-1.0.2/sl-1.2.4/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"/>
<script type="text/javascript" language="javascript" src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Disposisi Kepala</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('disposisikabag/admin'); ?>">Disposisi Surat Masuk</a></li>
                <li class="breadcrumb-item active">Disposisi Kepala</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
			<div class="card">
			<?php
	        $form = $this->beginWidget('CActiveForm', array(
	            'id' => 'suratmasuk-form',
	            'method' => 'POST',
	            'enableAjaxValidation' => true,
	            'clientOptions' => array(
	                'validateOnSubmit' => true,
	                'validateOnChange' => true
	            ),
	            'htmlOptions' => array(
	                'enctype' => 'multipart/form-data',
	                'onsubmit'=>'return validate()',
	            )
	        ));
	        ?>
	        <?php echo $form->errorSummary($model); ?>
	        <?php echo $form->hiddenField($model, 'status'); ?>
    			<div class="card-body">
		            <div class="form-body">
		                <h3 class="box-title">Agenda Surat Masuk</h3>
		                <hr class="m-t-0 m-b-40">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Indek:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->indexnomor; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Klasifikasi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->klasifikasi; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Kode:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->kode; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl. Penyerahan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglpenyerahan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl dan Nomor Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglsurat; ?> <?php echo $model->nosurat; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Asal Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->asal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Hal/Isi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->perihal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <hr class="m-t-0 m-b-40">
		                <!--/row-->
				        <div class="row">
				            <div class="col-md-12">
				                <h3 class="box-title">File yang sudah diupload</h3>
				                <hr class="m-t-0">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div id="d_files"></div>
				                    </div>
				                </div>
				            </div>
				            <!--/span-->
			            </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">Penerima Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <!-- <div class="col-md-12">
		                                <!-- <?php echo CHtml::hiddenField('jabatandata' , '', array('id' => 'jabatandata')); ?><?php echo CHtml::hiddenField('jabatandelete' , '', array('id' => 'jabatandelete')); ?>
										<h5 class="m-t-20">Diteruskan Kepada Yth.</h5>
										<select class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" id="slc_jabatandata" name="slc_jabatandata">
											<!--<optgroup label="Group">
												<option value="AK">Item</option>
											</optgroup>--c>
										</select>
		                            </div>-->
		                            <?php echo CHtml::hiddenField('jabatandata' , '', array('id' => 'jabatandata')); ?>
									<?php echo CHtml::hiddenField('jabatandelete' , '', array('id' => 'jabatandelete')); ?>
		                            <div class="col-md-12">
			                            <?php echo CHtml::button('Tambah Penerima Disposisi', array('class' => 'btn btn-success', 'onclick' => 'displayFormJabatan(\'add\',\'\');')); ?>
		                            </div>
		                            <div class="col-md-12">
		                                <div id="d_jabatan"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
				                <h3 class="box-title">Perintah Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group row">
		                            <?php echo CHtml::hiddenField('perintahdata' , '', array('id' => 'perintahdata')); ?>
									<?php echo CHtml::hiddenField('perintahdelete' , '', array('id' => 'perintahdelete')); ?>
		                            <div class="col-md-12">
			                            <?php echo CHtml::button('Tambah Perintah Disposisi', array('class' => 'btn btn-success', 'onclick' => 'displayFormPerintah(\'add\',\'\');')); ?>
		                            </div>
		                            <div class="col-md-12">
		                                <div id="d_perintah"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		            </div>
		            <div class="form-actions">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="row">
		                            <div class="col-md-offset-3 col-md-9">
							            <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-primary')); ?>
		                                <button type="button" class="btn btn-inverse">Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6"> </div>
		                </div>
		            </div>
		    	</div>
	        <?php $this->endWidget(); ?>
		    
		    </div>
		</div>
    </div>
</div>

<div class="modal fade" id="m_modifDataJabatan" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Pilih Jabatan Disposisi</h4>
		</div>
		<div style="padding-left:20px;padding-right:20px;">
			<div class="table-responsive">
				<table id="tabelsuratmasukjabatan" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="10%"></th>
                            <th width="20%">NAMA</th>
                            <th width="70%">KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
					</tbody>
                </table>
            </div>
	    </div>
	    <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" onclick="modifyDataJabatan();">SIMPAN</button>
       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
	    </div>
	  </div>
	</div>
</div>

<div class="modal fade" id="m_modifDataPerintah" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Pilih Perintah Disposisi</h4>
		</div>
		<div style="padding-left:20px;padding-right:20px;">
			<div class="table-responsive">
				<table id="tabelsuratmasukperintah" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="10%"></th>
                            <th width="20%">NAMA</th>
                            <th width="70%">KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
					</tbody>
                </table>
            </div>
	    </div>
	    <div class="modal-footer">
            <div class="col-md-6">
	            <div class="pull-left">
					<button type="button" class="btn btn-warning" onclick="displayFormPerintahIsi('add','');">TAMBAH PERINTAH LAIN</button>
			    </div>
		    </div>
            <div class="col-md-6">
	            <div class="pull-right">
			      	<button type="button" class="btn btn-primary" onclick="modifyDataPerintah();">SIMPAN</button>&nbsp;
		       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
			    </div>
		    </div>
	    </div>
	  </div>
	</div>
</div>

<div class="modal fade" id="m_modifDataPerintahIsi" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Pilih Perintah Penerima Disposisi</h4>
		</div>
		
		<div class="modal-body">
			<div id="d_descmsg"></div>
			
			<form id="f_perintah" class="form-horizontal">
				<?php echo CHtml::hiddenField('hid_det_mode' , '', array('id' => 'hid_det_mode')); ?>
				<?php echo CHtml::hiddenField('hid_det_id' , '', array('id' => 'hid_det_id')); ?>
				<?php echo CHtml::hiddenField('hid_det_model' , '', array('id' => 'hid_det_model')); ?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label for="txt_disposisi" class="col-lg-3 control-label">Disposisi</label> 
							<div class="col-lg-9">
							<input type="text" name="txt_disposisi" class="form-control" id="txt_disposisi" placeholder="Perintah Disposisi" maxlength="250">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	    <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" onclick="modifyDataPerintahIsi();">SIMPAN</button>
       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
	    </div>
	  </div>
	</div>
</div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    var v_detailTmpId = 0;
    var v_listDataPerintah = Array();
    var v_listDataJabatan = Array();
    var v_listDetailDeleted = Array();
	var v_listDataFiles = Array();
    var v_id = '';
    
	$(document).ready(function () {
        $(".select2").select2();
        
        loadJabatan();
		loadDetailJabatan();
        loadPerintah();
		loadDetailPerintah();
		loadDetailFiles();
    });
	
	$('#slc_jabatandata').on('change', function () {
		$('#jabatandata').val($(this).val());
	});
	
	function validate() {
		if (v_listDataJabatan.length <= 0) {
			alert('Pejabat penerima disposisi harus dipilih.');
			return false;
		}
		if (v_listDataPerintah.length <= 0) {
			alert('Perintah disposisi harus dipilih.');
			return false;
		}
		
		var v_temp = '';
		$.each( v_listDataJabatan, function( p_key, p_value ) {
			if (v_temp != '') {
				v_temp += '~#;#~';
			}
			v_temp += p_value.suratmasukjabatanid + '~#|#~';
			v_temp += p_value.suratmasukid + '~#|#~';
			v_temp += p_value.jabatanid + '~#|#~';
			v_temp += p_value.jabatan + '~#|#~';
		});
		$("#jabatandata").val(v_temp);
		
		v_temp = '';
		var v_filter;
		v_filter = v_listDetailDeleted.filter(function (p_element) {
		  	return p_element.model == 'suratmasukjabatan';
		});
		if (v_filter.length > 0) {
			$.each( v_filter, function( p_key, p_value ) {
				if (v_temp != '') {
					v_temp += '~#;#~';
				}
				v_temp += p_value.id + '~#|#~';
			});
		}
		$("#jabatandelete").val(v_temp);
		
		v_temp = '';
		$.each( v_listDataPerintah, function( p_key, p_value ) {
			if (v_temp != '') {
				v_temp += '~#;#~';
			}
			v_temp += p_value.suratmasukperintahid + '~#|#~';
			v_temp += p_value.suratmasukid + '~#|#~';
			v_temp += p_value.perintahid + '~#|#~';
			v_temp += p_value.perintah + '~#|#~';
		});
		$("#perintahdata").val(v_temp);
		
		v_temp = '';
		var v_filter;
		v_filter = v_listDetailDeleted.filter(function (p_element) {
		  	return p_element.model == 'suratmasukperintah';
		});
		if (v_filter.length > 0) {
			$.each( v_filter, function( p_key, p_value ) {
				if (v_temp != '') {
					v_temp += '~#;#~';
				}
				v_temp += p_value.id + '~#|#~';
			});
		}
		$("#perintahdelete").val(v_temp);
	}
    
	function loadJabatan(){
		 $("#tabelsuratmasukjabatan").dataTable({
	        "bPaginate": true,
			"ajax" : {
				url: "<?php echo CController::createUrl('disposisikabag/loadjabatan', array(''=>'')) ?>",
	            type: 'POST',
	            data: {
					
				}/*,
				"dataSrc": function (json) {
		           return $.parseJSON(json);
		        }	*/
			},
			//data: JSON.parse(data),
	        columns: [
	            
	            { title: "ID", data:"jabatanid" },
	            { title: "NAMA", data:"nama" },
	            { title: "KETERANGAN", data:"keterangan" }
	        ],
			'columnDefs': [
			 {
			    'targets': 0,
			    'checkboxes': {
			       'selectRow': true
			    }
			 }
			],
			'select': {
			 'style': 'multi'
			},
			"bDestroy": true,
			'order': [[1, 'asc']]
	    });
	}
    
	function loadPerintah(){
		 $("#tabelsuratmasukperintah").dataTable({
	        "bPaginate": true,
			"ajax" : {
				url: "<?php echo CController::createUrl('disposisikabag/loadperintah', array(''=>'')) ?>",
	            type: 'POST',
	            data: {
					
				}/*,
				"dataSrc": function (json) {
		           return $.parseJSON(json);
		        }	*/
			},
			//data: JSON.parse(data),
	        columns: [
	            
	            { title: "ID", data:"perintahid" },
	            { title: "NAMA", data:"nama" },
	            { title: "KETERANGAN", data:"keterangan" }
	        ],
			'columnDefs': [
			 {
			    'targets': 0,
			    'checkboxes': {
			       'selectRow': true
			    }
			 }
			],
			'select': {
			 'style': 'multi'
			},
			"bDestroy": true,
			'order': [[1, 'asc']]
	    });
	}

	function displayFormPerintah(p_mode, p_id) {
		loadPerintah();
		$("#formLabel").text('Pilih Perintah Disposisi');
		
		$('#m_modifDataPerintah').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifDataPerintah').modal('show');
	}

	function displayFormPerintahIsi(p_mode, p_id) {
		$('#m_modifDataPerintah').modal('hide');
		
		$("#hid_det_mode").val(p_mode);
		$("#hid_det_id").val(p_id);
		
		$("#formLabel").text('Pilih Perintah Disposisi');
		$("#txt_disposisi").val('');
		if (p_mode.toLowerCase() == 'edit') {
			var v_filter = v_listDataPerintah.filter(function (p_element) {
			  	return p_element.suratmasukperintahid === p_id;
			});
    		if (v_filter != null && v_filter != '') {
    			$.each( v_filter, function( p_key, p_value ) {
    				$("#txt_disposisi").val(p_value.perintah);
    			});
    		}
		}
		
		$('#m_modifDataPerintahIsi').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifDataPerintahIsi').modal('show');
	}
    
    function loadDetailPerintah() {
        v_listDataPerintah = [];
        $.ajax({
            url: "<?php echo CController::createUrl('disposisikabag/loadsuratmasukperintah') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukperintahid": p_value.suratmasukperintahid, 
										"suratmasukid": p_value.suratmasukid, 
										"perintahid": p_value.perintahid,
										"perintah": p_value.perintah	};
					v_listDataPerintah.push(v_newData);
			    });
            	
            	loadDataPerintah('d_perintah');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataPerintah(p_divName) {
		var v_filter = v_listDataPerintah;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukperintah" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 75%;">Perintah</th><th class="header" style="width:20%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukperintah">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.perintah+'</td>';
		    	v_rows = v_rows + '<td>';
		    	if (p_value.perintahid == null || p_value.perintahid == '') {
					v_rows = v_rows + '<input type="button" value="Edit" id="btn_ubah" class="btn btn-sm btn-warning" onclick="displayFormPerintahIsi(\'edit\',\''+p_value.suratmasukperintahid+'\');">';
				}
				v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataPerintah(\''+p_value.suratmasukperintahid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function modifyDataPerintah() {
		var v_tabelsuratmasukperintah = $('#tabelsuratmasukperintah').DataTable();
		var v_tblData = v_tabelsuratmasukperintah.rows('.selected').data();
		$.each(v_tblData, function( p_key, p_value ) {
			var v_filter;
			v_filter = v_listDataPerintah.filter(function (p_element) {
			  	return p_element.perintahid == p_value.perintahid;
			});
			if (v_filter.length > 0) {
				alert('Perintah ('+p_value.nama+') sudah pernah dipilih...');
				return;
			}
			else {
				v_detailTmpId ++;
				var v_newData = {	"suratmasukperintahid": "tmp__"+v_detailTmpId, 
									"suratmasukid": v_id,
									"perintahid": p_value.perintahid,
									"perintah": p_value.nama	};
				v_listDataPerintah.push(v_newData);
			}
		});
		
		$('#m_modifDataPerintah').modal('hide');
		
		loadDataPerintah('d_perintah');
	}
    
    function modifyDataPerintahIsi() {
		if ($("#txt_disposisi").val() == '') {
			alert('Disposisi harus diisi...');
			return;
		}
		if ($('#hid_det_mode').val().toLowerCase() == 'add') {
			v_detailTmpId ++;
			var v_newData = {	"suratmasukperintahid": "tmp__"+v_detailTmpId, 
								"suratmasukid": v_id,
								"perintahid": '',
								"perintah": $('#txt_disposisi').val()	};
			v_listDataPerintah.push(v_newData);
		}
		else {
	    	$.each( v_listDataPerintah, function( p_key, p_value ) {
	    		if (p_value.suratmasukperintahid ==  $('#hid_det_id').val()) {
					p_value.perintah = $('#txt_disposisi').val();
					
					return false;
				}	
	    	});
		}
		
		$('#m_modifDataPerintahIsi').modal('hide');
		
		loadDataPerintah('d_perintah');
	}

	function deleteDataPerintah(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratmasukperintah"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataPerintah.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratmasukperintahid'] === p_id) {
			      v_listDataPerintah.splice(p_index, 1);
			    }    
			});
			
			loadDataPerintah('d_perintah');
		}
	}

	function displayFormJabatan(p_mode, p_id) {
		loadJabatan();
		$("#formLabel").text('Pilih Penerima Disposisi');
		
		$('#m_modifDataJabatan').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifDataJabatan').modal('show');
	}
    
    function loadDetailJabatan() {
        v_listDataJabatan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('disposisikabag/loadsuratmasukjabatan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukjabatanid": p_value.suratmasukjabatanid, 
										"suratmasukid": p_value.suratmasukid, 
										"jabatanid": p_value.jabatanid,
										"jabatan": p_value.jabatan	};
					v_listDataJabatan.push(v_newData);
			    });
            	
            	loadDataJabatan('d_jabatan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataJabatan(p_divName) {
		var v_filter = v_listDataJabatan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukjabatan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 85%;">Jabatan</th><th class="header" style="width:10%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukjabatan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.jabatan+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataJabatan(\''+p_value.suratmasukjabatanid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function modifyDataJabatan() {
		var v_tabelsuratmasukjabatan = $('#tabelsuratmasukjabatan').DataTable();
		var v_tblData = v_tabelsuratmasukjabatan.rows('.selected').data();
		$.each(v_tblData, function( p_key, p_value ) {
			var v_filter;
			v_filter = v_listDataJabatan.filter(function (p_element) {
			  	return p_element.jabatanid == p_value.jabatanid;
			});
			if (v_filter.length > 0) {
				alert('Jabatan ('+p_value.nama+') sudah pernah dipilih...');
				return;
			}
			else {
				v_detailTmpId ++;
				var v_newData = {	"suratmasukjabatanid": "tmp__"+v_detailTmpId, 
									"suratmasukid": v_id,
									"jabatanid": p_value.jabatanid,
									"jabatan": p_value.nama	};
				v_listDataJabatan.push(v_newData);
			}
		});
		
		$('#m_modifDataJabatan').modal('hide');
		
		loadDataJabatan('d_jabatan');
	}

	function deleteDataJabatan(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratmasukjabatan"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataJabatan.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratmasukjabatanid'] === p_id) {
			      v_listDataJabatan.splice(p_index, 1);
			    }    
			});
			
			loadDataJabatan('d_jabatan');
		}
	}
    
    function loadDetailFiles() {
        v_listDataFiles = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadsuratmasukfiles') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukfilesid": p_value.suratmasukfilesid, 
										"suratmasukid": p_value.suratmasukid, 
										"filename": p_value.filename,
										"files": p_value.files	};
					v_listDataFiles.push(v_newData);
			    });
            	
            	loadDataFiles('d_files');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataFiles(p_divName) {
		var v_filter = v_listDataFiles;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
        v_rows = v_rows + '        <div class="card-columns el-element-overlay">';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/big/img5.jpg"> <img src="../assets/images/big/img5.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/users/1.jpg"> <img src="../assets/images/users/1.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '        </div>';
		
		v_rows = '';
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukfiles" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 75%;">File</th><th class="header" style="width:20%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukfiles">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.filename+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Preview" id="btn_delete" class="btn btn-sm btn-primary" onclick="window.open(\'<?php echo Yii::app()->baseUrl; ?>/files/'+p_value.files+'\', \'_blank\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
</script>
