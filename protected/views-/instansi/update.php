<?php
/* @var $this InstansiController */
/* @var $model Instansi */
Yii::import('ext.select2.Select2');

$this->breadcrumbs = array(
    'Instansi' => array('admin'),
    'Setting Instansi',
);
?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Setting Instansi</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('instansi/view'); ?>">Data Instansi</a></li>
                <li class="breadcrumb-item active">Setting Instansi</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body1">
    <div class="row">
		<div class="col-lg-12 col-md-12">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'jabatan-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
           	<div class="card">
				<div class="card-body">
			        <div class="row form-group">
			            <div class="col-lg-6">
			                <?php echo $form->labelEx($model, 'nama instansi'); ?>
			                <?php echo $form->textField($model, 'namainstansi', array('size' => 50, 'maxlength' => 150, 'class' => 'form-control', 'placeholder' => 'Nama Instansi')); ?>
			                <?php echo $form->error($model, 'namainstansi'); ?>
			            </div>
			            <div class="col-lg-6">
			                <?php echo $form->labelEx($model, 'nama kota'); ?>
			                <?php echo $form->textField($model, 'kota', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'Nama Kota')); ?>
			                <?php echo $form->error($model, 'kota'); ?>
			            </div>
			        </div>
					<div class="row form-group">
			            <div class="col-lg-12">
			                <?php echo $form->labelEx($model, 'alamat'); ?>
			                <?php echo $form->textArea($model, 'alamat', array('size' => 60, 'maxlength' => 150, 'class' => 'form-control', 'placeholder' => 'Alamat')); ?>
			                <?php echo $form->error($model, 'alamat'); ?>
			            </div>
			        </div>
			        <div class="row form-group">
			            <div class="col-lg-6">
			                <?php echo $form->labelEx($model, 'kode pos'); ?>
			                <?php echo $form->textField($model, 'kodepos', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'Kode Pos')); ?>
			                <?php echo $form->error($model, 'kodepos'); ?>
			            </div>
			            <div class="col-lg-6">
			                <?php echo $form->labelEx($model, 'email'); ?>
			                <?php echo $form->textField($model, 'email', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'E-mail')); ?>
			                <?php echo $form->error($model, 'email'); ?>
			            </div>
			        </div>
			        <div class="row form-group">
			            <div class="col-lg-6">
			                <?php echo $form->labelEx($model, 'telp'); ?>
			                <?php echo $form->textField($model, 'telp', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'Telp')); ?>
			                <?php echo $form->error($model, 'telp'); ?>
			            </div>
			            <div class="col-lg-6">
			                <?php echo $form->labelEx($model, 'fax'); ?>
			                <?php echo $form->textField($model, 'fax', array('size' => 50, 'maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'Fax')); ?>
			                <?php echo $form->error($model, 'fax'); ?>
			            </div>
			        </div>
			        <div class="row form-group">
			            <div class="col-lg-6">
			                <?php echo $form->labelEx($model, 'logo'); ?>
			                <?php echo $form->fileField($model, 'logo', array('size' => 50, 'maxlength' => 1000, 'class' => 'form-control', 'placeholder' => 'Logo')); ?>
			                <?php echo $form->error($model, 'logo'); ?>
			            </div>
		            </div>
					<div class="col-lg-12">
			            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary pull-right')); ?>
			        </div>
		    	</div><!-- form -->
			</div>
        <?php $this->endWidget(); ?>
		</div>
	</div>
</div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>