<?php
/* @var $this DisposisiController */
/* @var $model Suratmasuk */

Yii::import('ext.select2.Select2');
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Disposisi</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Disposisi</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">	
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
	<div class="form-group" id="" style="padding-right:15px;">
		<div class="row">
			<div class="col-lg-6">
			    <?php 
					echo Select2::activeDropDownList($model, 'bagianid', array('' => ''), array(
                    'prompt' => 'PILIH BAGIAN',
					'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px;'
                    ));
				?>
			</div>
        	<div class="col-lg-6">
			    <?php 
					echo Select2::activeDropDownList($model, 'subbagianid', array('' => ''), array(
                    'prompt' => 'PILIH SUB BAGIAN',
					'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px;'
                    ));
				?>
            </div>
        </div>
        <br>
		<div class="row">
			<div class="col-lg-12"> 
                <?php echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
        </div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'suratmasuk-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('disposisikabag/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => 'Unit Kerja',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Bagian',
                'name' => 'bagianship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['bagiankode']." ".$data['bagiannama'];
		        },
            ),
            array(
                'header' => 'Sub Bagian',
                'name' => 'subbagianship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['subbagiankode']." ".$data['subbagiannama'];
		        },
            ),
            array(
                'header' => 'Nomor Surat Masuk',
                'name' => 'suratmasukship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['indexnomor'];
		        },
            ),
            array(
                'template' => '{disposisi}{print}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'disposisi' => array(
                        'url' => 'Yii::app()->createUrl("/disposisikabag/disposisi", array("suratmasukid"=>$data["suratmasukid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "5") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'print' => array(
                        'url' => 'Yii::app()->createUrl("/disposisikabag/print", array("suratmasukid"=>$data["suratmasukid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "5") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
    function loadBagian() {
        $.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadbagian') ?>",
            type: 'POST',
            data: {isall:'true'},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val('');
                $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').html(data);
				if ("<?php echo $model->bagianid; ?>"!="")
                	$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val', "<?php echo $model->bagianid; ?>");
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	function tambahData() {
        js:document.location.href="<?php echo CController::createUrl('suratmasuk/create') ?>/bagianid/"+$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val();
    }
	$(document).ready(function () {
        loadBagian();
		$('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				$.fn.yiiGridView.update('suratmasuk-grid', {
					//data: $(this).serialize()
					data: {searchtext:$('#searchtext').val(),filterbagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val()},
				});
				return false;
			}
		});
		$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').change(function(e){
			$.fn.yiiGridView.update('suratmasuk-grid', {
				//data: $(this).serialize()
				data: {searchtext:$('#searchtext').val(),filterbagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val()},
			});
			return false;
		});
    });
</script>