<?php echo $this->renderPartial('_refs', array('model' => $model)); ?>
<?php
/* @var $this PesanController */
/* @var $model Pesan */

?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Tulis Pesan</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('pesan/admin'); ?>">Data Pesan</a></li>
                <li class="breadcrumb-item active">Tulis Pesan</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix"></div>


                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="row">
                                <div class="col-xlg-2 col-lg-3 col-md-4 ">
									<?php echo $this->renderPartial('_menu', array('model' => $model)); ?>
                                </div>
                                <div class="col-xlg-10 col-lg-9 col-md-8 bg-light border-left">
                                    <div class="card-body">
                                        <h3 class="card-title">Compose New Message</h3>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="To:">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Subject:">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="textarea_editor form-control" rows="15" placeholder="Enter text ..."></textarea>
                                        </div>
                                        <h4><i class="ti-link"></i> Attachment</h4>
                                        <form action="#" class="dropzone">
                                            <div class="fallback">
                                                <input name="file" type="file" multiple />
                                            </div>
                                        </form>
                                        <button type="submit" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Send</button>
                                        <button class="btn btn-dark m-t-20"><i class="fa fa-times"></i> Discard</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                