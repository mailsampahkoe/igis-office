<?php
/* @var $this PesanController */
/* @var $model Pesan */


?>
<?php
/* @var $this CustomerController */
/* @var $model PersonalInfo */

Yii::app()->clientScript->registerScript('search', "
$('#searchtext').keyup(function(e){
	if(e.keyCode == 13)
    {
		$.fn.yiiGridView.update('pesan-grid', {
			data: $(this).serialize()
		});
		return false;
	}
});

");
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data Pesan</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Data Pesan</li>
            </ol>
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('pesan/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tulis Pesan</button>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
        <div class="form-group" id="">
            <div class="col-lg-6" style="margin-right:15px;"> 
                <?php echo CHtml::textField('searchtext',"",array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
			
			
        </div>
    </div>
<div class="row">
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'pesan-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('pesan/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => '',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Nama Pesan',
                'name' => 'unitship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
            return
                    $data['kode'] . " " . $data['nama']	;
        },
            ),
            array(
                'template' => '{view}{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'view' => array(
                        'url' => 'Yii::app()->createUrl("/urusan/admin", array("pesanid"=>$data["pesanid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("view", "3") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/pesan/update", array("pesanid"=>$data["pesanid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "2") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/pesan/delete", array("pesanid"=>$data["pesanid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "2") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>