<?php
/* @var $this DisposisiController */
/* @var $model Suratmasuk */

Yii::import('ext.select2.Select2');
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Disposisi Kasubbag</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Disposisi Kasubbag</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">	
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
	<div class="form-group" id="" style="padding-right:15px;">
		<div class="row form-group">
			<div class="col-lg-12">
			    <?php 
                    echo Select2::activeDropDownList($model, 'status', array('all'=>'Semua', enumVar::STEP_I_DRAFT=>enumVar::STEP_I_DESC_DRAFT, enumVar::STEP_I_POSTED=>enumVar::STEP_I_DESC_POSTED, enumVar::STEP_I_KABAG_VERIFIED=>enumVar::STEP_I_DESC_KABAG_VERIFIED, enumVar::STEP_I_KASUBBAG_VERIFIED=>enumVar::STEP_I_DESC_KASUBBAG_VERIFIED, enumVar::STEP_I_FINISHED=>enumVar::STEP_I_DESC_FINISHED), array(
                        'prompt' => 'PILIH STATUS',
                        'class' => 'col-lg-12', 'style' => 'padding:0px',
						'selected' => 'selected'
                    ));
				?>
			</div>
        </div>
		<div class="row form-group">
			<div class="col-lg-12"> 
                <?php echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
        </div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'suratmasuk-grid',
        'itemsCssClass' => 'table table-hover color-table info-table',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('disposisikasubbag/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => 'Unit Kerja',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Nomor Surat Masuk',
                'name' => 'suratmasukship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['indexkode1'].'.'.$data['indexkode2'].'/'.$data['indexbulan'].'/'.$data['indexnomor'].'/'.$data['indextahun'];
		        },
            ),
            array(
                'header' => 'Tanggal Surat',
                'name' => 'tglsurat_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $this->dateIndo($data['tglsurat']);
		        },
            ),
            array(
                'header' => 'Asal Surat',
                'name' => 'asal_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['asal'];
		        },
            ),
            array(
                'header' => 'Status',
                'name' => 'status_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['ketstatus'];
		        },
            ),
            array(
                'template' => '{update}{disposisi}{upload}{print}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/disposisikasubbag/update", array("suratmasukid"=>$data["suratmasukid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "103") && !Yii::app()->user->isSuperadmin() || $data->status!=enumVar::getStep(enumVar::STEP_I_KASUBBAG_VERIFIED, 0, 0))?0:1',
                    ),
                    'disposisi' => array(
                        'url' => 'Yii::app()->createUrl("/disposisikasubbag/posting", array("suratmasukid"=>$data["suratmasukid"]))',
                        'imageUrl'=>Yii::app()->baseUrl."/images/portrait_edit.png",
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "103") && !Yii::app()->user->isSuperadmin() || $data->status!=enumVar::getStep(enumVar::STEP_I_KASUBBAG_VERIFIED, 0, 0))?0:1',
                    ),
                    'upload' => array(
                        'url' => 'Yii::app()->createUrl("/disposisikasubbag/upload", array("suratmasukid"=>$data["suratmasukid"]))',
                        'imageUrl'=>Yii::app()->baseUrl."/images/upload.png",
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "103") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'print' => array(
                        'url' => 'Yii::app()->createUrl("/disposisikasubbag/print", array("suratmasukid"=>$data["suratmasukid"]))',
                        'imageUrl'=>Yii::app()->baseUrl."/images/pdf2.png",
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "103") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
	$(document).ready(function () {
		$('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				filterData();
			}
		});
		$('#<?php echo CHtml::activeId($model, 'status') ?>').change(function(e){
			filterData();
		});
    });
	
	function filterData() {
		$.fn.yiiGridView.update('suratmasuk-grid', {
			//data: $(this).serialize()
			data: {searchtext:$('#searchtext').val(),filterstatus: $('#<?php echo CHtml::activeId($model, 'status') ?>').val()},
		});
		return false;
	}
</script>