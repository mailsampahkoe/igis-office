<?php
/* @var $this NotifikasiController */
/* @var $model Notifikasi */

Yii::app()->clientScript->registerScript('search', "
$('#searchtext').keyup(function(e){
	if(e.keyCode == 13)
    {
		$.fn.yiiGridView.update('notifikasi-grid', {
			data: $(this).serialize()
		});
		return false;
	}
});

");
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data Notifikasi</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Data Notifikasi</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
        <div class="form-group" id="">
            <div class="col-lg-6" style="margin-right:15px;"> 
                <?php echo CHtml::textField('searchtext',"",array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
			
			
        </div>
    </div>
<div class="row">
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'notifikasi-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('notifikasi/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => '',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Nama Notifikasi',
                'name' => 'unitship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
            return
                    $data['kode'] . " " . $data['nama']	;
        },
            ),
            array(
                'template' => '{view}{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'view' => array(
                        'url' => 'Yii::app()->createUrl("/urusan/admin", array("notifikasiid"=>$data["notifikasiid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("view", "3") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/notifikasi/update", array("notifikasiid"=>$data["notifikasiid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "2") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/notifikasi/delete", array("notifikasiid"=>$data["notifikasiid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "2") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>