<?php
/* @var $this JenisController */
/* @var $model Jenis */

$this->breadcrumbs = array(
    'Jenis' => array('admin'),
    'Ubah Jenis',
);
?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ubah Jenis</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('jenis/admin'); ?>">Data Jenis</a></li>
                <li class="breadcrumb-item active">Ubah Jenis</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>