<?php
/* @var $this TahunController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tahuns',
);

$this->menu=array(
	array('label'=>'Create Tahun', 'url'=>array('create')),
	array('label'=>'Manage Tahun', 'url'=>array('admin')),
);
?>

<h1>Tahun</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
