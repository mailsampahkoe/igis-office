<?php
/* @var $this CustomerController */
/* @var $model PersonalInfo */
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<script src="<?php echo $baseUrl; ?>/assets/node_modules/datatables/jquery.dataTables.min.js"></script>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">DATA CAPAIAN KINERJA KEGIATAN</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Dashboard</a></li>
                <li class="breadcrumb-item active">DATA CAPAIAN KINERJA KEGIATAN</li>
            </ol>
		</div>
    </div>
</div>
<style>
	#tablePerencanaan {
	    font-size: 10px;
		background-color: white;
	}
	#tablePerencanaan td { 
    	padding: 5px;
	}
	#tablePerencanaan th { 
    	padding: 5px;
		text-align: center;
	}
</style>
<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
<div class="table-responsive">	
 <div class="card">
  	<form class="card-body" method="post" action="<?php echo Yii::app()->createUrl("/site/dashboard2"); ?>">
		<h4 class="card-title">Pengukuran dan Capaian Kinerja</h4>
		<div class="form-group" id="" style="padding-right:15px;">
			<div class="row">
			<div class="col-lg-6">
            <?php 
				echo Select2::dropDownList('selunitid', $selunitid, array(), array(
                    'prompt' => 'PILIH SKPD',
					"onchange" => "this.form.submit()",
					'class' => 'col-lg-12', 'selected' => 'selected'
                ));
			?>
			</div>
        	<div class="col-lg-6">
			<?php
				echo Select2::dropDownList('selsubunitid', $selsubunitid, array(), array(
                    'prompt' => 'PILIH UNIT KERJA',
					"onchange" => "this.form.submit()",
                    'class' => 'col-lg-12', 'selected' => 'selected'
                )); 
			?>
			</div>
		</div>	
		</div>
		<div class="row">
			<div class="col-lg-6"> 
	            <h4 class="m-b-0">TAHUN <?php 
					$sql="select tahun from tbmtahun where dlt='0' order by tahun";
					$rows = Yii::app()->db->createCommand($sql)->queryAll();
					$arrtahun = array();
					foreach($rows AS $row){
						$arrtahun += array("$row[tahun]"=>"$row[tahun]"); 
		            }
					echo CHtml::dropDownList('tahun',$tahun,$arrtahun,array('class' => 'form-control col-3', "onchange" => "this.form.submit()")); 
				?></h4>
	        </div>
		</div>
		<br>
        
			<table id="tablePerencanaan" border="1">
				<thead>
                    <tr bgcolor="#01c0c8">
                        <th rowspan="2" align="center">NO</th>
                        <th rowspan="2" width="500px">SASARAN STRATEGIS</th>
                        <th rowspan="2" width="200px">INDIKATOR KINERJA</th>
                        <th colspan="3" align="center">TARGET</th>
						<th colspan="2" align="center">REALISASI</th>
						<th rowspan="2" align="center">PROGRAM</th>
						<th rowspan="2" align="center">KEGIATAN</th>
						<th rowspan="2" align="center">INDIKATOR KINERJA KEGIATAN</th>
						<th colspan="5" align="center">CAPAIAN KINERJA KEGIATAN</th>
						<th colspan="3" align="center">FISIK</th>
						<th colspan="3" align="center">PENYERAPAN ANGGARAN</th>
                    </tr>
					<tr bgcolor="#01c0c8">
                       	<th align="center">WAKTU</th>
						<th align="center">JUMLAH</th>
						<th align="center">SATUAN</th>
						<th align="center">JUMLAH</th>
						<th align="center">(%)</th>
						<th align="center">TARGET</th>
						<th align="center">JUMLAH</th>
						<th align="center">SATUAN</th>
						<th align="center">REALISASI</th>
						<th align="center">(%)</th>
						<th align="center">TARGET %</th>
						<th align="center">REALISASI %</th>
						<th align="center">(%)</th>
						<th align="center">TARGET</th>
						<th align="center">REALISASI</th>
						<th align="center">(%)</th>
                    </tr>
                </thead>
				<tbody>
					<?php 
						$filterunit = "";
						if ($selsubunitid != '' && $selsubunitid != '-') {
				            $filterunit = " and a.subunitid = '".$selsubunitid."'  ";
				        }
				        if ($selunitid != '' && $selunitid != '-') {
				            $filterunit .= " and a.unitid = '".$selunitid."' ";
				        }
						foreach($dataProvider->getData() as $data) {
							$sql = "SELECT a.indikatorid, a.indikatorkode, a.indikatornama, 
							       a.targetkinerja1, a.satuan1, a.targetkinerja2, a.satuan2, a.targetkinerja3, 
							       a.satuan3, a.targetkinerja4, a.satuan4, coalesce(b.realkinerja1,0) as realkinerja1, coalesce(b.uraian1,'') as uraian1, 
								   coalesce(b.realkinerja2,0) as realkinerja2, coalesce(b.uraian2,'') as uraian2, coalesce(b.realkinerja3,0) as realkinerja3, coalesce(b.uraian3,'') as uraian3, 
								   coalesce(b.realkinerja4,0) as realkinerja4, coalesce(b.uraian4,'') as uraian4 FROM tbindikator a 
							  left outer join tbrealkinerja b on a.indikatorid = b.indikatorid
							  where a.sasaranid='$data[sasaranid]' and a.thang='$tahun' $filterunit order by a.indikatorkode";
							$rowind = Yii::app()->db->createCommand($sql)->queryAll();
							$sql = "SELECT a.indikatorkegid, a.indikatorkegkode, a.indikatorkegnama, c.kegkode, c.kegnama, d.progkode, d.prognama,
							       a.targetkinerja1, a.satuan1, a.targetkinerja2, a.satuan2, a.targetkinerja3, 
							       a.satuan3, a.targetkinerja4, a.satuan4, 
								   a.targetfisik1, a.targetfisik2, a.targetfisik3, a.targetfisik4, a.targetkeu1, a.targetkeu2, a.targetkeu3, a.targetkeu4,
								   coalesce(b.realkinerja1,0) as realkinerja1, coalesce(b.realfisik1,0) as realfisik1, 
								   coalesce(b.realkinerja2,0) as realkinerja2, coalesce(b.realfisik2,0) as realfisik2, coalesce(b.realkinerja3,0) as realkinerja3, coalesce(b.realfisik3,0) as realfisik3, 
								   coalesce(b.realkinerja4,0) as realkinerja4, coalesce(b.realfisik4,0) as realfisik4, b.realkeu1, b.realkeu2, b.realkeu3, b.realkeu4 FROM tbindikatorkeg a 
							  left outer join tbrealkinerjakeg b on a.indikatorkegid = b.indikatorkegid
							  left outer join tbmkeg c on a.kegid = c.kegid
							  left outer join tbmprog d on c.progid = d.progid
							  where a.sasaranid='$data[sasaranid]' and a.thang='$tahun' $filterunit order by progkode, kegkode, a.indikatorkegkode";
							$rowindkeg = Yii::app()->db->createCommand($sql)->queryAll();
							$totalindikator = count($rowind);
							$totalindikatorkeg = count($rowindkeg);
							$prevsasarankode = null;
							$prevprogkode = null;
							$prevkegkode = null;
							if ($totalindikator>0 or $totalindikatorkeg>0){
							for ($i=0; $i<($totalindikator>$totalindikatorkeg ? $totalindikator : $totalindikatorkeg); $i++){
								for ($notw = 1; $notw <= 4; $notw++){
									if ($notw==1 && (is_null($prevsasarankode) || $prevsasarankode!=$data["sasarankode"])){
										echo "<tr>";
									 	echo "<td>".$data["sasarankode"]."</td>";
										echo "<td width='200px'>".$data["sasarannama"]."</td>";
									}else{
										echo "<tr><td></td><td></td>";
									}
									$prevsasarankode = $data["sasarankode"];
									if ($i<$totalindikator){ 
										if ($notw==1){
											echo "<td>".$rowind[$i]["indikatornama"]."</td>";
										}else{
											echo "<td></td>";		
										}
										
										echo "<td>TW ".$notw."</td><td>".$rowind[$i]["targetkinerja".$notw]."</td><td>".$rowind[$i]["satuan".$notw]."</td>";
										$persen = 0;
										if (floatval($rowind[$i]["targetkinerja".$notw])>0) $persen = ($rowind[$i]["realkinerja".$notw]/$rowind[$i]["targetkinerja".$notw]) * 100;
										echo "<td>".$rowind[$i]["realkinerja".$notw]."</td><td bgcolor='".($persen<100 ? "#ff7575" : "#80ff00")."'>".number_format($persen)."</td>";
									}else{
										echo "<td></td><td></td><td></td><td></td><td></td><td></td>";		
									}
									if ($i<$totalindikatorkeg){ 
										if ($notw==1){
											if (is_null($prevprogkode) || $prevprogkode!=$data["sasarankode"].$rowindkeg[$i]["progkode"])
												echo "<td>".$rowindkeg[$i]["prognama"]."</td>";
											else echo "<td></td>";
											if (is_null($prevkegkode) || $prevkegkode!=$data["sasarankode"].$rowindkeg[$i]["progkode"].$rowindkeg[$i]["kegkode"])
												echo "<td>".$rowindkeg[$i]["kegnama"]."</td>";
											else echo "<td></td>";
											echo "<td>".$rowindkeg[$i]["indikatorkegnama"]."</td>";
										}else{
											echo "<td></td><td></td><td></td>";		
										}
										$prevprogkode = $data["sasarankode"].$rowindkeg[$i]["progkode"];
										$prevkegkode = $data["sasarankode"].$rowindkeg[$i]["progkode"].$rowindkeg[$i]["kegkode"];
										echo "<td>TW ".$notw."</td><td>".$rowindkeg[$i]["targetkinerja".$notw]."</td><td>".$rowindkeg[$i]["satuan".$notw]."</td>";
										$persen = 0;
										if (floatval($rowindkeg[$i]["targetkinerja".$notw])>0) $persen = ($rowindkeg[$i]["realkinerja".$notw]/$rowindkeg[$i]["targetkinerja".$notw]) * 100;
										echo "<td>".$rowindkeg[$i]["realkinerja".$notw]."</td><td bgcolor='".($persen<100 ? "#ff7575" : "#80ff00")."'>".$persen."</td>";
										$persen = 0;
										if (floatval($rowindkeg[$i]["targetfisik".$notw])>0) $persen = ($rowindkeg[$i]["realfisik".$notw]/$rowindkeg[$i]["targetfisik".$notw]) * 100;
										echo "<td>".$rowindkeg[$i]["targetfisik".$notw]."</td><td>".$rowindkeg[$i]["realfisik".$notw]."</td><td bgcolor='".($persen<100 ? "#ff7575" : "#80ff00")."'>".number_format($persen)."</td>";
										$persen = 0;
										if (floatval($rowindkeg[$i]["targetkeu".$notw])>0) $persen = ($rowindkeg[$i]["realkeu".$notw]/$rowindkeg[$i]["targetkeu".$notw]) * 100;
										echo "<td>".number_format($rowindkeg[$i]["targetkeu".$notw],2)."</td><td>".number_format($rowindkeg[$i]["realkeu".$notw],2)."</td><td bgcolor='".($persen<100 ? "#ff7575" : "#80ff00")."'>".number_format($persen)."</td>";
										echo "<tr>";
									}else{
										echo "<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";		
									}
									echo "</tr>";
								}
							}
							
							}
						}
					?>
				</tbody>
			</table>
	</form>
 </div>	
 </div>
</div>
<script>
	$(document).ready(function() {
		loadUnit();
		loadSubunit();
	});
	function loadUnit() {
		$.ajax({
            url: "<?php echo CController::createUrl('subunit/loadunit') ?>",
            type: 'POST',
            data: {isall:'true'},
            success: function (data) {
				$('#selunitid').val('');
                $('#selunitid').html(data);
				if ("<?php echo $selunitid; ?>"!="")
                	$('#selunitid').select2().select2('val', "<?php echo $selunitid; ?>");
				loadSubunit();	
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	
    function loadSubunit() {
        $.ajax({
            url: "<?php echo CController::createUrl('indikator/loadsubunit') ?>",
            type: 'POST',
            data: {isall:'true',unitid: $('#selunitid').val()},
            success: function (data) {
                $('#selsubunitid').val('');
                $('#selsubunitid').html(data);
				if ("<?php echo $selsubunitid; ?>"!="")
                	$('#selsubunitid').select2().select2('val', "<?php echo $selsubunitid; ?>");
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
</script>