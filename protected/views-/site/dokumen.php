<?php
/* @var $this CustomerController */
/* @var $model PersonalInfo */
$baseUrl = Yii::app()->theme->baseUrl;
Yii::app()->clientScript->registerScript('search', "
$('#searchtext').keyup(function(e){
	if(e.keyCode == 13)
    {
		$.fn.yiiGridView.update('uploaddok-grid', {
			data: $(this).serialize()
		});
		return false;
	}
});

");
?>
<script src="<?php echo $baseUrl; ?>/assets/node_modules/datatables/jquery.dataTables.min.js"></script>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">DATA PERATURAN></h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Dashboard</a></li>
                <li class="breadcrumb-item active">DOKUMEN PERATURAN DOKUMEN PERUNDANGAN</li>
            </ol>
		</div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">	
<div class="table-responsive">	
 <div class="card">
  	<form class="card-body" method="post" action="<?php echo Yii::app()->createUrl("/site/dashboard1"); ?>">
		<h4 class="card-title">DOKUMEN PERATURAN DOKUMEN PERUNDANGAN</h4>
			<table id="tableDokumen" class="table table-bordered table-striped">
				<thead>
                    <tr bgcolor="#01c0c8">
                        <th>Uraian</th>
                        <th width="20px">File</th>
                    </tr>
                </thead>
				<tbody>
					<?php 
						foreach($dataProvider->getData() as $data) {
							echo "<tr>";
						 	echo "<td>".$data["keterangan"]."</td>";
							echo "<td align='center'>";
								if ($data["dokpath"]!=""){
									echo '<a title="Download" href="'.Yii::app()->createUrl("/dokperaturan/download", array("dokperaturanid"=>$data["dokperaturanid"])).'"><img src="'.Yii::app()->request->baseUrl.'/assets/icons/download.png" alt="Download" /></a>';
								}
							echo "</td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
		
	</form>
 </div>	
 </div>
</div>
<script>
	$(document).ready(function() {
		$('#tableDokumen').DataTable();	
	});
</script>