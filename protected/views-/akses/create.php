<?php
/* @var $this AksesController */
/* @var $model Akses */

?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Tambah Akses</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('akses/admin'); ?>">Data Akses</a></li>
                <li class="breadcrumb-item active">Tambah Akses</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>