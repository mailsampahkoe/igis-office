<?php
$baseUrl = Yii::app()->theme->baseUrl;

$this->breadcrumbs = array(
    'parent'=>array('home','site'),
    'Dashboard',
);

$persensuratmasuk = 0;
if ($dashboardsuratmasuk[0]['semua'] > 0) $persensuratmasuk = (($dashboardsuratmasuk[0]['suratmasuk'] / $dashboardsuratmasuk[0]['semua']) * 100);
$persendisposisikabag = 0;
if ($dashboardsuratmasuk[0]['semua'] > 0) $persendisposisikabag = (($dashboardsuratmasuk[0]['disposisikabag'] / $dashboardsuratmasuk[0]['semua']) * 100);
$persendisposisikasubbag = 0;
if ($dashboardsuratmasuk[0]['semua'] > 0) $persendisposisikasubbag = (($dashboardsuratmasuk[0]['disposisikasubbag'] / $dashboardsuratmasuk[0]['semua']) * 100);
$persensuratmasukselesai = 0;
if ($dashboardsuratmasuk[0]['semua'] > 0) $persensuratmasukselesai = (($dashboardsuratmasuk[0]['selesai'] / $dashboardsuratmasuk[0]['semua']) * 100);
$persensuratkeluar = 0;
if ($dashboardsuratkeluar[0]['semua'] > 0) $persensuratkeluar = (($dashboardsuratkeluar[0]['suratkeluar'] / $dashboardsuratkeluar[0]['semua']) * 100);
$persenverifikasikabag = 0;
if ($dashboardsuratkeluar[0]['semua'] > 0) $persenverifikasikabag = (($dashboardsuratkeluar[0]['verifikasikabag'] / $dashboardsuratkeluar[0]['semua']) * 100);
$persenverifikasikasubbag = 0;
if ($dashboardsuratkeluar[0]['semua'] > 0) $persenverifikasikasubbag = (($dashboardsuratkeluar[0]['verifikasikasubbag'] / $dashboardsuratkeluar[0]['semua']) * 100);
$persensuratkeluarselesai = 0;
if ($dashboardsuratkeluar[0]['semua'] > 0) $persensuratkeluarselesai = (($dashboardsuratkeluar[0]['selesai'] / $dashboardsuratkeluar[0]['semua']) * 100);
?>

<?php
if (!enumVar::BCINMAIN) {
?>

<style>
	.card-group {
		margin-bottom:10px;
	}
</style>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Dashboard</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
	
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<?php
}
?>
<!-- ============================================================== -->
<!-- Info box -->
<!-- ============================================================== -->
<!-- End Info box -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Over Visitor, Our income , slaes different and  sales prediction -->
<!-- ============================================================== -->
    <!-- Column -->
				<div class="card-group">
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body bg-orange" style="padding:5px; padding-bottom:0px">
							<h4>Surat Masuk</h4>
                        </div>
                    </div>
                </div>
				<div class="card-group">
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-cyan"><i class="icon-note"></i>&nbsp;Diposting</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratmasuk[0]['suratmasuk'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-cyan" role="progressbar" style="width: <?php echo $persensuratmasuk; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-purple"><i class="icon-doc"></i>&nbsp;Disposisi Kepala</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratmasuk[0]['disposisikabag'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-purple" role="progressbar" style="width: <?php echo $persendisposisikabag; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-dark"><i class="icon-doc"></i>&nbsp;Disposisi Kasubbag</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratmasuk[0]['disposisikasubbag'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-dark" role="progressbar" style="width: <?php echo $persendisposisikasubbag; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-primary"><i class="icon-screen-desktop"></i>&nbsp;Selesai</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratmasuk[0]['selesai'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: <?php echo $persensuratmasukselesai; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-success"><i class="icon-bag"></i>&nbsp;Semua</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratmasuk[0]['semua'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
				<div class="card-group">
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body bg-cyan" style="padding:5px; padding-bottom:0px">
							<h4>Surat Keluar</h4>
                        </div>
                    </div>
                </div>
				<div class="card-group">
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-cyan"><i class="icon-note"></i>&nbsp;Diposting</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratkeluar[0]['suratkeluar'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-cyan" role="progressbar" style="width: <?php echo $persensuratkeluar; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-dark"><i class="icon-doc"></i>&nbsp;Verifikasi Kasubbag</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratkeluar[0]['verifikasikasubbag'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-dark" role="progressbar" style="width: <?php echo $persenverifikasikabag; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-purple"><i class="icon-doc"></i>&nbsp;Verifikasi Kepala</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratkeluar[0]['verifikasikabag'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-purple" role="progressbar" style="width: <?php echo $persenverifikasikasubbag; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-primary"><i class="icon-screen-desktop"></i>&nbsp;Selesai</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratkeluar[0]['selesai'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" role="progressbar" style="width: <?php echo $persensuratkeluarselesai; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <h5 class="counter text-success"><i class="icon-bag"></i>&nbsp;Semua</h5>
                                            <p class="text-muted"><strong><h1><?php echo number_format($dashboardsuratkeluar[0]['semua'], 0); ?></h1></strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Info box -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Over Visitor, Our income , slaes different and  sales prediction -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Surat Masuk</h4>
                                <div id="d_chart_suratmasuk"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Surat Keluar</h4>
                                <div id="d_chart_suratkeluar"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
          
    <!-- Column -->
    
<!-- ============================================================== -->
<!-- Comment - table -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Comment - chats -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Over Visitor, Our income , slaes different and  sales prediction -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Todo, chat, notification -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->
<div class="right-sidebar">
    <script src="<?php echo $baseUrl; ?>/dist/js/dashboard1.js"></script>
	<script src="<?php echo $baseUrl; ?>/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
	
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/morrisjs/morris.min.js"></script>
</div>

<script>
    Morris.Donut({
        element: 'd_chart_suratmasuk',
        data: <?php echo(json_encode($chartsuratmasuk)); ?>,
        resize: true,
        colors:['#009efb', '#55ce63', '#2f3d4a', '#00eefb', '#5eee63', '#2f004a', '#bf2434', '#fcfc76', '#8b128b', '#8b1200', '#bf2434', '#fcfc76', '#8b128b', '#009efb', '#55ce63', '#2f3d4a', '#00eefb', '#5eee63', '#2f004a', '#ff004a']
    });

    Morris.Donut({
        element: 'd_chart_suratkeluar',
        data: <?php echo(json_encode($chartsuratkeluar)); ?>,
        resize: true,
        colors:['#bf2434', '#fcfc76', '#8b128b', '#009efb', '#55ce63', '#2f3d4a', '#00eefb', '#5eee63', '#2f004a', '#ff004a', '#009efb', '#55ce63', '#2f3d4a', '#00eefb', '#5eee63', '#2f004a', '#bf2434', '#fcfc76', '#8b128b', '#8b1200']
    });
</script>