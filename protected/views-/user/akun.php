<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
$baseUrl = Yii::app()->theme->baseUrl;

$this->breadcrumbs = array(
    'parent' => array('home','site'),
    'Pengaturan Akun',
);
?>

<?php
if (!enumVar::BCINMAIN) {
?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Pengaturan Akun</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Pengaturan Akun</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php
}
?>
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30">
                                	<img src="<?php echo ($model->picture != null && $model->picture != '' ? Yii::app()->request->baseUrl.'/foto/'.$model->picture : Yii::app()->request->baseUrl.'/foto/noimage.jpg'); ?>" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10"><?php echo $model->nama; ?></h4>
                                    <h6 class="card-subtitle"><?php echo $model->email; ?></h6>
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body"> <small class="text-muted">Email address </small>
                                <h6><?php echo $model->email; ?></h6> <small class="text-muted p-t-30 db">Phone</small>
                                <h6><?php echo $model->nohp; ?></h6> <small class="text-muted p-t-30 db">Address</small>
                                <h6><?php echo $model->alamat; ?></h6>
                            </div><br>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Informasi Akun</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="card-body">
									    <div class="row">
											<div class="col-12">
									           <div class="card"><div class="card-body">
									        <?php
									        $form = $this->beginWidget('CActiveForm', array(
									            'id' => 'user-form',
									            'method' => 'POST',
									            'enableAjaxValidation' => false,
									            'clientOptions' => array(
									                'validateOnSubmit' => true,
									                'validateOnChange' => true
									            ),
									            'htmlOptions' => array(
									                'enctype' => 'multipart/form-data',
									                'onsubmit'=>'return validate()',
													'class'=>'form-horizontal material',
									            )
									        ));
									        ?>
									        <div class="form-group row">
									            <?php echo $form->labelEx($model, 'nama', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-lg-10">
									                <?php echo $form->textField($model, 'nama', array('maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Nama User')); ?>
									                <?php echo $form->error($model, 'nama'); ?>
									            </div>
									        </div>
									        <div class="form-group row">
									            <?php echo $form->labelEx($model, 'alamat', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-lg-10">
									                <?php echo $form->textField($model, 'alamat', array('maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Alamat')); ?>
									                <?php echo $form->error($model, 'alamat'); ?>
									            </div>
									        </div>
									        <div class="form-group row">
									            <?php echo $form->labelEx($model, 'Password baru', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-lg-10">
													<?php echo $form->passwordField($model, 'password', array('class'=>'form-control','placeholder'=>'Password Baru')); ?>
									                <?php echo $form->error($model, 'password'); ?>
									            </div>
									        </div>
									        <div class="form-group row">
									            <?php echo $form->labelEx($model, 'Password Baru', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-lg-10">
													<?php echo $form->passwordField($model, 'repeat_password', array('class'=>'form-control','placeholder'=>'Password Baru (Ulang)')); ?>
									                <?php echo $form->error($model, 'repeat_password'); ?>
									            </div>
									        </div>
											<div class="form-group row">
									            <?php echo $form->labelEx($model, 'Nomor hp', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-lg-10">
									                <?php echo $form->textField($model, 'nohp', array('maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'No. HP')); ?>
									                <?php echo $form->error($model, 'nohp'); ?>
									            </div>
									        </div>
									        <div class="form-group row">
									            <?php echo $form->labelEx($model, 'E-mail', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-lg-10">
									                <?php echo $form->textField($model, 'email', array('maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'E-mail')); ?>
									                <?php echo $form->error($model, 'email'); ?>
									            </div>
									        </div>
											<div class="col-lg-12">
									            <br/><br/>
									            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary pull-right')); ?>
									        </div>
									        <?php $this->endWidget(); ?>
									    </div><!-- form -->
									</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->

<script type="text/javascript">
	$("#User_aksesid").select2({ containerCssClass: "myFont" });
</script>
<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
	$(document).ready(function () {
        loadAkses();
    });
	
	function loadAkses() {
        $.ajax({
            url: "<?php echo CController::createUrl('user/loadakses') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'aksesid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'aksesid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'aksesid') ?>').select2().select2('val', '<?php echo $model->aksesid; ?>');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
</script>