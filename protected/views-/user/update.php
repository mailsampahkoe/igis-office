<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'User' => array('admin'),
    'Ubah User',
);
?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ubah User</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('user/admin'); ?>">Data User</a></li>
                <li class="breadcrumb-item active">Ubah User</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>