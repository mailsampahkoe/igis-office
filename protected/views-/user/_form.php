<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
$baseUrl = Yii::app()->theme->baseUrl;
?>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>
<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'user-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
                'onsubmit'=>'return validate()',
				'class'=>'form-horizontal material',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
		<div class="form-group row">
			<?php echo $form->labelEx($model, 'hak akses', array('class' => 'control-label col-sm-2')); ?>
			<div class="col-sm-10">
			<?php
				echo Select2::activeDropDownList($model, 'aksesid', array('' => ''), array(
					'prompt' => 'PILIH HAK AKSES',
					'class' => 'col-sm-8', 'style' => 'padding:0px;font-family: Arial, Helvetica, sans-serif;',
					'onchange' => 'loadPegawai(false);',
					'selected' => 'selected'
				));
			?>
			<?php echo $form->error($model, 'aksesid'); ?>
			</div>
		</div>
		<div class="form-group row">
			<?php echo $form->labelEx($model, 'pegawai', array('class' => 'control-label col-sm-2')); ?>
			<div class="col-sm-10">
			<?php
				echo Select2::activeDropDownList($model, 'pegawaiid', array('' => ''), array(
					'prompt' => 'PILIH PEGAWAI',
					'class' => 'col-sm-8', 'style' => 'padding:0px;font-family: Arial, Helvetica, sans-serif;',
					'selected' => 'selected'
				));
			?>
			<?php echo $form->error($model, 'pegawaiid'); ?>
			</div>
		</div>
        <div class="form-group row">
            <?php echo $form->labelEx($model, 'nama lengkap', array('class' => 'control-label col-sm-2')); ?>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'nama', array('maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Nama Lengkap')); ?>
                <?php echo $form->error($model, 'nama'); ?>
            </div>
        </div>
        <div class="form-group row">
            <?php echo $form->labelEx($model, 'username', array('class' => 'control-label col-sm-2')); ?>
            <div class="col-lg-10">
                <?php echo $form->textField($model, 'username', array('maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Username')); ?>
                <?php echo $form->error($model, 'username'); ?>
            </div>
        </div>
        <div class="form-group row">
            <?php echo $form->labelEx($model, 'Password baru', array('class' => 'control-label col-sm-2')); ?>
            <div class="col-lg-4">
				<?php echo $form->passwordField($model, 'password', array('class'=>'form-control','placeholder'=>'Password Baru')); ?>
                <?php echo $form->error($model, 'password'); ?>
            </div>
            <?php echo $form->labelEx($model, 'Password Baru', array('class' => 'control-label col-sm-2')); ?>
            <div class="col-lg-4">
				<?php echo $form->passwordField($model, 'repeat_password', array('class'=>'form-control','placeholder'=>'Password Baru (Ulang)')); ?>
                <?php echo $form->error($model, 'repeat_password'); ?>
            </div>
        </div>
		<div class="form-group row">
            <?php echo $form->labelEx($model, 'Nomor hp', array('class' => 'control-label col-sm-2')); ?>
            <div class="col-lg-4">
                <?php echo $form->textField($model, 'nohp', array('maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'No. HP')); ?>
                <?php echo $form->error($model, 'nohp'); ?>
            </div>
            <?php echo $form->labelEx($model, 'E-mail', array('class' => 'control-label col-sm-2')); ?>
            <div class="col-lg-4">
                <?php echo $form->textField($model, 'email', array('maxlength' => 50, 'class' => 'form-control', 'placeholder' => 'E-mail')); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
        </div>
		<div class="form-group row">
            <?php echo $form->labelEx($model, 'Gambar', array('class' => 'control-label col-sm-2')); ?>
            <div class="col-lg-10">
                <?php echo $form->fileField($model, 'picture', array('maxlength' => 50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model, 'picture'); ?>
            </div>
        </div>
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary pull-right')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div>
</div>

<script type="text/javascript">
	$("#User_aksesid").select2({ containerCssClass: "myFont" });
</script>
<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
	$(document).ready(function () {
        loadAkses();
		loadPegawai(true);
		
		$('#' + '<?php echo CHtml::activeId($model, 'password') ?>').val('');
		$('#' + '<?php echo CHtml::activeId($model, 'repeat_password') ?>').val('');
    });
	
	function loadAkses() {
        $.ajax({
            url: "<?php echo CController::createUrl('user/loadakses') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'aksesid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'aksesid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'aksesid') ?>').select2().select2('val', '<?php echo $model->aksesid; ?>');
            	
			},
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	
	function loadPegawai(p_awal) {
        var v_aksesid = '';
		if (p_awal) {
			v_aksesid = '<?php echo $model->aksesid; ?>';
		}
		else {
			v_aksesid = $('#' + '<?php echo CHtml::activeId($model, 'aksesid') ?>').select2().select2('val');
		}
		$.ajax({
            url: "<?php echo CController::createUrl('user/loadpegawai') ?>",
            type: 'POST',
            data: { aksesid : v_aksesid },
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'pegawaiid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'pegawaiid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'pegawaiid') ?>').select2().select2('val', '<?php echo $model->pegawaiid; ?>');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
</script>