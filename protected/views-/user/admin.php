<?php
/* @var $this UserController */
/* @var $model UserInfo */

Yii::import('ext.select2.Select2');
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data User</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Data User</li>
            </ol>
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('user/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tambah User</button>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="row">	
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
	<div class="form-group" id="" style="padding-right:15px;">
		<div class="row form-group">
			<div class="col-lg-12">
			    <?php 
					echo Select2::activeDropDownList($model, 'aksesid', array('' => ''), array(
                        'prompt' => 'PILIH HAK AKSES',
                        'class' => 'col-lg-12', 'style' => 'padding:0px','selected' => 'selected'
                    ));
				?>
			</div>
        </div>
		<div class="row form-group">
			<div class="col-lg-12"> 
                <?php echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
        </div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'user-grid',
        'itemsCssClass' => 'table table-hover color-table info-table',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('user/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => 'User',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Grup',
                'name' => 'grupship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['aksesnama'];
		        },
            ),
            array(
                'header' => 'Username',
                'name' => 'usernameship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['username'];
		        },
            ),
            array(
                'header' => 'Nama',
                'name' => 'namaship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['nama'];
		        },
            ),
            array(
                'template' => '{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
					'update' => array(
                        'url' => 'Yii::app()->createUrl("/user/update", array("userid"=>$data["userid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "3") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/user/delete", array("userid"=>$data["userid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "3") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
    function loadAkses() {
        $.ajax({
            url: "<?php echo CController::createUrl('user/loadakses') ?>",
            type: 'POST',
            data: {isall:'true'},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'aksesid') ?>').val('');
                $('#<?php echo CHtml::activeId($model, 'aksesid') ?>').html(data);
				if ("<?php echo $model->aksesid; ?>"!="")
                	$('#<?php echo CHtml::activeId($model, 'aksesid') ?>').select2().select2('val', "<?php echo $model->aksesid; ?>");
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	function tambahData() {
        js:document.location.href="<?php echo CController::createUrl('user/create') ?>/aksesid/"+$('#<?php echo CHtml::activeId($model, 'aksesid') ?>').val();
    }
	$(document).ready(function () {
        loadAkses();
		$('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				$.fn.yiiGridView.update('user-grid', {
					//data: $(this).serialize()
					data: {searchtext:$('#searchtext').val(),filteraksesid: $('#<?php echo CHtml::activeId($model, 'aksesid') ?>').val()},
				});
				return false;
			}
		});
		$('#<?php echo CHtml::activeId($model, 'aksesid') ?>').change(function(e){
			$.fn.yiiGridView.update('user-grid', {
				//data: $(this).serialize()
				data: {searchtext:$('#searchtext').val(),filteraksesid: $('#<?php echo CHtml::activeId($model, 'aksesid') ?>').val()},
			});
			return false;
		});
    });
</script>