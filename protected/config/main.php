<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'SiapNew',
    'theme' => 'modern',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.components.enum.*',
        'application.extensions.*'
    ),
    //'defaultController'=>'Skpd',
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '1234',
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
	'timeZone' => 'Asia/Jakarta',
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'class' => 'application.components.EWebUser',
            'allowAutoLogin' => true,
        ),
        'global' => array(
            'class' => 'application.components.Globals',
        ),
        // uncomment the following to enable URLs in path-format
        /*'urlManager' => array(
            'urlFormat' => 'path',
            //'showScriptName' => false,
            'rules' => array(
            
            ),
        ),*/
        /* 'db'=>array(
          'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
          ), */
        // uncomment the following to use a MySQL database
        'db'=>array(
		   'connectionString'=>'mysql:host=localhost;dbname=igis-office',
		   'username'=>'root',
		   'password'=>'z12345212',
		),
        /*'db' => array(
            'connectionString' => 'pgsql:host=localhost;port=5432;dbname=igis-office',
            'emulatePrepare' => true,
            'username' => 'postgres',
			'password' => '123456',
            'charset' => 'utf8',
        ),*/
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    //'categories' => 'system.*',
                ),
                /*array(
                    'class' => 'CEmailLogRoute',
                    'levels' => 'error, warning',
                    'emails' => 'mailspamkoe@gmail.com',
                ),
		        array(
		            'class' => 'CWebLogRoute',
		            'enabled' => YII_DEBUG,
		            'levels' => 'error, warning, trace, info, notice',
		            'categories' => 'application',
		            'showInFireBug' => false,
		        ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
        'bootstrap'=>array(
            'class'=>'bootstrap.components.Bootstrap',
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'cs.doankz@gmail.com',
    ),
);
