<?php

class SiteController extends Controller {

    public $layout = '//layouts/column2';

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index','login', 'logout', 'register', 'recoverpass','getemailtoken','validateemailtoken','validateemail', 
                					'loadsubunit','error','dashboard1','dashboard2','dashboard4','dokumen', 'gallery', 'detailberita',
                					'confirmdatawpexist',
									'notifikasi'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('loadnotif'),
                'users' => array('@'),
            ),
			 /*array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index','menuapbd','menurealisasi','loaddata','loaddata2'),
                'users' => array('*'),
            ),*/
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {

		$sql = "select a.notifikasiid, a.judul, a.keterangan, a.tglnotifikasi, b.status, a.controller, a.action 
			from tsnotifikasi a
			inner join tsnotifikasiuser b on b.notifikasiid = a.notifikasiid and b.dlt = '0' and userid = '".Yii::app()->user->getuser('id')."' 
			where a.dlt='0'
			order by tglnotifikasi desc limit 4 ";
		$datanotifikasi = Yii::app()->db->createCommand($sql)->queryAll();
		$sql = "select a.pesanid, a.judul, a.keterangan, a.tglpesan, b.status, a.controller, a.action 
			from tspesan a
			inner join tspesanuser b on b.pesanid = a.pesanid and b.dlt = '0' and userid = '".Yii::app()->user->getuser('id')."' 
			where a.dlt='0'
			order by tglpesan desc limit 4 ";
		$datapesan = Yii::app()->db->createCommand($sql)->queryAll();
		$sql = "select beritaid, judul, keterangan, tglberita, flag, status, img, jenis, url from tsberita
			where dlt='0' and status = '1'
			order by tglberita desc limit 4 ";
		$databerita = Yii::app()->db->createCommand($sql)->queryAll();
		/*$sql = "select pengumumanid, pengirim, judul, keterangan, formattanggal(tglpengumuman::date) as tglpengumuman, flag, status, img from tspengumuman
			where dlt='0' and status = '1'
			order by tglpengumuman desc ";
		$datapengumuman = Yii::app()->db->createCommand($sql)->queryAll();*/

        $criteria = new CDbCriteria();
        $criteria->select = 'pengumumanid, pengirim, judul, keterangan, tglpengumuman, flag, status, img';
		$criteria->addCondition("dlt='0' and status = '1'");
        //$criteria->join .= ' LEFT JOIN tmrole tr ON tr.roleid=t.roleid';
		//$schema=Yii::app()->db->schema;
		//$builder=$schema->commandBuilder;
		//$datapengumuman=$builder->createFindCommand($schema->getTable('tspengumuman'), $criteria);
		$datapengumuman=Pengumuman::model()->findAll($criteria);
		
		$this->render('index', array('datapesan'=>$datapesan, 'datanotifikasi'=>$datanotifikasi, 'databerita'=>$databerita, 'datapengumuman'=>$datapengumuman, 'sql'=>$sql));
		//$this->render('index');
    }
    
        
    public function actionLoadNotif() {
        
    }
    
    public function actionMenurealisasi() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('menurealisasi');
        /* $Restoran = new Restoran();
          $dpRestoranFavorit = $Restoran->GetRestoranFav();
          $this->render('index', array(
          'dpRestoranFavorit' => $dpRestoranFavorit,
          )); */
    }
	
	public function actionlockedwarning($menu) {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('lockedwarning',array("menu" => $menu));
        /* $Restoran = new Restoran();
          $dpRestoranFavorit = $Restoran->GetRestoranFav();
          $this->render('index', array(
          'dpRestoranFavorit' => $dpRestoranFavorit,
          )); */
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
                mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {

        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];


            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
            	Yii::log('Sukses', 'info', 'application');
                Yii::app()->user->setTahun($_POST["tahun"]);
                Yii::app()->user->setIP($this->getIP());
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        $this->layout = 'column1';
        // display the login form
		$model->tahun = "".Yii::app()->user->getTahun()."";
		$sql="select nama from tmtahun where dlt='0' order by nama";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
        $this->renderPartial('login', array('model' => $model, 'tahun' => $rows));
    }

    /**
     * Displays the register page
     */
    public function actionRegister() {
		$message = array();
        /*$message    = array(
            'r'   => '0',
            'e'   => '1',
            'message'   => 'Gagal'
        );
        $message    = array(
            'r'   => '1',
            'e'   => '0',
            'message'   => 'Sukses melakukan registrasi, silakan tunggu verifikasi dari Pegawai kami untuk registrasi Anda. Kami akan mengirimkan hasil verifikasi melalui e-mail yang telah anda daftarkan. Terima kasih'
        );*/
		
        $model = new User;
		$model->scenario='register';

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'registerform') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
			//$userid = Globals::newID("tmuser", "userid");
			$model->userid = Globals::newID("tmuser", "userid");
			
			$model->aksesid = '3';
			$model->grup = '0';
			$model->status = '0';
			$model->tahun = date("Y");
			$model->picture=CUploadedFile::getInstance($model,'picture');
			$model->filenik=CUploadedFile::getInstance($model,'filenik');
			$model->filenpwp=CUploadedFile::getInstance($model,'filenpwp');
			$model->filelegal=CUploadedFile::getInstance($model,'filelegal');
			
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());

            // validate user input and redirect to the previous page if valid
            if ($model->validate()) {
				$pass = $model->encryptPassword($model->password);
				$model->password = $pass;
				
				$uploadedPicture = CUploadedFile::getInstance($model, 'picture');
				$model->picture = md5('pic'.strtotime("now")).'.jpg';//.$uploadedPicture->getExtensionName();
				$uploadedFileNIK = CUploadedFile::getInstance($model, 'filenik');
				$model->filenik = md5('nik'.strtotime("now")).'.jpg';//.$uploadedFileNIK->getExtensionName();
				$uploadedFileNPWP = CUploadedFile::getInstance($model, 'filenpwp');
				$model->filenpwp = md5('npwp'.strtotime("now")).'.jpg';//.$uploadedFileNPWP->getExtensionName();
				if ($model->filelegal != null && $model->filelegal != "") {
					$uploadedFileLegal = CUploadedFile::getInstance($model, 'filelegal');
					$model->filelegal = md5('legal'.strtotime("now")).'.jpg';//.$uploadedFileLegal->getExtensionName();
				}

				if($model->save()) {  
					$path = Yii::app()->basePath.'/../foto';
					$uploadedPicture->saveAs($path.'/'.$model->picture);
					$uploadedFileNIK->saveAs($path.'/'.$model->filenik);
					$uploadedFileNPWP->saveAs($path.'/'.$model->filenpwp);
					if ($model->filelegal != null && $model->filelegal != "") {
						$uploadedFileLegal->saveAs($path.'/'.$model->filelegal);
					}
	                //Globals::AdminLogging("create:unit:" . $model->aksesid . "");
	                //$this->redirect(array('view', 'aksesid' => $model->aksesid));
					//EUserFlash::setSuccessMessage('Thank you.');
	                //$this->redirect(Yii::app()->user->returnUrl);
					//$this->redirect(array('login'));
					
					$sql = "SELECT a.userid from tmuser a
							INNER JOIN tmakses b on b.aksesid = a.aksesid and b.dlt = '0' and b.kode = 'verifikator'
							WHERE a.dlt = '0' ";
			        $rows = Yii::app()->db->createCommand($sql)->queryAll();
			        if (count($rows) > 0) {
						$modelnotif = new Notifikasi;
						$modelnotif->notifikasiid = Globals::newID("tsnotifikasi", "notifikasiid");
						$modelnotif->judul = 'Registrasi User '.$model->npwp;
						$modelnotif->keterangan = 'User '.$model->npwp.' membutuhkan verifikasi untuk dapat melakukan login ke dalam Sistem Manajemen SPTPD.';
						$modelnotif->tglnotifikasi = $this->standard_date('DATE_RFC822',time());
						$modelnotif->controller = 'user';
						$modelnotif->action = 'aktivasi';
						$modelnotif->transaksiid = $model->userid;
						$modelnotif->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
						if ($modelnotif->save()) {
					        foreach ($rows as $row) {
								$modelnotifuser = new Notifikasiuser;
								$modelnotifuser->notifikasiuserid = Globals::newID("tsnotifikasiuser", "notifikasiuserid");
								$modelnotifuser->notifikasiid = $modelnotif->notifikasiid;
								$modelnotifuser->userid = $row['userid'];
								$modelnotifuser->status = '1';
								$modelnotifuser->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
					            $modelnotifuser->save();
					        }
						}
					}
					
			        $message    = array(
			            'r'   => '1',
			            'e'   => '0',
			            'message'   => 'Sukses melakukan registrasi, silakan tunggu verifikasi dari Pegawai kami untuk registrasi anda. Kami akan mengirimkan hasil verifikasi melalui e-mail yang telah anda daftarkan. Terima kasih.'
			        );
			        //echo json_encode($message);
				}else {
			        $message    = array(
			            'r'   => '0',
			            'e'   => '1',
			            'message'   => 'Gagal'
			        );
			        //echo json_encode($message);
                	//echo 'not save'; return;
				}
            }
        }
        $this->layout = 'column1';
        // display the register form
        $this->renderPartial('register', array('model' => $model, 'result' => $message));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
	
    public function actionGetEmailToken() {
		$sql = "SELECT a.emailverifikasiid from tsemailverifikasi a where a.dlt = '0' and a.email = '".$_POST['email']."' and a.email in (select email from tmuser where status = '0' or status = '1')";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		if (count($rows) <= 0) {
			$verifyemailcode = $this->getrandalphanumeric(6);
			$verifyemailcode = strtoupper($verifyemailcode);
			
			$model = Emailverifikasi::model()->findByAttributes(array('email' => $_POST['email']));
			if (!is_null($model)) {
				$model->kode = $verifyemailcode;
				$model->opedit = Yii::app()->user->name;
	            $model->tgledit = $this->standard_date('DATE_RFC822',time());
				$model->pcedit = Yii::app()->user->getIP();
				//$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
			}
			else {
				$model = new Emailverifikasi;
				$model->emailverifikasiid = Globals::newID("tsemailverifikasi", "emailverifikasiid");
				$model->email = $_POST['email'];
				$model->kode = $verifyemailcode;
				$model->tglgenerate = $this->standard_date('DATE_RFC822',time());
				$model->status = '2';
				$model->opadd = Yii::app()->user->name;
	            $model->tgladd = $this->standard_date('DATE_RFC822',time());
				$model->pcadd = Yii::app()->user->getIP();
				$model->opedit = Yii::app()->user->name;
	            $model->tgledit = $this->standard_date('DATE_RFC822',time());
				$model->pcedit = Yii::app()->user->getIP();
				$model->dlt = '0';
			}
			//$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
            	$success = '';
            	$emailsubject = 'Verifikasi Email Registrasi - e-SPTPD BPPRD Kota Batam';
            	$emailmessage = '
							<html>
							<head>
							<title>Verifikasi Email Registrasi untuk e-SPTPD BPPRD Kota Batam</title>
							</head>
							<body>
							<p>Silahkan masukkan kode : <b>'.$verifyemailcode.'</b> sebagai verifikasi bahwa email yang anda gunakan untuk registrasi adalah benar milik anda.</p>
							</body>
							</html>
							';
    			try {
					$result = $this->sendmail($_POST['email'], $emailsubject, $emailmessage);
	                $success = 'Email berisi kode verifikasi email telah berhasil dikirim ke email id anda : '.$_POST['email'].'.<br>Silakan buka email anda, dan masukkan kode verifikasi yang anda terima pada e-mail ke dalam kolom yang disediakan.';
			        $message    = array(
			            'r'   => '1',
			            'e'   => '0',
			            'message'   => $success
			        );
				}
				catch (Exception $e) {
					$success .= $e->getMessage();
			        $message    = array(
			            'r'   => '0',
			            'e'   => '1',
			            'message'   => $success
			        );
				}
				catch (RuntimeException $e) {
				    $success .= $e->getMessage();
			        $message    = array(
			            'r'   => '0',
			            'e'   => '1',
			            'message'   => $success
			        );
				}
			}
			else {
		        $message    = array(
		            'r'   => '0',
		            'e'   => '1',
		            'message'   => 'Gagal menyimpan kode ke e-mail anda, silakan ulang kembali...'
		        );
			}
		}
		else {
	        $message    = array(
	            'r'   => '0',
	            'e'   => '1',
	            'message'   => 'E-mail address sudah digunakan oleh pengguna lain, silakan gunakan alamat email lain...'
	        );
		}
        echo json_encode($message);
    }
    
    public function loadModelEmailverifikasi($id) {
        $model = Emailverifikasi::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
    public function actionValidateEmailToken() {
        $model = Emailverifikasi::model()->findByAttributes(array('kode' => $_POST['token'], 'email' => $_POST['email']));
		if (!is_null($model)) {
			$model->status = '2';
			$model->opedit = Yii::app()->user->name;
            $model->tgledit = $this->standard_date('DATE_RFC822',time());
			$model->pcedit = Yii::app()->user->getIP();
			//$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
			$model->save();
			$message    = array(
	            'r'   => '1',
	            'e'   => '0',
	            'message'   => 'Sukses melakukan verifikasi email'
	        );
		}else{
			$message    = array(
	            'r'   => '0',
	            'e'   => '1',
	            'message'   => 'Gagal melakukan verifikasi email. Kode yang anda masukkan tidak benar...'
	        );
		}
		echo json_encode($message);
    }
	
    public function actionValidateEmail() {
		$email = isset($_POST["email"]) ? $_POST["email"] : '';
        $message    = array(
            'r'   => '0',
            'e'   => '1',
            'message'   => 'E-mail address not found...'
        );
		if ($email == '') {
			$vmail = new verifyEmail();
			$vmail->setStreamTimeoutWait(20);
			
			$vmail->setEmailFrom('mailsampahkoe@gmail.com');

			if ($vmail->check($email)) {
				$message    = array(
					'r'   => '1',
					'e'   => '0',
					'message'   => 'E-mail address valid and exist'
				);
			} elseif (verifyEmail::validate($email)) {
				$message    = array(
					'r'   => '0',
					'e'   => '2',
					'message'   => 'E-mail address valid but not exist'
				);
			} else {
				$message    = array(
					'r'   => '0',
					'e'   => '3',
					'message'   => 'E-mail address not valid'
				);
			}
		}
		
		echo json_encode($message);
    }
	
	public function actionConfirmDataWPExist(){
        $user = Users::model()->find('npwp=?',array($_POST['npwpd']));
        if ($user->userid != '' && $user->userid != null) {
			$message    = array(
				'r'   => '0',
				'e'   => '1',
				'message'   => 'NPWPD ini sudah dibuatkan user...'
			);
			echo json_encode($message);
		}
		else {
			$params = array(
			    'user' => 'Admin',
			    'pass' => 'nimda1234567890212',
			    'opsi' => 'wp',
			    'npwpd' => $_POST['npwpd']
			);
			    /*'multiple' => array(
			        'value1',
			        'value2',
			        'value3',
			    )
			        */
			$json = $this->getWebapi($this->getAPIBase()."?r=webapi/confirmdataexist", $params, "POST");
			echo $json;
		}
	}
	
	public function actionNotifikasi($id) {
		$model = Notifikasi::model()->findByPk($id);
		if (!is_null($model)) {
			$modeluser = Notifikasiuser::model()->find('notifikasiid=? and userid=?',array($id, Yii::app()->user->getuser('id')));
			if (!is_null($modeluser)) {
				$modeluser->status = '0';
				$modeluser->attributes = array_merge($modeluser->attributes,$this->getLogAddDataInfo());
				
				$modeluser->save();
			}
			$this->redirect(array($model->controller.'/'.$model->action, $model->primarykey => $model->transaksiid));
		}
		else {
			$this->redirect(array('site'));
		}
	}
}
