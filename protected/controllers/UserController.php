<?php

class UserController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('akun', 'profil'),
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array('admin', 'view','loadakses','loadpegawai'),
                'expression' => '$user->getprivileges(\'view\',\'302\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('loadakses','password'),
                'expression' => '$user->getprivileges(\'view\',\'303\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create'),
                'expression' => '$user->getprivileges(\'create\',\'302\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update'),
                'expression' => '$user->getprivileges(\'edit\',\'302\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'302\') || $user->isSuperadmin()', //data anggota
            ), 
            array('allow',
                'actions' => array('adminverify','loadstatus'),
                'expression' => '$user->getprivileges(\'view\',\'304\') || $user->isSuperadmin()', //data verifikasi
            ),
            array('allow',
                'actions' => array('verify'),
                'expression' => '$user->getprivileges(\'edit\',\'304\')|| $user->isSuperadmin()', //data verifikasi
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate($aksesid="") {
        $model = new User;
		
        $this->performAjaxValidation($model);
		$model->scenario='create';
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
			$model->picture=CUploadedFile::getInstance($model,'picture');
			
			$folder_name = 'foto/';
			$userid = Globals::newID("tmuser", "userid");
			
			$valid = true;
			if ($model->pegawaiid == '') {
				$valid = false;
				$model->addError('pegawaiid', 'Pegawai harus dipilih terlebih dahulu');
			}
			else {
				$sql = "SELECT userid from tmuser where dlt = '0' and pegawaiid = '".$model->pegawaiid."' ";
		        $rows = Yii::app()->db->createCommand($sql)->queryAll();
				if (count($rows) > 0) {
					$valid = false;
					$model->addError('pegawaiid', 'Pegawai yang dipilih sudah memiliki username');
				}
			}
			
			//echo $model->pegawaiid.'-'.$valid;
			//var_dump($_POST);
			//var_dump($_FILES);
			//return;
			if ($valid) {
	            $model->userid = $userid;
				$model->status = '1';
				$model->tahun = Yii::app()->user->getTahun();
				$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
				if($model->validate())
				{
					//$userModel = User::model()->findByPk(Yii::app()->user->id);
					$pass = $model->encryptPassword($model->password);
					$model->password = $pass;
					
					if ($model->save()) {
						if ($model->picture != null && $model->picture != "") {
							$newfiles = $model->picture->name;
							$model->picture->saveAs($folder_name . $newfiles);
						}
		                //Globals::AdminLogging("create:unit:" . $model->aksesid . "");
		                //$this->redirect(array('view', 'aksesid' => $model->aksesid));
						$this->redirect(array('user/admin', 'filteraksesid'=>$model->aksesid));
		            }
	            }
			}
        }else{
			if ($aksesid!="") {
	            $model->aksesid = $aksesid;
	        }
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($userid) {
        $model = $this->loadModel($userid);
		
        $this->performAjaxValidation($model);
		$model->scenario='update';
		
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
			$model->picture=CUploadedFile::getInstance($model,'picture');
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
			
			$valid = true;
			if ($model->pegawaiid == '') {
				$valid = false;
				$model->addError('pegawaiid', 'Pegawai harus dipilih terlebih dahulu');
			}
			else {
				$sql = "SELECT userid from tmuser where dlt = '0' and pegawaiid = '".$model->pegawaiid."' and userid <> '".$userid."' ";
		        $rows = Yii::app()->db->createCommand($sql)->queryAll();
				if (count($rows) > 0) {
					$valid = false;
					$model->addError('pegawaiid', 'Pegawai yang dipilih sudah memiliki username');
				}
			}
			if ($model->password == '') {
				$valid = false;
				$model->addError('password', 'Isi password terlebih dahulu');
			}
			if ($valid) {
				$folder_name = 'foto/';
				if($model->validate())
				{
					//$userModel = User::model()->findByPk(Yii::app()->user->id);
					$pass = $model->encryptPassword($model->password);
					$model->password = $pass;
		            $model->tahun = Yii::app()->user->getTahun();
					if ($model->save()) {
						if (!empty($model->picture) && $model->picture != null && $model->picture != '') {
							$newfiles = $model->picture->name;
							$model->picture->saveAs($folder_name . $newfiles);
						}
						//Globals::AdminLogging("update:unit:" . $model->aksesid . "");
		               	//$this->redirect(array('user/	admin'));
						$this->redirect(array('user/admin', 'filteraksesid'=>$model->aksesid));
		            }
		        }
			}
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionAkun() {
        $model = $this->loadModel(Yii::app()->user->getuser('id'));
		
        $this->performAjaxValidation($model);
		$model->scenario='account';
		
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
			
			if($model->validate())
			{
				//$userModel = User::model()->findByPk(Yii::app()->user->id);
				$pass = $model->encryptPassword($model->password);
				$model->password = $pass;
	            $model->tahun = Yii::app()->user->getTahun();
				if ($model->save()) {
					//Globals::AdminLogging("update:unit:" . $model->aksesid . "");
	               	//$this->redirect(array('user/	admin'));
					$this->redirect(array('user/akun'));
	            }
	        }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('akun', array(
            'model' => $model,
        ));
    }

    public function actionProfil() {
        $this->breadcrumbs = array(
		    'Home' => array('admin','site'),
		    'Pengaturan Akun',
		);

        $model = $this->loadModel(Yii::app()->user->getuser('id'));
		
        $this->performAjaxValidation($model);
		$model->scenario='account';
		
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
			
			if($model->validate())
			{
				//$userModel = User::model()->findByPk(Yii::app()->user->id);
				$pass = $model->encryptPassword($model->password);
				$model->password = $pass;
	            $model->tahun = Yii::app()->user->getTahun();
				if ($model->save()) {
					//Globals::AdminLogging("update:unit:" . $model->aksesid . "");
	               	//$this->redirect(array('user/	admin'));
					$this->redirect(array('user/admin', 'filteraksesid'=>$model->aksesid));
	            }
	        }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('profil', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($userid) {
        $model = $this->loadModel($userid);
		$aksesid = $model->aksesid;
        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:unit:" . $model->aksesid . "");
           	$this->redirect(array('user/admin', 'filteraksesid'=>""));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('User');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
    public function loadModelDetail($id) {
        $modeldetail = Userunit::model()->findByPk($id);
        if ($modeldetail === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $modeldetail;
    }
	
	public function actionAdmin($aksesid="") {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User'])) {
            $model->attributes = $_GET['User'];
        }
		if (isset($_GET['filteraksesid'])) $model->aksesid = $_GET['filteraksesid'];
		else if ($aksesid!="") $model->aksesid = $aksesid;
		$criteria = new CDbCriteria();
        $criteria->select = 't.*, tr.kode as akseskode, tr.nama as aksesnama';
		$criteria->addCondition("t.dlt = '0'");
		$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.kode LIKE :searchtext or t.nama LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		if ($model->aksesid != '' && $model->aksesid != '-') {
            $criteria->addCondition("t.aksesid = '".$model->aksesid."' ");
        }
        $criteria->join .= " LEFT JOIN tmakses tr ON tr.aksesid=t.aksesid and tr.dlt='0' ";
        $dataProvider = new CActiveDataProvider('User', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'tr.nama, t.username, t.nama',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
	
	public function actionAdminVerify() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User'])) {
            $model->attributes = $_GET['User'];
        }
		
		$criteria = new CDbCriteria();
        $criteria->select = 't.*, tr.kode as akseskode, tr.nama as aksesnama';
		$criteria->addCondition("t.dlt = '0'");
		$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.kode ILIKE :searchtext or t.nama ILIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
        if (isset($_GET['filterstatus'])) {
	        $criteria->addCondition("t.status = '".$_GET['filterstatus']."' ");
        }
        else {
			$criteria->addCondition("t.status = '0' ");
		}
        $criteria->addCondition("t.aksesid = '3' "); //Untuk semua user dengan akses User
        $criteria->join .= " LEFT JOIN tmakses tr ON tr.aksesid=t.aksesid and tr.dlt='0' ";
        $dataProvider = new CActiveDataProvider('User', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'tr.nama, t.username, t.nama',
            ),));

        $this->render('adminverify', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }

    public function actionVerify($userid) {
        $model = $this->loadModel($userid);
		
        $this->performAjaxValidation($model);
		$model->scenario='verify';
		//var_dump($_POST);return;
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
			
			if($model->validate())
			{
				$emailmsg = '';
				if ($_POST['statusverifikasi'] == '0') {
		            $model->status = '1';
		            $emailmsg = 'Pelanggan yang terhormat,<br><br>';
		            $emailmsg .= 'Selamat, akun esptpd Anda dengan npwpd '.$model->npwpd.' sudah diverifikasi. Silahkan melakukan login dengan menggunakan npwpd dan password Anda untuk melakukan pelaporan pajak.<br>Terima kasih.<br><br><br>Salam kami,<br><br><br><strong>BP2RD Kota Batam</strong>';
				}
				else {
					$model->status = '2';
		            $emailmsg = 'Pelanggan yang terhormat,<br><br>';
		            $emailmsg .= 'Maaf, akun esptd Anda dengan npwpd '.$model->npwpd.' tidak dapat diverikasi.<br>Keterangan: '.$_POST['catatanverifikasi'].'Silahkan cek kembali kelengkapan data pendaftaran Anda dan lakukan daftar ulang.<br>Terima kasih.<br><br><br>Salam kami,<br><br><br><strong>BP2RD Kota Batam</strong>';
				}
				if ($model->save()) {
					$modelverifikasi = new Userverifikasi;
					$modelverifikasi->userverifikasiid = Globals::newID("tmuserverifikasi", "userverifikasiid");
					$modelverifikasi->userid = $model->userid;
					$modelverifikasi->verifikatorid = Yii::app()->user->getuser('id');
					$modelverifikasi->tglverifikasi = $this->standard_date('DATE_RFC822',time());
					$modelverifikasi->status = $model->status;
					$modelverifikasi->catatan = $_POST['catatanverifikasi'];
					$modelverifikasi->keterangan = '-';
					$modelverifikasi->attributes = array_merge($modelverifikasi->attributes,$this->getLogAddDataInfo());
					$modelverifikasi->save();
					
					$modelnotif = new Notifikasi;
					$modelnotif->notifikasiid = Globals::newID("tsnotifikasi", "notifikasiid");
					$modelnotif->judul = 'Selamat datang '.$model->nama;
					$modelnotif->keterangan = 'Selamat datang di Sistem Manajemen SPTPD (e-SPTPD) BP2RD Kota Batam.';
					$modelnotif->tglnotifikasi = $this->standard_date('DATE_RFC822',time());
					$modelnotif->controller = 'site';
					$modelnotif->action = 'index';
					$modelnotif->transaksiid = $model->userid;
					$modelnotif->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
					if ($modelnotif->save()) {
						$modelnotifuser = new Notifikasiuser;
						$modelnotifuser->notifikasiuserid = Globals::newID("tsnotifikasiuser", "notifikasiuserid");
						$modelnotifuser->notifikasiid = $modelnotif->notifikasiid;
						$modelnotifuser->userid = $model->userid;
						$modelnotifuser->status = '1';
						$modelnotifuser->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			            $modelnotifuser->save();
		            }
		            
	            	$result = $this->sendmail($model->email, 'Verifikasi User e-SPTPD BP2RD Kota Batam', $emailmsg);
	            	
					$this->redirect(array('user/adminverify'));
	            }
	        }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('verify', array(
            'model' => $model,
        ));
    }

    public function actionPassword() {
    	$userid = Yii::app()->user->getuser('id');
        $model = $this->loadModel($userid);
		
        $this->performAjaxValidation($model);
        
		$model->scenario='password';
		$valid = false;
		
		if (isset($_POST['User'])) {
			$model->setAttributes($_POST['User']);
			
			if($model->validate())
			{
				$userModel = User::model()->findByPk(Yii::app()->user->id);
				$pass = $userModel->encryptPassword($_POST['User']['password']);
				$userModel->password = $pass;
				$data = $userModel->update();
				$userModel->password = '';
			}

			if(isset($data))
			{
				Yii::app()->user->setFlash('success',"Password changed successfully!");
				$this->redirect(array('User/password'), true);
			}
		}
		else {
			$model->password = '';
		}
		
		$this->render('password', array('model'=>$model,true));
		//$this->render('password',array('model'=>$model));	
    }
    
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadAkses() {
        $sql = "";
		$filter = "";
		$modeluser = User::model()->findByPk(Yii::app()->user->id);
		if ($modeluser->grup=="2"){
			if (!is_null($modeluser->aksesid) && $modeluser->aksesid != '' && $modeluser->aksesid != '-') {
	            $filter .= " and aksesid = '".$modeluser->aksesid."' ";
	        }
		}
        //$filter .= " and kode <> 'user' ";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as aksesid,'--FILTER AKSES--' as aksesvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT aksesid,nama as aksesvw, 2 as urutan FROM tmakses where dlt='0' $filter order by urutan, aksesvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['aksesid']), CHtml::encode($row['aksesvw']), true);
        }
    }

    public function actionLoadPegawai() {
        $sql = "";
		$filter = "";
		if (isset($_POST['aksesid']) && $_POST['aksesid'] != '') {
			$modelakses = Akses::model()->findByPk($_POST['aksesid']);
			if ($modelakses->kode == 'kabag') {
				$filter = " and b.level = '4' ";
			}
			elseif ($modelakses->kode == 'kasubbag') {
				$filter = " and b.level = '5' ";
			}
			elseif ($modelakses->kode == 'staf') {
				$filter = " and b.level = '10' ";
			}
		}
		
		$modeluser = User::model()->findByPk(Yii::app()->user->id);
		$sql = " SELECT '' as pegawaiid,'-' as pegawaivw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.pegawaiid, concat(a.kode, ' - ', a.nama) as pegawaivw, 2 as urutan 
				FROM tmpegawai a 
				INNER JOIN tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
				where a.dlt='0' $filter order by urutan, pegawaivw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['pegawaiid']), CHtml::encode($row['pegawaivw']), true);
        }
    }

    public function actionLoadStatus() {
    	$option = CHtml::tag('option', array('value' => '0'), 'Belum Aktif', true);
    	$option .= CHtml::tag('option', array('value' => '1'), 'Sudah Aktif', true);
    	$option .= CHtml::tag('option', array('value' => '2'), 'Ditolak', true);
        echo $option;
    }
}
