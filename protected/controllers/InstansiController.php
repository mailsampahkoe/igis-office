<?php

class InstansiController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index'),
                'expression' => '$user->getprivileges(\'view\',\'1\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update'),
                'expression' => '$user->getprivileges(\'edit\',\'1\')|| $user->isSuperadmin()', //data anggota
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionIndex() {
		$sql = "SELECT * FROM tminstansi a 
				where a.dlt='0' ";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		if (count($rows) > 0) {
	        $model = Instansi::model()->findByPk($rows[0]['instansiid']);
		}
	    else {
			$model = new Instansi;
		}
        $this->performAjaxValidation($model);
        
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionUpdate() {
		$sql = "SELECT * FROM tminstansi a 
				where a.dlt='0' ";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		if (count($rows) > 0) {
	        $model = Instansi::model()->findByPk($rows[0]['instansiid']);
		}
	    else {
			$model = new Instansi;
		}
        $this->performAjaxValidation($model);
        
        if (isset($_POST['Instansi'])) {
            $model->attributes = $_POST['Instansi'];
			$model->logo=CUploadedFile::getInstance($model,'logo');
			
			if (!empty($_FILES)) {
	            /*$fileName = $_FILES['Instansi[logo]']['name']; //get the file name
	            $fileSize = $_FILES['Instansi[logo]']['size']; //get the size
	            $fileError = $_FILES['Instansi[logo]']['error']; //get the error when upload
	            $file = $_FILES['Instansi[logo]'];
				$newfiles = '';
	            $extension = str_replace(".", "", substr($fileName, strlen($fileName) - 4, 4));
				
	            if ($fileSize > 0 || $fileError == 0) { //check if the file is corrupt or error
					$temp_file = $_FILES['Instansi[logo]']['tmp_name'];
					$newfiles = Globals::newID("tminstansi", "instansiid") . '.' . $extension;
					$location = $folder_name . $newfiles;// $_FILES['Instansi[logo]']['name'];
					move_uploaded_file($temp_file, $location);
				}
				$model->logo = $newfiles;*/
			}
			
			$folder_name = 'files/';
			if (count($rows) > 0) {
				$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
			}
		    else {
				$instansiid = Globals::newID("tminstansi", "instansiid");;
			
				$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			}
            if ($model->save()) {
				$newfiles = $model->logo->name;
				$model->logo->saveAs($folder_name . $newfiles);
				$this->redirect(array('index'));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }
   	
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Instansi::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'instansi-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}