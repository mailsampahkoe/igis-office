<?php

class SuratkeluarController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view'),
                'expression' => '$user->getprivileges(\'view\',\'105\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create','kode','getnextno'),
                'expression' => '$user->getprivileges(\'create\',\'105\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update','posting','upload','upload1','getnextno'),
                'expression' => '$user->getprivileges(\'edit\',\'105\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'105\') || $user->isSuperadmin()', //data anggota
            ), 
            array('allow',
                'actions' => array('print'),
                'expression' => '$user->getprivileges(\'print\',\'105\') || $user->isSuperadmin()', //data anggota
            ), 
            array('allow',
                'actions' => array('loadunit','loadsubunit','loadbagian','loadsubbagian','loadjenis','loadpejabat','loadjabatan', 'loadsuratkeluarjabatan', 'loadsuratkeluardetail', 'loadsuratkeluarlampiran', 'loadsuratkeluartembusan', 'loadsuratkeluarfiles'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate($unitid="") {
        $model = new Suratkeluar;
		
        $this->performAjaxValidation($model);
        if (isset($_POST['Suratkeluar'])) {
            $model->attributes = $_POST['Suratkeluar'];
			$suratkeluarid = Globals::newID("ttsuratkeluar", "suratkeluarid");
            $model->suratkeluarid = $suratkeluarid;
            $model->pegawaiid = Yii::app()->user->getinfouser('pegawaiid');
            $model->tglpenyerahan = $this->datePostgres($model->tglpenyerahan);
            $model->tglsurat = $this->datePostgres($model->tglsurat);
            $model->tglkirim = $this->datePostgres($model->tglkirim);
			$model->tahun = Yii::app()->user->getTahun();
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
                //Globals::AdminLogging("create:unit:" . $model->unitid . "");
				if (isset($_POST['detaildelete']) && $_POST['detaildelete'] != '') {
					$this->deleteDetail($_POST['detaildelete']);
				}
				if (isset($_POST['detaildata']) && $_POST['detaildata'] != '') {
					$this->saveDetail($_POST['detaildata'], $suratkeluarid);
				}
				if (isset($_POST['jabatandelete']) && $_POST['jabatandelete'] != '') {
					$this->deleteJabatan($_POST['jabatandelete']);
				}
				if (isset($_POST['jabatandata']) && $_POST['jabatandata'] != '') {
					$this->saveJabatan($_POST['jabatandata'], $suratkeluarid);
				}
				if (isset($_POST['lampirandelete']) && $_POST['lampirandelete'] != '') {
					$this->deleteLampiran($_POST['lampirandelete']);
				}
				if (isset($_POST['lampirandata']) && $_POST['lampirandata'] != '') {
					$this->saveLampiran($_POST['lampirandata'], $suratkeluarid);
				}
				if (isset($_POST['tembusandelete']) && $_POST['tembusandelete'] != '') {
					$this->deleteTembusan($_POST['tembusandelete']);
				}
				if (isset($_POST['tembusandata']) && $_POST['tembusandata'] != '') {
					$this->saveTembusan($_POST['tembusandata'], $suratkeluarid);
				}
				if (isset($_POST['filedelete']) && $_POST['filedelete'] != '') {
					$this->deleteFiles($_POST['filedelete']);
				}
				if (isset($_POST['filedata']) && $_POST['filedata'] != '') {
					$result = $this->saveFiles($_POST['filedata'], $suratkeluarid);
				}
				$this->redirect(array('suratkeluar/admin', 'filterunitid'=>$model->unitid));
            }
        }else{
			$model->bagianid = Yii::app()->user->getinfouser('bagianid');
			$model->subbagianid = Yii::app()->user->getinfouser('subbagianid');
            //$model->indextahun = Yii::app()->user->getTahun();
			$model->tglpenyerahan = $this->dateIndo($model->tglpenyerahan);
            $model->tglsurat = $this->dateIndo($model->tglsurat);
            $model->tglkirim = $this->dateIndo($model->tglkirim);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($suratkeluarid) {
        $model = $this->loadModel($suratkeluarid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Suratkeluar'])) {
            $model->attributes = $_POST['Suratkeluar'];
            //$model->pegawaiid = Yii::app()->user->getinfouser('pegawaiid');
            $model->tglpenyerahan = $this->datePostgres($model->tglpenyerahan);
            $model->tglsurat = $this->datePostgres($model->tglsurat);
            $model->tglkirim = $this->datePostgres($model->tglkirim);
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				if (isset($_POST['detaildelete']) && $_POST['detaildelete'] != '') {
					$this->deleteDetail($_POST['detaildelete']);
				}
				if (isset($_POST['detaildata']) && $_POST['detaildata'] != '') {
					$this->saveDetail($_POST['detaildata'], $suratkeluarid);
				}
				if (isset($_POST['jabatandelete']) && $_POST['jabatandelete'] != '') {
					$this->deleteJabatan($_POST['jabatandelete']);
				}
				if (isset($_POST['jabatandata']) && $_POST['jabatandata'] != '') {
					$this->saveJabatan($_POST['jabatandata'], $suratkeluarid);
				}
				if (isset($_POST['lampirandelete']) && $_POST['lampirandelete'] != '') {
					$this->deleteLampiran($_POST['lampirandelete']);
				}
				if (isset($_POST['lampirandata']) && $_POST['lampirandata'] != '') {
					$this->saveLampiran($_POST['lampirandata'], $suratkeluarid);
				}
				if (isset($_POST['tembusandelete']) && $_POST['tembusandelete'] != '') {
					$this->deleteTembusan($_POST['tembusandelete']);
				}
				if (isset($_POST['tembusandata']) && $_POST['tembusandata'] != '') {
					$this->saveTembusan($_POST['tembusandata'], $suratkeluarid);
				}
				if (isset($_POST['filedelete']) && $_POST['filedelete'] != '') {
					$this->deleteFiles($_POST['filedelete']);
				}
				if (isset($_POST['filedata']) && $_POST['filedata'] != '') {
					$result = $this->saveFiles($_POST['filedata'], $suratkeluarid);
				}
				//Globals::AdminLogging("update:unit:" . $model->unitid . "");
               	//$this->redirect(array('suratkeluar/	admin'));
				$this->redirect(array('suratkeluar/admin', 'filterunitid'=>$model->unitid));
            }
        }
        else {
            $model->tglpenyerahan = $this->dateIndo($model->tglpenyerahan);
            $model->tglsurat = $this->dateIndo($model->tglsurat);
            $model->tglkirim = $this->dateIndo($model->tglkirim);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionLoadSuratkeluarJabatan() {
        $sql = "";
		$filter = "and suratkeluarid='".$_POST['id']."'";
		$sql = "SELECT a.*, b.nama as jabatan FROM ttsuratkeluarjabatan a
				left outer join tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
				where a.dlt='0' $filter order by b.nama";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo json_encode($rows);
    }

    public function actionLoadSuratkeluarDetail() {
        $sql = "";
		$filter = "and suratkeluarid='".$_POST['id']."'";
		$sql = "SELECT * FROM ttsuratkeluardetail where dlt='0' $filter order by indeknomor";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

		echo json_encode($rows);
    }

    public function actionLoadSuratkeluarLampiran() {
        $sql = "";
		$filter = "and suratkeluarid='".$_POST['id']."'";
		$sql = "SELECT * FROM ttsuratkeluarlampiran where dlt='0' $filter order by nourut";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

		echo json_encode($rows);
    }

    public function actionLoadSuratkeluarTembusan() {
        $sql = "";
		$filter = "and suratkeluarid='".$_POST['id']."'";
		$sql = "SELECT * FROM ttsuratkeluartembusan where dlt='0' $filter order by nourut";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

		echo json_encode($rows);
    }
	
	private function saveJabatan($dataparam, $masterid) {
        $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        //suratkeluarjabatanid - suratkeluarid - jabatanid
		        $data = explode('~#|#~', $record);
	            $suratkeluarjabatanid = $data[0];
		        $suratkeluarid = $masterid;
		        $jabatanid = $data[2];
		        $jabatan = $data[3];
		        
				if (!Globals::isDataExist("select count(suratkeluarjabatanid) as count from ttsuratkeluarjabatan where dlt='0' and suratkeluarid='$suratkeluarid' and jabatanid='$jabatanid' ")){
					$modeldetail = new Suratkeluarjabatan;
			        $modeldetail->suratkeluarjabatanid = Globals::newID("ttsuratkeluarjabatan", "suratkeluarjabatanid");
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->jabatanid = $jabatanid;
			        $modeldetail->statusbaca = '0';
			        $modeldetail->keterangan = $jabatan;
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogEditDataInfo());
			        
		            $modeldetail->save();
				}
            }
        }
	}
	
	private function deleteJabatan($dataparam) {
	    $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        $data = explode('~#|#~', $record);
		        $modeldetail = Suratkeluarjabatan::model()->findByPk($data[0]);
				echo $modeldetail->suratkeluarjabatanid.'-'.$data[0];
		        $this->performAjaxValidation($modeldetail);
				$modeldetail->attributes = $this->getLogDeleteDataInfo();
		        $modeldetail->save();
            }
        }
	}
	
	private function saveDetail($dataparam, $masterid) {
        $records = explode('~#;#~', $dataparam);
        $nourut = 1;
		foreach ($records as $record) {
            if ($record != null) {
		        //suratkeluardetailid - suratkeluarid - kode - indekkode1 - indekkode2 - indeknomor - indekbulan - indektahun - klasifikasi
		        $data = explode('~#|#~', $record);
	            $suratkeluardetailid = $data[0];
		        $suratkeluarid = $masterid;
		        $kode = $data[2];
		        $indekkode1 = $data[3];
		        $indekkode2 = $data[4];
		        $indeknomor = $data[5];
		        $indekbulan = $data[6];
		        $indektahun = $data[7];
		        $klasifikasi = $data[8];
		        $perihal = $data[9];
		        $tglsurat = $data[10];
		        $jenisid = $data[11];
		        
				if ($suratkeluardetailid == '' || substr($suratkeluardetailid, 0, 5) == 'tmp__'){
			        $modeldetail = new Suratkeluardetail;
			        $modeldetail->suratkeluardetailid = Globals::newID("ttsuratkeluardetail", "suratkeluardetailid");
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->jenisid = $jenisid;
			        $modeldetail->kode = $kode;
			        $modeldetail->indekkode1 = $indekkode1;
			        $modeldetail->indekkode2 = $indekkode2;
			        $modeldetail->indeknomor = $indeknomor;
			        $modeldetail->indekbulan = $indekbulan;
			        $modeldetail->indektahun = $indektahun;
		            $modeldetail->tglsurat = $this->datePostgres($tglsurat);
			        $modeldetail->klasifikasi = $klasifikasi;
			        $modeldetail->perihal = $perihal;
			        $modeldetail->keterangan = '';
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogAddDataInfo());
			        
		            $modeldetail->save();
				}
				else {
			        $modeldetail = Suratkeluardetail::model()->find('LOWER(suratkeluardetailid)=? ',array($suratkeluardetailid));
			        $modeldetail->suratkeluardetailid = $suratkeluardetailid;
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->jenisid = $jenisid;
			        $modeldetail->kode = $kode;
			        $modeldetail->indekkode1 = $indekkode1;
			        $modeldetail->indekkode2 = $indekkode2;
			        $modeldetail->indeknomor = $indeknomor;
			        $modeldetail->indekbulan = $indekbulan;
			        $modeldetail->indektahun = $indektahun;
		            $modeldetail->tglsurat = $this->datePostgres($tglsurat);
			        $modeldetail->klasifikasi = $klasifikasi;
			        $modeldetail->perihal = $perihal;
			        $modeldetail->keterangan = '';
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogEditDataInfo());
			        
		            $modeldetail->save();
				}
				
				$nourut++;
            }
        }
	}
	
	private function deleteDetail($dataparam) {
	    $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        $data = explode('~#|#~', $record);
		        $modeldetail = Suratkeluardetail::model()->findByPk($data[0]);

		        $this->performAjaxValidation($modeldetail);
				$modeldetail->attributes = $this->getLogDeleteDataInfo();
		        $modeldetail->save();
            }
        }
	}
	
	private function saveLampiran($dataparam, $masterid) {
        $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        //suratkeluarlampiranid - suratkeluarid - lampiranid
		        $data = explode('~#|#~', $record);
	            $suratkeluarlampiranid = $data[0];
		        $suratkeluarid = $masterid;
		        $nourut = $data[2];
		        $lampiran = $data[3];
		        
				if ($suratkeluarlampiranid == '' || substr($suratkeluarlampiranid, 0, 5) == 'tmp__'){
					$modeldetail = new Suratkeluarlampiran;
			        $modeldetail->suratkeluarlampiranid = Globals::newID("ttsuratkeluarlampiran", "suratkeluarlampiranid");
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->nourut = $nourut;
			        $modeldetail->lampiran = $lampiran;
			        $modeldetail->statusbaca = '0';
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogAddDataInfo());
			        
		            $modeldetail->save();
				}
				else {
			        $modeldetail = Suratkeluarlampiran::model()->find('LOWER(suratkeluarlampiranid)=? ',array($suratkeluarlampiranid));
			        $modeldetail->suratkeluarlampiranid = $suratkeluarlampiranid;
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->nourut = $nourut;
			        $modeldetail->lampiran = $lampiran;
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogEditDataInfo());
			        
		            $modeldetail->save();
				}
            }
        }
	}
	
	private function deleteLampiran($dataparam) {
	    $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        $data = explode('~#|#~', $record);
		        $modeldetail = Suratkeluarlampiran::model()->findByPk($data[0]);
				echo $modeldetail->suratkeluarlampiranid.'-'.$data[0];
		        $this->performAjaxValidation($modeldetail);
				$modeldetail->attributes = $this->getLogDeleteDataInfo();
		        $modeldetail->save();
            }
        }
	}
	
	private function saveTembusan($dataparam, $masterid) {
        $records = explode('~#;#~', $dataparam);
        $nourut = 1;
		foreach ($records as $record) {
            if ($record != null) {
		        //suratkeluartembusanid - suratkeluarid - tembusanid
		        $data = explode('~#|#~', $record);
	            $suratkeluartembusanid = $data[0];
		        $suratkeluarid = $masterid;
		        $nourut = $data[2];
		        $tembusan = $data[3];
		        
				if ($suratkeluartembusanid == '' || substr($suratkeluartembusanid, 0, 5) == 'tmp__'){
			        $modeldetail = new Suratkeluartembusan;
			        $modeldetail->suratkeluartembusanid = Globals::newID("ttsuratkeluartembusan", "suratkeluartembusanid");
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->nourut = $nourut;
			        $modeldetail->tembusan = $tembusan;
			        $modeldetail->keterangan = '';
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogAddDataInfo());
			        
		            $modeldetail->save();
				}
				else {
			        $modeldetail = Suratkeluartembusan::model()->find('LOWER(suratkeluartembusanid)=? ',array($suratkeluartembusanid));
			        $modeldetail->suratkeluartembusanid = $suratkeluartembusanid;
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->nourut = $nourut;
			        $modeldetail->tembusan = $tembusan;
			        $modeldetail->keterangan = '';
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogEditDataInfo());
			        
		            $modeldetail->save();
				}
				
				$nourut++;
            }
        }
	}
	
	private function deleteTembusan($dataparam) {
	    $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        $data = explode('~#|#~', $record);
		        $modeldetail = Suratkeluartembusan::model()->findByPk($data[0]);

		        $this->performAjaxValidation($modeldetail);
				$modeldetail->attributes = $this->getLogDeleteDataInfo();
		        $modeldetail->save();
            }
        }
	}

    public function actionPosting($suratkeluarid) {
        $model = $this->loadModel($suratkeluarid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Suratkeluar'])) {
            $model->attributes = $_POST['Suratkeluar'];
            $model->status = '1';
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				$this->saveHistorySuratkeluar($model->suratkeluarid, Yii::app()->user->getinfouser('pegawaiid'), $model->status, '-', '');
				$sql = "SELECT b.bagianid, b.subbagianid
						FROM ttsuratkeluarjabatan a 
						inner join tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
						where a.dlt='0' 
						and a.suratkeluarid = '".$suratkeluarid."'
						";
		        $rows = Yii::app()->db->createCommand($sql)->queryAll();
				if (count($rows) > 0) {
			        foreach ($rows as $row) {
						$this->saveNotificationSubbagian($model->suratkeluarid, $row['bagianid'], $row['subbagianid'], '5', 'Surat Keluar Verifikasi Kasubbag', 'Surat keluar  telah diverifikasi oleh Kasubbag', 'verifikasikasubbag', 'posting', 'suratkeluarid');
					}
				}
				$this->redirect(array('suratkeluar/admin', 'filterunitid'=>$model->unitid));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('posting', array(
            'model' => $model,
        ));
    }

    public function actionUpload1() {
		$folder_name = 'files/';
		
		for ($icounter = 0; $icounter < count($_FILES); $icounter++) {
			if (!empty($_FILES)) {
	            $fileName = $_FILES['upload']['name']; //get the file name
	            $fileSize = $_FILES['upload']['size']; //get the size
	            $fileError = $_FILES['upload']['error']; //get the error when upload
	            $file = $_FILES['upload'];
	            $extension = str_replace(".", "", substr($fileName, strlen($fileName) - 4, 4));
				$newfiles = '';
				
	            if ($fileSize > 0 || $fileError == 0) { //check if the file is corrupt or error
					$temp_file = $_FILES['upload']['tmp_name'];
					$newfiles = Globals::newID("ttsuratkeluar", "suratkeluarid") . '.' . $extension;
					$location = $folder_name . $newfiles;// $_FILES['upload']['name'];
					move_uploaded_file($temp_file, $location);
				}
			}
			
			$result = array();
		}
		
		echo json_encode(array('r'=>'1', 'error' => '0', 'files' => $newfiles, 'filename' => $fileName));
    }

    public function actionUpload($suratkeluarid) {
        $model = $this->loadModel($suratkeluarid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST)) {
			$result = false;
			if (isset($_POST['filedelete']) && $_POST['filedelete'] != '') {
				$this->deleteFiles($_POST['filedelete']);
			}
			if (isset($_POST['filedata']) && $_POST['filedata'] != '') {
				$result = $this->saveFiles($_POST['filedata'], $suratkeluarid);
			}
			if ($result) {
				$this->redirect(array('suratkeluar/admin'));
			}
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('upload', array(
            'model' => $model,
        ));
    }
	
	private function saveFiles($dataparam, $masterid) {
        $records = explode('~#;#~', $dataparam);
        $nourut = 1;
		$result = false;
		
		foreach ($records as $record) {
            if ($record != null) {
		        //filesid
		        $data = explode('~#|#~', $record);
	            $files = $data[0];
	            $filename = $data[1];
		        $suratkeluarid = $masterid;
				
				if (!Globals::isDataExist("select count(suratkeluarfilesid) as count from ttsuratkeluarfiles where dlt='0' and suratkeluarid='$suratkeluarid' and filename='$filename' ")){
			        $modeldetail = new Suratkeluarfiles;
			        $modeldetail->suratkeluarfilesid = Globals::newID("ttsuratkeluarfiles", "suratkeluarfilesid");
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->files = $files;
			        $modeldetail->nourut = $nourut;
			        $modeldetail->filename = $filename;
			        $modeldetail->keterangan = '';
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogEditDataInfo());
			        
		            $result = $modeldetail->save();
				}
				
				$nourut++;
            }
        }
		
		return $result;
	}
	
	private function deleteFiles($dataparam) {
	    $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        $data = explode('~#|#~', $record);
		        if ($data[0] != '') {
					$modeldetail = Suratkeluarfiles::model()->findByPk($data[0]);
					if (!is_null($modeldetail)) {
				        $this->performAjaxValidation($modeldetail);
						$modeldetail->attributes = $this->getLogDeleteDataInfo();
				        $modeldetail->save();
					}
				}
            }
        }
	}

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($suratkeluarid) {
        $model = $this->loadModel($suratkeluarid);
		$unitid = $model->unitid;
        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:unit:" . $model->unitid . "");
           	$this->redirect(array('suratkeluar/admin', 'filterunitid'=>""));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Suratkeluar');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Suratkeluar::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin($unitid="") {
        $model = new Suratkeluar('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Suratkeluar'])) {
            $model->attributes = $_GET['Suratkeluar'];
        }
		//if (isset($_GET['filterunitid'])) $model->unitid = $_GET['filterunitid'];
		//else if ($unitid!="") $model->unitid = $unitid;
		$criteria = new CDbCriteria();
        $criteria->select = "t.*, 
							case(coalesce(t.status, 0)) 
								when ".enumVar::STEP_O_POSTED." then '".enumVar::STEP_O_DESC_POSTED."' 
								when ".enumVar::STEP_O_KAUNIT_VERIFIED." then '".enumVar::STEP_O_DESC_KAUNIT_VERIFIED."' 
								when ".enumVar::STEP_O_KASUBUNIT_VERIFIED." then '".enumVar::STEP_O_DESC_KASUBUNIT_VERIFIED."' 
								when ".enumVar::STEP_O_KABID_VERIFIED." then '".enumVar::STEP_O_DESC_KABID_VERIFIED."' 
								when ".enumVar::STEP_O_KASUBBID_VERIFIED." then '".enumVar::STEP_O_DESC_KASUBBID_VERIFIED."' 
								when ".enumVar::STEP_O_KABAG_VERIFIED." then '".enumVar::STEP_O_DESC_KABAG_VERIFIED."' 
								when ".enumVar::STEP_O_KASUBBAG_VERIFIED." then '".enumVar::STEP_O_DESC_KASUBBAG_VERIFIED."' 
								when ".enumVar::STEP_O_FINISHED." then '".enumVar::STEP_O_DESC_FINISHED."' 
								when ".enumVar::STEP_O_CANCELLED." then '".enumVar::STEP_O_DESC_CANCELLED."' 
								when ".enumVar::STEP_O_DENIED." then '".enumVar::STEP_O_DESC_DENIED."' 
								else '".enumVar::STEP_O_DESC_DRAFT."' 
								end as ketstatus";
		$criteria->addCondition("t.dlt = '0'");
		$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.kode LIKE :searchtext or t.nama LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		if (isset($_GET['filterbagianid']) && $_GET['filterbagianid'] != 'all') {
			//$criteria->addCondition("t.bagianid = '".$_GET['filterbagianid']."' ");
		}
		elseif (!isset($_GET['filterbagianid'])) {
			$model->bagianid = Yii::app()->user->getinfouser('bagianid');
			//$criteria->addCondition("t.bagianid = '".$model->bagianid."' ");
		}
		if (isset($_GET['filtersubbagianid']) && $_GET['filtersubbagianid'] != 'all') {
			//$criteria->addCondition("t.subbagianid = '".$_GET['filtersubbagianid']."' ");
		}
		elseif (!isset($_GET['filtersubbagianid'])) {
			$model->subbagianid = Yii::app()->user->getinfouser('subbagianid');
			//$criteria->addCondition("t.subbagianid = '".$model->subbagianid."' ");
		}
		if (isset($_GET['filterstatus']) && $_GET['filterstatus'] != 'all') {
			$criteria->addCondition("t.status = '".$_GET['filterstatus']."' ");
		}
		elseif (!isset($_GET['filterstatus'])) {
			$model->status = '0';
			$criteria->addCondition("t.status = '".$model->status."' ");
		}
		
		if (!Yii::app()->user->isSuperadmin()) {
			$criteria->addCondition("t.pegawaiid = '".Yii::app()->user->getinfouser('pegawaiid')."' ");
		}
		/*if ($model->unitid != '' && $model->unitid != '-') {
            $criteria->addCondition("t.unitid = '".$model->unitid."' ");
        }*/
        //$criteria->join .= " LEFT JOIN tmbagian b ON b.bagianid=t.bagianid and b.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmsubbagian sb ON sb.subbagianid=t.subbagianid and sb.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmunit u ON u.unitid=t.unitid and u.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmsubunit su ON su.subunitid=t.subunitid and su.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Suratkeluar', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 't.indexnomor',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'suratkeluar-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadUnit() {
        $sql = "";
		$filter = "";
		$modeluser = Users::model()->findByPk(Yii::app()->user->id);
		if ($modeluser->grup=="2"){
			if (!is_null($modeluser->unitid) && $modeluser->unitid != '' && $modeluser->unitid != '-') {
	            $filter .= " and unitid = '".$modeluser->unitid."' ";
	        }
		}
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as unitid,'--FILTER SKPD--' as unitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.unitid,concat(c.kode, b.kode, a.kode,' ',a.nama) as unitvw, 2 as urutan 
				FROM tmunit a 
				inner join tmurusan b on b.urusanid = a.urusanid and b.dlt = '0'
				inner join tmkelurusan c on c.kelurusanid = b.kelurusanid and c.dlt = '0'
				where a.dlt='0' 
				and a.tahun = '".Yii::app()->user->getTahun()."'
				order by urutan, unitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['unitid']), CHtml::encode($row['unitvw']), true);
        }
    }

    public function actionLoadSubunit() {
        $sql = "";
		$filter = (isset($_POST['unitid']) && $_POST['unitid']!="") ? " and unitid='$_POST[unitid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subunitid,'--FILTER UNIT KERJA--' as subunitvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subunitid,'-' as subunitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subunitid,concat(kode,' ',nama) as subunitvw, 2 as urutan FROM tmsubunit where dlt='0' $filter order by urutan, subunitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subunitid']), CHtml::encode($row['subunitvw']), true);
        }
    }

    public function actionLoadBagian() {
        $sql = "";
		$filter = "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as bagianid,'--FILTER BAGIAN--' as bagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.bagianid,concat(a.kode,' ',a.nama) as bagianvw, 2 as urutan 
				FROM tmbagian a 
				where a.dlt='0' 
				
				order by urutan, bagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['bagianid']), CHtml::encode($row['bagianvw']), true);
        }
    }

    public function actionLoadSubbagian() {
        $sql = "";
		$filter = (isset($_POST['bagianid']) && $_POST['bagianid']!="") ? " and bagianid='$_POST[bagianid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subbagianid,'--FILTER SUB BAGIAN--' as subbagianvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subbagianid,'-' as subbagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subbagianid,concat(kode,' ',nama) as subbagianvw, 2 as urutan FROM tmsubbagian where dlt='0' $filter order by urutan, subbagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subbagianid']), CHtml::encode($row['subbagianvw']), true);
        }
    }

    public function actionLoadPejabat() {
        $sql = "";
		$filter = "";
		$sql = " SELECT '' as id,'' as value,'--PILIH PEJABAT--' as text, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.pegawaiid as id,a.kode as value, a.nama as text, 2 as urutan 
				FROM tmpegawai a 
				inner join tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
				where a.dlt='0' and b.level = '4'
				order by urutan, text";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		echo json_encode($rows);

        /*foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['id']), CHtml::encode($row['text']), true);
        }*/
    }

    public function actionLoadJenis() {
        $sql = "";
		$filter = "";
		$sql = " SELECT '' as id,'' as value,'--PILIH JENIS SURAT--' as text, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.jenisid as id,a.kode as value, concat(a.kode,' - ',a.nama) as text, 2 as urutan 
				FROM tmjenis a 
				where a.dlt='0' 				
				order by urutan, text";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		echo json_encode($rows);

        /*foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['jenisid']), CHtml::encode($row['jenisvw']), true);
        }*/
    }

    public function actionLoadSuratkeluarFiles() {
        $sql = "";
		$filter = "and suratkeluarid='".$_POST['id']."'";
		$sql = "SELECT * FROM ttsuratkeluarfiles where dlt='0' $filter order by nourut";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

		echo json_encode($rows);
    }

    public function actionLoadJabatan() {
        $sql = "";
		$filter = "";
		//$filter = (isset($_POST['bagianid']) && $_POST['bagianid']!="") ? " and bagianid='$_POST[bagianid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as jabatanid,'--FILTER JABATAN--' as jabatanvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as jabatanid,'-' as jabatanvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT jabatanid,nama as jabatanvw, 2 as urutan FROM tmjabatan where dlt='0'and level='5' and coalesce(terimadisposisi, 0) = 1 $filter order by urutan, jabatanvw";
		
		$sql = "SELECT jabatanid, nama, keterangan FROM tmjabatan where dlt='0' and level='5' and coalesce(terimadisposisi, 0) = 1 $filter order by nama";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        //foreach ($rows as $row) {
            //echo CHtml::tag('option', array('value' => $row['jabatanid']), CHtml::encode($row['jabatanvw']), true);
        //}
		
		$hasil = $this->ExportToJSONDataSource(array("data" => $rows));
		echo $hasil;
    }
	
	public function actionKode() {
        try {
            $sql = "select coalesce(max(cast(replace(kode,'.','') as integer))+1,1) as kode from tmbagian where dlt='0' ";
            $nextKode = "01.";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextKode = $result['kode'];
            }

            while (strlen($nextKode) < 2) {
                $nextKode = "0" . $nextKode;
            }

            echo $nextKode . ".";
        } catch (Exception $e) {
            echo "01.";
        }
    }
	
	public function actionGetNextNo() {
		echo $this->getNextNo($_POST['jenisid']);
    }
	
	public function getNextNo($jenisid) {
        try {
            $sql = "select coalesce(max(cast(replace(indexnomor,'.','') as integer))+1,1) as indexnomor from ttsuratkeluar where jenisid = '".$jenisid."' and dlt='0' ";
            $nextNomor = "1";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextNomor = $result['indexnomor'];
            }
			
            return $nextNomor;
        } catch (Exception $e) {
            return "1";
        }
    }
	
	public function actionPrint($suratkeluarid)
    {
		$modelprint = new Suratkeluar;
		$modelprint->previewpdf($suratkeluarid);
    }
}