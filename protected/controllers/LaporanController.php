<?php

class LaporanController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'index', 'view', 'print'),
                'expression' => '$user->getprivileges(\'view\',\'201\') || $user->isSuperadmin()',
            ),
            array('allow',
                'actions' => array('print'),
                'expression' => '$user->getprivileges(\'view\',\'61\') || $user->isSuperadmin()',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        //$dataProvider = new CActiveDataProvider('Rptskaskpd');
        $this->render('index', array(
            //'dataProvider' => $dataProvider,
        ));
    }
   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Rptskaskpd::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin() {
        //$dataProvider = new CActiveDataProvider('Rptskaskpd');
        $this->render('admin', array(
            //'dataProvider' => $dataProvider,
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'akses-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
	
	public function actionPrint() {
		$jenis = '';
		$token = '';
		$bulan = 'all';
		$tahun = date('Y');
		
		if (!isset($_GET['jenis'])) {
			echo 'Tidak ada';
			return;
		}
		else {
			$jenis = $_GET['jenis'];
		}
		if (!isset($_GET['token'])) {
			echo 'Tidak ada token';
			return;
		}
		else {
			$token = $_GET['token'];
		}
		if (isset($_GET['bulan'])) {
			$bulan = $_GET['bulan'];
		}
		if (isset($_GET['tahun'])) {
			$tahun = $_GET['tahun'];
		}
		if (!isset($_GET['token'])) {
			echo 'Tidak ada token';
			return;
		}
		if ($_GET['jenis'] == 'suratmasuk') {
			$this->printRptSuratMasuk($token, $bulan, $tahun);
		}
		elseif ($_GET['jenis'] == 'disposisikabag') {
			$this->printRptDisposisiKabag($token, $bulan, $tahun);
		}
		elseif ($_GET['jenis'] == 'disposisikasubbag') {
			$this->printRptDisposisiKasubbag($token, $bulan, $tahun);
		}
		elseif ($_GET['jenis'] == 'pelaksanaan') {
			$this->printRptPelaksanaan($token, $bulan, $tahun);
		}
		elseif ($_GET['jenis'] == 'suratkeluar') {
			$this->printRptSuratKeluar($token, $bulan, $tahun);
		}
		elseif ($_GET['jenis'] == 'verifikasikabag') {
			$this->printRptVerifikasiKabag($token, $bulan, $tahun);
		}
		elseif ($_GET['jenis'] == 'verifikasikasubbag') {
			$this->printRptVerifikasiKasubbag($token, $bulan, $tahun);
		}
		elseif ($_GET['jenis'] == 'pengiriman') {
			$this->printRptPengiriman($token, $bulan, $tahun);
		}
    }
	
	private function printRptSuratMasuk($token, $bulan, $tahun) {
        //$this->getInstansi();
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");
		
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

        $icounter = 1;
        $listdata = '';
		$sql="select a.*, b.tglverifikasi
			from ttsuratmasuk a
			inner join ttsuratmasukhistory b on b.suratmasukid = a.suratmasukid and b.dlt = '0' 
			where a.dlt='0' and b.status = '".enumVar::STEP_I_POSTED."'
			order by a.tglsurat asc
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdata .= '<tr>';
			$listdata .= '<td style="text-align: center;" width="20">'.$icounter.'.</td><td width="100">'.$result['indexnomor'].'</td><td width="60">'.$this->dateIndo($result['tglverifikasi']).'</td><td width="200">'.$result['perihal'].'</td><td width="120">'.$result['asal'].'</td>';
			$listdata .= '</tr>';
			$icounter++;
		}
		if ($listdata == '') {
			$listdata .= '<tr>';
			$listdata .= '<td width="20"></td><td width="100"></td><td width="60"></td><td width="200"></td><td width="120"></td>';
			$listdata .= '</tr>';
		}
		
		$pdf->SetFont('arial', '', 10);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<p style="text-align: center;"><h3>SURAT MASUK</h3></p>
<table border="0.1" cellpadding="2" cellspacing="0">
	<thead>
		<tr>
		<td width="20" style="text-align: center;">No.</td><td width="100" style="text-align: center;">Nomor</td><td width="60" style="text-align: center;">Tanggal</td><td width="200" style="text-align: center;">Hal</td><td width="120" style="text-align: center;">Asal</td>
		</tr>
	</thead>
	<tbody>
		$listdata
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

		$this->setCookieToken('fileDownloadToken', $token, false);
        $pdf->Output("surat_masuk_posted.pdf", "I");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
	
	private function printRptDisposisiKabag($token, $bulan, $tahun) {
        //$this->getInstansi();
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");
		
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

        $icounter = 1;
        $listdata = '';
		$sql="select a.*, b.tglverifikasi
			from ttsuratmasuk a
			inner join ttsuratmasukhistory b on b.suratmasukid = a.suratmasukid and b.dlt = '0' 
			where a.dlt='0' and b.status = '".enumVar::STEP_I_KABAG_VERIFIED."'
			order by a.tglsurat asc
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdata .= '<tr>';
			$listdata .= '<td style="text-align: center;" width="20">'.$icounter.'.</td><td width="100">'.$result['indexnomor'].'</td><td width="60">'.$this->dateIndo($result['tglverifikasi']).'</td><td width="200">'.$result['perihal'].'</td><td width="120">'.$result['asal'].'</td>';
			$listdata .= '</tr>';
			$icounter++;
		}
		if ($listdata == '') {
			$listdata .= '<tr>';
			$listdata .= '<td width="20"></td><td width="100"></td><td width="60"></td><td width="200"></td><td width="120"></td>';
			$listdata .= '</tr>';
		}
		
		$pdf->SetFont('arial', '', 10);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<p style="text-align: center;"><h3>SURAT MASUK (DISPOSISI KABAG)</h3></p>
<table border="0.1" cellpadding="2" cellspacing="0">
	<thead>
		<tr>
		<td width="20" style="text-align: center;">No.</td><td width="100" style="text-align: center;">Nomor</td><td width="60" style="text-align: center;">Tanggal</td><td width="200" style="text-align: center;">Hal</td><td width="120" style="text-align: center;">Asal</td>
		</tr>
	</thead>
	<tbody>
		$listdata
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

		$this->setCookieToken('fileDownloadToken', $token, false);
        $pdf->Output("surat_masuk_kabag.pdf", "I");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
	
	private function printRptDisposisiKasubbag($token, $bulan, $tahun) {
        //$this->getInstansi();
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");
		
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

        $icounter = 1;
        $listdata = '';
		$sql="select a.*, b.tglverifikasi
			from ttsuratmasuk a
			inner join ttsuratmasukhistory b on b.suratmasukid = a.suratmasukid and b.dlt = '0' 
			where a.dlt='0' and b.status = '".enumVar::STEP_I_KASUBBAG_VERIFIED."'
			order by a.tglsurat asc
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdata .= '<tr>';
			$listdata .= '<td style="text-align: center;" width="20">'.$icounter.'.</td><td width="100">'.$result['indexnomor'].'</td><td width="60">'.$this->dateIndo($result['tglverifikasi']).'</td><td width="200">'.$result['perihal'].'</td><td width="120">'.$result['asal'].'</td>';
			$listdata .= '</tr>';
			$icounter++;
		}
		if ($listdata == '') {
			$listdata .= '<tr>';
			$listdata .= '<td width="20"></td><td width="100"></td><td width="60"></td><td width="200"></td><td width="120"></td>';
			$listdata .= '</tr>';
		}
		
		$pdf->SetFont('arial', '', 10);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<p style="text-align: center;"><h3>SURAT MASUK (DISPOSISI KASUBBAG / KASIE)</h3></p>
<table border="0.1" cellpadding="2" cellspacing="0">
	<thead>
		<tr>
		<td width="20" style="text-align: center;">No.</td><td width="100" style="text-align: center;">Nomor</td><td width="60" style="text-align: center;">Tanggal</td><td width="200" style="text-align: center;">Hal</td><td width="120" style="text-align: center;">Asal</td>
		</tr>
	</thead>
	<tbody>
		$listdata
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

		$this->setCookieToken('fileDownloadToken', $token, false);
        $pdf->Output("surat_masuk_kasubbag.pdf", "I");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
	
	private function printRptPelaksanaan($token, $bulan, $tahun) {
        //$this->getInstansi();
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");
		
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

        $icounter = 1;
        $listdata = '';
		$sql="select a.*, b.tglverifikasi
			from ttsuratmasuk a
			inner join ttsuratmasukhistory b on b.suratmasukid = a.suratmasukid and b.dlt = '0' 
			where a.dlt='0' and b.status = '".enumVar::STEP_I_FINISHED."'
			order by a.tglsurat asc
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdata .= '<tr>';
			$listdata .= '<td style="text-align: center;" width="20">'.$icounter.'.</td><td width="100">'.$result['indexnomor'].'</td><td width="60">'.$this->dateIndo($result['tglverifikasi']).'</td><td width="200">'.$result['perihal'].'</td><td width="120">'.$result['asal'].'</td>';
			$listdata .= '</tr>';
			$icounter++;
		}
		if ($listdata == '') {
			$listdata .= '<tr>';
			$listdata .= '<td width="20"></td><td width="100"></td><td width="60"></td><td width="200"></td><td width="120"></td>';
			$listdata .= '</tr>';
		}
		
		$pdf->SetFont('arial', '', 10);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<p style="text-align: center;"><h3>SURAT MASUK (PELAKSANAAN)</h3></p>
<table border="0.1" cellpadding="2" cellspacing="0">
	<thead>
		<tr>
		<td width="20" style="text-align: center;">No.</td><td width="100" style="text-align: center;">Nomor</td><td width="60" style="text-align: center;">Tanggal</td><td width="200" style="text-align: center;">Hal</td><td width="120" style="text-align: center;">Asal</td>
		</tr>
	</thead>
	<tbody>
		$listdata
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

		$this->setCookieToken('fileDownloadToken', $token, false);
        $pdf->Output("surat_masuk_pelaksanaan.pdf", "I");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
	
	private function printRptSuratKeluar($token, $bulan, $tahun) {
        //$this->getInstansi();
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");
		
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

        $icounter = 1;
        $listdata = '';
		$sql="select a.*, b.tglverifikasi
			from ttsuratkeluar a
			inner join ttsuratkeluarhistory b on b.suratkeluarid = a.suratkeluarid and b.dlt = '0' 
			where a.dlt='0' and b.status = '".enumVar::STEP_O_POSTED."'
			order by a.tglsurat asc
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdata .= '<tr>';
			$listdata .= '<td style="text-align: center;" width="20">'.$icounter.'.</td><td width="100">'.$result['indexnomor'].'</td><td width="60">'.$this->dateIndo($result['tglverifikasi']).'</td><td width="200">'.$result['keterangan'].'</td><td width="120">'.$result['kepada'].'</td>';
			$listdata .= '</tr>';
			$icounter++;
		}
		if ($listdata == '') {
			$listdata .= '<tr>';
			$listdata .= '<td width="20"></td><td width="100"></td><td width="60"></td><td width="200"></td><td width="120"></td>';
			$listdata .= '</tr>';
		}
		
		$pdf->SetFont('arial', '', 10);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<p style="text-align: center;"><h3>SURAT KELUAR</h3></p>
<table border="0.1" cellpadding="2" cellspacing="0">
	<thead>
		<tr>
		<td width="20" style="text-align: center;">No.</td><td width="100" style="text-align: center;">Nomor</td><td width="60" style="text-align: center;">Tanggal</td><td width="200" style="text-align: center;">Keterangan</td><td width="120" style="text-align: center;">Tujuan</td>
		</tr>
	</thead>
	<tbody>
		$listdata
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

		$this->setCookieToken('fileDownloadToken', $token, false);
        $pdf->Output("surat_keluar_posted.pdf", "I");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
	
	private function printRptVerifikasiKabag($token, $bulan, $tahun) {
        //$this->getInstansi();
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");
		
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

        $icounter = 1;
        $listdata = '';
		$sql="select a.*, b.tglverifikasi
			from ttsuratkeluar a
			inner join ttsuratkeluarhistory b on b.suratkeluarid = a.suratkeluarid and b.dlt = '0' 
			where a.dlt='0' and b.status = '".enumVar::STEP_O_POSTED."'
			order by a.tglsurat asc
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdata .= '<tr>';
			$listdata .= '<td style="text-align: center;" width="20">'.$icounter.'.</td><td width="100">'.$result['indexnomor'].'</td><td width="60">'.$this->dateIndo($result['tglverifikasi']).'</td><td width="200">'.$result['keterangan'].'</td><td width="120">'.$result['kepada'].'</td>';
			$listdata .= '</tr>';
			$icounter++;
		}
		if ($listdata == '') {
			$listdata .= '<tr>';
			$listdata .= '<td width="20"></td><td width="100"></td><td width="60"></td><td width="200"></td><td width="120"></td>';
			$listdata .= '</tr>';
		}
		
		$pdf->SetFont('arial', '', 10);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<p style="text-align: center;"><h3>SURAT KELUAR (VERIFIKASI KABAG)</h3></p>
<table border="0.1" cellpadding="2" cellspacing="0">
	<thead>
		<tr>
		<td width="20" style="text-align: center;">No.</td><td width="100" style="text-align: center;">Nomor</td><td width="60" style="text-align: center;">Tanggal</td><td width="200" style="text-align: center;">Keterangan</td><td width="120" style="text-align: center;">Tujuan</td>
		</tr>
	</thead>
	<tbody>
		$listdata
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

		$this->setCookieToken('fileDownloadToken', $token, false);
        $pdf->Output("surat_keluar_kabag.pdf", "I");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
	
	private function printRptVerifikasiKasubbag($token, $bulan, $tahun) {
        //$this->getInstansi();
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");
		
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

        $icounter = 1;
        $listdata = '';
		$sql="select a.*, b.tglverifikasi
			from ttsuratkeluar a
			inner join ttsuratkeluarhistory b on b.suratkeluarid = a.suratkeluarid and b.dlt = '0' 
			where a.dlt='0' and b.status = '".enumVar::STEP_O_KASUBBAG_VERIFIED."'
			order by a.tglsurat asc
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdata .= '<tr>';
			$listdata .= '<td style="text-align: center;" width="20">'.$icounter.'.</td><td width="100">'.$result['indexnomor'].'</td><td width="60">'.$this->dateIndo($result['tglverifikasi']).'</td><td width="200">'.$result['keterangan'].'</td><td width="120">'.$result['kepada'].'</td>';
			$listdata .= '</tr>';
			$icounter++;
		}
		if ($listdata == '') {
			$listdata .= '<tr>';
			$listdata .= '<td width="20"></td><td width="100"></td><td width="60"></td><td width="200"></td><td width="120"></td>';
			$listdata .= '</tr>';
		}
		
		$pdf->SetFont('arial', '', 10);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<p style="text-align: center;"><h3>SURAT KELUAR (VERIFIKASI KASUBBAG / KASIE)</h3></p>
<table border="0.1" cellpadding="2" cellspacing="0">
	<thead>
		<tr>
		<td width="20" style="text-align: center;">No.</td><td width="100" style="text-align: center;">Nomor</td><td width="60" style="text-align: center;">Tanggal</td><td width="200" style="text-align: center;">Keterangan</td><td width="120" style="text-align: center;">Tujuan</td>
		</tr>
	</thead>
	<tbody>
		$listdata
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

		$this->setCookieToken('fileDownloadToken', $token, false);
        $pdf->Output("surat_keluar_kasubbag.pdf", "I");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
	
	private function printRptPengiriman($token, $bulan, $tahun) {
        //$this->getInstansi();
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");
		
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

        $icounter = 1;
        $listdata = '';
		$sql="select a.*, b.tglverifikasi
			from ttsuratkeluar a
			inner join ttsuratkeluarhistory b on b.suratkeluarid = a.suratkeluarid and b.dlt = '0' 
			where a.dlt='0' and b.status = '".enumVar::STEP_O_FINISHED."'
			order by a.tglsurat asc
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdata .= '<tr>';
			$listdata .= '<td style="text-align: center;" width="20">'.$icounter.'.</td><td width="100">'.$result['indexnomor'].'</td><td width="60">'.$this->dateIndo($result['tglverifikasi']).'</td><td width="200">'.$result['keterangan'].'</td><td width="120">'.$result['kepada'].'</td>';
			$listdata .= '</tr>';
			$icounter++;
		}
		if ($listdata == '') {
			$listdata .= '<tr>';
			$listdata .= '<td width="20"></td><td width="100"></td><td width="60"></td><td width="200"></td><td width="120"></td>';
			$listdata .= '</tr>';
		}
		
		$pdf->SetFont('arial', '', 10);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<p style="text-align: center;"><h3>SURAT KELUAR (PENGIRIMAN)</h3></p>
<table border="0.1" cellpadding="2" cellspacing="0">
	<thead>
		<tr>
		<td width="20" style="text-align: center;">No.</td><td width="100" style="text-align: center;">Nomor</td><td width="60" style="text-align: center;">Tanggal</td><td width="200" style="text-align: center;">Keterangan</td><td width="120" style="text-align: center;">Tujuan</td>
		</tr>
	</thead>
	<tbody>
		$listdata
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

		$this->setCookieToken('fileDownloadToken', $token, false);
        $pdf->Output("surat_keluar_pengiriman.pdf", "I");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
    
    function setCookieToken(
	    $cookieName, $cookieValue, $httpOnly = true, $secure = false ) {

	    // See: http://stackoverflow.com/a/1459794/59087
	    // See: http://shiflett.org/blog/2006/mar/server-name-versus-http-host
	    // See: http://stackoverflow.com/a/3290474/59087
	    setcookie(
	        $cookieName,
	        $cookieValue,
	        2147483647,            // expires January 1, 2038
	        "/",                   // your path
	        $_SERVER["HTTP_HOST"], // your domain
	        $secure,               // Use true over HTTPS
	        $httpOnly              // Set true for $AUTH_COOKIE_NAME
	    );
	}
}