<?php

class PesanController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            //========================
            
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin', 'transfer', 'verifikasi', 'view_verifikasi'),
//                'users' => array('admin'),
//            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($pesanid) {
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('view', array(
            //'model' => $this->loadModel($pesanid),
            'model' => new Pesan, //temporary
        ));
    }

    public function actionCreate() {
        $model = new Pesan;

        $this->performAjaxValidation($model);
        if (isset($_POST['Pesan'])) {
            $model->attributes = $_POST['Pesan'];
			$pesanid = Globals::newID("tmpesan", "pesanid");
            $model->pesanid = $pesanid;
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
                //Globals::AdminLogging("create:unit:" . $model->pesanid . "");
                //$this->redirect(array('view', 'pesanid' => $model->pesanid));
				$this->redirect(array('pesan/admin'));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($pesanid) {
        $model = $this->loadModel($pesanid);

        $this->performAjaxValidation($model);

        if (isset($_POST['Pesan'])) {
            $model->attributes = $_POST['Pesan'];
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				//Globals::AdminLogging("update:tahap:" . $model->pesanid . "");
               	 $this->redirect(array('pesan/admin'));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($pesanid) {
		$model = $this->loadModel($pesanid);

        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:tahap:" . $model->pesanid . "");
           	$this->redirect(array('pesan/admin'));
        }
        //Globals::AdminLogging("delete:member:" . $id . "");

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Pesan');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Pesan::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin() {
        $model = new Pesan('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Pesan'])) {
            $model->attributes = $_GET['Pesan'];
        }

        $criteria = new CDbCriteria();
        $criteria->select = 't.*';
		$criteria->addCondition("dlt = '0'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('judul LIKE :searchtext or keterangan LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
        //$criteria->join .= ' LEFT JOIN tmrole tr ON tr.roleid=t.roleid';
        $dataProvider = new CActiveDataProvider('Pesan', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'judul, keterangan',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pesan-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

   

}
