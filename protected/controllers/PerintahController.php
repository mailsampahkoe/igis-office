<?php

class PerintahController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view'),
                'expression' => '$user->getprivileges(\'view\',\'9\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('loadunit'),
                'expression' => '$user->getprivileges(\'view\',\'9\') || $user->getprivileges(\'view\',\'8\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create','kode'),
                'expression' => '$user->getprivileges(\'create\',\'9\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update'),
                'expression' => '$user->getprivileges(\'edit\',\'9\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'9\') || $user->isSuperadmin()', //data anggota
            ), 
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new Perintah;
		
        $this->performAjaxValidation($model);
        if (isset($_POST['Perintah'])) {
            $model->attributes = $_POST['Perintah'];
			$perintahid = Globals::newID("tmperintah", "perintahid");
            $model->perintahid = $perintahid;
			$model->tahun = Yii::app()->user->getTahun();
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
                //Globals::AdminLogging("create:unit:" . $model->unitid . "");
                //$this->redirect(array('view', 'unitid' => $model->unitid));
				$this->redirect(array('perintah/admin'));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($perintahid) {
        $model = $this->loadModel($perintahid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Perintah'])) {
            $model->attributes = $_POST['Perintah'];
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				//Globals::AdminLogging("update:unit:" . $model->unitid . "");
               	//$this->redirect(array('perintah/	admin'));
				$this->redirect(array('perintah/admin'));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($perintahid) {
        $model = $this->loadModel($perintahid);
        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:unit:" . $model->unitid . "");
           	$this->redirect(array('perintah/admin'));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Perintah');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Perintah::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin() {
        $model = new Perintah('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Perintah'])) {
            $model->attributes = $_GET['Perintah'];
        }
		$criteria = new CDbCriteria();
        $criteria->select = 't.*';
		$criteria->addCondition("t.dlt = '0'");
		//$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.kode LIKE :searchtext or t.nama LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
        $dataProvider = new CActiveDataProvider('Perintah', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 't.nourut, t.nama',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'perintah-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
	
	public function actionKode() {
        try {
            $sql = "select coalesce(max(cast(replace(kode,'.','') as integer))+1,1) as kode from tmperintah where dlt='0' ";
            $nextKode = "1";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextKode = $result['kode'];
            }

            echo $nextKode;
        } catch (Exception $e) {
            echo "1";
        }
    }
}