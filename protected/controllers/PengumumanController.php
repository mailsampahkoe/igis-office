<?php

class PengumumanController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view'),
                'expression' => '$user->getprivileges(\'view\',\'304\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create','kode'),
                'expression' => '$user->getprivileges(\'create\',\'304\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update'),
                'expression' => '$user->getprivileges(\'edit\',\'304\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'304\') || $user->isSuperadmin()', //data anggota
            ), 
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate($pengumumanid="") {
        $model = new Pengumuman;
		
        $this->performAjaxValidation($model);
        if (isset($_POST['Pengumuman'])) {
            $model->attributes = $_POST['Pengumuman'];
			$pengumumanid = Globals::newID("tspengumuman", "pengumumanid");
            $model->pengumumanid = $pengumumanid;
            $model->tglpengumuman = $this->datePostgres($model->tglpengumuman);
            $model->flag = '1';
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
                //Globals::AdminLogging("create:pengumuman:" . $model->pengumumanid . "");
				$this->redirect(array('pengumuman/admin'));
            }
        }else{
            $model->tglpengumuman = $this->dateIndo($model->tglpengumuman);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($pengumumanid) {
        $model = $this->loadModel($pengumumanid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Pengumuman'])) {
            $model->attributes = $_POST['Pengumuman'];
            $model->flag = '1';
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            $model->tglpengumuman = $this->datePostgres($model->tglpengumuman);
            if ($model->save()) {
				//Globals::AdminLogging("update:pengumuman:" . $model->pengumumanid . "");
				$this->redirect(array('pengumuman/admin'));
            }
        }
		else {
            $model->tglpengumuman = $this->dateIndo($model->tglpengumuman);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($pengumumanid) {
        $model = $this->loadModel($pengumumanid);
		$this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:pengumuman:" . $model->pengumumanid . "");
           	$this->redirect(array('pengumuman/admin'));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Pengumuman');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Pengumuman::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin($pengumumanid="") {
        $model = new Pengumuman('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Pengumuman'])) {
            $model->attributes = $_GET['Pengumuman'];
        }
		$criteria = new CDbCriteria();
        $criteria->select = 't.*';
		$criteria->addCondition("t.dlt = '0'");
		//$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.judul LIKE :searchtext or t.keterangan LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
        $dataProvider = new CActiveDataProvider('Pengumuman', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 't.tglpengumuman, t.pengirim',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pengumuman-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}