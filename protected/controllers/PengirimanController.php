<?php

class PengirimanController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view','loadunit','loadsubunit','loadbagian','loadsubbagian','loadsuratkeluarjabatan'),
                'expression' => '$user->getprivileges(\'view\',\'108\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update','posting','upload','upload1'),
                'expression' => '$user->getprivileges(\'edit\',\'108\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('print'),
                'expression' => '$user->getprivileges(\'print\',\'108\') || $user->isSuperadmin()', //data anggota
            ), 
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionUpdate($suratkeluarid) {
        $model = $this->loadModel($suratkeluarid);
		
		if ($model->status != enumVar::getStep(enumVar::STEP_O_FINISHED, 0, 1)) {
			$this->redirect(array('pengiriman/admin'));
		}
		else {
	        $this->performAjaxValidation($model);
	        if (isset($_POST['Suratkeluar'])) {
	            $model->attributes = $_POST['Suratkeluar'];
				$model->tglkirim = $this->datePostgres($model->tglkirim);
				$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
	            if ($model->save()) {
					//Globals::AdminLogging("update:unit:" . $model->unitid . "");
	               	$this->redirect(array('pengiriman/admin'));
	            }
	        }
	        else {
	            $model->tglkirim = $this->dateIndo($model->tglkirim);
			}
			Yii::app()->clientScript->registerCoreScript('jquery.ui');
	        $this->render('update', array(
	            'model' => $model
	        ));
		}
    }

    public function actionPosting($suratkeluarid) {
        $model = $this->loadModel($suratkeluarid);
		
		if ($model->status != enumVar::getStep(enumVar::STEP_O_FINISHED, 0, 1)) {
			$this->redirect(array('pengiriman/admin'));
		}
		else {
	        $this->performAjaxValidation($model);
	        if (isset($_POST['Suratkeluar'])) {
	            $model->attributes = $_POST['Suratkeluar'];
	            $model->status = enumVar::STEP_O_FINISHED;
				$model->tglkirim = $this->datePostgres($model->tglkirim);
				$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
	            if ($model->save()) {
					$this->saveHistorySuratkeluar($model->suratkeluarid, Yii::app()->user->getinfouser('pegawaiid'), $model->status, '-', '');
					//Globals::AdminLogging("update:unit:" . $model->unitid . "");
	               	$this->redirect(array('pengiriman/admin'));
	            }
	        }
	        else {
	            $model->tglkirim = $this->dateIndo($model->tglkirim);
			}
			Yii::app()->clientScript->registerCoreScript('jquery.ui');
	        $this->render('posting', array(
	            'model' => $model
	        ));
		}
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Suratkeluar');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Suratkeluar::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin($unitid="") {
        $model = new Suratkeluar('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Suratkeluar'])) {
            $model->attributes = $_GET['Suratkeluar'];
        }
		//if (isset($_GET['filterunitid'])) $model->unitid = $_GET['filterunitid'];
		//else if ($unitid!="") $model->unitid = $unitid;
		$criteria = new CDbCriteria();
        $criteria->select = "t.*, 
							case(coalesce(t.status, 0)) 
								when ".enumVar::STEP_O_POSTED." then '".enumVar::STEP_O_DESC_POSTED."' 
								when ".enumVar::STEP_O_KAUNIT_VERIFIED." then '".enumVar::STEP_O_DESC_KAUNIT_VERIFIED."' 
								when ".enumVar::STEP_O_KASUBUNIT_VERIFIED." then '".enumVar::STEP_O_DESC_KASUBUNIT_VERIFIED."' 
								when ".enumVar::STEP_O_KABID_VERIFIED." then '".enumVar::STEP_O_DESC_KABID_VERIFIED."' 
								when ".enumVar::STEP_O_KASUBBID_VERIFIED." then '".enumVar::STEP_O_DESC_KASUBBID_VERIFIED."' 
								when ".enumVar::STEP_O_KABAG_VERIFIED." then '".enumVar::STEP_O_DESC_KABAG_VERIFIED."' 
								when ".enumVar::STEP_O_KASUBBAG_VERIFIED." then '".enumVar::STEP_O_DESC_KASUBBAG_VERIFIED."' 
								when ".enumVar::STEP_O_FINISHED." then '".enumVar::STEP_O_DESC_FINISHED."' 
								when ".enumVar::STEP_O_CANCELLED." then '".enumVar::STEP_O_DESC_CANCELLED."' 
								when ".enumVar::STEP_O_DENIED." then '".enumVar::STEP_O_DESC_DENIED."' 
								else '".enumVar::STEP_O_DESC_DRAFT."' 
								end as ketstatus";
		$criteria->addCondition("t.dlt = '0'");
		//$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.kode LIKE :searchtext or t.nama LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		if (isset($_GET['filterbagianid']) && $_GET['filterbagianid'] != 'all') {
			//$criteria->addCondition("t.bagianid = '".$_GET['filterbagianid']."' ");
		}
		elseif (!isset($_GET['filterbagianid'])) {
			$model->bagianid = Yii::app()->user->getinfouser('bagianid');
			//$criteria->addCondition("t.bagianid = '".$model->bagianid."' ");
		}
		if (isset($_GET['filtersubbagianid']) && $_GET['filtersubbagianid'] != 'all') {
			//$criteria->addCondition("t.subbagianid = '".$_GET['filtersubbagianid']."' ");
		}
		elseif (!isset($_GET['filtersubbagianid'])) {
			$model->subbagianid = Yii::app()->user->getinfouser('subbagianid');
			//$criteria->addCondition("t.subbagianid = '".$model->subbagianid."' ");
		}
		if (isset($_GET['filterstatus']) && $_GET['filterstatus'] != 'all') {
			$criteria->addCondition("t.status = '".$_GET['filterstatus']."' ");
		}
		elseif (!isset($_GET['filterstatus'])) {
			$model->status = '3';
			$criteria->addCondition("t.status = '".$model->status."' ");
		}
		/*if ($model->unitid != '' && $model->unitid != '-') {
            $criteria->addCondition("t.unitid = '".$model->unitid."' ");
        }*/
		if (!Yii::app()->user->isSuperadmin()) {
			$criteria->addCondition("t.pegawaiid = '".Yii::app()->user->getinfouser('pegawaiid')."' ");
		}
        //$criteria->join .= " LEFT JOIN tmbagian b ON b.bagianid=t.bagianid and b.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmsubbagian sb ON sb.subbagianid=t.subbagianid and sb.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmunit u ON u.unitid=t.unitid and u.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmsubunit su ON su.subunitid=t.subunitid and su.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Suratkeluar', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 't.indexnomor',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'suratkeluar-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadUnit() {
        $sql = "";
		$filter = "";
		$modeluser = Users::model()->findByPk(Yii::app()->user->id);
		if ($modeluser->grup=="2"){
			if (!is_null($modeluser->unitid) && $modeluser->unitid != '' && $modeluser->unitid != '-') {
	            $filter .= " and unitid = '".$modeluser->unitid."' ";
	        }
		}
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as unitid,'--FILTER SKPD--' as unitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.unitid,concat(c.kode, b.kode, a.kode,' ',a.nama) as unitvw, 2 as urutan 
				FROM tmunit a 
				inner join tmurusan b on b.urusanid = a.urusanid and b.dlt = '0'
				inner join tmkelurusan c on c.kelurusanid = b.kelurusanid and c.dlt = '0'
				where a.dlt='0' 
				and a.tahun = '".Yii::app()->user->getTahun()."'
				order by urutan, unitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['unitid']), CHtml::encode($row['unitvw']), true);
        }
    }

    public function actionLoadSubunit() {
        $sql = "";
		$filter = (isset($_POST['unitid']) && $_POST['unitid']!="") ? " and unitid='$_POST[unitid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subunitid,'--FILTER UNIT KERJA--' as subunitvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subunitid,'-' as subunitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subunitid,concat(kode,' ',nama) as subunitvw, 2 as urutan FROM tmsubunit where dlt='0' $filter order by urutan, subunitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subunitid']), CHtml::encode($row['subunitvw']), true);
        }
    }

    public function actionLoadBagian() {
        $sql = "";
		$filter = "";
		$modeluser = User::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as bagianid,'--FILTER BAGIAN--' as bagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.bagianid,concat(a.kode,' ',a.nama) as bagianvw, 2 as urutan 
				FROM tmbagian a 
				where a.dlt='0' 
				
				order by urutan, bagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['bagianid']), CHtml::encode($row['bagianvw']), true);
        }
    }

    public function actionLoadSubbagian() {
        $sql = "";
		$filter = (isset($_POST['bagianid']) && $_POST['bagianid']!="") ? " and bagianid='$_POST[bagianid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subbagianid,'--FILTER SUB BAGIAN--' as subbagianvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subbagianid,'-' as subbagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subbagianid,concat(kode,' ',nama) as subbagianvw, 2 as urutan FROM tmsubbagian where dlt='0' $filter order by urutan, subbagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subbagianid']), CHtml::encode($row['subbagianvw']), true);
        }
    }

    public function actionLoadJabatan() {
        $sql = "";
		$filter = "";
		//$filter = (isset($_POST['bagianid']) && $_POST['bagianid']!="") ? " and bagianid='$_POST[bagianid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as jabatanid,'--FILTER JABATAN--' as jabatanvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as jabatanid,'-' as jabatanvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT jabatanid,nama as jabatanvw, 2 as urutan FROM tmjabatan where dlt='0' $filter order by urutan, jabatanvw";
		
		$sql = "SELECT jabatanid, nama, keterangan FROM tmjabatan where dlt='0' $filter order by nama";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        //foreach ($rows as $row) {
            //echo CHtml::tag('option', array('value' => $row['jabatanid']), CHtml::encode($row['jabatanvw']), true);
        //}
		
		$hasil = $this->ExportToJSONDataSource(array("data" => $rows));
		echo $hasil;
    }

    public function actionLoadSuratkeluarJabatan() {
        $sql = "";
		$filter = "and suratkeluarid='".$_POST['id']."'";
		$sql = "SELECT a.*, b.nama as jabatan FROM ttsuratkeluarjabatan a
				left outer join tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
				where a.dlt='0' $filter order by b.nama";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		
		echo json_encode($rows);
    }

    public function actionLoadPerintah() {
        $sql = "";
		$filter = "";
		$sql = "SELECT perintahid, nama, keterangan, nourut FROM tmperintah where dlt='0' $filter order by nourut";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		
		$hasil = $this->ExportToJSONDataSource(array("data" => $rows));
		echo $hasil;
    }

    public function actionLoadSuratkeluarPerintah() {
        $sql = "";
		$filter = "and suratkeluarid='".$_POST['id']."'";
		$sql = "SELECT * FROM ttsuratkeluarperintah where dlt='0' $filter order by nourut";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

		echo json_encode($rows);
    }

    public function actionLoadPegawai() {
        $sql = "";
		$filter = "";
		$modeluser = User::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as pegawaiid,'--FILTER PEGAWAI--' as pegawaivw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.pegawaiid,concat(a.kode,' - ',a.nama) as pegawaivw, 2 as urutan 
				FROM tmpegawai a 
				where a.dlt='0' 
				
				order by urutan, pegawaivw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['pegawaiid']), CHtml::encode($row['pegawaivw']), true);
        }
    }

    public function actionLoadSuratkeluarPegawai() {
        $sql = "";
		$filter = "and suratkeluarid='".$_POST['id']."'";
		$sql = "SELECT a.*, concat(b.kode, ' - ', b.nama) as pegawaiview
				FROM ttsuratkeluarpegawai a
				inner join tmpegawai b on b.pegawaiid = a.pegawaiid and b.dlt = '0'
				where a.dlt='0' $filter order by pegawaiview ";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

		echo json_encode($rows);
    }
	
	public function actionKode() {
        try {
            $sql = "select coalesce(max(cast(replace(kode,'.','') as integer))+1,1) as kode from tmbagian where dlt='0' ";
            $nextKode = "01.";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextKode = $result['kode'];
            }

            while (strlen($nextKode) < 2) {
                $nextKode = "0" . $nextKode;
            }

            echo $nextKode . ".";
        } catch (Exception $e) {
            echo "01.";
        }
    }
	
	public function actionPrint($suratkeluarid)
    {
		$modelprint = new Suratkeluar;
		$modelprint->previewpdf($suratkeluarid);
    }

    public function actionUpload1() {
		$folder_name = 'files/';
		
		for ($icounter = 0; $icounter < count($_FILES); $icounter++) {
			if (!empty($_FILES)) {
	            $fileName = $_FILES['upload']['name']; //get the file name
	            $fileSize = $_FILES['upload']['size']; //get the size
	            $fileError = $_FILES['upload']['error']; //get the error when upload
	            $file = $_FILES['upload'];
	            $extension = str_replace(".", "", substr($fileName, strlen($fileName) - 4, 4));
				$newfiles = '';
				
	            if ($fileSize > 0 || $fileError == 0) { //check if the file is corrupt or error
					$temp_file = $_FILES['upload']['tmp_name'];
					$newfiles = Globals::newID("ttsuratkeluar", "suratkeluarid") . '.' . $extension;
					$location = $folder_name . $newfiles;// $_FILES['upload']['name'];
					move_uploaded_file($temp_file, $location);
				}
			}
			
			$result = array();
		}
		
		echo json_encode(array('r'=>'1', 'error' => '0', 'files' => $newfiles, 'filename' => $fileName));
    }

    public function actionUpload($suratkeluarid) {
        $model = $this->loadModel($suratkeluarid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST)) {
			$result = false;
			if (isset($_POST['filedelete']) && $_POST['filedelete'] != '') {
				$this->deleteFiles($_POST['filedelete']);
			}
			if (isset($_POST['filedata']) && $_POST['filedata'] != '') {
				$result = $this->saveFiles($_POST['filedata'], $suratkeluarid);
			}
			if ($result) {
				$this->redirect(array('pengiriman/admin'));
			}
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('upload', array(
            'model' => $model,
        ));
    }
	
	private function saveFiles($dataparam, $masterid) {
        $records = explode('~#;#~', $dataparam);
        $nourut = 1;
		$result = false;
		
		foreach ($records as $record) {
            if ($record != null) {
		        //filesid
		        $data = explode('~#|#~', $record);
	            $files = $data[0];
	            $filename = $data[1];
		        $suratkeluarid = $masterid;
				
				if (!Globals::isDataExist("select count(suratkeluarfilesid) as count from ttsuratkeluarfiles where dlt='0' and suratkeluarid='$suratkeluarid' and filename='$filename' ")){
			        $modeldetail = new Suratkeluarfiles;
			        $modeldetail->suratkeluarfilesid = Globals::newID("ttsuratkeluarfiles", "suratkeluarfilesid");
			        $modeldetail->suratkeluarid = $suratkeluarid;
			        $modeldetail->files = $files;
			        $modeldetail->nourut = $nourut;
			        $modeldetail->filename = $filename;
			        $modeldetail->keterangan = '';
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogEditDataInfo());
			        
		            $result = $modeldetail->save();
				}
				
				$nourut++;
            }
        }
		
		return $result;
	}
	
	private function deleteFiles($dataparam) {
	    $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        $data = explode('~#|#~', $record);
		        if ($data[0] != '') {
					$modeldetail = Suratkeluarfiles::model()->findByPk($data[0]);
					if (!is_null($modeldetail)) {
				        $this->performAjaxValidation($modeldetail);
						$modeldetail->attributes = $this->getLogDeleteDataInfo();
				        $modeldetail->save();
					}
				}
            }
        }
	}
}