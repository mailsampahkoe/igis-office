<?php

class DashboardController extends Controller {

    public $layout = '//layouts/column2';

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
		$sql = "select sum(suratmasuk) as suratmasuk, sum(disposisikasubbag) as disposisikasubbag, sum(disposisikabag) as disposisikabag, sum(selesai) as selesai, sum(semua) as semua 
				from (
					select 
						case(coalesce(t.status, 0)) when ".enumVar::STEP_I_POSTED." then 1 else 0 end as suratmasuk, 
						case(coalesce(t.status, 0)) when ".enumVar::STEP_I_KASUBBAG_VERIFIED." then 1 else 0 end as disposisikasubbag, 
						case(coalesce(t.status, 0)) when ".enumVar::STEP_I_KABAG_VERIFIED." then 1 else 0 end as disposisikabag, 
						case(coalesce(t.status, 0)) when ".enumVar::STEP_I_FINISHED." then 1 else 0 end as selesai, 
						1 as semua
					from ttsuratmasuk t where t.dlt = '0' and coalesce(t.status, 0) <> '".enumVar::STEP_I_DRAFT."'
				) a";
		$ressm = Yii::app()->db->createCommand($sql)->queryAll();
		$sql = "select sum(suratkeluar) as suratkeluar, sum(verifikasikasubbag) as verifikasikasubbag, sum(verifikasikabag) as verifikasikabag, sum(selesai) as selesai, sum(semua) as semua 
				from (
					select 
						case(coalesce(t.status, 0)) when ".enumVar::STEP_O_POSTED." then 1 else 0 end as suratkeluar, 
						case(coalesce(t.status, 0)) when ".enumVar::STEP_O_KASUBBAG_VERIFIED." then 1 else 0 end as verifikasikasubbag, 
						case(coalesce(t.status, 0)) when ".enumVar::STEP_O_KABAG_VERIFIED." then 1 else 0 end as verifikasikabag, 
						case(coalesce(t.status, 0)) when ".enumVar::STEP_O_FINISHED." then 1 else 0 end as selesai, 
						1 as semua
					from ttsuratkeluar t where t.dlt = '0' and coalesce(t.status, 0) <> '".enumVar::STEP_O_DRAFT."'
				) a";
		$ressk = Yii::app()->db->createCommand($sql)->queryAll();
		$sql = "select j.kode, j.nama, concat(j.kode, '-', j.nama) as jenisview, count(t.suratmasukid) as jumlah
                    from ttsuratmasuk t 
                    inner join tmjenis j on j.jenisid = t.jenisid and t.dlt = '0'
                    where t.dlt = '0' and coalesce(t.status, 0) <> '".enumVar::STEP_I_DRAFT."'
                    group by j.kode, j.nama";
		$reschartsm = Yii::app()->db->createCommand($sql)->queryAll();
		$reschartsmnew = array();
		if (count($reschartsm) > 0) {
			foreach($reschartsm as $data) {
				$reschartsmnew[] = array('label'=>$data['jenisview'], 'value'=>$data['jumlah']);
			}
		}
		
		$sql = "select j.kode, j.nama, concat(j.kode, '-', j.nama) as jenisview, count(d.suratkeluardetailid) as jumlah
                    from ttsuratkeluar t
					inner join ttsuratkeluardetail d on d.suratkeluarid = t.suratkeluarid and d.dlt = '0'
                    inner join tmjenis j on j.jenisid = d.jenisid and t.dlt = '0'
                    where t.dlt = '0' and coalesce(t.status, 0) <> '".enumVar::STEP_O_DRAFT."'
                    group by j.kode, j.nama";
		$reschartsk = Yii::app()->db->createCommand($sql)->queryAll();
		$reschartsknew = array();
		if (count($reschartsk) > 0) {
			foreach($reschartsk as $data) {
				$reschartsknew[] = array('label'=>$data['jenisview'], 'value'=>$data['jumlah']);
			}
		}
		
		$this->render('index', array('dashboardsuratmasuk' => $ressm, 'dashboardsuratkeluar' => $ressk, 'chartsuratmasuk' => $reschartsmnew, 'chartsuratkeluar' => $reschartsknew));
    }
}
