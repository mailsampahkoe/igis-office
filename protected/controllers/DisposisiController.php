<?php

class DisposisiController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view','loadunit','loadsubunit','loadbagian','loadsubbagian'),
                'expression' => '$user->getprivileges(\'view\',\'5\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('loadunit'),
                'expression' => '$user->getprivileges(\'view\',\'7\') || $user->getprivileges(\'view\',\'8\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update'),
                'expression' => '$user->getprivileges(\'edit\',\'5\')|| $user->isSuperadmin()', //data anggota
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionUpdate($suratmasukid) {
        $model = $this->loadModel($suratmasukid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Suratmasuk'])) {
        	var_dump($_POST);return;
            $model->attributes = $_POST['Suratmasuk'];
            $model->tglpenyerahan = $this->datePostgres($model->tglpenyerahan);
            $model->tglsurat = $this->datePostgres($model->tglsurat);
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				//Globals::AdminLogging("update:unit:" . $model->unitid . "");
               	//$this->redirect(array('suratmasuk/	admin'));
				$this->redirect(array('suratmasuk/admin', 'filterunitid'=>$model->unitid));
            }
        }
        else {
            $model->tglpenyerahan = $this->dateIndo($model->tglpenyerahan);
            $model->tglsurat = $this->dateIndo($model->tglsurat);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Suratmasuk');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Suratmasuk::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin($unitid="") {
        $model = new Suratmasuk('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Suratmasuk'])) {
            $model->attributes = $_GET['Suratmasuk'];
        }
		//if (isset($_GET['filterunitid'])) $model->unitid = $_GET['filterunitid'];
		//else if ($unitid!="") $model->unitid = $unitid;
		$criteria = new CDbCriteria();
        $criteria->select = 't.*, b.kode as bagiankode, b.nama as bagiannama, sb.kode as subbagiankode, sb.nama as subbagiannama';
		$criteria->addCondition("t.dlt = '0'");
		$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('kode t.kode ILIKE :searchtext or t.nama ILIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		/*if ($model->unitid != '' && $model->unitid != '-') {
            $criteria->addCondition("t.unitid = '".$model->unitid."' ");
        }*/
        $criteria->join .= " LEFT JOIN tmbagian b ON b.bagianid=t.bagianid and b.dlt='0' ";
        $criteria->join .= " LEFT JOIN tmsubbagian sb ON sb.subbagianid=t.subbagianid and sb.dlt='0' ";
        $criteria->join .= " LEFT JOIN tmunit u ON u.unitid=t.unitid and u.dlt='0' ";
        $criteria->join .= " LEFT JOIN tmsubunit su ON su.subunitid=t.subunitid and su.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Suratmasuk', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'b.kode, sb.kode, t.indexnomor',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'suratmasuk-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadUnit() {
        $sql = "";
		$filter = "";
		$modeluser = Users::model()->findByPk(Yii::app()->user->id);
		if ($modeluser->grup=="2"){
			if (!is_null($modeluser->unitid) && $modeluser->unitid != '' && $modeluser->unitid != '-') {
	            $filter .= " and unitid = '".$modeluser->unitid."' ";
	        }
		}
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as unitid,'--FILTER SKPD--' as unitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.unitid,concat(c.kode, b.kode, a.kode,' ',a.nama) as unitvw, 2 as urutan 
				FROM tmunit a 
				inner join tmurusan b on b.urusanid = a.urusanid and b.dlt = '0'
				inner join tmkelurusan c on c.kelurusanid = b.kelurusanid and c.dlt = '0'
				where a.dlt='0' 
				and a.tahun = '".Yii::app()->user->getTahun()."'
				order by urutan, unitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['unitid']), CHtml::encode($row['unitvw']), true);
        }
    }

    public function actionLoadSubunit() {
        $sql = "";
		$filter = (isset($_POST['unitid']) && $_POST['unitid']!="") ? " and unitid='$_POST[unitid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subunitid,'--FILTER UNIT KERJA--' as subunitvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subunitid,'-' as subunitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subunitid,concat(kode,' ',nama) as subunitvw, 2 as urutan FROM tmsubunit where dlt='0' $filter order by urutan, subunitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subunitid']), CHtml::encode($row['subunitvw']), true);
        }
    }

    public function actionLoadBagian() {
        $sql = "";
		$filter = "";
		$modeluser = User::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as bagianid,'--FILTER BAGIAN--' as bagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.bagianid,concat(a.kode,' ',a.nama) as bagianvw, 2 as urutan 
				FROM tmbagian a 
				where a.dlt='0' 
				
				order by urutan, bagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['bagianid']), CHtml::encode($row['bagianvw']), true);
        }
    }

    public function actionLoadSubbagian() {
        $sql = "";
		$filter = (isset($_POST['bagianid']) && $_POST['bagianid']!="") ? " and bagianid='$_POST[bagianid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subbagianid,'--FILTER SUB BAGIAN--' as subbagianvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subbagianid,'-' as subbagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subbagianid,concat(kode,' ',nama) as subbagianvw, 2 as urutan FROM tmsubbagian where dlt='0' $filter order by urutan, subbagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subbagianid']), CHtml::encode($row['subbagianvw']), true);
        }
    }
}