<?php

class UsersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view', 'Lockmenu'),
                'expression' => '$user->getprivileges(\'view\',\'9\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create'),
                'expression' => '$user->getprivileges(\'create\',\'9\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update', 'tolak', 'terima', 'statusprint'),
                'expression' => '$user->getprivileges(\'edit\',\'9\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'9\') || $user->isSuperadmin()', //data anggota
            ), array('allow',
                'actions' => array('resetpassword'),
                'expression' => '$user->isSuperadmin()', //data anggota
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('changepassword', 'uploadimage', 'cropimage',),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionresetpassword() {
        $id = $_POST['id'];
        $model = $this->loadModel($id);
        $model->password = Globals::cryptPassword($model->username);
        if ($model->save()) {
//            Globals::AdminLogging("reset password:users:" . $id . "");
            echo '{"success": true,'
            . '"pesan": "Proses Reset Password Anggota ini Berhasil"'
            . '}';
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Users;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            $userid = Globals::newID("tmuser", "userid");
            $model->userid = $userid;

			
            $model->password = Globals::cryptPassword($model->password);
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			
			if ($model->save()) {
                if ($model->roleid <> '1') {
//                    $userid = $model->getPrimaryKey();

                    $sql = "select a.menuid from tmmenu a
                    left outer join tmusermenu b on a.menuid = b.menuid and b.userid='$userid'
                    where b.menuid is null";
				//	echo $sql;return;
                    $rows = Yii::app()->db->createCommand($sql)->queryAll();
                    foreach ($rows as $row) {

                        $usermenuid = $this->findfield("tmusermenu", "usermenuid", "and menuid='" . $row["menuid"] . "' and userid='" . $userid . "'");
                        if ($usermenuid != "") {
                            $modeldetail = $this->loadModel($usermenuid);
                        } else {
                            $modeldetail = new Usermenu;
                            $modeldetail->usermenuid = Globals::newID("tmusermenu", "usermenuid");
                        }

                        $modeldetail->userid = $userid;
                        $modeldetail->menuid = $row["menuid"];
                        $modeldetail->view = '0';
                        $modeldetail->add = '0';
                        $modeldetail->edit = '0';
                        $modeldetail->del = '0';
                        $modeldetail->print = '0';

                        $modeldetail->save();
                    }
                    $sql = "update tmusermenu set view='0', add='0', edit='0', del='0', print='0' where userid = '$userid' ";
                    Yii::app()->db->createCommand($sql)->execute();


                    if (isset($_POST['vw']) && count($_POST['vw']) > 0) {
                        $ids = implode("','", $_POST['vw']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set view='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }

                    if (isset($_POST['ad']) && count($_POST['ad']) > 0) {
                        $ids = implode("','", $_POST['ad']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set add='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }

                    if (isset($_POST['edit']) && count($_POST['edit']) > 0) {
                        $ids = implode("','", $_POST['edit']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set edit='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                    if (isset($_POST['del']) && count($_POST['del']) > 0) {
                        $ids = implode("','", $_POST['del']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set del='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                    if (isset($_POST['print']) && count($_POST['print']) > 0) {
                        $ids = implode("','", $_POST['print']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set print='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                    //  Globals::AdminLogging("create:users:" . $userid . "");
                    $this->redirect(array('admin'));
                }else{
					$sql = "select a.menuid from tmmenu a
                    left outer join tmusermenu b on a.menuid = b.menuid and b.userid='$userid'
                    where b.menuid is null";
				//	echo $sql;return;
                    $rows = Yii::app()->db->createCommand($sql)->queryAll();
                    foreach ($rows as $row) {

                        $usermenuid = $this->findfield("tmusermenu", "usermenuid", "and menuid='" . $row["menuid"] . "' and userid='" . $userid . "'");
                        if ($usermenuid != "") {
                            $modeldetail = $this->loadModel($usermenuid);
                        } else {
                            $modeldetail = new Usermenu;
                            $modeldetail->usermenuid = Globals::newID("tmusermenu", "usermenuid");
                        }

                        $modeldetail->userid = $userid;
                        $modeldetail->menuid = $row["menuid"];
                        $modeldetail->view = '1';
                        $modeldetail->add = '1';
                        $modeldetail->edit = '1';
                        $modeldetail->del = '1';
                        $modeldetail->print = '1';

                        $modeldetail->save();
                    }
				}
            }
        }
        $model->isaktif = '1';
        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function findfield($table, $field, $where) {
        $sql = "select $field as result from $table where coalesce($field,'')<> '' $where";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        $hasil = "";
        foreach ($results AS $result) {
            $hasil = $result['result'];
        }
        return $hasil;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];

            // $model->password = md5($model->password);
            $imagePath = Yii::app()->basePath . '/../assets/tmp/';
            $imageUrl = Yii::app()->request->baseUrl . '/assets/tmp/';
            $model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
                
                if ($model->roleid <> '1') {
                    $userid = $model->getPrimaryKey();
                    $sql = "select a.menuid from tmmenu a
                    left outer join tmusermenu b on a.menuid = b.menuid and b.userid='$userid'
                    where b.menuid is null";

                    $rows = Yii::app()->db->createCommand($sql)->queryAll();
                    foreach ($rows as $row) {

                        $usermenuid = $this->findfield("tmusermenu", "usermenuid", "and menuid='" . $row["menuid"] . "' and userid='" . $userid . "'");
                        if ($usermenuid != "") {
                            $modeldetail = $this->loadModel($usermenuid);
                        } else {
                            $modeldetail = new Usermenu;
                            $modeldetail->usermenuid = Globals::newID("tmusermenu", "usermenuid");
                        }

                        $modeldetail->userid = $userid;
                        $modeldetail->menuid = $row["menuid"];
                        $modeldetail->view = '0';
                        $modeldetail->add = '0';
                        $modeldetail->edit = '0';
                        $modeldetail->del = '0';
                        $modeldetail->print = '0';

                        $modeldetail->save();
                    }
                    $sql = "update tmusermenu set view='0', add='0', edit='0', del='0', print='0' where userid = '$userid' ";
                    Yii::app()->db->createCommand($sql)->execute();


                    if (isset($_POST['vw']) && count($_POST['vw']) > 0) {
                        $ids = implode("','", $_POST['vw']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set view='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }

                    if (isset($_POST['ad']) && count($_POST['ad']) > 0) {
                        $ids = implode("','", $_POST['ad']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set add='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }

                    if (isset($_POST['edit']) && count($_POST['edit']) > 0) {
                        $ids = implode("','", $_POST['edit']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set edit='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                    if (isset($_POST['del']) && count($_POST['del']) > 0) {
                        $ids = implode("','", $_POST['del']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set del='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                    if (isset($_POST['print']) && count($_POST['print']) > 0) {
                        $ids = implode("','", $_POST['print']);
                        $ids = "'$ids'";
                        $sql = "update tmusermenu set print='1' where menuid in ($ids) and userid = '$userid' ";
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                }
                //   Globals::AdminLogging("update:users:" . $userid . "");
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
	
	function simpanPenguncian($menuid){
		$model = Lockmenu::model()->findByAttributes(array('menuid' => $menuid, "thang" => Yii::app()->user->getTahun(), "dlt" => "0"));
		if (is_null($model)){
			$model = new Lockmenu;
			$model->lockmenuid = Globals::newID("tblockmenu", "lockmenuid");
			$model->menuid = $menuid;
			$model->thang = Yii::app()->user->getTahun();
			$model->islock = "1";
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			$model->save();
		}else{
			$model->islock = "1";
			$model->save();
		}
	}
	public function actionLockmenu() {
        $model = new Menu('search');
        $model->unsetAttributes();  // clear any default values

        $criteria = new CDbCriteria();
		if(isset($_GET['chk']) || isset($_GET['issave'])) {
			$arrvid = "";
			if(isset($_GET['chk'])){
				foreach($_GET['chk'] as $val) {
					$this->simpanPenguncian($val);
					$arrvid .= "'$val',";
				}	
			}
			$arrvid .= "''";
			Yii::app()->db->createCommand("update tblockmenu set islock = '0' where thang='".Yii::app()->user->getTahun()."' and menuid not in ($arrvid)")->execute();
			
		}
		
        $criteria->select = "t.*, case when coalesce(tr.islock,'0')='0' then '0' else '1' end as islock";
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            //$criteria->addCondition('topikkode LIKE :searchtext or topiknama LIKE :searchtext ');
            //$criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		$criteria->addCondition(" t.menuid in ('7','8','9','10') ");
		$criteria->join .= " LEFT JOIN tblockmenu tr ON tr.menuid=t.menuid and tr.thang='".Yii::app()->user->getTahun()."'";
        $dataProvider = new CActiveDataProvider('Menu', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'jenis, urutan',
            ),));

        $this->render('lockmenu', array(
            'dataProvider' => $dataProvider,
            'anggotaid' => $anggotaid,
            'model' => $model
        ));
    }
	
    public function actionChangePassword() {
        $model = $this->loadModel(Yii::app()->user->id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];

            $sql="select crypt('$model->password',gen_salt('md5')) as password";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();
            $result = "";
			foreach ($rows as $row) {
				$model->password = $row['password'];
			}
            
            if ($model->save()) {
                //   Globals::AdminLogging("change password:users:" . Yii::app()->user->id . "");
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }

        $this->render('changepassword', array('model' => $model,));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
		$model = $this->loadModel($id);
        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:topik:" . $model->topikid . "");
           	$this->redirect(array('topik/admin'));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Users');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users'])) {
            $model->attributes = $_GET['Users'];
        }


        $criteria = new CDbCriteria();
        $criteria->select = 't.*,tr.role, d.subbagnama, tr2.unitnama, tr3.subunitnama';
		$criteria->addCondition("t.dlt = '0'");
        $criteria->join .= ' LEFT JOIN tmrole tr ON tr.roleid=t.roleid';
		$criteria->join .= " LEFT JOIN tmunit tr2 ON t.unitid=tr2.unitid and tr2.dlt='0' ";
        $criteria->join .= " LEFT JOIN tmsubunit tr3 ON tr3.subunitid=t.subunitid and tr3.dlt='0' ";
        $criteria->join .= " LEFT JOIN tmsubbag d ON d.subbagid=t.subbagid and d.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Users', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'roleid DESC',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
