<?php

class SubunitController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view','loadunit'),
                'expression' => '$user->getprivileges(\'view\',\'5\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('loadunit'),
                'expression' => '$user->getprivileges(\'view\',\'7\') || $user->getprivileges(\'view\',\'8\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create','kode'),
                'expression' => '$user->getprivileges(\'create\',\'5\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update'),
                'expression' => '$user->getprivileges(\'edit\',\'5\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'3\') || $user->isSuperadmin()', //data anggota
            ), 
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate($unitid="") {
        $model = new Subunit;
		
        $this->performAjaxValidation($model);
        if (isset($_POST['Subunit'])) {
            $model->attributes = $_POST['Subunit'];
			$subunitid = Globals::newID("tmsubunit", "subunitid");
            $model->subunitid = $subunitid;
			$model->tahun = Yii::app()->user->getTahun();
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
                //Globals::AdminLogging("create:unit:" . $model->unitid . "");
                //$this->redirect(array('view', 'unitid' => $model->unitid));
				$this->redirect(array('subunit/admin', 'filterunitid'=>$model->unitid));
            }
        }else{
			if ($unitid!="") {
	            $model->unitid = $unitid;
	        }
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($subunitid) {
        $model = $this->loadModel($subunitid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Subunit'])) {
            $model->attributes = $_POST['Subunit'];
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				//Globals::AdminLogging("update:unit:" . $model->unitid . "");
               	//$this->redirect(array('subunit/	admin'));
				$this->redirect(array('subunit/admin', 'filterunitid'=>$model->unitid));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($subunitid) {
        $model = $this->loadModel($subunitid);
		$unitid = $model->unitid;
        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:unit:" . $model->unitid . "");
           	$this->redirect(array('subunit/admin', 'filterunitid'=>""));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Subunit');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Subunit::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin($unitid="") {
        $model = new Subunit('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Subunit'])) {
            $model->attributes = $_GET['Subunit'];
        }
		if (isset($_GET['filterunitid'])) $model->unitid = $_GET['filterunitid'];
		else if ($unitid!="") $model->unitid = $unitid;
		$criteria = new CDbCriteria();
        $criteria->select = 't.*, tr.kode as unitkode, tr.nama as unitnama';
		$criteria->addCondition("t.dlt = '0'");
		$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.kode LIKE :searchtext or t.nama LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		if ($model->unitid != '' && $model->unitid != '-') {
            $criteria->addCondition("t.unitid = '".$model->unitid."' ");
        }
        $criteria->join .= " LEFT JOIN tmunit tr ON tr.unitid=t.unitid and tr.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Subunit', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'tr.kode, t.kode, t.nama',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'subunit-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadUnit() {
        $sql = "";
		$filter = "";
		$modeluser = Users::model()->findByPk(Yii::app()->user->id);
		if ($modeluser->grup=="2"){
			if (!is_null($modeluser->unitid) && $modeluser->unitid != '' && $modeluser->unitid != '-') {
	            $filter .= " and unitid = '".$modeluser->unitid."' ";
	        }
		}
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as unitid,'--FILTER SKPD--' as unitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.unitid,c.kode || b.kode || a.kode||' '||a.nama as unitvw, 2 as urutan 
				FROM tmunit a 
				inner join tmurusan b on b.urusanid = a.urusanid and b.dlt = '0'
				inner join tmkelurusan c on c.kelurusanid = b.kelurusanid and c.dlt = '0'
				where a.dlt='0' 
				and a.tahun = '".Yii::app()->user->getTahun()."'
				order by urutan, unitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['unitid']), CHtml::encode($row['unitvw']), true);
        }
    }
	
	public function actionKode() {
        try {
            $sql = "select coalesce(max(cast(replace(kode,'.','') as integer))+1,1) as kode from tmsubunit where dlt='0' and unitid='$_POST[unitid]'";
            $nextKode = "01.";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextKode = $result['kode'];
            }

            while (strlen($nextKode) < 2) {
                $nextKode = "0" . $nextKode;
            }

            echo $nextKode . ".";
        } catch (Exception $e) {
            echo "01.";
        }
    }

   

}
