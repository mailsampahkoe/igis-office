<?php

class SuratmasukController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view'),
                'expression' => '$user->getprivileges(\'view\',\'101\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('loadunit'),
                'expression' => '$user->getprivileges(\'view\',\'101\') || $user->getprivileges(\'view\',\'8\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create','kode','getnextno'),
                'expression' => '$user->getprivileges(\'create\',\'101\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update','posting','upload','upload1','getnextno'),
                'expression' => '$user->getprivileges(\'edit\',\'101\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'101\') || $user->isSuperadmin()', //data anggota
            ), 
            array('allow',
                'actions' => array('print'),
                'expression' => '$user->getprivileges(\'print\',\'101\') || $user->isSuperadmin()', //data anggota
            ), 
            array('allow',
                'actions' => array('loadunit','loadsubunit','loadbagian','loadsubbagian','loadjenis','loadpejabat','loadsuratmasukfiles'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate($suratmasukid="") {
        $model = new Suratmasuk;
		
        $this->performAjaxValidation($model);
        if (isset($_POST['Suratmasuk'])) {
            $model->attributes = $_POST['Suratmasuk'];
			$suratmasukid = Globals::newID("ttsuratmasuk", "suratmasukid");
            $model->suratmasukid = $suratmasukid;
            $model->pegawaiid = Yii::app()->user->getinfouser('pegawaiid');
            $model->tglpenyerahan = $this->datePostgres($model->tglpenyerahan);
            $model->tglsurat = $this->datePostgres($model->tglsurat);
			if ($model->tglkembali == null || $model->tglkembali == '') {
				$model->tglkembali = null;
			}
			else {
				$model->tglkembali = $this->datePostgres($model->tglkembali);
			}
			$model->tahun = Yii::app()->user->getTahun();
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
				if (isset($_POST['filedelete']) && $_POST['filedelete'] != '') {
					$this->deleteFiles($_POST['filedelete']);
				}
				if (isset($_POST['filedata']) && $_POST['filedata'] != '') {
					$result = $this->saveFiles($_POST['filedata'], $suratmasukid);
				}
                //Globals::AdminLogging("create:unit:" . $model->unitid . "");
				$this->redirect(array('suratmasuk/admin'));
            }
        }else{
			//$model->bagianid = Yii::app()->user->getinfouser('bagianid');
			//$model->subbagianid = Yii::app()->user->getinfouser('subbagianid');
            $model->indexkode1 = '';
            $model->indextahun = date('Y');
            $model->tglpenyerahan = $this->dateIndo($model->tglpenyerahan);
            $model->tglsurat = $this->dateIndo($model->tglsurat);
            $model->kembalipada = "Kearsipan Subbag. TU";
			$model->tglkembali = $this->dateIndo($model->tglkembali);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($suratmasukid) {
        $model = $this->loadModel($suratmasukid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Suratmasuk'])) {
            $model->attributes = $_POST['Suratmasuk'];
            //$model->pegawaiid = Yii::app()->user->getinfouser('pegawaiid');
            $model->tglpenyerahan = $this->datePostgres($model->tglpenyerahan);
            $model->tglsurat = $this->datePostgres($model->tglsurat);
			$model->tglkembali = $this->datePostgres($model->tglkembali);
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				if (isset($_POST['filedelete']) && $_POST['filedelete'] != '') {
					$this->deleteFiles($_POST['filedelete']);
				}
				if (isset($_POST['filedata']) && $_POST['filedata'] != '') {
					$result = $this->saveFiles($_POST['filedata'], $suratmasukid);
				}
				//Globals::AdminLogging("update:unit:" . $model->unitid . "");
				$this->redirect(array('suratmasuk/admin'));
            }
        }
        else {
            $model->tglpenyerahan = $this->dateIndo($model->tglpenyerahan);
            $model->tglsurat = $this->dateIndo($model->tglsurat);
            $model->tglkembali = $this->dateIndo($model->tglkembali);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionPosting($suratmasukid) {
        $model = $this->loadModel($suratmasukid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Suratmasuk'])) {
            $model->attributes = $_POST['Suratmasuk'];
            $model->status = enumVar::getStep($model->status, 1, 0);
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				$this->saveHistorySuratmasuk($model->suratmasukid, $model->pegawaiid, $model->status, '-', '');
				
				$this->saveNotificationPegawai($model->suratmasukid, $model->pejabatid, 'Surat masuk telah diposting', 'Surat masuk dengan nomor '.$model->indexnomor.' telah diposting', 'disposisikabag', 'admin', 'suratmasukid');
				
				$this->redirect(array('suratmasuk/admin'));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('posting', array(
            'model' => $model,
        ));
    }

    public function actionUpload1() {
		$folder_name = 'files/';
		
		for ($icounter = 0; $icounter < count($_FILES); $icounter++) {
			if (!empty($_FILES)) {
	            $fileName = $_FILES['upload']['name']; //get the file name
	            $fileSize = $_FILES['upload']['size']; //get the size
	            $fileError = $_FILES['upload']['error']; //get the error when upload
	            $file = $_FILES['upload'];
	            $extension = str_replace(".", "", substr($fileName, strlen($fileName) - 4, 4));
				$newfiles = '';
				
	            if ($fileSize > 0 || $fileError == 0) { //check if the file is corrupt or error
					$temp_file = $_FILES['upload']['tmp_name'];
					$newfiles = Globals::newID("ttsuratmasuk", "suratmasukid") . '.' . $extension;
					$location = $folder_name . $newfiles;// $_FILES['upload']['name'];
					move_uploaded_file($temp_file, $location);
				}
			}
			
			$result = array();
		}
		
		echo json_encode(array('r'=>'1', 'error' => '0', 'files' => $newfiles, 'filename' => $fileName));
    }

    public function actionUpload($suratmasukid) {
        $model = $this->loadModel($suratmasukid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST)) {
			$result = false;
			if (isset($_POST['filedelete']) && $_POST['filedelete'] != '') {
				$this->deleteFiles($_POST['filedelete']);
			}
			if (isset($_POST['filedata']) && $_POST['filedata'] != '') {
				$result = $this->saveFiles($_POST['filedata'], $suratmasukid);
			}
			if ($result) {
				$this->redirect(array('suratmasuk/admin'));
			}
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('upload', array(
            'model' => $model,
        ));
    }
	
	private function saveFiles($dataparam, $masterid) {
        $records = explode('~#;#~', $dataparam);
        $nourut = 1;
		$result = false;
		
		foreach ($records as $record) {
            if ($record != null) {
		        //filesid
		        $data = explode('~#|#~', $record);
	            $files = $data[0];
	            $filename = $data[1];
		        $suratmasukid = $masterid;
				
				if (!Globals::isDataExist("select count(suratmasukfilesid) as count from ttsuratmasukfiles where dlt='0' and suratmasukid='$suratmasukid' and filename='$filename' ")){
			        $modeldetail = new Suratmasukfiles;
			        $modeldetail->suratmasukfilesid = Globals::newID("ttsuratmasukfiles", "suratmasukfilesid");
			        $modeldetail->suratmasukid = $suratmasukid;
			        $modeldetail->files = $files;
			        $modeldetail->nourut = $nourut;
			        $modeldetail->filename = $filename;
			        $modeldetail->keterangan = '';
			        $modeldetail->tahun = Yii::app()->user->getTahun();
					$modeldetail->attributes = array_merge($modeldetail->attributes,$this->getLogEditDataInfo());
			        
		            $result = $modeldetail->save();
				}
				
				$nourut++;
            }
        }
		
		return $result;
	}
	
	private function deleteFiles($dataparam) {
	    $records = explode('~#;#~', $dataparam);
        foreach ($records as $record) {
            if ($record != null) {
		        $data = explode('~#|#~', $record);
		        if ($data[0] != '') {
					$modeldetail = Suratmasukfiles::model()->findByPk($data[0]);
					if (!is_null($modeldetail)) {
				        $this->performAjaxValidation($modeldetail);
						$modeldetail->attributes = $this->getLogDeleteDataInfo();
				        $modeldetail->save();
					}
				}
            }
        }
	}

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($suratmasukid) {
        $model = $this->loadModel($suratmasukid);
		$unitid = $model->unitid;
        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:unit:" . $model->unitid . "");
           	$this->redirect(array('suratmasuk/admin', 'filterunitid'=>""));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Suratmasuk');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Suratmasuk::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin($unitid="") {
        $model = new Suratmasuk('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Suratmasuk'])) {
            $model->attributes = $_GET['Suratmasuk'];
        }
		//if (isset($_GET['filterunitid'])) $model->unitid = $_GET['filterunitid'];
		//else if ($unitid!="") $model->unitid = $unitid;
		$criteria = new CDbCriteria();
        $criteria->select = "t.*, 
							case(coalesce(t.status, 0)) 
								when ".enumVar::STEP_I_POSTED." then '".enumVar::STEP_I_DESC_POSTED."' 
								when ".enumVar::STEP_I_KAUNIT_VERIFIED." then '".enumVar::STEP_I_DESC_KAUNIT_VERIFIED."' 
								when ".enumVar::STEP_I_KASUBUNIT_VERIFIED." then '".enumVar::STEP_I_DESC_KASUBUNIT_VERIFIED."' 
								when ".enumVar::STEP_I_KABID_VERIFIED." then '".enumVar::STEP_I_DESC_KABID_VERIFIED."' 
								when ".enumVar::STEP_I_KASUBBID_VERIFIED." then '".enumVar::STEP_I_DESC_KASUBBID_VERIFIED."' 
								when ".enumVar::STEP_I_KABAG_VERIFIED." then '".enumVar::STEP_I_DESC_KABAG_VERIFIED."' 
								when ".enumVar::STEP_I_KASUBBAG_VERIFIED." then '".enumVar::STEP_I_DESC_KASUBBAG_VERIFIED."' 
								when ".enumVar::STEP_I_FINISHED." then '".enumVar::STEP_I_DESC_FINISHED."' 
								when ".enumVar::STEP_I_CANCELLED." then '".enumVar::STEP_I_DESC_CANCELLED."' 
								when ".enumVar::STEP_I_DENIED." then '".enumVar::STEP_I_DESC_DENIED."' 
								else '".enumVar::STEP_I_DESC_DRAFT."' 
								end as ketstatus ";
		$criteria->addCondition("t.dlt = '0'");
		$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.indexnomor LIKE :searchtext or t.perihal LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		if (isset($_GET['filterbagianid']) && $_GET['filterbagianid'] != 'all') {
			//$criteria->addCondition("t.bagianid = '".$_GET['filterbagianid']."' ");
		}
		elseif (!isset($_GET['filterbagianid'])) {
			$model->bagianid = Yii::app()->user->getinfouser('bagianid');
			//$criteria->addCondition("t.bagianid = '".$model->bagianid."' ");
		}
		if (isset($_GET['filtersubbagianid']) && $_GET['filtersubbagianid'] != 'all') {
			//$criteria->addCondition("t.subbagianid = '".$_GET['filtersubbagianid']."' ");
		}
		elseif (!isset($_GET['filtersubbagianid'])) {
			$model->subbagianid = Yii::app()->user->getinfouser('subbagianid');
			//$criteria->addCondition("t.subbagianid = '".$model->subbagianid."' ");
		}
		if (isset($_GET['filterstatus']) && $_GET['filterstatus'] != 'all') {
			$criteria->addCondition("t.status = '".$_GET['filterstatus']."' ");
		}
		elseif (!isset($_GET['filterstatus'])) {
			$model->status = '0';
			$criteria->addCondition("t.status = '".$model->status."' ");
		}
		
		if (!Yii::app()->user->isSuperadmin()) {
			$criteria->addCondition("t.pegawaiid = '".Yii::app()->user->getinfouser('pegawaiid')."' ");
		}
		/*if ($model->unitid != '' && $model->unitid != '-') {
            $criteria->addCondition("t.unitid = '".$model->unitid."' ");
        }*/
        //$criteria->join .= " LEFT JOIN tmbagian b ON b.bagianid=t.bagianid and b.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmsubbagian sb ON sb.subbagianid=t.subbagianid and sb.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmunit u ON u.unitid=t.unitid and u.dlt='0' ";
        //$criteria->join .= " LEFT JOIN tmsubunit su ON su.subunitid=t.subunitid and su.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Suratmasuk', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 't.indexnomor',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'suratmasuk-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadUnit() {
        $sql = "";
		$filter = "";
		$modeluser = Users::model()->findByPk(Yii::app()->user->id);
		if ($modeluser->grup=="2"){
			if (!is_null($modeluser->unitid) && $modeluser->unitid != '' && $modeluser->unitid != '-') {
	            $filter .= " and unitid = '".$modeluser->unitid."' ";
	        }
		}
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as unitid,'--FILTER SKPD--' as unitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.unitid,concat(c.kode, b.kode, a.kode,' ',a.nama) as unitvw, 2 as urutan 
				FROM tmunit a 
				inner join tmurusan b on b.urusanid = a.urusanid and b.dlt = '0'
				inner join tmkelurusan c on c.kelurusanid = b.kelurusanid and c.dlt = '0'
				where a.dlt='0' 
				and a.tahun = '".Yii::app()->user->getTahun()."'
				order by urutan, unitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['unitid']), CHtml::encode($row['unitvw']), true);
        }
    }

    public function actionLoadSubunit() {
        $sql = "";
		$filter = (isset($_POST['unitid']) && $_POST['unitid']!="") ? " and unitid='$_POST[unitid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subunitid,'--FILTER UNIT KERJA--' as subunitvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subunitid,'-' as subunitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subunitid,concat(kode,' ',nama) as subunitvw, 2 as urutan FROM tmsubunit where dlt='0' $filter order by urutan, subunitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subunitid']), CHtml::encode($row['subunitvw']), true);
        }
    }

    public function actionLoadBagian() {
        $sql = "";
		$filter = "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as bagianid,'--FILTER BAGIAN--' as bagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.bagianid,concat(a.kode,' ',a.nama) as bagianvw, 2 as urutan 
				FROM tmbagian a 
				where a.dlt='0' 
				
				order by urutan, bagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['bagianid']), CHtml::encode($row['bagianvw']), true);
        }
    }

    public function actionLoadSubbagian() {
        $sql = "";
		$filter = (isset($_POST['bagianid']) && $_POST['bagianid']!="") ? " and bagianid='$_POST[bagianid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subbagianid,'--FILTER SUB BAGIAN--' as subbagianvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subbagianid,'-' as subbagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subbagianid,concat(kode,' ',nama) as subbagianvw, 2 as urutan FROM tmsubbagian where dlt='0' $filter order by urutan, subbagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subbagianid']), CHtml::encode($row['subbagianvw']), true);
        }
    }

    public function actionLoadPejabat() {
        $sql = "";
		$filter = "";
		$sql = " SELECT '' as id,'' as value,'--PILIH PEJABAT--' as text, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.pegawaiid as id,a.kode as value, a.nama as text, 2 as urutan 
				FROM tmpegawai a 
				inner join tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
				where a.dlt='0' and b.level = '4'
				order by urutan, text";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		echo json_encode($rows);

        /*foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['id']), CHtml::encode($row['text']), true);
        }*/
    }

    public function actionLoadJenis() {
        $sql = "";
		$filter = "";
		$sql = " SELECT '' as id,'' as value,'--PILIH JENIS SURAT--' as text, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.jenisid as id,a.kode as value, concat(a.kode,' - ',a.nama) as text, 2 as urutan 
				FROM tmjenis a 
				where a.dlt='0' 				
				order by urutan, text";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
		echo json_encode($rows);

        /*foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['jenisid']), CHtml::encode($row['jenisvw']), true);
        }*/
    }

    public function actionLoadSuratmasukFiles() {
        $sql = "";
		$filter = "and suratmasukid='".$_POST['id']."'";
		$sql = "SELECT * FROM ttsuratmasukfiles where dlt='0' $filter order by nourut";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

		echo json_encode($rows);
    }
	
	public function actionGetNextNo() {
		echo $this->getNextNo($_POST['jenisid']);
    }
	
	public function getNextNo($jenisid) {
        try {
            $sql = "select coalesce(max(cast(replace(indexnomor,'.','') as integer))+1,1) as indexnomor from ttsuratmasuk where jenisid = '".$jenisid."' and dlt='0' ";
            $nextNomor = "1";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextNomor = $result['indexnomor'];
            }
			
            return $nextNomor;
        } catch (Exception $e) {
            return "1";
        }
    }
	
	public function actionPrint($suratmasukid)
    {
		$modelprint = new Suratmasuk;
		$modelprint->previewpdf($suratmasukid);
	}
}