<?php

class PegawaiController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view','loadunit','loadsubunit','loadbagian','loadsubbagian','loadjabatan','loadsubbag', 'kode'),
                'expression' => '$user->getprivileges(\'view\',\'7\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create','kode'),
                'expression' => '$user->getprivileges(\'create\',\'7\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update'),
                'expression' => '$user->getprivileges(\'edit\',\'7\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'7\') || $user->isSuperadmin()', //data anggota
            ), 
            array('allow',
                'actions' => array('loadsubbag'),
                'expression' => '$user->getprivileges(\'del\',\'7\') || $user->getprivileges(\'del\',\'9\') || $user->isSuperadmin()', //data anggota
            ), 
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionDownload($pegawaiid) {
		$location = $this->loadModel($pegawaiid)->dokpath;
		if (!is_null($location) && $location!=""){
		 	$path = Yii::getPathOfAlias('webroot').'/'.$location;
	 		$this->downloadFile($path);
		}	
    }
	
	public function downloadFile($fullpath){
	  if(!empty($fullpath)){ 
	  	  $ext = strtolower(pathinfo($fullpath, PATHINFO_EXTENSION));
		  if ($ext=="pdf")
	      	header("Content-type:application/pdf"); //for pdf file
		  else if ($ext=="txt")	
	      	header('Content-Type:text/plain; charset=ISO-8859-15');
	      else if ($ext=="jpg" || $ext=="jpeg" || $ext=="png" || $ext=="gif")	
	      	header('content-type image/jpeg');
	      
	      header('Content-Disposition: attachment; filename="'.basename($fullpath).'"'); 
	      header('Content-Length: ' . filesize($fullpath));
	      readfile($fullpath);
	      Yii::app()->end();
	  }
	}

    public function actionCreate($bagianid="",$subbagianid="") {
		//echo "tes"; return;
        $model = new Pegawai;
		$this->performAjaxValidation($model);
        if (isset($_POST['Pegawai'])) {
            $model->attributes = $_POST['Pegawai'];
			$pegawaiid = Globals::newID("tmpegawai", "pegawaiid");
            $model->pegawaiid = $pegawaiid;
			if ($model->subbagianid=="-" || $model->subbagianid=="") $model->subbagianid = null;
            $model->tanggallahir = $this->datePostgres($model->tanggallahir);
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
				/*if(isset($_POST['chkjabatan'])) {
					$strincludejab = "-1";
					foreach($_POST['chkjabatan'] as $val) {
						$modelJabatan = new Pegawaijabatan;
						$modelJabatan->pegawaijabatanid = Globals::newID("tmpegawaijabatan", "pegawaijabatanid");
						$modelJabatan->pegawaiid = $pegawaiid;
						$modelJabatan->jabatan = intval($val);
						$modelJabatan->statusaktif = 1;
						$modelJabatan->attributes = array_merge($modelJabatan->attributes,$this->getLogAddDataInfo());
						$modelJabatan->save();		
					}
				}*/
				$this->redirect(array('pegawai/admin', 'filterbagianid'=>$model->bagianid, 'filtersubbagianid'=>$model->subbagianid));
            }
        }else{
			if ($bagianid!="") {
	            $model->bagianid = $bagianid;
			}
	        if ($subbagianid!="") {
	            $model->subbagianid = $subbagianid;
			}
			$model->statuspernikahan = "1";
			$model->statusaktif = "1";
			$model->jeniskelamin = '0';
			//$model->tanggallahir = date("d-m-1990");
            $model->tanggallahir = $this->dateIndo($model->tanggallahir);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
			'strjab' => "",
        ));
    }

    public function actionUpdate($pegawaiid) {
        $model = $this->loadModel($pegawaiid);
		$this->performAjaxValidation($model);
		
        if (isset($_POST['Pegawai'])) {
			
            $model->attributes = $_POST['Pegawai'];
			if ($model->subbagianid=="-" || $model->subbagianid=="") $model->subbagianid = null;
            $model->tanggallahir = $this->datePostgres($model->tanggallahir);
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				/*if(isset($_POST['chkjabatan'])) {
					$strincludejab = "-1";
					foreach($_POST['chkjabatan'] as $val) {
						if (!Globals::isDataExist("select count(pegawaijabatanid) as count from tmpegawaijabatan where dlt='0' and pegawaiid='$pegawaiid' and jabatan='$val' ")){
							$modelJabatan = new Pegawaijabatan;
							$modelJabatan->pegawaijabatanid = Globals::newID("tmpegawaijabatan", "pegawaijabatanid");
							$modelJabatan->pegawaiid = $pegawaiid;
							$modelJabatan->jabatan = intval($val);
							$modelJabatan->statusaktif = 1;
							$modelJabatan->attributes = array_merge($modelJabatan->attributes,$this->getLogAddDataInfo());
							$modelJabatan->save();		
						}
						$strincludejab.=",".$val;
					}
					Yii::app()->db->createCommand("delete from tmpegawaijabatan where pegawaiid='$pegawaiid' and jabatan not in ($strincludejab) ")->execute();
				}*/
				$this->redirect(array('pegawai/admin', 'filterbagianid'=>$model->bagianid, 'filtersubbagianid'=>$model->subbagianid));
            }
        }
		else {
            $model->tanggallahir = $this->dateIndo($model->tanggallahir);
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$rows = Yii::app()->db->createCommand("select group_concat(jabatan separator ',') as strjab from tmpegawaijabatan where dlt='0' and pegawaiid='$pegawaiid' ")->queryAll();
		$strjab = "";
		if (count($rows)>0) $strjab = $rows[0]["strjab"];
        $this->render('update', array(
            'model' => $model,
			'strjab' => $strjab,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($pegawaiid) {
        $model = $this->loadModel($pegawaiid);

        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:bagian:" . $model->subbagianid . "");
           	$this->redirect(array('admin'));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Pegawai');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Pegawai::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin() {
        $model = new Pegawai('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Pegawai'])) {
            $model->attributes = $_GET['Pegawai'];
        }
		/*if (isset($_GET['filterunitid'])) $model->unitid = $_GET['filterunitid'];
		if (isset($_GET['filtersubunitid'])) $model->subunitid = $_GET['filtersubunitid'];*/
		if (isset($_GET['filterbagianid'])) $model->bagianid = $_GET['filterbagianid'];
		if (isset($_GET['filtersubbagianid'])) $model->subbagianid = $_GET['filtersubbagianid'];
		$criteria = new CDbCriteria();
        $criteria->select = 't.*, tr2.kode as bagiankode, tr2.nama as bagiannama, tr.kode as subbagiankode, tr.nama as subbagiannama';
		$criteria->addCondition("t.dlt = '0'");
		//$criteria->addCondition("t.thang = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('alamat LIKE :searchtext or kode LIKE :searchtext or nama LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		if ($model->subbagianid != '' && $model->subbagianid != '-') {
            $criteria->addCondition("t.subbagianid = '".$model->subbagianid."' ");
        }
        if ($model->bagianid != '' && $model->bagianid != '-') {
            $criteria->addCondition("t.bagianid = '".$model->bagianid."' ");
        }
        $criteria->join .= " LEFT JOIN tmsubbagian tr ON tr.subbagianid=t.subbagianid and tr.dlt='0' ";
        $criteria->join .= " LEFT JOIN tmbagian tr2 ON t.bagianid=tr2.bagianid and tr2.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Pegawai', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'tr2.kode, tr.kode, t.kode',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pegawai-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadUnit() {
        $sql = "";
		$filter = "";
		$modeluser = User::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as unitid,'--FILTER SKPD--' as unitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.unitid,concat(a.kode,' ',a.nama) as unitvw, 2 as urutan 
				FROM tmunit a 
				where a.dlt='0' 
				
				order by urutan, unitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['unitid']), CHtml::encode($row['unitvw']), true);
        }
    }

    public function actionLoadSubunit() {
        $sql = "";
		$filter = (isset($_POST['unitid']) && $_POST['unitid']!="") ? " and unitid='$_POST[unitid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subunitid,'--FILTER UNIT KERJA--' as subunitvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subunitid,'-' as subunitvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subunitid,concat(kode,' ',nama) as subunitvw, 2 as urutan FROM tmsubunit where dlt='0' $filter order by urutan, subunitvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subunitid']), CHtml::encode($row['subunitvw']), true);
        }
    }

    public function actionLoadBagian() {
        $sql = "";
		$filter = "";
		$modeluser = User::model()->findByPk(Yii::app()->user->id);
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as bagianid,'--FILTER BAGIAN--' as bagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.bagianid,concat(a.kode,' ',a.nama) as bagianvw, 2 as urutan 
				FROM tmbagian a 
				where a.dlt='0' 
				
				order by urutan, bagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['bagianid']), CHtml::encode($row['bagianvw']), true);
        }
    }

    public function actionLoadSubbagian() {
        $sql = "";
		$filter = (isset($_POST['bagianid']) && $_POST['bagianid']!="") ? " and bagianid='$_POST[bagianid]' " : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as subbagianid,'--FILTER SUB BAGIAN--' as subbagianvw, 1 as urutan UNION ALL ";
		else
			$sql = " SELECT '-' as subbagianid,'-' as subbagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT subbagianid,concat(kode,' ',nama) as subbagianvw, 2 as urutan FROM tmsubbagian where dlt='0' $filter order by urutan, subbagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['subbagianid']), CHtml::encode($row['subbagianvw']), true);
        }
    }

    public function actionLoadJabatan() {
        $sql = "";
		$filter = (isset($_POST['bagianid']) && $_POST['bagianid']!="") ? " and bagianid='$_POST[bagianid]' " : "";
		$filter .= (isset($_POST['subbagianid'])) ? " and coalesce(subbagianid,'')='".($_POST['subbagianid']=="-" ? "" : $_POST['subbagianid'])."'" : "";
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as jabatanid,'--FILTER JABATAN--' as jabatanvw, 1 as urutan, 0 as level UNION ALL ";
		else
		$sql = "SELECT jabatanid,concat(nama) as jabatanvw, 2 as urutan, level FROM tmjabatan where dlt='0' $filter order by urutan, level";
		$rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['jabatanid']), CHtml::encode($row['jabatanvw']), true);
        }
		echo $sql;
    }
}
