<?php

class SubbagianController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view','loadbagian'),
                'expression' => '$user->getprivileges(\'view\',\'6\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('loadbagian'),
                'expression' => '$user->getprivileges(\'view\',\'6\') || $user->getprivileges(\'view\',\'8\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create','kode'),
                'expression' => '$user->getprivileges(\'create\',\'6\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update'),
                'expression' => '$user->getprivileges(\'edit\',\'6\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'6\') || $user->isSuperadmin()', //data anggota
            ), 
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate($bagianid="") {
        $model = new Subbagian;
		
        $this->performAjaxValidation($model);
        if (isset($_POST['Subbagian'])) {
            $model->attributes = $_POST['Subbagian'];
			$subbagianid = Globals::newID("tmsubbagian", "subbagianid");
            $model->subbagianid = $subbagianid;
			$model->tahun = Yii::app()->user->getTahun();
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
                //Globals::AdminLogging("create:bagian:" . $model->bagianid . "");
                //$this->redirect(array('view', 'bagianid' => $model->bagianid));
				$this->redirect(array('subbagian/admin', 'filterbagianid'=>$model->bagianid));
            }
        }else{
			if ($bagianid!="") {
	            $model->bagianid = $bagianid;
	        }
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($subbagianid) {
        $model = $this->loadModel($subbagianid);
		
        $this->performAjaxValidation($model);

        if (isset($_POST['Subbagian'])) {
            $model->attributes = $_POST['Subbagian'];
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				//Globals::AdminLogging("update:bagian:" . $model->bagianid . "");
               	//$this->redirect(array('subbagian/	admin'));
				$this->redirect(array('subbagian/admin', 'filterbagianid'=>$model->bagianid));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($subbagianid) {
        $model = $this->loadModel($subbagianid);
		$bagianid = $model->bagianid;
        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:bagian:" . $model->bagianid . "");
           	$this->redirect(array('subbagian/admin', 'filterbagianid'=>""));
        }
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Subbagian');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Subbagian::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin($bagianid="") {
        $model = new Subbagian('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Subbagian'])) {
            $model->attributes = $_GET['Subbagian'];
        }
		if (isset($_GET['filterbagianid'])) $model->bagianid = $_GET['filterbagianid'];
		else if ($bagianid!="") $model->bagianid = $bagianid;
		$criteria = new CDbCriteria();
        $criteria->select = 't.*, tr.kode as bagiankode, tr.nama as bagiannama';
		$criteria->addCondition("t.dlt = '0'");
		//$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.kode LIKE :searchtext or t.nama LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
		if ($model->bagianid != '' && $model->bagianid != '-') {
            $criteria->addCondition("t.bagianid = '".$model->bagianid."' ");
        }
        $criteria->join .= " LEFT JOIN tmbagian tr ON tr.bagianid=t.bagianid and tr.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Subbagian', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'tr.kode, t.kode, t.nama',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'subbagian-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadBagian() {
        $sql = "";
		$filter = "";
		/*$modeluser = Users::model()->findByPk(Yii::app()->user->id);
		if ($modeluser->grup=="2"){
			if (!is_null($modeluser->bagianid) && $modeluser->bagianid != '' && $modeluser->bagianid != '-') {
	            $filter .= " and bagianid = '".$modeluser->bagianid."' ";
	        }
		}*/
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as bagianid,'--FILTER BAGIAN--' as bagianvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT a.bagianid,concat(a.kode,' ',a.nama) as bagianvw, 2 as urutan 
				FROM tmbagian a 
				where a.dlt='0' 
				and a.tahun = '".Yii::app()->user->getTahun()."'
				order by urutan, bagianvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['bagianid']), CHtml::encode($row['bagianvw']), true);
        }
    }
	
	public function actionKode() {
        try {
            $sql = "select coalesce(max(cast(replace(kode,'.','') as integer))+1,1) as kode from tmsubbagian where dlt='0' and bagianid='$_POST[bagianid]'";
            $nextKode = "01.";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextKode = $result['kode'];
            }

            while (strlen($nextKode) < 2) {
                $nextKode = "0" . $nextKode;
            }

            echo $nextKode . ".";
        } catch (Exception $e) {
            echo "01.";
        }
    }

   

}
