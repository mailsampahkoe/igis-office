<?php

class UnitController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('admin', 'view','loadurusan'),
                'expression' => '$user->getprivileges(\'view\',\'4\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('create','kode'),
                'expression' => '$user->getprivileges(\'create\',\'4\') || $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('update','kode','delete'),
                'expression' => '$user->getprivileges(\'edit\',\'4\')|| $user->isSuperadmin()', //data anggota
            ),
            array('allow',
                'actions' => array('delete'),
                'expression' => '$user->getprivileges(\'del\',\'4\') || $user->isSuperadmin()', //data anggota
            ), 
            //========================
            
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin', 'transfer', 'verifikasi', 'view_verifikasi'),
//                'users' => array('admin'),
//            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {
        $model = new Unit;

        $this->performAjaxValidation($model);
        if (isset($_POST['Unit'])) {
            $model->attributes = $_POST['Unit'];
			$unitid = Globals::newID("tmunit", "unitid");
            $model->unitid = $unitid;
			$model->tahun = Yii::app()->user->getTahun();
			$model->attributes = array_merge($model->attributes,$this->getLogAddDataInfo());
			if ($model->save()) {
                //Globals::AdminLogging("create:unit:" . $model->unitid . "");
                //$this->redirect(array('view', 'unitid' => $model->unitid));
				$this->redirect(array('unit/admin'));
            }
        }else{
			$model->jenis =enumVar::JENIS_SKPD_SKPD;
		}
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		$this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($unitid) {
        $model = $this->loadModel($unitid);

        $this->performAjaxValidation($model);

        if (isset($_POST['Unit'])) {
            $model->attributes = $_POST['Unit'];
			$model->attributes = array_merge($model->attributes,$this->getLogEditDataInfo());
            if ($model->save()) {
				//Globals::AdminLogging("update:tahap:" . $model->unitid . "");
               	 $this->redirect(array('unit/admin'));
            }
        }
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($unitid) {
		$model = $this->loadModel($unitid);

        $this->performAjaxValidation($model);
		$model->attributes = $this->getLogDeleteDataInfo();
        if ($model->save()) {
			//Globals::AdminLogging("update:tahap:" . $model->unitid . "");
           	$this->redirect(array('unit/admin'));
        }
        //Globals::AdminLogging("delete:member:" . $id . "");

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Unit');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

   
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Unit::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
	
	public function actionAdmin($urusanid="") {
        $model = new Unit('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Unit'])) {
            $model->attributes = $_GET['Unit'];
        }

        $criteria = new CDbCriteria();
        $criteria->select = 't.*, tr.kode as urusankode, tr.nama as urusannama';
		$criteria->addCondition("t.dlt = '0'");
		$criteria->addCondition("t.tahun = '".Yii::app()->user->getTahun()."'");
		if (isset($_GET['filterurusanid'])) $model->urusanid = $_GET['filterurusanid'];
		else if ($urusanid!="") $model->urusanid = $urusanid;
		if (isset($_GET['searchtext']) && $_GET['searchtext'] != '') {
            $criteria->addCondition('t.kode LIKE :searchtext or t.nama LIKE :searchtext ');
            $criteria->params = array(':searchtext' => '%' . $_GET['searchtext'] . '%');
        }
        $criteria->join .= " LEFT JOIN tmurusan tr ON tr.urusanid=t.urusanid and tr.dlt='0' ";
        $dataProvider = new CActiveDataProvider('Unit', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
            'sort' => array(
                'defaultOrder' => 'tr.kode, t.kode, t.nama',
            ),));

        $this->render('admin', array(
            'dataProvider' => $dataProvider,
            'model' => $model
        ));
    }
    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'unit-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
	
	public function actionLoadUrusan() {
        $sql = "";
		$filter = "";
		$modeluser = Users::model()->findByPk(Yii::app()->user->id);
		if ($modeluser->grup=="2"){
			if (!is_null($modeluser->urusanid) && $modeluser->urusanid != '' && $modeluser->urusanid != '-') {
	            $filter .= " and urusanid = '".$modeluser->urusanid."' ";
	        }
		}
		if (isset($_POST['isall']) && $_POST['isall']!="")
			$sql = " SELECT '-' as urusanid,'--FILTER URUSAN--' as urusanvw, 1 as urutan UNION ALL ";
		$sql .= "SELECT urusanid,kode||' '||nama as urusanvw, 2 as urutan FROM tmurusan where dlt='0' $filter order by urutan, urusanvw";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($rows as $row) {
            echo CHtml::tag('option', array('value' => $row['urusanid']), CHtml::encode($row['urusanvw']), true);
        }
    }
	
	public function actionKode() {
        try {
            $sql = "select coalesce(max(cast(replace(kode,'.','') as integer))+1,1) as kode from tmunit where dlt='0'";
            $nextKode = "01.";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($results AS $result) {
                $nextKode = $result['kode'];
            }

            while (strlen($nextKode) < 2) {
                $nextKode = "0" . $nextKode;
            }

            echo $nextKode . ".";
        } catch (Exception $e) {
            echo "01.";
        }
    }

   

}
