<?php

/**
 * This is the model class for table "ttsuratkeluar".
 *
 * The followings are the available columns in table 'ttsuratkeluar':
 * @property string $suratkeluarid
 * @property string $unitid
 * @property string $subunitid
 * @property string $bagianid
 * @property string $subbagianid
 * @property string $pejabatid
 * @property string $pegawaiid
 * @property string $indexkode1
 * @property string $indexkode2
 * @property string $indexbulan
 * @property string $indexnomor
 * @property string $indextahun
 * @property string $kode
 * @property string $klasifikasi
 * @property string $tglpenyerahan
 * @property string $nosurat
 * @property string $tglsurat
 * @property string $kepada
 * @property string $perihal
 * @property string $kirimvia
 * @property string $tglkirim
 * @property string $jamkirim
 * @property integer $paraf
 * @property integer $status
 * @property string $keterangan
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Tmunit $unit
 * @property Tmsubunit $subunit
 * @property Tmbagian $bagian
 * @property Tmsubbagian $subbagian
 * @property Ttsuratkeluarjabatan[] $ttsuratkeluarjabatans
 * @property Ttsuratkeluartembusan[] $ttsuratkeluartembusans
 */
class Suratkeluar extends CActiveRecord
{
	public $unitnama="";
	public $unitkode="";
	public $subunitnama="";
	public $subunitkode="";
	public $bagiannama="";
	public $bagiankode="";
	public $subbagiannama="";
	public $subbagiankode="";
	public $ketstatus;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Suratkeluar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ttsuratkeluar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('suratkeluarid', 'required'),
			array('paraf, status, dlt', 'numerical', 'integerOnly'=>true),
			array('suratkeluarid, unitid, subunitid, bagianid, subbagianid, pejabatid, pegawaiid, indexkode1, indexkode2, indexbulan, indexnomor, indextahun, kode, klasifikasi, jamkirim', 'length', 'max'=>20),
			array('nosurat, kirimvia', 'length', 'max'=>100),
			array('kepada, perihal, keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tglpenyerahan, tglsurat, tglkirim, tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('suratkeluarid, unitid, subunitid, bagianid, subbagianid, pejabatid, pegawaiid, indexkode1, indexkode2, indexbulan, indexnomor, indextahun, kode, klasifikasi, tglpenyerahan, nosurat, tglsurat, kepada, perihal, kirimvia, tglkirim, jamkirim, paraf, status, keterangan, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'unit' => array(self::BELONGS_TO, 'Tmunit', 'unitid'),
			'subunit' => array(self::BELONGS_TO, 'Tmsubunit', 'subunitid'),
			'bagian' => array(self::BELONGS_TO, 'Tmbagian', 'bagianid'),
			'subbagian' => array(self::BELONGS_TO, 'Tmsubbagian', 'subbagianid'),
			'pejabat' => array(self::BELONGS_TO, 'Tmpegawai', 'pejabatid'),
			'pegawai' => array(self::BELONGS_TO, 'Tmpegawai', 'pegawaiid'),
			'ttsuratkeluarjabatans' => array(self::HAS_MANY, 'Ttsuratkeluarjabatan', 'suratkeluarid'),
			'ttsuratkeluartembusans' => array(self::HAS_MANY, 'Ttsuratkeluartembusan', 'suratkeluarid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'suratkeluarid' => 'Suratkeluarid',
			'unitid' => 'Unitid',
			'subunitid' => 'Subunitid',
			'bagianid' => 'Bagianid',
			'subbagianid' => 'Subbagianid',
			'pejabatid' => 'pejabatid',
			'pegawaiid' => 'pegawaiid',
			'indexkode1' => 'Indexkode1',
			'indexkode2' => 'Indexkode2',
			'indexbulan' => 'Indexbulan',
			'indexnomor' => 'Indexnomor',
			'indextahun' => 'Indextahun',
			'kode' => 'Kode',
			'klasifikasi' => 'Klasifikasi',
			'tglpenyerahan' => 'Tglpenyerahan',
			'nosurat' => 'Nosurat',
			'tglsurat' => 'Tglsurat',
			'kepada' => 'Kepada',
			'perihal' => 'Perihal',
			'kirimvia' => 'Kirimvia',
			'tglkirim' => 'Tglkirim',
			'jamkirim' => 'Jamkirim',
			'paraf' => 'Paraf',
			'status' => 'Status',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('suratkeluarid',$this->suratkeluarid,true);
		$criteria->compare('unitid',$this->unitid,true);
		$criteria->compare('subunitid',$this->subunitid,true);
		$criteria->compare('bagianid',$this->bagianid,true);
		$criteria->compare('subbagianid',$this->subbagianid,true);
		$criteria->compare('pejabatid',$this->pejabatid,true);
		$criteria->compare('pegawaiid',$this->pegawaiid,true);
		$criteria->compare('indexkode1',$this->indexkode1,true);
		$criteria->compare('indexkode2',$this->indexkode2,true);
		$criteria->compare('indexbulan',$this->indexbulan,true);
		$criteria->compare('indexnomor',$this->indexnomor,true);
		$criteria->compare('indextahun',$this->indextahun,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('klasifikasi',$this->klasifikasi,true);
		$criteria->compare('tglpenyerahan',$this->tglpenyerahan,true);
		$criteria->compare('nosurat',$this->nosurat,true);
		$criteria->compare('tglsurat',$this->tglsurat,true);
		$criteria->compare('kepada',$this->kepada,true);
		$criteria->compare('perihal',$this->perihal,true);
		$criteria->compare('kirimvia',$this->kirimvia,true);
		$criteria->compare('tglkirim',$this->tglkirim,true);
		$criteria->compare('jamkirim',$this->jamkirim,true);
		$criteria->compare('paraf',$this->paraf);
		$criteria->compare('status',$this->status);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

    private function dateIndo($tanggal) {
        $result = null;
        $result = substr($tanggal, 8, 2) . '-' . substr($tanggal, 5, 2) . '-' . substr($tanggal, 0, 4);
        if ($result == "--") {
            return null;
        }
        return $result;
    }

    private function dateLongIndo($tanggal) {
        $result = null;
		$bulankode = substr($tanggal, 5, 2);
		if ($bulankode == '01' || $bulankode == '1') {
			$bulan = 'Januari';
		}
        elseif ($bulankode == '02' || $bulankode == '2') {
			$bulan = 'Pebruari';
		}
        elseif ($bulankode == '03' || $bulankode == '3') {
			$bulan = 'Maret';
		}
        elseif ($bulankode == '04' || $bulankode == '4') {
			$bulan = 'April';
		}
        elseif ($bulankode == '05' || $bulankode == '5') {
			$bulan = 'Mei';
		}
        elseif ($bulankode == '06' || $bulankode == '6') {
			$bulan = 'Juni';
		}
        elseif ($bulankode == '07' || $bulankode == '7') {
			$bulan = 'Juli';
		}
        elseif ($bulankode == '08' || $bulankode == '8') {
			$bulan = 'Agustus';
		}
        elseif ($bulankode == '09' || $bulankode == '9') {
			$bulan = 'September';
		}
        elseif ($bulankode == '10') {
			$bulan = 'Oktober';
		}
        elseif ($bulankode == '11') {
			$bulan = 'Nopember';
		}
        elseif ($bulankode == '12') {
			$bulan = 'Desember';
		}
		else {
			$bulan = $bulankode;
		}
        $result = substr($tanggal, 8, 2) . ' ' . $bulan . ' ' . substr($tanggal, 0, 4);
        if ($result == "  ") {
            return null;
        }
        return $result;
    }
	
	public function previewpdf($suratkeluarid)
    {
        //$this->getInstansi();
        
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Keluar");

		// set header and footer fonts
		//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        //$pdf->Cell(0,10,"Example 002",1,1,'C');*/
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

		$sql='';
		//replacelist(a.kesimpulan) as kesimpulan,
		$sql="select a.nama
			from tmpegawai a
			inner join tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
			where a.dlt='0' and b.level = '4' ";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        
		$kepalanama = '';
		foreach ($results AS $result)
        {
			$kepalanama = $result['nama'];
		}
		$sql="select a.*
			from ttsuratkeluar a
			where a.dlt='0' and a.suratkeluarid = '".$suratkeluarid."'
			order by nosurat
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        $nosurat = '';
        $tglsurat = '';
        $tglpenyerahan = '';
		$kepada = '';
        
        $icounter = 0;
        $listpegawai1 = '';
        $listpegawai2 = '';
        
        foreach ($results AS $result)
        {
        	$icounter++;
        	
	        $nosurat = $result['nosurat'];
	        $tglsurat = $this->dateLongIndo($result['tglsurat']);
			$tglpenyerahan = $result['tglpenyerahan'];
			$kepada = $result['kepada'];
		}
		
        $icounter = 1;
        $listjabatan = '';
		$sql="select a.jabatanid, b.nama
			from ttsuratkeluarjabatan a
			inner join tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
			where a.dlt='0' and a.suratkeluarid = '".$suratkeluarid."'
			order by b.nama
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listjabatan .= '<tr>';
			$listjabatan .= '<td width="20" style="text-align: center;">'.$icounter.'.</td><td width="220">'.$result['nama'].'</td>';
			$listjabatan .= '</tr>';
			$icounter++;
		}
		if ($listjabatan != '') {
			$listjabatan = '<table border="0" cellpadding="0" cellspacing="0">'.$listjabatan.'</table>';
		}
		
        $icounter = 1;
        $listdetail = '';
		$listperihal = '';
		$sql="select a.suratkeluardetailid, a.indekkode1, a.indekkode2, a.indekbulan, a.indeknomor, a.indektahun, a.perihal 
			from ttsuratkeluardetail a
			where a.dlt='0' and a.suratkeluarid = '".$suratkeluarid."'
			order by a.indekkode1, a.indekkode2, a.indekbulan, a.indeknomor, a.indektahun
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listdetail .= '<tr>';
			$listdetail .= '<td colspan="2" width="240">'.$result['indekkode1'].'.'.$result['indekkode2'].'/'.$result['indekbulan'].'/'.$result['indeknomor'].'/'.$result['indektahun'].'</td>';
			$listdetail .= '</tr>';
			
			$listperihal .= '<tr>';
			$listperihal .= '<td colspan="2" width="240">'.$result['perihal'].'</td>';
			$listperihal .= '</tr>';
			
			$icounter++;
		}
		if ($listdetail != '') {
			$listdetail = '<table border="0" cellpadding="0" cellspacing="0">'.$listdetail.'</table>';
		}
		if ($listperihal != '') {
			$listperihal = '<table border="0" cellpadding="0" cellspacing="0">'.$listperihal.'</table>';
		}
		
		$icounter = 1;
        $listlampiran = '';
		$sql="select a.suratkeluarlampiranid, a.lampiran
			from ttsuratkeluarlampiran a
			where a.dlt='0' and a.suratkeluarid = '".$suratkeluarid."'
			order by a.nourut
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listlampiran .= '<tr>';
			$listlampiran .= '<td width="20" style="text-align: center;">'.$icounter.'.</td><td width="220">'.$result['lampiran'].'</td>';
			$listlampiran .= '</tr>';
			$icounter++;
		}
		if ($listlampiran != '') {
			$listlampiran = '<table border="0" cellpadding="0" cellspacing="0">'.$listlampiran.'</table>';
		}
		
		$icounter = 1;
        $listtembusan = '';
		$sql="select a.suratkeluartembusanid, a.tembusan
			from ttsuratkeluartembusan a
			where a.dlt='0' and a.suratkeluarid = '".$suratkeluarid."'
			order by a.nourut
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listtembusan .= '<tr>';
			$listtembusan .= '<td width="20" style="text-align: center;">'.$icounter.'.</td><td width="220">'.$result['tembusan'].'</td>';
			$listtembusan .= '</tr>';
			$icounter++;
		}
		if ($listtembusan != '') {
			$listtembusan = '<table border="0" cellpadding="0" cellspacing="0">'.$listtembusan.'</table>';
		}
		
		
		//var_dump($kesimpulan);return;
		$pdf->SetFont('arial', '', 16);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	td {
		font-family: Arial, serif, Times;
	}
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-bottom-thick {
		border-bottom: 2px groove #000;
		outline: 1px solid #000;
		//outline-offset: -19px;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	
</style>
<table border="0" cellpadding="2" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="2"><h3>KEMENTERIAN KESEHATAN RI<br>BTKLPP KELAS I BATAM</h3></td>
		</tr>
		<tr>
			<td width="241" height="60px" class="border-right border-bottom-thick">
				Agenda Surat Keluar No. <br>
				<br><br>
				Diselesaikan oleh penyelenggara:<br>
				<br><br>
				Diperiksa oleh Penanggung Jawab :<br>
				<br><br>
			</td>
			<td width="241" class="border-bottom-thick">
				Agenda Surat Keluar No. :<br>
				$listdetail
			</td>
		</tr>
		<tr>
			<td colspan="2">
			</td>
		</tr>
		<tr>
			<td height="30px">
				Terlebih dahulu/konfirmasi:
			</td>
			<td>
				Batam, $tglsurat
			</td>
		</tr>
		<tr>
			<td height="60px" colspan="2">
				$listjabatan
			</td>
		</tr>
		<tr>
			<td height="80px">
				Ditetapkan:<br>
				Kepala,
			</td>
			<td>
				Kepada<br>
				$kepada
			</td>
		</tr>
		<tr>
			<td height="30" colspan="2">
				<strong>$kepalanama</strong>
			</td>
		</tr>
		<tr>
			<td height="80px">
				Lampiran:<br>
				$listlampiran
			</td>
			<td>
				Tembusan:<br>
				$listtembusan
			</td>
		</tr>
		<tr>
			<td height="30" colspan="2">
				Hal:<br>
				$listperihal
			</td>
		</tr>
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

        $pdf->Output("surat_keluar.pdf", "D");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
}