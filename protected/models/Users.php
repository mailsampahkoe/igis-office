<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $userid
 * @property string $username
 * @property string $password
 * @property string $status
 * @property integer $roleid
 *
 * The followings are the available model relations:
 * @property Role $role
 * @property VerificationLog[] $verificationLogs
 */
class Users extends CActiveRecord {

    public $repeat_password;
    //will hold the encrypted password for update actions.
    public $initialPassword;
	public $subunitnama;
	public $subbagnama;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbmuser';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('password, repeat_password', 'required', 'message' => "Passwords wajib diisi", 'on' => 'insert'),
            array('roleid', 'numerical', 'integerOnly' => true),
			array('userid, unitid, subunitid, subbagid, pcadd, opedit, pcedit', 'length', 'max'=>20),
            array('username', 'length', 'max' => 40),
            array('nama', 'length', 'max' => 50),
            array('password, repeat_password', 'length', 'min' => 6, 'max' => 40),
            array('status, dlt', 'length', 'max' => 1),
			array('tgladd, tgledit', 'safe'),
//            array('photo_type', 'length', 'max' => 20),
//            array('photo', 'safe'),
      //      array('password', 'compare', 'compareAttribute' => 'repeat_password', 'message' => "Passwords tidak sesuai"),
//            array('username', 'ValidatorNameGoesHere', 'readOnly' => true, 'on' => 'update'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('userid, username, password, status, roleid, id_prov, id_kab, id_kec, photo, photo_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'role' => array(self::BELONGS_TO, 'Role', 'roleid'),
            'verificationLogs' => array(self::HAS_MANY, 'VerificationLog', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'userid' => 'ID',
			'username' => 'Username',
            'password' => 'Password',
            'status' => 'Status',
            'roleid' => 'Tipe',
            'photo' => 'Foto'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('userid', $this->userid, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('roleid', $this->roleid);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function validatePassword($username, $password) {
        //$password = str_replace("'","''",$password);
      
        $sql="select password=crypt('$password',password) as password from tbmuser where lower(username)=lower('$username') and dlt='0'";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        $result = $rows[0];
        $hasil = $result['password'];
        
        return $hasil;
    }

    public function encryptPassword($password) {
        $sql="select crypt('$password',password) as password from tbmuser ";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        $result = $rows[0];
        $hasil = $result['password'];
        
        return $hasil;
    }
    
    

    function getMenu() {
        $criteria = new CDbCriteria();
        $criteria->select = 'a.parent,a.menu,a.url';

        $criteria->condition = "a.ishide='0'";
        $dataProvider = new CActiveDataProvider('Menu', array('criteria' => $criteria,
            'pagination' => array('pageSize' => 100),
            'sort' => array(
                'defaultOrder' => 'id DESC',
        )));
        return $dataProvider;
    }

}
