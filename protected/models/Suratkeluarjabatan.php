<?php

/**
 * This is the model class for table "ttsuratkeluarjabatan".
 *
 * The followings are the available columns in table 'ttsuratkeluarjabatan':
 * @property string $suratkeluarjabatanid
 * @property string $suratkeluarid
 * @property string $jabatanid
 * @property string $keterangan
 * @property integer $statusbaca
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Ttsuratkeluar $suratkeluar
 * @property Tmjabatan $jabatan
 */
class Suratkeluarjabatan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Suratkeluarjabatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ttsuratkeluarjabatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('suratkeluarjabatanid', 'required'),
			array('statusbaca, dlt', 'numerical', 'integerOnly'=>true),
			array('suratkeluarjabatanid, suratkeluarid, jabatanid', 'length', 'max'=>20),
			array('keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('suratkeluarjabatanid, suratkeluarid, jabatanid, keterangan, statusbaca, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'suratkeluar' => array(self::BELONGS_TO, 'Ttsuratkeluar', 'suratkeluarid'),
			'jabatan' => array(self::BELONGS_TO, 'Tmjabatan', 'jabatanid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'suratkeluarjabatanid' => 'Suratkeluarjabatanid',
			'suratkeluarid' => 'Suratkeluarid',
			'jabatanid' => 'Jabatanid',
			'keterangan' => 'Keterangan',
			'statusbaca' => 'Statusbaca',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('suratkeluarjabatanid',$this->suratkeluarjabatanid,true);
		$criteria->compare('suratkeluarid',$this->suratkeluarid,true);
		$criteria->compare('jabatanid',$this->jabatanid,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('statusbaca',$this->statusbaca);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}