<?php

/**
 * This is the model class for table "tmsubbagian".
 *
 * The followings are the available columns in table 'tmsubbagian':
 * @property string $subbagianid
 * @property string $bagianid
 * @property string $kode
 * @property string $nama
 * @property string $alamat
 * @property string $keterangan
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Tmjabatan[] $tmjabatans
 * @property Tmbagian $bagian
 * @property Ttsuratmasuk[] $ttsuratmasuks
 */
class Subbagian extends CActiveRecord
{
	public $bagiannama="";
	public $bagiankode="";
	/**
	 * Returns the static model of the specified AR class.
	 * @return Subbagian the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmsubbagian';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subbagianid', 'required'),
			array('dlt', 'numerical', 'integerOnly'=>true),
			array('subbagianid, bagianid, kode', 'length', 'max'=>20),
			array('nama, alamat, keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('subbagianid, bagianid, kode, nama, alamat, keterangan, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tmjabatans' => array(self::HAS_MANY, 'Tmjabatan', 'subbagianid'),
			'bagian' => array(self::BELONGS_TO, 'Tmbagian', 'bagianid'),
			'ttsuratmasuks' => array(self::HAS_MANY, 'Ttsuratmasuk', 'subbagianid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'subbagianid' => 'Subbagianid',
			'bagianid' => 'Bagianid',
			'kode' => 'Kode',
			'nama' => 'Nama',
			'alamat' => 'Alamat',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('subbagianid',$this->subbagianid,true);
		$criteria->compare('bagianid',$this->bagianid,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}