<?php

/**
 * This is the model class for table "ttsuratkeluarlampiran".
 *
 * The followings are the available columns in table 'ttsuratkeluarlampiran':
 * @property string $suratkeluarlampiranid
 * @property string $suratkeluarid
 * @property string $lampiran
 * @property string $keterangan
 * @property integer $nourut
 * @property integer $statusbaca
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Ttsuratkeluar $suratkeluar
 */
class Suratkeluarlampiran extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Suratkeluarlampiran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ttsuratkeluarlampiran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('suratkeluarlampiranid', 'required'),
			array('nourut, statusbaca, dlt', 'numerical', 'integerOnly'=>true),
			array('suratkeluarlampiranid, suratkeluarid', 'length', 'max'=>20),
			array('lampiran', 'length', 'max'=>150),
			array('keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('suratkeluarlampiranid, suratkeluarid, lampiran, keterangan, nourut, statusbaca, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'suratkeluar' => array(self::BELONGS_TO, 'Ttsuratkeluar', 'suratkeluarid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'suratkeluarlampiranid' => 'Suratkeluarlampiranid',
			'suratkeluarid' => 'Suratkeluarid',
			'lampiran' => 'Lampiran',
			'keterangan' => 'Keterangan',
			'statusbaca' => 'Statusbaca',
			'nourut' => 'Nourut',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('suratkeluarlampiranid',$this->suratkeluarlampiranid,true);
		$criteria->compare('suratkeluarid',$this->suratkeluarid,true);
		$criteria->compare('lampiran',$this->lampiran,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('statusbaca',$this->statusbaca);
		$criteria->compare('nourut',$this->nourut);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}