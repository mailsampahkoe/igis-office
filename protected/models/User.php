<?php

/**
 * This is the model class for table "tmuser".
 *
 * The followings are the available columns in table 'tmuser':
 * @property string $userid
 * @property string $pegawaiid
 * @property string $username
 * @property string $password
 * @property integer $grup
 * @property string $aksesid
 * @property string $nama
 * @property string $nohp
 * @property string $email
 * @property string $picture
 * @property integer $status
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property string $dlt
 * @property integer $jeniskelamin
 * @property string $alamat
 * @property string $nik
 * @property string $filenik
 * @property string $npwp
 * @property string $filenpwp
 *
 * The followings are the available model relations:
 * @property Tmakses $akses
 * @property Tmpegawai $pegawai
 * @property Tsnotifikasiuser[] $tsnotifikasiusers
 * @property Tspesanuser[] $tspesanusers
 */
class User extends CActiveRecord
{
	public $akseskode = "";
	public $aksesnama = "";
	public $old_password;
	public $new_password;
	public $repeat_password;
	public $emailstatus;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmuser';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'required'),
			array('grup, status, jeniskelamin', 'numerical', 'integerOnly'=>true),
			array('userid, pegawaiid, aksesid, nohp, nik', 'length', 'max'=>20),
			array('npwp', 'length', 'max'=>30),
			array('filenik, filenpwp', 'length', 'max'=>254),
			array('username, nama, email', 'length', 'max'=>50),
			array('password, alamat', 'length', 'max'=>254),
			array('picture', 'length', 'max'=>100),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('dlt', 'length', 'max'=>1),
			array('old_password, password, repeat_password', 'required', 'on' => 'password'),
			array('old_password', 'findPasswords', 'on' => 'password'),
			array('repeat_password', 'compare', 'compareAttribute'=>'password', 'operator'=>'==','message'=>'Please enter the same password twice', 'on'=>'password, create, update, register'),
	        array('password','passwordalphanumeric', 'on'=>'password, create, update, register'),  
			array('tgladd, tgledit', 'safe'),
			array('email', 'email'),
			//array('filenik, filenpwp', 'required', 'on' => 'register'),
			//array('picture, filenik, filenpwp', 'file', 'types'=>'jpg, gif, png', 'safe' => false),
			array('picture, filenik, filenpwp', 'file', 'mimeTypes'=>'image/jpeg, image/gif, image/png', 'on' => 'register'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, pegawaiid, username, password, grup, aksesid, nama, nohp, email, picture, status, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt, jeniskelamin, alamat, nik, filenik, npwp, filenpwp', 'safe', 'on'=>'search'),
		);
	}
	
	//matching the old password with your existing password.
	public function findPasswords($attribute, $params)
	{
		$user = User::model()->findByPk(Yii::app()->user->id);
		if ($user->password != $this->encryptPassword($this->old_password))
			$this->addError($attribute, 'Password lama tidak valid.');
			//$this->addError($attribute, 'Old password is incorrect.'.$user->password.' != '.$this->encryptPassword($this->old_password));
	}

	public function passwordalphanumeric($attribute_name,$params){
	    if(!empty($this->password)){
	        if (preg_match('~^[a-z0-9]*[0-9][a-z0-9]*$~i',$this->password)) {
	            // $subject is alphanumeric and contains at least 1 number
	        } else {   // failed
	            $this->addError($attribute_name,'Enter password with digits');
	        }
	    }
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'akses' => array(self::BELONGS_TO, 'Tmakses', 'aksesid'),
			'pegawai' => array(self::BELONGS_TO, 'Tmpegawai', 'pegawaiid'),
			'tsnotifikasiusers' => array(self::HAS_MANY, 'Tsnotifikasiuser', 'userid'),
			'tspesanusers' => array(self::HAS_MANY, 'Tspesanuser', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'pegawaiid' => 'Pegawaiid',
			'username' => 'Username',
			'password' => 'Password',
			'grup' => 'Grup',
			'aksesid' => 'Aksesid',
			'nama' => 'Nama',
			'nohp' => 'Nohp',
			'email' => 'Email',
			'picture' => 'Picture',
			'status' => 'Status',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
			'jeniskelamin' => 'Jeniskelamin',
			'alamat' => 'Alamat',
			'nik' => 'Nik',
			'filenik' => 'Filenik',
			'npwp' => 'Npwp',
			'filenpwp' => 'Filenpwp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('pegawaiid',$this->pegawaiid,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('grup',$this->grup);
		$criteria->compare('aksesid',$this->aksesid,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('nohp',$this->nohp,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt,true);
		$criteria->compare('jeniskelamin',$this->jeniskelamin);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('filenik',$this->filenik,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('filenpwp',$this->filenpwp,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
    public function validatePassword($username, $password) {
        //$sql="select password=crypt('$password',password) as password from tmuser where lower(username)=lower('$username') and dlt='0'";
        $sql="select password=md5('$password') as password from tmuser where lower(username)=lower('$username') and dlt='0'";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        $result = $rows[0];
        $hasil = $result['password'];
        
        return $hasil;
    }

    public function encryptPassword($password) {
        //$sql="select crypt('$password',password) as password from tmuser ";
        $sql="select md5('$password') as password from tmuser ";
        $rows = Yii::app()->db->createCommand($sql)->queryAll();
        $result = $rows[0];
        $hasil = $result['password'];
        
        return $hasil;
    }
}