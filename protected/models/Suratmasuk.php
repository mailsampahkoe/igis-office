<?php

/**
 * This is the model class for table "ttsuratmasuk".
 *
 * The followings are the available columns in table 'ttsuratmasuk':
 * @property string $suratmasukid
 * @property string $unitid
 * @property string $subunitid
 * @property string $bagianid
 * @property string $subbagianid
 * @property string $pejabatid
 * @property string $pegawaiid
 * @property string $jenisid
 * @property string $indexkode1
 * @property string $indexkode2
 * @property string $indexbulan
 * @property string $indexnomor
 * @property string $indextahun
 * @property string $kode
 * @property string $klasifikasi
 * @property string $tglpenyerahan
 * @property string $nosurat
 * @property string $tglsurat
 * @property string $asal
 * @property string $perihal
 * @property string $tglpelaksanaan
 * @property string $catatanpelaksanaan
 * @property string $kembalipada
 * @property string $tglkembali
 * @property string $tglterima
 * @property string $jamkembali
 * @property integer $paraf
 * @property integer $status
 * @property string $keterangan
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Tmunit $unit
 * @property Tmsubunit $subunit
 * @property Tmbagian $bagian
 * @property Tmsubbagian $subbagian
 * @property Tmpegawai $jabatan
 * @property Tmpegawai $pegawai
 * @property Tmsubbagian $subbagian
 * @property Ttsuratmasukjabatan[] $ttsuratmasukjabatans
 * @property Ttsuratmasukperintah[] $ttsuratmasukperintahs
 */
class Suratmasuk extends CActiveRecord
{
	public $unitnama="";
	public $unitkode="";
	public $subunitnama="";
	public $subunitkode="";
	public $bagiannama="";
	public $bagiankode="";
	public $subbagiannama="";
	public $subbagiankode="";
	public $ketstatus;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Suratmasuk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ttsuratmasuk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('suratmasukid', 'required'),
			array('paraf, status, dlt', 'numerical', 'integerOnly'=>true),
			array('suratmasukid, unitid, subunitid, bagianid, subbagianid, pejabatid, pegawaiid, jenisid, indexkode1, indexkode2, indexbulan, indexnomor, indextahun, kode, klasifikasi, jamkembali', 'length', 'max'=>20),
			array('nosurat, kembalipada', 'length', 'max'=>100),
			array('asal, perihal, catatanpelaksanaan, keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tglpenyerahan, tglsurat, tglpelaksanaan, tglkembali, tglterima, tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('suratmasukid, unitid, subunitid, bagianid, subbagianid, pejabatid, pegawaiid, jenisid, indexkode1, indexkode2, indexbulan, indexnomor, indextahun, kode, klasifikasi, tglpenyerahan, nosurat, tglsurat, asal, perihal, tglpelaksanaan, catatanpelaksanaan, kembalipada, tglkembali, tglterima, jamkembali, paraf, status, keterangan, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'unit' => array(self::BELONGS_TO, 'Tmunit', 'unitid'),
			'subunit' => array(self::BELONGS_TO, 'Tmsubunit', 'subunitid'),
			'bagian' => array(self::BELONGS_TO, 'Tmbagian', 'bagianid'),
			'subbagian' => array(self::BELONGS_TO, 'Tmsubbagian', 'subbagianid'),
			'pejabat' => array(self::BELONGS_TO, 'Tmpegawai', 'pejabatid'),
			'pegawai' => array(self::BELONGS_TO, 'Tmpegawai', 'pegawaiid'),
			'subbagian' => array(self::BELONGS_TO, 'Tmjenis', 'jenisid'),
			'ttsuratmasukjabatans' => array(self::HAS_MANY, 'Ttsuratmasukjabatan', 'suratmasukid'),
			'ttsuratmasukperintahs' => array(self::HAS_MANY, 'Ttsuratmasukperintah', 'suratmasukid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'suratmasukid' => 'Suratmasukid',
			'unitid' => 'Unitid',
			'subunitid' => 'Subunitid',
			'bagianid' => 'Bagianid',
			'subbagianid' => 'Subbagianid',
			'pejabatid' => 'pejabatid',
			'pegawaiid' => 'pegawaiid',
			'jenisid' => 'Jenisid',
			'indexkode1' => 'Indexkode1',
			'indexkode2' => 'Indexkode2',
			'indexbulan' => 'Indexbulan',
			'indexnomor' => 'Indexnomor',
			'indextahun' => 'Indextahun',
			'kode' => 'Kode',
			'klasifikasi' => 'Klasifikasi',
			'tglpenyerahan' => 'Tglpenyerahan',
			'nosurat' => 'Nosurat',
			'tglsurat' => 'Tglsurat',
			'asal' => 'Asal',
			'perihal' => 'Perihal',
			'tglpelaksanaan' => 'Tglpelaksanaan',
			'catatanpelaksanaan' => 'Catatanpelaksanaan',
			'kembalipada' => 'Kembalipada',
			'tglkembali' => 'Tglkembali',
			'tglterima' => 'Tglterima',
			'jamkembali' => 'Jamkembali',
			'paraf' => 'Paraf',
			'status' => 'Status',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('suratmasukid',$this->suratmasukid,true);
		$criteria->compare('unitid',$this->unitid,true);
		$criteria->compare('subunitid',$this->subunitid,true);
		$criteria->compare('bagianid',$this->bagianid,true);
		$criteria->compare('subbagianid',$this->subbagianid,true);
		$criteria->compare('pejabatid',$this->pejabatid,true);
		$criteria->compare('pegawaiid',$this->pegawaiid,true);
		$criteria->compare('jenisid',$this->jenisid,true);
		$criteria->compare('indexkode1',$this->indexkode1,true);
		$criteria->compare('indexkode2',$this->indexkode2,true);
		$criteria->compare('indexbulan',$this->indexbulan,true);
		$criteria->compare('indexnomor',$this->indexnomor,true);
		$criteria->compare('indextahun',$this->indextahun,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('klasifikasi',$this->klasifikasi,true);
		$criteria->compare('tglpenyerahan',$this->tglpenyerahan,true);
		$criteria->compare('nosurat',$this->nosurat,true);
		$criteria->compare('tglsurat',$this->tglsurat,true);
		$criteria->compare('asal',$this->asal,true);
		$criteria->compare('perihal',$this->perihal,true);
		$criteria->compare('tglpelaksanaan',$this->tglpelaksanaan,true);
		$criteria->compare('catatanpelaksanaan',$this->catatanpelaksanaan,true);
		$criteria->compare('kembalipada',$this->kembalipada,true);
		$criteria->compare('tglkembali',$this->tglkembali,true);
		$criteria->compare('tglterima',$this->tglterima,true);
		$criteria->compare('jamkembali',$this->jamkembali,true);
		$criteria->compare('paraf',$this->paraf);
		$criteria->compare('status',$this->status);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

    private function dateIndo($tanggal) {
        $result = null;
        $result = substr($tanggal, 8, 2) . '-' . substr($tanggal, 5, 2) . '-' . substr($tanggal, 0, 4);
        if ($result == "--") {
            return null;
        }
        return $result;
    }
	
	public function previewpdf($suratmasukid)
    {
        //$this->getInstansi();
        
        $pdf = Yii::createComponent('application.extensions.tcpdf.tcpdf',
                            'P', 'cm', 'A4', true, 'UTF-8');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor("3Digit");
        $pdf->SetTitle("IGIS-Office");
        $pdf->SetSubject("IGIS-Office");
        $pdf->SetKeywords("TND, IGIS-Office, Surat Masuk");

		// set header and footer fonts
		//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //$pdf->AliasNbPages();
		$pdf->SetFont('Times', '', 9, '', true);
        //$pdf->Cell(0,10,"Example 002",1,1,'C');*/
		$pdf->SetMargins(2, 2, 2, 2);
		$pdf->AddPage('P','A4');

		$sql='';
		//replacelist(a.kesimpulan) as kesimpulan,
		$sql="select a.*
			from ttsuratmasuk a
			where a.dlt='0' and a.suratmasukid = '".$suratmasukid."'
			order by nosurat
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        $kode = '';
        $klasifikasi = '';
        $tglpenyerahan = '';
        $nosurat = '';
        $tglsurat = '';
        $asal = '';
        $perihal = '';
        $tglpelaksanaan = '';
        $catatanpelaksanaan = '';
        $kembalipada = '';
        $tglkembali = '';
        
        $icounter = 0;
        
        foreach ($results AS $result)
        {
        	$icounter++;
        	
	        $kode = $result['kode'];
	        $klasifikasi = $result['klasifikasi'];
	        $index = $result['indexkode1'].'.'.$result['indexkode2'].'/'.$result['indexbulan'].'/'.$result['indexnomor'].'/'.$result['indextahun'];
	        $nosurat = $result['nosurat'];
	        $tglsurat = $this->dateIndo($result['tglsurat']);
			$tglpenyerahan = $this->dateIndo($result['tglpenyerahan']);
	        $asal = $result['asal'];
	        $perihal = $result['perihal'];
	        $tglpelaksanaan = $this->dateIndo($result['tglpelaksanaan']);
	        $catatanpelaksanaan = $result['catatanpelaksanaan'];
	        $kembalipada = $result['kembalipada'];
	        $tglkembali = $this->dateIndo($result['tglkembali']);
	        $tglterima = $this->dateIndo($result['tglterima']);
	        $jamkembali = $result['jamkembali'];
		}
		
        $icounter = 1;
        $listjabatan = '';
		$sql="select a.jabatanid, b.nama
			from ttsuratmasukjabatan a
			inner join tmjabatan b on b.jabatanid = a.jabatanid and b.dlt = '0'
			where a.dlt='0' and a.suratmasukid = '".$suratmasukid."'
			order by b.nama
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
		$listjabatan .= '<tr><td colspan="2"><strong>Diteruskan Kepada Yth.</strong></td></tr>';
		$listjabatan .= '<tr><td colspan="2"></td></tr>';
        foreach ($results AS $result)
        {
			$listjabatan .= '<tr>';
			$listjabatan .= '<td width="30" style="text-align: center;">'.$icounter.'.</td><td width="210">'.$result['nama'].'</td>';
			$listjabatan .= '</tr>';
			$icounter++;
		}
		if ($listjabatan != '') {
			$listjabatan = '<table border="0" cellpadding="0" cellspacing="0">'.$listjabatan.'</table>';
		}
		
        $icounter = 1;
        $listpegawai = '';
		$sql="select a.pegawaiid, b.nama, a.disposisi
			from ttsuratmasukpegawai a
			inner join tmpegawai b on b.pegawaiid = a.pegawaiid and b.dlt = '0'
			where a.dlt='0' and a.suratmasukid = '".$suratmasukid."'
			order by b.nama
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results AS $result)
        {
			$listpegawai .= '<tr>';
			$listpegawai .= '<td width="30" style="text-align: center;">'.$icounter.'.</td><td width="210">'.$result['nama'].'</td>';
			$listpegawai .= '</tr>';
			$listpegawai .= '<tr>';
			$listpegawai .= '<td width="30" style="text-align: center;"></td><td width="210">'.$result['disposisi'].'</td>';
			$listpegawai .= '</tr>';
			$icounter++;
		}
		if ($listpegawai != '') {
			$listpegawai = '<tr><td colspan="2"></td></tr>'.'<tr><td width="30" style="text-align: center;"></td><td width="210">Pegawai yang diperintahkan</td></tr>'.$listpegawai;
			$listpegawai = '<table border="0" cellpadding="0" cellspacing="0">'.$listpegawai.'</table>';
		}
        
		$icounter = 1;
        $listperintah = '';
		$sql="select a.perintahid, a.perintah
			from ttsuratmasukperintah a
			left outer join tmperintah b on b.perintahid = a.perintahid and b.dlt = '0'
			where a.dlt='0' and a.suratmasukid = '".$suratmasukid."'
			order by a.nourut
		";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
		$listperintah .= '<tr><td colspan="2"></td></tr>';
		$listperintah .= '<tr><td colspan="2"></td></tr>';
        foreach ($results AS $result)
        {
			$listperintah .= '<tr>';
			$listperintah .= '<td width="30" style="text-align: center;">'.$icounter.'.</td><td width="210">'.$result['perintah'].'</td>';
			$listperintah .= '</tr>';
			$icounter++;
		}
		if ($listperintah != '') {
			$listperintah = '<table border="0" cellpadding="0" cellspacing="0">'.$listperintah.'</table>';
		}
		
		$pdf->SetFont('arial', '', 12);
		$pdf->SetXY(2,2);
$html = <<<EOD
<style>
	p {
    	padding-top: 25px;
		font-size:9px;
		text-align: justify;
	}
	div {
  		margin: 0px;
  		padding: 0px;
	}
	td {
		font-size:9px;
	}
	.border-all {
		border: 0.1px solid black;
	}
	.border-top {
		border-top: 0.1px solid black;
	}
	.border-bottom {
		border-bottom: 0.1px solid black;
	}
	.border-left {
		border-left: 0.1px solid black;
	}
	.border-right {
		border-right: 0.1px solid black;
	}
	.font-bold {
		font-weight: bold;
	}
</style>
<table border="0" cellpadding="2" cellspacing="0">
	<tbody>
		<tr>
			<td style="border: 0.1px solid black;" align="center" colspan="6"><h2>LEMBAR DISPOSISI</h2></td>
		</tr>
		<tr>
			<td width="50" height="20px" class="border-left">Indek</td><td width="10" height="20px">:</td><td width="181" height="20px" class="border-right" align="justify">$index</td>
			<td width="241" height="20px" class="border-right" colspan="3">Rahasia</td>
		</tr>
		<tr>
			<td width="50" height="10px" class="border-bottom border-left">Kode</td><td width="10" class="border-bottom">:</td><td width="181" class="border-bottom border-right" align="justify">$kode</td>
			<td width="80.5" height="10px" class="border-bottom">Tgl. Penyerahan</td><td width="10" class="border-bottom">:</td><td width="150.5" class="border-bottom border-right" align="justify">$tglpenyerahan</td>
		</tr>
		<tr>
			<td width="150" height="20px" class="border-left">Tgl. dan Nomor Surat</td><td width="10">:</td><td width="322" class="border-right" align="justify">$tglsurat / $nosurat</td>
		</tr>
		<tr>
			<td width="150" height="20px" class="border-left">Asal Surat</td><td width="10">:</td><td width="322" class="border-right" align="justify">$asal</td>
		</tr>
		<tr>
			<td width="150" height="70px" class="border-bottom border-left">Hal/Isi</td><td width="10" class="border-bottom">:</td><td width="322" class="border-bottom border-right" align="justify">$perihal</td>
		</tr>
		<tr>
			<td width="241" height="450px" class="border-left border-right" colspan="3">$listjabatan<br>$listpegawai</td>
			<td width="241" height="450px" class="border-right" colspan="3">$listperintah</td>
		</tr>
		<tr>
			<td width="241" height="15px" class="border-top border-left border-right font-bold" colspan="3">Setelah digunakan harap dikembalikan</td>
			<td width="241" height="15px" class="border-top border-right font-bold" colspan="3" align="center"><strong>TANDA TERIMA</strong></td>
		</tr>
		<tr>
			<td width="50" height="10px" class="border-left">Kepada</td><td width="10">:</td><td width="181" class="border-right" align="justify">$kembalipada</td>
			<td width="80.5" height="10px">Tanggal</td><td width="10">:</td><td width="150.5" class="border-right" align="justify">$tglterima</td>
		</tr>
		<tr>
			<td width="50" height="10px" class="border-left">Tanggal</td><td width="10">:</td><td width="181" class="border-right" align="justify">$tglkembali</td>
			<td width="80.5" height="10px">Pukul</td><td width="10">:</td><td width="150.5" class="border-right" align="justify">$jamkembali</td>
		</tr>
		<tr>
			<td width="50" height="10px" class="border-bottom border-left"></td><td width="10" class="border-bottom"></td><td width="181" class="border-bottom border-right" align="justify"></td>
			<td width="80.5" height="10px" class="border-bottom">Paraf</td><td width="10" class="border-bottom">:</td><td width="150.5" class="border-bottom border-right" align="justify"></td>
		</tr>
	</tbody>
</table>
EOD;
		// output the HTML content
		$pdf->writeHTML($html);
		
		$current_top = $pdf->getY();

        $pdf->Output("surat_masuk.pdf", "D");
		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		Yii::$app->response->headers->add('Content-Type', 'application/pdf');
		Yii::app()->end();

		return $this->render('pdf');
    }
}