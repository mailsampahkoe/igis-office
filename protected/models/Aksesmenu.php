<?php

/**
 * This is the model class for table "traksesmenu".
 *
 * The followings are the available columns in table 'traksesmenu':
 * @property string $aksesmenuid
 * @property string $aksesid
 * @property string $menuid
 * @property string $view
 * @property string $add
 * @property string $edit
 * @property string $del
 * @property string $print
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property string $dlt
 *
 * The followings are the available model relations:
 * @property Tmakses $akses
 * @property Tmmenu $menu
 */
class Aksesmenu extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Aksesmenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'traksesmenu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('aksesmenuid', 'required'),
			array('aksesmenuid, aksesid, menuid', 'length', 'max'=>20),
			array('view, add, edit, del, print, dlt', 'length', 'max'=>1),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('aksesmenuid, aksesid, menuid, view, add, edit, del, print, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'akses' => array(self::BELONGS_TO, 'Tmakses', 'aksesid'),
			'menu' => array(self::BELONGS_TO, 'Tmmenu', 'menuid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'aksesmenuid' => 'Aksesmenuid',
			'aksesid' => 'Aksesid',
			'menuid' => 'Menuid',
			'view' => 'View',
			'add' => 'Add',
			'edit' => 'Edit',
			'del' => 'Del',
			'print' => 'Print',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('aksesmenuid',$this->aksesmenuid,true);
		$criteria->compare('aksesid',$this->aksesid,true);
		$criteria->compare('menuid',$this->menuid,true);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('add',$this->add,true);
		$criteria->compare('edit',$this->edit,true);
		$criteria->compare('del',$this->del,true);
		$criteria->compare('print',$this->print,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}