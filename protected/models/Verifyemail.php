<?php

/**
 * This is the model class for table "tsverifyemail".
 *
 * The followings are the available columns in table 'tsverifyemail':
 * @property string $verifyemailid
 * @property string $email
 * @property string $kode
 * @property string $tglgenerate
 * @property integer $status
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property boolean $dlt
 */
class Verifyemail extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Verifyemail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tsverifyemail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('verifyemailid, email, kode', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('verifyemailid, kode', 'length', 'max'=>20),
			array('email', 'length', 'max'=>100),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tglgenerate, tgladd, tgledit, dlt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('verifyemailid, email, kode, tglgenerate, status, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'verifyemailid' => 'Verifyemailid',
			'email' => 'Email',
			'kode' => 'Kode',
			'tglgenerate' => 'Tglgenerate',
			'status' => 'Status',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('verifyemailid',$this->verifyemailid,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('tglgenerate',$this->tglgenerate,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}