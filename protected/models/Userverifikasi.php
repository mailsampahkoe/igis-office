<?php

/**
 * This is the model class for table "tmuserverifikasi".
 *
 * The followings are the available columns in table 'tmuserverifikasi':
 * @property string $userverifikasiid
 * @property string $userid
 * @property string $verifikatorid
 * @property string $tglverifikasi
 * @property integer $status
 * @property string $catatan
 * @property string $keterangan
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property boolean $dlt
 *
 * The followings are the available model relations:
 * @property Tmuser $user
 * @property Tmuser $verifikator
 */
class Userverifikasi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Userverifikasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmuserverifikasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userverifikasiid', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('userverifikasiid, userid, verifikatorid', 'length', 'max'=>20),
			array('catatan, keterangan', 'length', 'max'=>254),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tglverifikasi, tgladd, tgledit, dlt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userverifikasiid, userid, verifikatorid, tglverifikasi, status, catatan, keterangan, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Tmuser', 'userid'),
			'verifikator' => array(self::BELONGS_TO, 'Tmuser', 'verifikatorid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userverifikasiid' => 'Userverifikasiid',
			'userid' => 'Userid',
			'verifikatorid' => 'Verifikatorid',
			'tglverifikasi' => 'Tglverifikasi',
			'status' => 'Status',
			'catatan' => 'Catatan',
			'keterangan' => 'Keterangan',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userverifikasiid',$this->userverifikasiid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('verifikatorid',$this->verifikatorid,true);
		$criteria->compare('tglverifikasi',$this->tglverifikasi,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}