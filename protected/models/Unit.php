<?php

/**
 * This is the model class for table "tmunit".
 *
 * The followings are the available columns in table 'tmunit':
 * @property string $unitid
 * @property string $urusanid
 * @property string $kode
 * @property string $nama
 * @property string $alias
 * @property integer $jenis
 * @property string $alamat
 * @property string $keterangan
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property string $dlt
 *
 * The followings are the available model relations:
 * @property Tmpegawai[] $tmpegawais
 * @property Truserunit[] $truserunits
 * @property Tmurusan $urusan
 * @property Tmsubunit[] $tmsubunits
 * @property Ttanggaran[] $ttanggarans
 */
class Unit extends CActiveRecord
{
	public $urusannama="";
	public $urusankode="";
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Unit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmunit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unitid, urusanid, kode, nama', 'required', 'message'=>'{attribute} harus diisi'),
			array('jenis', 'numerical', 'integerOnly'=>true),
			array('unitid, urusanid, kode', 'length', 'max'=>20),
			array('nama, alamat, keterangan', 'length', 'max'=>254),
			array('alias', 'length', 'max'=>50),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('dlt', 'length', 'max'=>1),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('unitid, urusanid, kode, nama, alias, jenis, alamat, keterangan, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tmpegawais' => array(self::HAS_MANY, 'Tmpegawai', 'unitid'),
			'truserunits' => array(self::HAS_MANY, 'Truserunit', 'unitid'),
			'urusan' => array(self::BELONGS_TO, 'Tmurusan', 'urusanid'),
			'tmsubunits' => array(self::HAS_MANY, 'Tmsubunit', 'unitid'),
			'ttanggarans' => array(self::HAS_MANY, 'Ttanggaran', 'unitid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'unitid' => 'Unitid',
			'urusanid' => 'Urusan',
			'kode' => 'Kode',
			'nama' => 'Nama',
			'alias' => 'Alias',
			'jenis' => 'Jenis',
			'alamat' => 'Alamat',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('unitid',$this->unitid,true);
		$criteria->compare('urusanid',$this->urusanid,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('jenis',$this->jenis);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}