<?php

/**
 * This is the model class for table "tmusermenu".
 *
 * The followings are the available columns in table 'user_menu':
 * @property integer $id
 * @property integer $userid
 * @property integer $menuid
 * @property integer $view
 * @property integer $add
 * @property integer $edit
 * @property integer $del
 * @property integer $print
 */
class Usermenu extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usermenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmusermenu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usermenuid, userid, menuid', 'required'),
			    array('usermenuid,userid,menuid', 'length', 'max' => 40),
			array('view, add, edit, del, print', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('usermenuid, userid, menuid, view, add, edit, del, print', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userid' => 'User',
			'menuid' => 'Menu',
			'view' => 'View',
			'add' => 'Add',
			'edit' => 'Edit',
			'del' => 'Del',
			'print' => 'Print',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('menuid',$this->menuid);
		$criteria->compare('view',$this->view);
		$criteria->compare('add',$this->add);
		$criteria->compare('edit',$this->edit);
		$criteria->compare('del',$this->del);
		$criteria->compare('print',$this->print);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}