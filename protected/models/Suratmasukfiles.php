<?php

/**
 * This is the model class for table "ttsuratmasukfiles".
 *
 * The followings are the available columns in table 'ttsuratmasukfiles':
 * @property string $suratmasukfilesid
 * @property string $suratmasukid
 * @property integer $nourut
 * @property string $files
 * @property string $filename
 * @property string $keterangan
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Ttsuratmasuk $suratmasuk
 */
class Suratmasukfiles extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Suratmasukfiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ttsuratmasukfiles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('suratmasukfilesid', 'required'),
			array('nourut, dlt', 'numerical', 'integerOnly'=>true),
			array('suratmasukfilesid, suratmasukid', 'length', 'max'=>20),
			array('files, filename, keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('suratmasukfilesid, suratmasukid, nourut, files, filename, keterangan, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'suratmasuk' => array(self::BELONGS_TO, 'Ttsuratmasuk', 'suratmasukid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'suratmasukfilesid' => 'Suratmasukfilesid',
			'suratmasukid' => 'Suratmasukid',
			'nourut' => 'Nourut',
			'files' => 'Files',
			'filename' => 'Filename',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('suratmasukfilesid',$this->suratmasukfilesid,true);
		$criteria->compare('suratmasukid',$this->suratmasukid,true);
		$criteria->compare('nourut',$this->nourut);
		$criteria->compare('files',$this->files,true);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}