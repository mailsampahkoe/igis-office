<?php

/**
 * This is the model class for table "tmkelurahan".
 *
 * The followings are the available columns in table 'tmkelurahan':
 * @property string $kelurahanid
 * @property string $kecamatanid
 * @property string $kelurahankode
 * @property string $kelurahannama
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property string $dlt
 *
 * The followings are the available model relations:
 * @property Tmkecamatan $kecamatan
 */
class Kelurahan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Kelurahan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmkelurahan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kelurahanid', 'required'),
			array('kelurahanid, kecamatanid, opadd, pcadd, opedit, pcedit', 'length', 'max'=>20),
			array('kelurahankode', 'length', 'max'=>10),
			array('kelurahannama', 'length', 'max'=>100),
			array('tahun', 'length', 'max'=>4),
			array('dlt', 'length', 'max'=>1),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('kelurahanid, kecamatanid, kelurahankode, kelurahannama, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kecamatan' => array(self::BELONGS_TO, 'Tmkecamatan', 'kecamatanid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kelurahanid' => 'Kelurahanid',
			'kecamatanid' => 'Kecamatanid',
			'kelurahankode' => 'Kelurahankode',
			'kelurahannama' => 'Kelurahannama',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kelurahanid',$this->kelurahanid,true);
		$criteria->compare('kecamatanid',$this->kecamatanid,true);
		$criteria->compare('kelurahankode',$this->kelurahankode,true);
		$criteria->compare('kelurahannama',$this->kelurahannama,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}