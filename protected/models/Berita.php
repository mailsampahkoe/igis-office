<?php

/**
 * This is the model class for table "tsberita".
 *
 * The followings are the available columns in table 'tsberita':
 * @property string $beritaid
 * @property string $judul
 * @property string $keterangan
 * @property string $tglberita
 * @property integer $flag
 * @property integer $status
 * @property string $img
 * @property integer $jenis
 * @property string $url
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property boolean $dlt
 */
class Berita extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Berita the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tsberita';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('beritaid', 'required'),
			array('flag, status, jenis', 'numerical', 'integerOnly'=>true),
			array('beritaid', 'length', 'max'=>20),
			array('judul', 'length', 'max'=>150),
			array('img', 'length', 'max'=>30),
			array('url', 'length', 'max'=>250),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('keterangan, tglberita, tgladd, tgledit, dlt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('beritaid, judul, keterangan, tglberita, flag, status, img, jenis, url, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'beritaid' => 'Beritaid',
			'judul' => 'Judul',
			'keterangan' => 'Keterangan',
			'tglberita' => 'Tglberita',
			'flag' => 'Flag',
			'status' => 'Status',
			'img' => 'Img',
			'jenis' => 'Jenis',
			'url' => 'Url',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('beritaid',$this->beritaid,true);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tglberita',$this->tglberita,true);
		$criteria->compare('flag',$this->flag);
		$criteria->compare('status',$this->status);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('jenis',$this->jenis);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}