<?php

/**
 * This is the model class for table "tmpegawaijabatankegiatan".
 *
 * The followings are the available columns in table 'tmpegawaijabatankegiatan':
 * @property string $pegawaijabatankegiatanid
 * @property string $pegawaijabatanid
 * @property string $kegiatanid
 * @property string $keterangan
 * @property integer $statusaktif
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property string $dlt
 *
 * The followings are the available model relations:
 * @property Tmpegawaijabatan $pegawaijabatan
 * @property Tmkegiatan $kegiatan
 */
class Pegawaijabatankegiatan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pegawaijabatankegiatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmpegawaijabatankegiatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pegawaijabatankegiatanid', 'required'),
			array('statusaktif', 'numerical', 'integerOnly'=>true),
			array('pegawaijabatankegiatanid, pegawaijabatanid, kegiatanid', 'length', 'max'=>20),
			array('keterangan', 'length', 'max'=>254),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('dlt', 'length', 'max'=>1),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pegawaijabatankegiatanid, pegawaijabatanid, kegiatanid, keterangan, statusaktif, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pegawaijabatan' => array(self::BELONGS_TO, 'Tmpegawaijabatan', 'pegawaijabatanid'),
			'kegiatan' => array(self::BELONGS_TO, 'Tmkegiatan', 'kegiatanid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pegawaijabatankegiatanid' => 'Pegawaijabatankegiatanid',
			'pegawaijabatanid' => 'Pegawaijabatanid',
			'kegiatanid' => 'Kegiatanid',
			'keterangan' => 'Keterangan',
			'statusaktif' => 'Statusaktif',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pegawaijabatankegiatanid',$this->pegawaijabatankegiatanid,true);
		$criteria->compare('pegawaijabatanid',$this->pegawaijabatanid,true);
		$criteria->compare('kegiatanid',$this->kegiatanid,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('statusaktif',$this->statusaktif);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}