<?php

/**
 * This is the model class for table "tmsubunit".
 *
 * The followings are the available columns in table 'tmsubunit':
 * @property string $subunitid
 * @property string $unitid
 * @property string $kode
 * @property string $nama
 * @property string $alamat
 * @property string $keterangan
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property string $dlt
 *
 * The followings are the available model relations:
 * @property Tmpegawai[] $tmpegawais
 * @property Truserunit[] $truserunits
 * @property Tmunit $unit
 * @property Ttanggaran[] $ttanggarans
 */
class Subunit extends CActiveRecord
{
	public $unitnama="";
	public $unitkode="";
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Subunit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmsubunit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subunitid, unitid, kode, nama', 'required', 'message'=>'{attribute} harus diisi'),
			array('subunitid, unitid, kode', 'length', 'max'=>20),
			array('nama, alamat, keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('dlt', 'length', 'max'=>1),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('subunitid, unitid, kode, nama, alamat, keterangan, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tmpegawais' => array(self::HAS_MANY, 'Tmpegawai', 'subunitid'),
			'truserunits' => array(self::HAS_MANY, 'Truserunit', 'subunitid'),
			'unit' => array(self::BELONGS_TO, 'Tmunit', 'unitid'),
			'ttanggarans' => array(self::HAS_MANY, 'Ttanggaran', 'subunitid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'subunitid' => 'Subunitid',
			'unitid' => 'SKPD',
			'kode' => 'Kode',
			'nama' => 'Nama',
			'alamat' => 'Alamat',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('subunitid',$this->subunitid,true);
		$criteria->compare('unitid',$this->unitid,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}