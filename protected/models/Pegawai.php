<?php

/**
 * This is the model class for table "tmpegawai".
 *
 * The followings are the available columns in table 'tmpegawai':
 * @property string $pegawaiid
 * @property string $unitid
 * @property string $subunitid
 * @property string $bagianid
 * @property string $subbagianid
 * @property string $jabatanid
 * @property string $kode
 * @property string $nama
 * @property string $gelardepan
 * @property string $gelarbelakang1
 * @property string $gelarbelakang2
 * @property string $tempatlahir
 * @property string $tanggallahir
 * @property string $alamat
 * @property integer $jeniskelamin
 * @property integer $jeniskepegawaian
 * @property integer $statusaktif
 * @property integer $statuspernikahan
 * @property string $nohp
 * @property string $email
 * @property string $keterangan
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Tmpegawaijabatan[] $tmpegawaijabatans
 * @property Tmuser[] $tmusers
 */
class Pegawai extends CActiveRecord
{
	public $unitkode = "";
	public $unitnama = "";
	public $subunitkode = "";
	public $subunitnama = "";
	public $bagiankode = "";
	public $bagiannama = "";
	public $subbagiankode = "";
	public $subbagiannama = "";
	/**
	 * Returns the static model of the specified AR class.
	 * @return Pegawai the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmpegawai';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pegawaiid', 'required'),
			array('jeniskelamin, jeniskepegawaian, statusaktif, statuspernikahan, dlt', 'numerical', 'integerOnly'=>true),
			array('pegawaiid, unitid, subunitid, bagianid, subbagianid, jabatanid, kode', 'length', 'max'=>20),
			array('nama, alamat, keterangan', 'length', 'max'=>254),
			array('gelardepan, gelarbelakang1, gelarbelakang2, nohp', 'length', 'max'=>30),
			array('tempatlahir, email', 'length', 'max'=>50),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tanggallahir, tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pegawaiid, unitid, subunitid, bagianid, subbagianid, jabatanid, kode, nama, gelardepan, gelarbelakang1, gelarbelakang2, tempatlahir, tanggallahir, alamat, jeniskelamin, jeniskepegawaian, statusaktif, statuspernikahan, nohp, email, keterangan, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tmpegawaijabatans' => array(self::HAS_MANY, 'Tmpegawaijabatan', 'pegawaiid'),
			'tmusers' => array(self::HAS_MANY, 'Tmuser', 'pegawaiid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pegawaiid' => 'Pegawaiid',
			'unitid' => 'Unitid',
			'subunitid' => 'Subunitid',
			'bagianid' => 'Bagianid',
			'subbagianid' => 'Subbagianid',
			'jabatanid' => 'Jabatanid',
			'kode' => 'Kode',
			'nama' => 'Nama',
			'gelardepan' => 'Gelardepan',
			'gelarbelakang1' => 'Gelarbelakang1',
			'gelarbelakang2' => 'Gelarbelakang2',
			'tempatlahir' => 'Tempatlahir',
			'tanggallahir' => 'Tanggallahir',
			'alamat' => 'Alamat',
			'jeniskelamin' => 'Jeniskelamin',
			'jeniskepegawaian' => 'Jeniskepegawaian',
			'statusaktif' => 'Statusaktif',
			'statuspernikahan' => 'Statuspernikahan',
			'nohp' => 'Nohp',
			'email' => 'Email',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pegawaiid',$this->pegawaiid,true);
		$criteria->compare('unitid',$this->unitid,true);
		$criteria->compare('subunitid',$this->subunitid,true);
		$criteria->compare('bagianid',$this->bagianid,true);
		$criteria->compare('subbagianid',$this->subbagianid,true);
		$criteria->compare('jabatanid',$this->jabatanid,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('gelardepan',$this->gelardepan,true);
		$criteria->compare('gelarbelakang1',$this->gelarbelakang1,true);
		$criteria->compare('gelarbelakang2',$this->gelarbelakang2,true);
		$criteria->compare('tempatlahir',$this->tempatlahir,true);
		$criteria->compare('tanggallahir',$this->tanggallahir,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('jeniskelamin',$this->jeniskelamin);
		$criteria->compare('jeniskepegawaian',$this->jeniskepegawaian);
		$criteria->compare('statusaktif',$this->statusaktif);
		$criteria->compare('statuspernikahan',$this->statuspernikahan);
		$criteria->compare('nohp',$this->nohp,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}