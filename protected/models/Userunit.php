<?php

/**
 * This is the model class for table "truserunit".
 *
 * The followings are the available columns in table 'truserunit':
 * @property string $userunitid
 * @property string $userid
 * @property string $unitid
 * @property string $subunitid
 * @property string $statusaktif
 * @property string $keterangan
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property string $dlt
 *
 * The followings are the available model relations:
 * @property Tmsubunit $subunit
 * @property Tmunit $unit
 * @property Tmuser $user
 */
class Userunit extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Userunit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'truserunit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userunitid', 'required'),
			array('userunitid, userid, unitid, subunitid', 'length', 'max'=>20),
			array('statusaktif, dlt', 'length', 'max'=>1),
			array('keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userunitid, userid, unitid, subunitid, statusaktif, keterangan, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subunit' => array(self::BELONGS_TO, 'Tmsubunit', 'subunitid'),
			'unit' => array(self::BELONGS_TO, 'Tmunit', 'unitid'),
			'user' => array(self::BELONGS_TO, 'Tmuser', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userunitid' => 'Userunitid',
			'userid' => 'Userid',
			'unitid' => 'Unitid',
			'subunitid' => 'Subunitid',
			'statusaktif' => 'Statusaktif',
			'keterangan' => 'Keterangan',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userunitid',$this->userunitid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('unitid',$this->unitid,true);
		$criteria->compare('subunitid',$this->subunitid,true);
		$criteria->compare('statusaktif',$this->statusaktif,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}