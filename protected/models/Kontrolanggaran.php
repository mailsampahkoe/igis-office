<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel {

    public $kelurusanid;
    public $kelurusankode;
    public $kelurusannama;
    public $urusanid;
    public $urusankode;
    public $urusannama;
    public $unitid;
    public $unitkode;
    public $unitnama;
    public $subunitid;
    public $subunitkode;
    public $subunitnama;
    public $kelurusankegid;
    public $kelurusankegkode;
    public $kelurusankegnama;
    public $urusankegid;
    public $urusankegkode;
    public $urusankegnama;
    public $programid;
    public $programkode;
    public $programnama;
    public $kegiatanid;
    public $kegiatankode;
    public $kegiatannama;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
		return array(
			array('unitid, urusanid, kode', 'length', 'max'=>20),
			array('nama, alamat, keterangan', 'length', 'max'=>254),
			array('alias', 'length', 'max'=>50),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('dlt', 'length', 'max'=>1),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('unitid, urusanid, kelurusanid', 'safe', 'on'=>'search'),
		);
    }


}
