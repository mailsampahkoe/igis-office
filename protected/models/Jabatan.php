<?php

/**
 * This is the model class for table "tmjabatan".
 *
 * The followings are the available columns in table 'tmjabatan':
 * @property string $jabatanid
 * @property string $unitid
 * @property string $subunitid
 * @property string $bagianid
 * @property string $subbagianid
 * @property string $nama
 * @property string $level
 * @property string $keterangan
 * @property integer $terimadisposisi
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Tmunit $unit
 * @property Tmsubunit $subunit
 * @property Tmbagian $bagian
 * @property Tmsubbagian $subbagian
 * @property Ttsuratmasukjabatan[] $ttsuratmasukjabatans
 */
class Jabatan extends CActiveRecord
{
	public $unitnama="";
	public $unitkode="";
	public $subunitnama="";
	public $subunitkode="";
	public $bagiannama="";
	public $bagiankode="";
	public $subbagiannama="";
	public $subbagiankode="";
	/**
	 * Returns the static model of the specified AR class.
	 * @return Jabatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmjabatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jabatanid', 'required'),
			array('terimadisposisi, level, dlt', 'numerical', 'integerOnly'=>true),
			array('jabatanid, unitid, subunitid, bagianid, subbagianid', 'length', 'max'=>20),
			array('nama', 'length', 'max'=>100),
			array('keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('jabatanid, unitid, subunitid, bagianid, subbagianid, nama, level, keterangan, terimadisposisi, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'unit' => array(self::BELONGS_TO, 'Tmunit', 'unitid'),
			'subunit' => array(self::BELONGS_TO, 'Tmsubunit', 'subunitid'),
			'bagian' => array(self::BELONGS_TO, 'Tmbagian', 'bagianid'),
			'subbagian' => array(self::BELONGS_TO, 'Tmsubbagian', 'subbagianid'),
			'ttsuratmasukjabatans' => array(self::HAS_MANY, 'Ttsuratmasukjabatan', 'jabatanid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'jabatanid' => 'Jabatanid',
			'unitid' => 'Unitid',
			'subunitid' => 'Subunitid',
			'bagianid' => 'Bagianid',
			'subbagianid' => 'Subbagianid',
			'nama' => 'Nama',
			'level' => 'Level',
			'keterangan' => 'Keterangan',
			'terimadisposisi' => 'Terimadisposisi',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('jabatanid',$this->jabatanid,true);
		$criteria->compare('unitid',$this->unitid,true);
		$criteria->compare('subunitid',$this->subunitid,true);
		$criteria->compare('bagianid',$this->bagianid,true);
		$criteria->compare('subbagianid',$this->subbagianid,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('terimadisposisi',$this->terimadisposisi);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}