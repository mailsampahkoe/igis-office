<?php

/**
 * This is the model class for table "ttsuratmasukhistory".
 *
 * The followings are the available columns in table 'ttsuratmasukhistory':
 * @property string $suratmasukhistoryid
 * @property string $suratmasukid
 * @property string $pegawaiid
 * @property string $nourut
 * @property string $tglverifikasi
 * @property string $catatan
 * @property string $keterangan
 * @property integer $status
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Ttsuratmasuk $suratmasuk
 * @property Tmpegawai $pegawai
 */
class Suratmasukhistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Suratmasukhistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ttsuratmasukhistory';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('suratmasukhistoryid', 'required'),
			array('nourut, status, dlt', 'numerical', 'integerOnly'=>true),
			array('suratmasukhistoryid, suratmasukid, pegawaiid', 'length', 'max'=>20),
			array('catatan, keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tglverifikasi, tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('suratmasukhistoryid, suratmasukid, pegawaiid, nourut, tglverifikasi, catatan, keterangan, status, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'suratmasuk' => array(self::BELONGS_TO, 'Ttsuratmasuk', 'suratmasukid'),
			'pegawai' => array(self::BELONGS_TO, 'Tmpegawai', 'pegawaiid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'suratmasukhistoryid' => 'Suratmasukhistoryid',
			'suratmasukid' => 'Suratmasukid',
			'pegawaiid' => 'Pegawaiid',
			'nourut' => 'Nourut',
			'tglverifikasi' => 'Tglverifikasi',
			'catatan' => 'Catatan',
			'keterangan' => 'Keterangan',
			'status' => 'Statusbaca',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('suratmasukhistoryid',$this->suratmasukhistoryid,true);
		$criteria->compare('suratmasukid',$this->suratmasukid,true);
		$criteria->compare('pegawaiid',$this->pegawaiid,true);
		$criteria->compare('nourut',$this->nourut,true);
		$criteria->compare('tglverifikasi',$this->tglverifikasi,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}