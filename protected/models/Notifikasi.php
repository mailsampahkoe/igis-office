<?php

/**
 * This is the model class for table "tsnotifikasi".
 *
 * The followings are the available columns in table 'tsnotifikasi':
 * @property string $notifikasiid
 * @property string $judul
 * @property string $keterangan
 * @property string $tglnotifikasi
 * @property string $controller
 * @property string $action
 * @property string $primarykey
 * @property string $transaksiid
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property boolean $dlt
 *
 * The followings are the available model relations:
 * @property Tsnotifikasiuser[] $tsnotifikasiusers
 */
class Notifikasi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Notifikasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tsnotifikasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('notifikasiid', 'required'),
			array('notifikasiid, transaksiid', 'length', 'max'=>20),
			array('judul', 'length', 'max'=>50),
			array('keterangan', 'length', 'max'=>250),
			array('controller, action, primarykey', 'length', 'max'=>30),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tglnotifikasi, tgladd, tgledit, dlt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('notifikasiid, judul, keterangan, tglnotifikasi, controller, action, primarykey, transaksiid, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tsnotifikasiusers' => array(self::HAS_MANY, 'Tsnotifikasiuser', 'notifikasiid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'notifikasiid' => 'Notifikasiid',
			'judul' => 'Judul',
			'keterangan' => 'Keterangan',
			'tglnotifikasi' => 'Tglnotifikasi',
			'controller' => 'Controller',
			'action' => 'Action',
			'primarykey' => 'Primarykey',
			'transaksiid' => 'Transaksiid',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('notifikasiid',$this->notifikasiid,true);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tglnotifikasi',$this->tglnotifikasi,true);
		$criteria->compare('controller',$this->controller,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('primarykey',$this->primarykey,true);
		$criteria->compare('transaksiid',$this->transaksiid,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}