<?php

/**
 * This is the model class for table "tspesan".
 *
 * The followings are the available columns in table 'tspesan':
 * @property string $pesanid
 * @property string $judul
 * @property string $keterangan
 * @property string $tglpesan
 * @property integer $flag
 * @property string $controller
 * @property string $action
 * @property string $transaksiid
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property boolean $dlt
 *
 * The followings are the available model relations:
 * @property Tspesanuser[] $tspesanusers
 */
class Pesan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Pesan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tspesan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pesanid', 'required'),
			array('flag', 'numerical', 'integerOnly'=>true),
			array('pesanid, transaksiid', 'length', 'max'=>20),
			array('judul', 'length', 'max'=>50),
			array('keterangan', 'length', 'max'=>250),
			array('controller, action', 'length', 'max'=>30),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tglpesan, tgladd, tgledit, dlt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pesanid, judul, keterangan, tglpesan, flag, controller, action, transaksiid, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tspesanusers' => array(self::HAS_MANY, 'Tspesanuser', 'pesanid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pesanid' => 'Pesanid',
			'judul' => 'Judul',
			'keterangan' => 'Keterangan',
			'tglpesan' => 'Tglpesan',
			'flag' => 'Flag',
			'controller' => 'Controller',
			'action' => 'Action',
			'transaksiid' => 'Transaksiid',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pesanid',$this->pesanid,true);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tglpesan',$this->tglpesan,true);
		$criteria->compare('flag',$this->flag);
		$criteria->compare('controller',$this->controller,true);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('transaksiid',$this->transaksiid,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}