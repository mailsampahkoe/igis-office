<?php

/**
 * This is the model class for table "tmkecamatan".
 *
 * The followings are the available columns in table 'tmkecamatan':
 * @property string $kecamatanid
 * @property string $kotaid
 * @property string $kecamatankode
 * @property string $kecamatannama
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property string $dlt
 *
 * The followings are the available model relations:
 * @property Tmkota $kota
 * @property Tmkelurahan[] $tmkelurahans
 */
class Kecamatan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Kecamatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tmkecamatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kecamatanid', 'required'),
			array('kecamatanid, kotaid, opadd, pcadd, opedit, pcedit', 'length', 'max'=>20),
			array('kecamatankode', 'length', 'max'=>10),
			array('kecamatannama', 'length', 'max'=>100),
			array('tahun', 'length', 'max'=>4),
			array('dlt', 'length', 'max'=>1),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('kecamatanid, kotaid, kecamatankode, kecamatannama, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kota' => array(self::BELONGS_TO, 'Tmkota', 'kotaid'),
			'tmkelurahans' => array(self::HAS_MANY, 'Tmkelurahan', 'kecamatanid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kecamatanid' => 'Kecamatanid',
			'kotaid' => 'Kotaid',
			'kecamatankode' => 'Kecamatankode',
			'kecamatannama' => 'Kecamatannama',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kecamatanid',$this->kecamatanid,true);
		$criteria->compare('kotaid',$this->kotaid,true);
		$criteria->compare('kecamatankode',$this->kecamatankode,true);
		$criteria->compare('kecamatannama',$this->kecamatannama,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}