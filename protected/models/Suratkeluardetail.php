<?php

/**
 * This is the model class for table "ttsuratkeluardetail".
 *
 * The followings are the available columns in table 'ttsuratkeluardetail':
 * @property string $suratkeluardetailid
 * @property string $suratkeluarid
 * @property string $jenisid
 * @property string $kode
 * @property string $indekkode1
 * @property string $indekkode2
 * @property string $indeknomor
 * @property string $indekbulan
 * @property string $indektahun
 * @property string $tglsurat
 * @property string $klasifikasi
 * @property string $perihal
 * @property string $keterangan
 * @property integer $paraf
 * @property integer $statusbaca
 * @property string $tahun
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 *
 * The followings are the available model relations:
 * @property Ttsuratkeluar $suratkeluar
 */
class Suratkeluardetail extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Suratkeluardetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ttsuratkeluardetail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('suratkeluardetailid', 'required'),
			array('paraf, statusbaca, dlt', 'numerical', 'integerOnly'=>true),
			array('suratkeluardetailid, suratkeluarid, jenisid', 'length', 'max'=>20),
			array('kode, klasifikasi, indekkode1, indekkode2, indeknomor, indekbulan, indektahun', 'length', 'max'=>150),
			array('perihal, keterangan', 'length', 'max'=>254),
			array('tahun', 'length', 'max'=>4),
			array('opadd, pcadd, opedit, pcedit', 'length', 'max'=>40),
			array('tglsurat, tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('suratkeluardetailid, suratkeluarid, jenisid, kode, klasifikasi, indekkode1, indekkode2, indeknomor, indekbulan, indektahun, tglsurat, perihal, keterangan, paraf, statusbaca, tahun, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'suratkeluar' => array(self::BELONGS_TO, 'Ttsuratkeluar', 'suratkeluarid'),
			'suratkeluar' => array(self::BELONGS_TO, 'Tmjenis', 'jenisid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'suratkeluardetailid' => 'Suratkeluardetailid',
			'suratkeluarid' => 'Suratkeluarid',
			'jenisid' => 'Jenisid',
			'kode' => 'Kode',
			'klasifikasi' => 'Klasifikasi',
			'indekkode1' => 'Indekkode1',
			'indekkode2' => 'Indekkode2',
			'indeknomor' => 'Indeknomor',
			'indekbulan' => 'Indekbulan',
			'indektahun' => 'Indektahun',
			'tglsurat' => 'Tglsurat',
			'perihal' => 'Perihal',
			'keterangan' => 'Keterangan',
			'statusbaca' => 'Statusbaca',
			'paraf' => 'Nourut',
			'tahun' => 'Tahun',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('suratkeluardetailid',$this->suratkeluardetailid,true);
		$criteria->compare('suratkeluarid',$this->suratkeluarid,true);
		$criteria->compare('jenisid',$this->jenisid,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('klasifikasi',$this->klasifikasi,true);
		$criteria->compare('indekkode1',$this->indekkode1,true);
		$criteria->compare('indekkode2',$this->indekkode2,true);
		$criteria->compare('indeknomor',$this->indeknomor,true);
		$criteria->compare('indekbulan',$this->indekbulan,true);
		$criteria->compare('indektahun',$this->indektahun,true);
		$criteria->compare('tglsurat',$this->tglsurat,true);
		$criteria->compare('perihal',$this->perihal,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('statusbaca',$this->statusbaca);
		$criteria->compare('paraf',$this->paraf);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}