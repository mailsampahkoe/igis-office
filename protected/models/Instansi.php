<?php

/**
 * This is the model class for table "tminstansi".
 *
 * The followings are the available columns in table 'tminstansi':
 * @property string $instansiid
 * @property string $namainstansi
 * @property string $alamat
 * @property string $telp
 * @property string $fax
 * @property string $kodepos
 * @property string $email
 * @property string $jenisinstansi
 * @property string $jenisdaerah
 * @property string $kepda
 * @property string $kota
 * @property string $namakepda
 * @property string $logo
 * @property string $picpemda
 * @property string $namakadis
 * @property string $nipkadis
 * @property string $namakasi
 * @property string $nipkasi
 * @property string $namakabid
 * @property string $nipkabid
 * @property string $penomoran
 * @property string $penomoran1
 * @property string $penomoran2
 * @property string $opadd
 * @property string $tgladd
 * @property string $pcadd
 * @property string $opedit
 * @property string $tgledit
 * @property string $pcedit
 * @property integer $dlt
 */
class Instansi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Instansi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tminstansi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('instansiid', 'required'),
			array('dlt', 'numerical', 'integerOnly'=>true),
			array('instansiid, opadd, pcadd, opedit, pcedit', 'length', 'max'=>20),
			array('telp, fax, kodepos, email, jenisinstansi, kepda, kota, namakepda, nipkadis, nipkasi, nipkabid, penomoran, penomoran1, penomoran2', 'length', 'max'=>50),
			array('namainstansi, alamat', 'length', 'max'=>150),
			array('jenisdaerah', 'length', 'max'=>1),
			array('logo, picpemda', 'length', 'max'=>1000),
			array('namakadis, namakasi, namakabid', 'length', 'max'=>100),
			array('tgladd, tgledit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('instansiid, namainstansi, alamat, telp, fax, kodepos, email, jenisinstansi, jenisdaerah, kepda, kota, namakepda, logo, picpemda, namakadis, nipkadis, namakasi, nipkasi, namakabid, nipkabid, penomoran, penomoran1, penomoran2, opadd, tgladd, pcadd, opedit, tgledit, pcedit, dlt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'instansiid' => 'Instansiid',
			'namainstansi' => 'Namainstansi',
			'alamat' => 'Alamat',
			'telp' => 'Telp',
			'fax' => 'Fax',
			'kodepos' => 'Kodepos',
			'email' => 'Email',
			'jenisinstansi' => 'Jenisinstansi',
			'jenisdaerah' => 'Jenisdaerah',
			'kepda' => 'Kepda',
			'kota' => 'Kota',
			'namakepda' => 'Namakepda',
			'logo' => 'Logo',
			'picpemda' => 'Picpemda',
			'namakadis' => 'Namakadis',
			'nipkadis' => 'Nipkadis',
			'namakasi' => 'Namakasi',
			'nipkasi' => 'Nipkasi',
			'namakabid' => 'Namakabid',
			'nipkabid' => 'Nipkabid',
			'penomoran' => 'Penomoran',
			'penomoran1' => 'Penomoran1',
			'penomoran2' => 'Penomoran2',
			'opadd' => 'Opadd',
			'tgladd' => 'Tgladd',
			'pcadd' => 'Pcadd',
			'opedit' => 'Opedit',
			'tgledit' => 'Tgledit',
			'pcedit' => 'Pcedit',
			'dlt' => 'Dlt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('instansiid',$this->instansiid,true);
		$criteria->compare('namainstansi',$this->namainstansi,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('telp',$this->telp,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('kodepos',$this->kodepos,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('jenisinstansi',$this->jenisinstansi,true);
		$criteria->compare('jenisdaerah',$this->jenisdaerah,true);
		$criteria->compare('kepda',$this->kepda,true);
		$criteria->compare('kota',$this->kota,true);
		$criteria->compare('namakepda',$this->namakepda,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('picpemda',$this->picpemda,true);
		$criteria->compare('namakadis',$this->namakadis,true);
		$criteria->compare('nipkadis',$this->nipkadis,true);
		$criteria->compare('namakasi',$this->namakasi,true);
		$criteria->compare('nipkasi',$this->nipkasi,true);
		$criteria->compare('namakabid',$this->namakabid,true);
		$criteria->compare('nipkabid',$this->nipkabid,true);
		$criteria->compare('penomoran',$this->penomoran,true);
		$criteria->compare('penomoran1',$this->penomoran1,true);
		$criteria->compare('penomoran2',$this->penomoran2,true);
		$criteria->compare('opadd',$this->opadd,true);
		$criteria->compare('tgladd',$this->tgladd,true);
		$criteria->compare('pcadd',$this->pcadd,true);
		$criteria->compare('opedit',$this->opedit,true);
		$criteria->compare('tgledit',$this->tgledit,true);
		$criteria->compare('pcedit',$this->pcedit,true);
		$criteria->compare('dlt',$this->dlt);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}