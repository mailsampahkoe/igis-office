<?php
/* @var $this PerintahController */
/* @var $model Perintah */

$this->breadcrumbs = array(
    'Perintah' => array('admin'),
    'Ubah Perintah',
);
?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ubah Perintah</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('perintah/admin'); ?>">Data Perintah</a></li>
                <li class="breadcrumb-item active">Ubah Perintah</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>