<?php
/* @var $this PerintahController */
/* @var $model Perintah */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
?>
<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'perintah-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'no urut'); ?>
                <?php echo $form->numberField($model, 'nourut', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Nomor Urut')); ?>
                <?php echo $form->error($model, 'nourut'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'nama perintah disposisi'); ?>
                <?php echo $form->textField($model, 'nama', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Nama Perintah Disposisi')); ?>
                <?php echo $form->error($model, 'nama'); ?>
            </div>
        </div>
		<div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'keterangan'); ?>
                <?php echo $form->textArea($model, 'keterangan', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Keterangan')); ?>
                <?php echo $form->error($model, 'keterangan'); ?>
            </div>
        </div>
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary')); ?>
            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('perintah/admin'); ?>';">Cancel</button>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div></div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    
	$(document).ready(function () {
		<?php if ($ismode == "add") { ?>
		    getKode();
		<?php } ?>
    });
    
    function getKode() {
        $.ajax({
            url: "<?php echo CController::createUrl('perintah/kode') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'kode') ?>').val(data);
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

</script>