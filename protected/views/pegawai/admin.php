<?php
/* @var $this PegawaiController */
/* @var $model Pegawai */

$this->breadcrumbs = array(
    'PEGAWAI' => array('admin'),
    'DATA PEGAWAI',
);

Yii::import('ext.select2.Select2');
$this->breadcrumbs = array(
    'parent' => array('home','site'),
    'Pegawai',
);
?>

<?php
if (!enumVar::BCINMAIN) {
?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">DATA PEGAWAI</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Dashboard</a></li>
                <li class="breadcrumb-item active">DATA PEGAWAI</li>
            </ol>
			<?php if (Yii::app()->user->getprivileges("create", "7") || Yii::app()->user->isSuperadmin()) { ?>
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('pegawai/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tambah Pegawai</button>
			<?php } ?>
        </div>
    </div>
</div>
<?php
}
?>
<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
<div class="table-responsive" style="">
	<div class="card">
	<div class="card-body">
	<?php
	if (enumVar::BCINMAIN) {
	?>
	<div class="row page-titles">
	    <div class="col-md-12 align-self-center text-right">
	        <div class="d-flex justify-content-end align-items-center">
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('pegawai/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tambah Pegawai</button>
	        </div>
	    </div>
	</div>
	<div class="row">&nbsp;
	</div>
	<?php
	}
	?>
	<div class="form-group" id="" style="padding-right:15px;">
		<div class="row">
			<div class="col-lg-6">
            <?php 
				echo Select2::activeDropDownList($model, 'bagianid', array(), array(
                    'prompt' => 'PILIH BAGIAN',
					'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px;'
                ));
			?>
			</div>
        	<div class="col-lg-6">
			<?php
				echo Select2::activeDropDownList($model, 'subbagianid', array(), array(
                    'prompt' => 'PILIH SUB BAGIAN',
                    'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px;'
                )); 
			?>
			</div>
		</div>	
    	<br>
        <div class="row">
			<div class="col-lg-12"> 
	            <?php 
					echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); 
				?>
	        </div>
		</div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'pegawai-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('pegawai/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => '',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Bagian / Sub Bagian',
                'name' => 'skpd',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['bagiannama'].(is_null($data['subbagiannama']) ? "" : " / ".$data['subbagiannama']);
		        },
            ),
            array(
                'header' => 'NIP',
                'name' => 'kode_',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['kode'];
		        },
            ),
            array(
                'header' => 'Nama',
                'name' => 'nama_',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['nama'];
		        },
            ),
            array(
                'header' => 'Email/No HP',
                'name' => 'email_',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['email']."/".$data['nohp'];
		        },
            ),
            array(
                'template' => '{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/pegawai/update", array("pegawaiid"=>$data["pegawaiid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "7") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/pegawai/delete", array("pegawaiid"=>$data["pegawaiid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "7") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
	
	function loadBagian() {
		$.ajax({
            url: "<?php echo CController::createUrl('pegawai/loadbagian') ?>",
            type: 'POST',
            data: {isall:'true'},
            success: function (data) {
				$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val('');
                $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').html(data);
				if ("<?php echo $model->bagianid; ?>"!="")
                	$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val', "<?php echo $model->bagianid; ?>");
				loadSubbagian();	
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	
    function loadSubbagian() {
        $.ajax({
            url: "<?php echo CController::createUrl('pegawai/loadsubbagian') ?>",
            type: 'POST',
            data: {isall:'true',bagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val()},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').val('');
                $('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').html(data);
				if ("<?php echo $model->subbagianid; ?>"!="")
                	$('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').select2().select2('val', "<?php echo $model->subbagianid; ?>");
				loadSubbag();
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	
	function tambahData() {
        js:document.location.href="<?php echo CController::createUrl('pegawai/create') ?>/bagianid/"+$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val()+"/subbagianid/"+$('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').val();
    }
	$(document).ready(function () {
        loadBagian();
		$('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				$.fn.yiiGridView.update('pegawai-grid', {
					//data: $(this).serialize()
					data: {searchtext:$('#searchtext').val(),filterbagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val(),filtersubbagianid: $('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').val()},
				});
				return false;
			}
		});
		$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').change(function(e){
			$('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').val('');
			loadSubbagian();
			$.fn.yiiGridView.update('pegawai-grid', {
				//data: $(this).serialize()
				data: {searchtext:$('#searchtext').val(),filterbagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val(),filtersubbagianid: $('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').val()},
			});
			return false;
		});
		$('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').change(function(e){
			$.fn.yiiGridView.update('pegawai-grid', {
				//data: $(this).serialize()
				data: {searchtext:$('#searchtext').val(),filterbagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val(),filtersubbagianid: $('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').val()},
			});
			return false;
		});
		
		
    });
</script>