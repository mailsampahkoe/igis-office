<div class="col-lg-4">
    <section class="box">
        <header class="panel_header"></header>
        <div class="content-body">
            <center>
                <img src="<?php echo $model->photo == "" ? Yii::app()->request->baseUrl . "/assets/logo/blank.jpg" : Yii::app()->controller->createUrl('member/loadimagephoto', array('id' => $model->id)) ?>" width="200px">

                <h3><b><?php echo $model->member_name; ?></b></h3>
                <h4>
                    <?php echo "DPD " . Member::getKabProvKec($model->member_sub_district_id, "nama_kab") . ", " . "DPC " . Member::getKabProvKec($model->member_sub_district_id, "nama_kec") ?>
                </h4>

                <input type="checkbox" id="is_print" class="skin-square-blue" onclick="statusPrint()">
                <label id="is_print_label" class="icheck-label form-label" for="square-checkbox-4">KTA BELUM CETAK</label>
				<?php if (Yii::app()->user->getprivileges("edit", "6") || Yii::app()->user->isSuperadmin()) {
                    ?>
                <button class="btn btn-danger btn-block " onclick="blokir()" type="button">BLOKIR PENGGUNA</button>
				 <?php } ?>
                <?php if (Yii::app()->user->getprivileges("print", "6") || Yii::app()->user->isSuperadmin()) {
                    ?>
                    <button class="btn btn-primary btn-block" onclick="printProfil()" type="button">CETAK PROFIL</button>
                    <button class="btn btn-info btn-block" onclick="printKTA()"  type="button">CETAK KTA</button>
                <?php } ?>
				 <?php if (Yii::app()->user->getprivileges("edit", "6") || Yii::app()->user->isSuperadmin()) {
                    ?>
                <button class="btn btn-success btn-block" type="button" onclick="updateProfil()" >UPDATE PROFIL</a></button>
				 <?php } ?>
                <button class="btn btn-warning btn-block" type="button">SETTING LAYANAN</button>
                <br/>

                <img src="<?php echo $model->card_identity == "" ? Yii::app()->request->baseUrl . "/assets/logo/blank_card.png" : Yii::app()->controller->createUrl('member/loadimage', array('id' => $model->id)) ?>" width="100%" height="200">
            </center>
        </div>
    </section></div>
<div class="col-lg-8" style="right: 0px;">
    <section class="box">
        <header class="panel_header"></header>
        <div class="content-body">
            <h3>INFORMASI ANGGOTA</h3>
            <hr style="border-bottom: 1px solid green;">    

            <?php
            $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                    array(
                        'label' => 'No KTA',
                        'name' => 'membership_id',
                        'type' => 'raw',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                    // 'value' => $model->membership_id
                    ),
                    array(
                        'label' => 'DPD',
                        'name' => 'member_sub_district_id',
                        'type' => 'raw',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                        'value' => Member::getKabProvKec($model->member_sub_district_id, "nama_prov"),
                    ),
                    array(
                        'label' => 'DPC',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                        'name' => 'member_sub_district_id',
                        'type' => 'raw',
                        'value' => Member::getKabProvKec($model->member_sub_district_id, "nama_kab"),
                    ),
                    array(
                        'label' => 'Nama',
                        'name' => 'member_name',
                        'type' => 'raw',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                    //'value' => $model->first_name . " " . $model->last_name
                    ),
                    array(
                        'label' => 'Jenis Kelamin',
                        'name' => 'gender',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                        'type' => 'raw',
                        'value' => Member::getGender($model->gender),
                        'htmlOptions' => array('style' => 'width:50px;text-align: center;'),
                    ), array(
                        'label' => 'Phone',
                        'name' => 'cellular_phone_number',
                        'type' => 'raw',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                        'value' => $model->cellular_phone_number . "<br/>" . $model->home_phone_number
                    ),
                    array(
                        'label' => 'Tempat/Tanggal Lahir',
                        'name' => 'birth_place',
                        'type' => 'raw',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                        'value' => $model->birth_place . " / " . Globals::dateIndonesia($model->date_of_birth)
                    ),
                    array(
                        'label' => 'Status Kawin',
                        'name' => 'is_married',
                        'type' => 'raw',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                        'value' => $model->is_married == "Y" ? "Kawin" : "Tidak Kawin"
                    ),
                    'blood_type',
                    'occupation',
                    'couple_name',
                    'children_name',
                    array(
                        'label' => 'Alamat',
                        'name' => 'address',
                        'type' => 'raw',
                        'headerHtmlOptions' => array('style' => 'text-align: left;'),
                        'value' => $model->address . " " . $model->home_number . " RT " . $model->rt .
                        " RW " . $model->rw . ""
                    //   " <br/> Kab." . Member::getID("nama", "kabupaten", "id_kab=$model->member_city_id") . " " . $model->postal_code
                    ),
                    'email',
                    'facebook',
                    'twitter',
                ),
            ));
            ?>
        </div>
    </section>
</div>
<script type="text/javascript">
    function printKTA()
    {

        var myWindow = window.open('<?php echo Yii::app()->controller->createUrl('member/cetakkta', array('id' => $model->id)) ?>', '', '');

        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        //        myWindow.close();
    }

    function printProfil()
    {

        var myWindow = window.open('<?php echo Yii::app()->controller->createUrl('member/cetakprofil', array('id' => $model->id)) ?>', '', '');

        myWindow.document.close();
        myWindow.focus();
        myWindow.print();
        //        myWindow.close();
    }

    function updateProfil() {
        window.location = '<?php echo Yii::app()->createUrl('member/update', array('id' => $model->id)) ?>';
    }

    function statusPrint() {
        $.ajax({
            url: "<?php echo CController::createUrl('member/statusprint') ?>",
            type: 'POST',
            data: {id: <?php echo $model->id; ?>, value: $('#is_print').is(':checked')},
            success: function (data) {
                var obj = $.parseJSON(data);
                if (obj.success) {
                    // alert(obj.pesan);
                }
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
    
    function blokir() {
        $.ajax({
            url: "<?php echo CController::createUrl('member/blokir') ?>",
            type: 'POST',
            data: {id: <?php echo $model->id; ?>},
            success: function (data) {
                var obj = $.parseJSON(data);
                if (obj.success) {
                    alert(obj.pesan);
                    window.location = "<?php echo Yii::app()->controller->createUrl('member/admin') ?>";
                }
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

    $("#is_print").change(function () {
        // alert($("#is_print").is(':checked'));
        if ($("#is_print").is(':checked')) {
            $("#is_print_label").text("KTA SUDAH CETAK");
        } else {
            $("#is_print_label").text("KTA BELUM CETAK");
        }
    });

    $(document).ready(function () {
        //  alert(<?php //echo $model->is_printed;       ?>);
        if ("<?php echo $model->is_printed; ?>" == "1") {
            $('#is_print').prop('checked', true);
            $("#is_print_label").text("KTA SUDAH CETAK");
        } else {
            $('#is_print').prop('checked', false);
            $("#is_print_label").text("KTA BELUM CETAK");
        }
    });

</script>