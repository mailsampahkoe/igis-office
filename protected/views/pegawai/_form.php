<?php
/* @var $this PegawaiController */
/* @var $model Pegawai */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
?>
<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'pegawai-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
        <div class="form-group">
			<div class="row">
            <div class="col-lg-6">
                <?php echo $form->labelEx($model, 'bagian'); ?><br>
                <?php
                    echo Select2::activeDropDownList($model, 'bagianid', array(), array(
                        'prompt' => 'PILIH BAGIAN',
						'onchange' => '{loadSubbagian(),loadJabatan()}',
                        'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px'
                    ));
                ?>
                <?php echo $form->error($model, 'bagianid'); ?>
            </div>
            <div class="col-lg-6">
                <?php echo $form->labelEx($model, 'sub bagian'); ?><br>
                <?php
                    echo Select2::activeDropDownList($model, 'subbagianid', array(), array(
                        'prompt' => 'PILIH SUB BAGIAN',
						'onchange' => '{loadJabatan()}',
						'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px'
                    ));
                ?>
                <?php echo $form->error($model, 'subbagianid'); ?>
            </div>
			</div>
        </div>
        <div class="form-group">
			<div class="row">
            <div class="col-lg-6">
                <?php echo $form->labelEx($model, 'N I P'); ?>
                <?php echo $form->textField($model, 'kode', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'N I P')); ?>
                <?php echo $form->error($model, 'kode'); ?>
            </div>
			<div class="col-lg-6">
                <?php echo $form->labelEx($model, 'nama pegawai'); ?>
                <?php echo $form->textField($model, 'nama', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Nama Pegawai')); ?>
                <?php echo $form->error($model, 'nama'); ?>
            </div>
			</div>
        </div>
        
		<div class="form-group">
            <div class="row">
				<div class="col-lg-6">
				<label>Gelar</label>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
	            <?php echo $form->textField($model, 'gelardepan', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Gelar Depan')); ?>
				</div>
	        	<div class="col-lg-4">
				<?php echo $form->textField($model, 'gelarbelakang1', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Gelar Belakang 1')); ?>
				</div>
				<div class="col-lg-4">
				<?php echo $form->textField($model, 'gelarbelakang2', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Gelar Belakang 2')); ?>
				</div>
			</div>	
        </div>
        <div class="form-group">
			<div class="row">
	            <div class="col-lg-6">
	                <?php echo $form->labelEx($model, 'jabatan'); ?><br>
	                <?php
	                    echo Select2::activeDropDownList($model, 'jabatanid', array(), array(
	                        'prompt' => 'PILIH JABATAN',
	                        'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px'
	                    ));
	                ?>
	                <?php echo $form->error($model, 'jabatanid'); ?>
	            </div>
            </div>
        </div>
		<div class="form-group">
            <div class="row">
				<div class="col-lg-4">
				<?php echo $form->labelEx($model, 'tempat lahir'); ?>
	            <?php echo $form->textField($model, 'tempatlahir', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Tempat Lahir')); ?>
				</div>
	        	<div class="col-lg-4">
				<?php echo $form->labelEx($model, 'tanggal lahir'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'language' => 'id',
                    'attribute' => 'tanggallahir',
                    'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy'),
                    'htmlOptions' => array('readonly' => false, 'class' => 'form-control')
                ));
                ?> 
				</div>
				<div class="col-lg-4">
				<?php echo $form->labelEx($model, 'jenis kelamin'); ?><br>
                <?php
                echo $form->radioButtonList($model, 'jeniskelamin', array(
                    "0" => 'Laki-laki',
                    "1" => 'Perempuan'
                        ), array(
                    'labelOptions' => array('class' => 'iradio_flat-blue'), // add this code
                    'separator' => '  ',
                ));
                ?> 
				</div>
			</div>	
        </div>
		<div class="form-group">
			<div class="row">
            <div class="col-lg-6">
                <?php echo $form->labelEx($model, 'no hp'); ?>
                <?php echo $form->textField($model, 'nohp', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'No HP')); ?>
                <?php echo $form->error($model, 'nohp'); ?>
            </div>
			<div class="col-lg-6">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->emailField($model, 'email', array('size' => 40, 'placeholder' => 'Email', 'class' => 'form-control')); ?>
				<?php echo $form->error($model, 'email'); ?>
            </div>
			</div>
        </div>
		<div class="form-group">
			<div class="row">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'alamat'); ?>
                <?php echo $form->textField($model, 'alamat', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Alamat')); ?>
                <?php echo $form->error($model, 'alamat'); ?>
            </div></div>
        </div>
		<div class="form-group">
			<div class="row">
            <div class="col-lg-6">
                <?php echo $form->labelEx($model, 'status pernikahan'); ?><br>
                <?php
                    echo Select2::activeDropDownList($model, 'statuspernikahan', array("0"=>"Belum menikah","1"=>"Sudah menikah","2"=>"Janda","3"=>"Duda"), array(
                        'class' => 'col-lg-12', 'selected' => 'selected', 'style' => 'padding:0px'
                    ));
                ?>
                <?php echo $form->error($model, 'statuspernikahan'); ?>
            </div>
			<div class="col-lg-6">
                <?php echo $form->labelEx($model, 'status aktif'); ?>
                <br>
				<?php
                echo $form->radioButtonList($model, 'statusaktif', array(
                    "1" => 'Aktif',
                    "0" => 'Non Aktif'
                        ), array(
                    'labelOptions' => array('class' => 'iradio_flat-blue'), // add this code
                    'separator' => '  ',
                ));
                ?>
                <?php echo $form->error($model, 'statusaktif'); ?>
            </div>
			</div>
        </div>
		<!--<div class="form-group">
            <div class="row">
            <div class="col-lg-12">
                <h3>Jabatan</h3>
            </div></div>
			<div class="row">
			<?php
				$datas = new enumVar;
				$arrlabeljabatan = $datas->listJabatan("name");
				$arridjabatan = $datas->listJabatan();
				$lengtharr = count($arrlabeljabatan);
			?>
            <div class="col-lg-4">
              <?php
			  		for ($i=0; $i<intval($lengtharr/2); $i++){
						$linkkeg = "";
						if ($arridjabatan[$i]==enumVar::JABATAN_PPTK) $linkkeg = "&nbsp&nbsp<a href=\"#\">Daftar Kegiatan</a>";
						echo CHtml::checkBox('chkjabatan[]',(strpos($strjab, ",".$arridjabatan[$i].",")!==false), array("id"=>"jabatan".$arridjabatan[$i],"value"=>"".$arridjabatan[$i]));
						echo " ".$arrlabeljabatan[$i]."$linkkeg <br>";
					}
				?>  
            </div>
			<div class="col-lg-6">
              <?php
					for ($i=intval($lengtharr/2); $i<$lengtharr; $i++){
						echo CHtml::checkBox('chkjabatan[]',false, array("id"=>"jabatan".$arridjabatan[$i],"value"=>"".$arridjabatan[$i]));
						echo " ".$arrlabeljabatan[$i]."<br>";
					}
				?>  
            </div>
			</div>
        </div>
		<br>-->
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary')); ?>
            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('pegawai/admin'); ?>';">Cancel</button>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div></div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    
	$(document).ready(function () {
		loadBagian();
    });
	
	
	function loadBagian() {
		$.ajax({
            url: "<?php echo CController::createUrl('pegawai/loadbagian') ?>",
            type: 'POST',
            success: function (data) {
				$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val('');
                $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').html(data);
				if ("<?php echo $model->bagianid; ?>"!="")
                	$('#<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val', "<?php echo $model->bagianid; ?>");
				loadSubbagian();
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	
	function loadSubbagian() {
        $.ajax({
            url: "<?php echo CController::createUrl('pegawai/loadsubbagian') ?>",
            type: 'POST',
            data: {bagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val()},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'subbagianid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'subbagianid') ?>').select2().select2('val', '<?php echo $model->subbagianid; ?>');
				
				loadJabatan();
            },
            error: function (jqXHR, status, err) {
                //alert(err);
				loadJabatan();
            }
        });
    }
	
	function loadJabatan() {
        $.ajax({
            url: "<?php echo CController::createUrl('pegawai/loadjabatan') ?>",
            type: 'POST',
            data: {bagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val(), subbagianid: $('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').val()},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'jabatanid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'jabatanid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'jabatanid') ?>').select2().select2('val', '<?php echo $model->jabatanid; ?>');
				
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
    
	
</script>