<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<?php
/* @var $this PelaksanaanController */
/* @var $model Suratmasuk */

$this->breadcrumbs = array(
    'Pelaksanaan' => array('admin'),
    'Pelaksanaan',
);
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Pelaksanaan</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('pelaksanaan/admin'); ?>">Pelaksanaan</a></li>
                <li class="breadcrumb-item active">Pelaksanaan</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
			<div class="card">
			<?php
	        $form = $this->beginWidget('CActiveForm', array(
	            'id' => 'suratmasuk-form',
	            'method' => 'POST',
	            'enableAjaxValidation' => true,
	            'clientOptions' => array(
	                'validateOnSubmit' => true,
	                'validateOnChange' => true
	            ),
	            'htmlOptions' => array(
	                'enctype' => 'multipart/form-data',
                'onsubmit'=>'return validate()',
	            )
	        ));
	        ?>
	        <?php echo $form->errorSummary($model); ?>
	        <?php echo $form->hiddenField($model, 'status'); ?>
    			<div class="card-body">
		            <div class="form-body">
		                <h3 class="box-title">Agenda Surat Masuk</h3>
		                <hr class="m-t-0 m-b-40">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Indek:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->indexnomor; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Klasifikasi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->klasifikasi; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Kode:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->kode; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl. Penyerahan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglpenyerahan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl dan Nomor Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglsurat; ?> <?php echo $model->nosurat; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Asal Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->asal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Hal/Isi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->perihal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <hr class="m-t-0 m-b-40">
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">Penerima Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_jabatan"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
				                <h3 class="box-title">Perintah Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group row">
		                            <div class="col-md-12">
		                                <div id="d_perintah"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">Staf Penerima Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_pegawai"></div>
		                            </div>
		                        </div>
		                    </div>
				            <div class="col-md-6">
				                <h3 class="box-title">File yang sudah diupload</h3>
				                <hr class="m-t-0">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div id="d_files"></div>
				                    </div>
				                </div>
				            </div>
				            <!--/span-->
			            </div>
		                <!--/row-->
						<hr>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4 col-sm-4">
								<?php echo $form->labelEx($model, 'tanggal pelaksanaan'); ?>
								<?php
								$this->widget('zii.widgets.jui.CJuiDatePicker', array(
									'model' => $model,
									'language' => 'id',
									'attribute' => 'tglpelaksanaan',
									'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy', 'changeMonth'=>true, 'changeYear'=>true),
									'htmlOptions' => array('readonly' => false, 'class' => 'form-control', 'maxlength' => 10)
								));
								?> 
								</div>
								<div class="col-md-8 col-sm-8">
									<?php echo $form->labelEx($model, 'catatan'); ?><br>
									<?php echo $form->textField($model, 'catatanpelaksanaan', array('size' => 60, 'maxlength' => 254, 'class' => 'form-control', 'placeholder' => 'Catatan')); ?>
									<?php echo $form->error($model, 'catatanpelaksanaan'); ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-8 col-sm-8">
									<?php echo $form->labelEx($model, 'dikembalikan pada'); ?><br>
									<?php echo $form->textField($model, 'kembalipada', array('size' => 60, 'maxlength' => 254, 'class' => 'form-control', 'placeholder' => 'Dikembalikan pada')); ?>
									<?php echo $form->error($model, 'kembalipada'); ?>
								</div>
								<div class="col-md-4 col-sm-4">
								<?php echo $form->labelEx($model, 'tanggal kembali'); ?>
								<?php
								$this->widget('zii.widgets.jui.CJuiDatePicker', array(
									'model' => $model,
									'language' => 'id',
									'attribute' => 'tglkembali',
									'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy', 'changeMonth'=>true, 'changeYear'=>true),
									'htmlOptions' => array('readonly' => false, 'class' => 'form-control', 'maxlength' => 10)
								));
								?> 
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4 col-sm-4">
								<?php echo $form->labelEx($model, 'tanggal terima'); ?>
								<?php
								$this->widget('zii.widgets.jui.CJuiDatePicker', array(
									'model' => $model,
									'language' => 'id',
									'attribute' => 'tglterima',
									'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy', 'changeMonth'=>true, 'changeYear'=>true),
									'htmlOptions' => array('readonly' => false, 'class' => 'form-control', 'maxlength' => 10)
								));
								?> 
								</div>
								<div class="col-md-4 col-sm-4">
									<?php echo $form->labelEx($model, 'jam terima'); ?><br>
									<?php echo $form->textField($model, 'jamterima', array('size' => 60, 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Jam Terima')); ?>
									<?php echo $form->error($model, 'jamterima'); ?>
								</div>
							</div>
						</div>
		                <div class="row">
		                    <div class="col-md-12">
				                <h3 class="box-title">Konfirmasi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group row">
		                            <div class="col-md-10">
		                                <input type="checkbox" name="konfirmasi" id="konfirmasi" <?php if ($confirmed == '1') echo 'disabled checked'; ?>> &nbsp;<strong>Dengan menceklis, Anda menyatakan setuju telah melakukan konfirmasi.</strong></input>
		                            </div>
		                        </div>
		                    </div>
	                    </div>
		                <!--/row-->
		            </div>
		            <div class="form-actions">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="row">
		                            <div class="col-md-offset-3 col-md-9">
							            <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-primary')); ?>
		                                <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('pelaksanaan/admin'); ?>';">Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6"> </div>
		                </div>
		            </div>
		    	</div>
	        <?php $this->endWidget(); ?>
		    
		    </div>
		</div>
    </div>
</div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    var v_detailTmpId = 0;
    var v_listDataPerintah = Array();
    var v_listDataJabatan = Array();
    var v_listDataPegawai = Array();
	var v_listDataFiles = Array();
    var v_id = '';
    
	$(document).ready(function () {
        $(".select2").select2();
        
		loadDetailJabatan();
		loadDetailPerintah();
        loadDetailPegawai();
		loadDetailFiles();
    });
	
	function validate() {
	}
    
    function loadDetailPegawai() {
        v_listDataPegawai = [];
        $.ajax({
            url: "<?php echo CController::createUrl('pelaksanaan/loadsuratmasukpegawai') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukpegawaiid": p_value.suratmasukpegawaiid, 
										"suratmasukid": p_value.suratmasukid, 
										"pegawaiid": p_value.pegawaiid, 
										"pegawaiview": p_value.pegawaiview,
										"disposisi": p_value.disposisi,
										"statusbaca": p_value.statusbaca,
										"ketstatusbaca": p_value.ketstatusbaca	};
					v_listDataPegawai.push(v_newData);
			    });
            	
            	loadDataPegawai('d_pegawai');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataPegawai(p_divName) {
		var v_filter = v_listDataPegawai;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukpegawai" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 35%;">Pegawai</th><th class="header" style="width: 30%;">Disposisi</th><th class="header" style="width: 30%;">Status</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukpegawai">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.pegawaiview+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.disposisi+'</td>';
				if (p_value.statusbaca == '0' || p_value.statusbaca == null) {
		    		v_rows = v_rows + '<td style="color: red; font-weight: bold;">'+p_value.ketstatusbaca+'</td>';
				}
				else {
		    		v_rows = v_rows + '<td style="color: green; font-weight: bold;">'+p_value.ketstatusbaca+'</td>';
				}
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailPerintah() {
        v_listDataPerintah = [];
        $.ajax({
            url: "<?php echo CController::createUrl('pelaksanaan/loadsuratmasukperintah') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukperintahid": p_value.suratmasukperintahid, 
										"suratmasukid": p_value.suratmasukid, 
										"perintahid": p_value.perintahid,
										"perintah": p_value.perintah	};
					v_listDataPerintah.push(v_newData);
			    });
            	
            	loadDataPerintah('d_perintah');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataPerintah(p_divName) {
		var v_filter = v_listDataPerintah;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukperintah" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 85%;">Perintah</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukperintah">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.perintah+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailJabatan() {
        v_listDataJabatan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('pelaksanaan/loadsuratmasukjabatan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukjabatanid": p_value.suratmasukjabatanid, 
										"suratmasukid": p_value.suratmasukid, 
										"jabatanid": p_value.jabatanid,
										"jabatan": p_value.jabatan	};
					v_listDataJabatan.push(v_newData);
			    });
            	
            	loadDataJabatan('d_jabatan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataJabatan(p_divName) {
		var v_filter = v_listDataJabatan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukjabatan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 85%;">Jabatan</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukjabatan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.jabatan+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailFiles() {
        v_listDataFiles = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadsuratmasukfiles') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukfilesid": p_value.suratmasukfilesid, 
										"suratmasukid": p_value.suratmasukid, 
										"filename": p_value.filename,
										"files": p_value.files	};
					v_listDataFiles.push(v_newData);
			    });
            	
            	loadDataFiles('d_files');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataFiles(p_divName) {
		var v_filter = v_listDataFiles;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
        v_rows = v_rows + '        <div class="card-columns el-element-overlay">';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/big/img5.jpg"> <img src="../assets/images/big/img5.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/users/1.jpg"> <img src="../assets/images/users/1.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '        </div>';
		
		v_rows = '';
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukfiles" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 75%;">File</th><th class="header" style="width:20%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukfiles">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.filename+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Preview" id="btn_delete" class="btn btn-sm btn-primary" onclick="window.open(\'<?php echo Yii::app()->baseUrl; ?>/files/'+p_value.files+'\', \'_blank\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
</script>
