<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<?php
/* @var $this Upload FileController */
/* @var $model Suratmasuk */

$this->breadcrumbs = array(
    'Upload File' => array('admin'),
    'Upload File',
);
?>
<!-- <link href="<?php echo $baseUrl; ?>/assets/node_modules/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $baseUrl; ?>/assets/node_modules/dropzone-master/dist/dropzone.js"></script> -->
<link href="<?php echo $baseUrl; ?>/dist/css/pages/user-card.css" rel="stylesheet">

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Upload File</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('upload/admin'); ?>">Upload File</a></li>
                <li class="breadcrumb-item active">Upload File</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
			<div class="card">
			<?php
	        $form = $this->beginWidget('CActiveForm', array(
	            'id' => 'suratmasuk-form',
	            'method' => 'POST',
	            'enableAjaxValidation' => true,
	            'clientOptions' => array(
	                'validateOnSubmit' => true,
	                'validateOnChange' => true
	            ),
	            'htmlOptions' => array(
	                'enctype' => 'multipart/form-data',
	                'onsubmit'=>'return validate()',
					//'class'=>'dropzone'
	            )
	        ));
	        ?>
	        <?php echo $form->errorSummary($model); ?>
	        <?php echo CHtml::hiddenField('filedata' , '', array('id' => 'filedata')); ?>
	        <?php echo CHtml::hiddenField('filedelete' , '', array('id' => 'filedelete')); ?>
	        <?php echo $form->hiddenField($model, 'status'); ?>
    			<div class="card-body">
		            <div class="form-body">
		                <h3 class="box-title">Agenda Surat Masuk</h3>
		                <hr class="m-t-0 m-b-40">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Indek:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->indexnomor; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Klasifikasi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->klasifikasi; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Kode:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->kode; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl. Penyerahan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglpenyerahan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl dan Nomor Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglsurat; ?> <?php echo $model->nosurat; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Asal Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->asal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Hal/Isi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->perihal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <hr class="m-t-0 m-b-40">
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl Pelaksanaan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglpelaksanaan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Catatan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->catatanpelaksanaan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">Penerima Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_jabatan"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
				                <h3 class="box-title">Perintah Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group row">
		                            <div class="col-md-12">
		                                <div id="d_perintah"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <div class="row">
		                    <div class="col-md-12">
				                <h3 class="box-title">Staf Penerima Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_pegawai"></div>
		                            </div>
		                        </div>
		                    </div>
			            </div>
						<hr>
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">File yang sudah diupload</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_files"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
				                <h3 class="box-title">Upload File</h3>
				                <hr class="m-t-0">
		                        <div class="form-group row">
		                            <div class="col-md-12">
								<?php echo $form->labelEx($model, 'File Surat'); ?>
								<div class="dropzone" id="dropzoneForm"></div>
								<!-- <?php
									$this->widget('ext.dropzone.EDropzone', array(
    									'name'=>'upload',
    									'url' => $this->createUrl('controller/action'),
									    'mimeTypes' => array('image/png', 'image/jpg', 'image/jpeg'),
									    'onSuccess' => 'alert("ok");',
    									'options' => array('autoProcessQueue'=>'false'),
									));
								?> -->
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
						
		                <!--/row-->
		            </div>
		            <div class="form-actions">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="row">
		                            <div class="col-md-offset-3 col-md-9">
							            <?php echo CHtml::submitButton('Update', array('id' => 'btn_submit', 'class' => 'btn btn-primary')); ?>
		                                <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('pelaksanaan/admin'); ?>';">Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6"> </div>
		                </div>
		            </div>
		    	</div>
	        <?php $this->endWidget(); ?>
		    
		    </div>
		</div>
    </div>
</div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    var v_detailTmpId = 0;
    var v_listDataPerintah = Array();
    var v_listDataJabatan = Array();
    var v_listDataPegawai = Array();
	var v_listDataFiles = Array();
	var v_listDetailDeleted = Array();
    var v_id = '';
    var myDropzone;
	var v_uploadEnd = false;
	var v_uploadRes = false;
	
	$(document).ready(function () {
        Dropzone.options.dropzoneForm = {
			acceptedFiles:'.png,.jpg,.jpeg,.gif,.pdf,.bmp',
			autoProcessQueue:false,
			url:"<?php echo Yii::app()->createUrl('suratmasuk/upload1'); ?>",
			paramName:'upload',
			maxFiles: 10,
			//parallelUploads: 5,
			//uploadMultiple: true,
			addRemoveLinks: true,
			init:function(){
				this.on("maxfilesexceeded", function(file){
			        alert("No more files please!");
			    });
				this.on('success',function(file, response){
					var v_response = JSON.parse(response);
					var v_newData = {"id": "file", "files": v_response.files, "filename": v_response.filename};
					v_listDataFiles.push(v_newData);
				});
				myDropzone = this;
				/*var submitButton = document.querySelector('#btn_submit');
				submitButton.addEventListener("click", function() {
					myDropzone.processQueue();
				});*/
				v_uploadRes = false;
				this.on('complete',function(file, response){
					if (this.getUploadingFiles().length == 0 && this.getQueuedFiles().length > 0) {
						myDropzone.processQueue();
					}
					
					if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
						var _this = this;
						_this.removeAllFiles();
						v_uploadRes = true;
						v_uploadEnd = true;
						
						//alert('complete->'+v_listDataFiles);
						$("#suratmasuk-form").submit();
					}
				});
			},
		};
		
		$(".select2").select2();
        
		loadDetailJabatan();
		loadDetailPerintah();
        loadDetailPegawai();
        loadDetailFiles();
    });
	
	function validate() {
		var v_count = myDropzone.files.length;
		
		if (v_count <= 0) v_uploadRes = true;
		if (v_uploadRes == false) {
			myDropzone.processQueue();
			return false;
		}
		else {
			var v_temp = '';
			var v_filter;
			v_filter = v_listDataFiles.filter(function (p_element) {
			  	return p_element.id == 'file';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.files + '~#|#~';
					v_temp += p_value.filename + '~#|#~';
				});
			}
			$("#filedata").val(v_temp);
			
			v_temp = '';
			var v_filter;
			v_filter = v_listDetailDeleted.filter(function (p_element) {
			  	return p_element.model == 'suratmasukfiles';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.id + '~#|#~';
				});
			}
			$("#filedelete").val(v_temp);
			//alert('go');
			return true;
		}
	}
    
    function loadDetailFiles() {
        v_listDataFiles = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadsuratmasukfiles') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukfilesid": p_value.suratmasukfilesid, 
										"suratmasukid": p_value.suratmasukid, 
										"filename": p_value.filename,
										"files": p_value.files	};
					v_listDataFiles.push(v_newData);
			    });
            	
            	loadDataFiles('d_files');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataFiles(p_divName) {
		var v_filter = v_listDataFiles;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
        v_rows = v_rows + '        <div class="card-columns el-element-overlay">';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/big/img5.jpg"> <img src="../assets/images/big/img5.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/users/1.jpg"> <img src="../assets/images/users/1.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '        </div>';
		
		v_rows = '';
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukfiles" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 65%;">File</th><th class="header" style="width:30%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukfiles">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.filename+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Preview" id="btn_delete" class="btn btn-sm btn-primary" onclick="window.open(\'<?php echo Yii::app()->baseUrl; ?>/files/'+p_value.files+'\', \'_blank\')">';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataFiles(\''+p_value.suratmasukfilesid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}

	function deleteDataFiles(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratmasukfiles"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataFiles.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratmasukfilesid'] === p_id) {
			      v_listDataFiles.splice(p_index, 1);
			    }    
			});
			
			loadDataFiles('d_files');
		}
	}
    
    function loadDetailPegawai() {
        v_listDataPegawai = [];
        $.ajax({
            url: "<?php echo CController::createUrl('pelaksanaan/loadsuratmasukpegawai') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukpegawaiid": p_value.suratmasukpegawaiid, 
										"suratmasukid": p_value.suratmasukid, 
										"pegawaiid": p_value.pegawaiid, 
										"pegawaiview": p_value.pegawaiview,
										"disposisi": p_value.disposisi,
										"statusbaca": p_value.statusbaca,
										"ketstatusbaca": p_value.ketstatusbaca	};
					v_listDataPegawai.push(v_newData);
			    });
            	
            	loadDataPegawai('d_pegawai');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataPegawai(p_divName) {
		var v_filter = v_listDataPegawai;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukpegawai" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 35%;">Pegawai</th><th class="header" style="width: 30%;">Disposisi</th><th class="header" style="width: 30%;">Status</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukpegawai">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.pegawaiview+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.disposisi+'</td>';
				if (p_value.statusbaca == '0' || p_value.statusbaca == null) {
		    		v_rows = v_rows + '<td style="color: red; font-weight: bold;">'+p_value.ketstatusbaca+'</td>';
				}
				else {
		    		v_rows = v_rows + '<td style="color: green; font-weight: bold;">'+p_value.ketstatusbaca+'</td>';
				}
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailPerintah() {
        v_listDataPerintah = [];
        $.ajax({
            url: "<?php echo CController::createUrl('pelaksanaan/loadsuratmasukperintah') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukperintahid": p_value.suratmasukperintahid, 
										"suratmasukid": p_value.suratmasukid, 
										"perintahid": p_value.perintahid,
										"perintah": p_value.perintah	};
					v_listDataPerintah.push(v_newData);
			    });
            	
            	loadDataPerintah('d_perintah');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataPerintah(p_divName) {
		var v_filter = v_listDataPerintah;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukperintah" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 85%;">Perintah</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukperintah">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.perintah+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailJabatan() {
        v_listDataJabatan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('pelaksanaan/loadsuratmasukjabatan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukjabatanid": p_value.suratmasukjabatanid, 
										"suratmasukid": p_value.suratmasukid, 
										"jabatanid": p_value.jabatanid,
										"jabatan": p_value.jabatan	};
					v_listDataJabatan.push(v_newData);
			    });
            	
            	loadDataJabatan('d_jabatan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataJabatan(p_divName) {
		var v_filter = v_listDataJabatan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukjabatan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 85%;">Jabatan</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukjabatan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.jabatan+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
</script>
