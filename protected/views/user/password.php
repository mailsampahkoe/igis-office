<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
?>
<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
$baseUrl = Yii::app()->theme->baseUrl;

$this->breadcrumbs = array(
    'User' => array('adminverify'),
    'Verifikasi User',
);
?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ubah Password</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Ubah Password</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30">
                                	<img src="<?php echo ($model->picture != null && $model->picture != '' ? Yii::app()->request->baseUrl.'/foto/'.$model->picture : Yii::app()->request->baseUrl.'/foto/noimage.jpg'); ?>" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10"><? echo $model->nama; ?></h4>
                                    <h6 class="card-subtitle"><? echo $model->email; ?></h6>
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body"> <small class="text-muted">Email address </small>
                                <h6><?php echo $model->email; ?></h6> <small class="text-muted p-t-30 db">Phone</small>
                                <h6><?php echo $model->nohp; ?></h6> <small class="text-muted p-t-30 db">Address</small>
                                <h6><?php echo $model->alamat; ?></h6>
                            </div><br>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Password</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="card-body">
									    <div class="row">
											<div class="col-12">
									           <div class="card"><div class="card-body">
									        <?php
									        $form = $this->beginWidget('CActiveForm', array(
									            'id' => 'user-form',
									            'method' => 'POST',
									            'enableAjaxValidation' => false,
									            'clientOptions' => array(
									                'validateOnSubmit' => true,
									                'validateOnChange' => true
									            ),
									            'htmlOptions' => array(
									                'enctype' => 'multipart/form-data',
													'class'=>'form-horizontal material',
									            )
									        ));
									        ?>
									        <?php echo $form->errorSummary($model); ?>
											<div class="form-group row">
												<?php echo $form->labelEx($model, 'aksesid', array('class' => 'control-label col-sm-2')); ?>
												<div class="col-sm-10">
												<?php
													echo Select2::activeDropDownList($model, 'aksesid', array('' => ''), array(
														'prompt' => 'PILIH Kel. User',
														'class' => 'col-sm-8', 'style' => 'padding:0px;font-family: Arial, Helvetica, sans-serif;',
														'onchange' => 'getKode()',
														'disabled' => 'disabled',
														'selected' => 'selected'
													));
												?>
												<?php echo $form->error($model, 'aksesid'); ?>
												</div>
											</div>
									        <div class="form-group row">
									            <?php echo $form->labelEx($model, 'username', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-sm-10">
													<?php echo $form->textField($model, 'username', array('class'=>'form-control','placeholder'=>'Username','readonly'=>'readonly')); ?>
									                <?php echo $form->error($model, 'username'); ?>
									            </div>
									        </div>
											<!-- passwordFieldRow -->
									        <div class="form-group row">
									            <?php echo $form->labelEx($model, 'password lama', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-sm-10">
													<?php echo $form->passwordField($model, 'old_password', array('class'=>'form-control','placeholder'=>'Password Lama')); ?>
									                <?php echo $form->error($model, 'old_password'); ?>
									            </div>
									        </div>
									        <div class="form-group row">
									            <?php echo $form->labelEx($model, 'Password baru', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-sm-10">
													<?php echo $form->passwordField($model, 'password', array('class'=>'form-control','placeholder'=>'Password Baru')); ?>
									                <?php echo $form->error($model, 'password'); ?>
									            </div>
									        </div>
											<div class="form-group row">
									            <?php echo $form->labelEx($model, 'Password Baru (Ulang)', array('class' => 'control-label col-sm-2')); ?>
									            <div class="col-sm-10">
													<?php echo $form->passwordField($model, 'repeat_password', array('class'=>'form-control','placeholder'=>'Password Baru (Ulang)')); ?>
									                <?php echo $form->error($model, 'repeat_password'); ?>
									            </div>
									        </div>
											<div class="col-lg-12">
									            <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-primary pull-right')); ?>
									        </div>
									        <?php $this->endWidget(); ?>
									    </div><!-- form -->
									</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->

<script type="text/javascript">
	$(document).ready(function () {
        
    });
</script>

<script type="text/javascript">
	$("#User_aksesid").select2({ containerCssClass: "myFont" });
    
	$(document).ready(function () {
        loadAkses();
    });
	
	function loadAkses() {
        $.ajax({
            url: "<?php echo CController::createUrl('user/loadakses') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'aksesid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'aksesid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'aksesid') ?>').select2().select2('val', '<?php echo $model->aksesid; ?>');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
</script>