<?php
/* @var $this UserController */
/* @var $model UserInfo */

Yii::import('ext.select2.Select2');
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data User</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Data User</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
        
		
    </div>
	<br/>
<div class="row">	
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
	<div class="form-group" id="" style="padding-right:15px;">
			    <?php 
					echo Select2::activeDropDownList($model, 'status', array('' => ''), array(
                        'prompt' => 'PILIH STATUS',
                        'class' => 'col-lg-6', 'selected' => 'selected'
                    ));
				?>
            
			<div class="col-lg-6"> 
                <?php echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
        </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'user-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('user/adminverify'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => 'User',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Username',
                'name' => 'usernameship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['username'];
		        },
            ),
            array(
                'header' => 'Nama',
                'name' => 'namaship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['nama'];
		        },
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{verify}{down}',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
					'verify' => array(
						'url' => 'Yii::app()->createUrl("/user/verify", array("userid"=>$data["userid"]))',
						'imageUrl'=>Yii::app()->request->baseUrl.'/images/update.png',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "3") && !Yii::app()->user->isSuperadmin() || (!$data["status"]=="0"))?false:true',
                    ),
			        'down' => array
			        (
			            'label'=>'[-]',
			            'url'=>'"#"',
			            'visible'=>'false',
			            'click'=>'function(){alert("Going down!");}',
			        ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
    function loadStatus() {
        $.ajax({
            url: "<?php echo CController::createUrl('user/loadstatus') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'status') ?>').val('');
                $('#<?php echo CHtml::activeId($model, 'status') ?>').html(data);
                $('#<?php echo CHtml::activeId($model, 'status') ?>').select2().select2('val', "0");
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	$(document).ready(function () {
        loadStatus();
		$('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				$.fn.yiiGridView.update('user-grid', {
					//data: $(this).serialize()
					data: {searchtext:$('#searchtext').val(),filterstatus: $('#<?php echo CHtml::activeId($model, 'status') ?>').val()},
				});
				return false;
			}
		});
		$('#<?php echo CHtml::activeId($model, 'status') ?>').change(function(e){
			$.fn.yiiGridView.update('user-grid', {
				//data: $(this).serialize()
				data: {searchtext:$('#searchtext').val(),filterstatus: $('#<?php echo CHtml::activeId($model, 'status') ?>').val()},
			});
			return false;
		});
    });
</script>