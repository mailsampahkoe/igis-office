<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
$baseUrl = Yii::app()->theme->baseUrl;

$this->breadcrumbs = array(
    'User' => array('adminverify'),
    'Verifikasi User',
);
?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Verifikasi User</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('user/adminverify'); ?>">Data User</a></li>
                <li class="breadcrumb-item active">Verifikasi User</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30">
                                	<img src="<?php echo ($model->picture != null && $model->picture != '' ? Yii::app()->request->baseUrl.'/foto/'.$model->picture : Yii::app()->request->baseUrl.'/foto/noimage.jpg'); ?>" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10"><? echo $model->nama; ?></h4>
                                    <h6 class="card-subtitle"><? echo $model->email; ?></h6>
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body"> <small class="text-muted">Email address </small>
                                <h6><?php echo $model->email; ?></h6> <small class="text-muted p-t-30 db">Phone</small>
                                <h6><?php echo $model->nohp; ?></h6> <small class="text-muted p-t-30 db">Address</small>
                                <h6><?php echo $model->alamat; ?></h6>
                                <br/>
                                <button class="btn btn-circle btn-secondary"><i class="fa fa-facebook"></i></button>
                                <button class="btn btn-circle btn-secondary"><i class="fa fa-twitter"></i></button>
                                <button class="btn btn-circle btn-secondary"><i class="fa fa-youtube"></i></button>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profil</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="card-body">
                                        <!--<div class="row">
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nama Lengkap</strong>
                                                <br>
                                                <p class="text-muted"><?php echo $model->nama; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                                <br>
                                                <p class="text-muted"><?php echo $model->nohp; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                                <br>
                                                <p class="text-muted"><?php echo $model->email; ?></p>
                                            </div>
                                            <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                                                <br>
                                                <p class="text-muted"><?php echo $model->alamat; ?></p>
                                            </div>
                                        </div>
                                        <hr>-->
								        <?php
								        $form = $this->beginWidget('CActiveForm', array(
								            'id' => 'user-form',
								            'method' => 'POST',
								            'enableAjaxValidation' => false,
								            'clientOptions' => array(
								                'validateOnSubmit' => true,
								                'validateOnChange' => true
								            ),
								            'htmlOptions' => array(
								                'enctype' => 'multipart/form-data',
								                'onsubmit'=>'return validate()',
												'class'=>'form-horizontal material',
								            )
								        ));
								        ?>
								        <?php echo $form->hiddenField($model, 'status'); ?>
								        <?php echo $form->errorSummary($model); ?>
								        <div class="form-group row">
								            <div class="col-lg-2">Nama</div>
								            <div class="col-lg-10"><strong><?php echo $model->nama; ?></strong></div>
								        </div>
								        <div class="form-group row">
								            <div class="col-lg-2">Username</div>
								            <div class="col-lg-10"><strong><?php echo $model->username; ?></strong></div>
								        </div>
								        <div class="form-group row">
								            <div class="col-lg-2">NPWPD</div>
								            <div class="col-lg-10"><strong><?php echo $model->npwp; ?></strong></div>
								        </div>
								        <div class="form-group row">
								            <div class="col-lg-2">NIK</div>
								            <div class="col-lg-10"><strong><?php echo $model->nik; ?></strong></div>
								        </div>
								        
				                        <div class="row">
				                            <!-- column -->
				                            <div class="col-lg-4 col-md-4">
				                                <!-- Card -->
				                                <div class="card" style="text-align: center">
													<?php
													$ext = strtolower(pathinfo($model->filenpwp, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
													if ($model->filenpwp != null && $model->filenpwp != '' && in_array($ext, array('gif', 'jpg', 'jpeg', 'png'))) {
													?>
													<img class="card-img-top img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/foto/'.$model->filenpwp; ?>" alt="NPWP">
													<?php
													} else {
													?>
													<img class="card-img-top img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/foto/noimage.jpg'; ?>" alt="NPWP">
													<?php
													}
													?>
				                                    <div class="card-body">
				                                        <h4 class="card-title">NPWP</h4>
				                                        <a href="javascript:void(0)" class="btn btn-warning">Preview</a>
				                                    </div>
				                                </div>
				                                <!-- Card -->
				                            </div>
				                            <!-- column -->
				                            <!-- column -->
				                            <div class="col-lg-4 col-md-4">
				                                <!-- Card -->
				                                <div class="card" style="text-align: center">
													<?php
													$ext = strtolower(pathinfo($model->filenik, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
													if ($model->filenik != null && $model->filenik != '' && in_array($ext, array('gif', 'jpg', 'jpeg', 'png'))) {
													?>
				                                    <img class="card-img-top img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/foto/'.$model->filenik; ?>" alt="KTP">
													<?php
													} else {
													?>
				                                    <img class="card-img-top img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/foto/noimage.jpg'; ?>" alt="KTP">
													<?php
													}
													?>
				                                    <div class="card-body">
				                                        <h4 class="card-title">KTP</h4>
				                                        <a href="javascript:void(0)" class="btn btn-warning">Preview</a>
				                                    </div>
				                                </div>
				                                <!-- Card -->
				                            </div>
				                            <!-- column -->
				                            <!-- column -->
				                            <div class="col-lg-4 col-md-4">
				                                <!-- Card -->
				                                <div class="card" style="text-align: center">
													<?php
													$ext = strtolower(pathinfo($model->filenpwp, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
													if ($model->filelegal != null && $model->filelegal != '' && in_array($ext, array('gif', 'jpg', 'jpeg', 'png'))) {
													?>
				                                    <img class="card-img-top img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/foto/'.$model->filelegal; ?>" alt="Akta / Legal">
													<?php
													} else {
													?>
				                                    <img class="card-img-top img-responsive" src="<?php echo Yii::app()->request->baseUrl.'/foto/noimage.jpg'; ?>" alt="Akta / Legal">
													<?php
													}
													?>
				                                    <div class="card-body">
				                                        <h4 class="card-title">Akta / Legal</h4>
				                                        <a href="javascript:void(0)" class="btn btn-warning">Preview</a>
				                                    </div>
				                                </div>
				                                <!-- Card -->
				                            </div>
				                            <!-- column -->
										</div>
								        <div class="form-group row">
								            <div class="col-lg-2">
								            <?php echo CHtml::label('Hasil Verifikasi', ''); ?>
								            </div>
								            <div class="col-lg-10">
												<?php echo CHtml::dropDownList('statusverifikasi', $select, array('0' => 'Diterima', '1' => 'Ditolak')); ?>
								            </div>
								        </div>
								        <div class="form-group row">
								            <div class="col-lg-2">
								            <?php echo CHtml::label('Catatan', ''); ?>
								            </div>
								            <div class="col-lg-10">
									            <?php echo CHtml::textField('catatanverifikasi','',array('size'=>50,'maxlength'=>254)); ?>
								            </div>
								        </div>

										<div class="col-lg-12">
								            <?php echo CHtml::submitButton('Verifikasi', array('class' => 'btn btn-primary pull-right')); ?>
								        </div>
								        <?php $this->endWidget(); ?>
								        <br/><br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->

<script type="text/javascript">
	$(document).ready(function () {
        
    });
</script>