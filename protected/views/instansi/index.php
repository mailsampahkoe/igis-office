<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'parent'=>array('home','site'),
    'Ubah User1',
);
?>

<?php
if (!enumVar::BCINMAIN) {
?>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Instansi</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Instansi</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php
}
?>

<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<!-- Row -->
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30">
                	<img src="<?php echo ($model->logo != null && $model->logo != '' ? Yii::app()->request->baseUrl.'/files/'.$model->logo : Yii::app()->request->baseUrl.'/foto/noimage.jpg'); ?>" class="img-circle" width="150" height="150" />
                    <h6 class="card-title m-t-10"><?php echo $model->namainstansi; ?></h6>
                </center>
				<div style="height:287px"></div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Informasi Instansi</a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="profile" role="tabpanel">
                    <div class="card-body">
			            <div class="form-body">
			                <!--/row-->
			                <div class="row">
			                    <div class="col-md-12">
			                        <div class="form-group row">
			                            <label class="control-label text-left col-md-3">Nama Instansi:</label>
			                            <div class="col-md-9">
			                                <p class="form-control-static"> <?php echo $model->namainstansi; ?> </p>
			                            </div>
			                        </div>
			                    </div>
			                    <!--/span-->
			                </div>
			                <!--/row-->
			                <div class="row">
			                    <div class="col-md-12">
			                        <div class="form-group row">
			                            <label class="control-label text-left col-md-3">Nama Kota:</label>
			                            <div class="col-md-9">
			                                <p class="form-control-static"> <?php echo $model->kota; ?> </p>
			                            </div>
			                        </div>
			                    </div>
			                    <!--/span-->
			                </div>
			                <!--/row-->
			                <div class="row">
			                    <div class="col-md-12">
			                        <div class="form-group row">
			                            <label class="control-label text-left col-md-3">Alamat:</label>
			                            <div class="col-md-9">
			                                <p class="form-control-static"> <?php echo $model->alamat; ?> </p>
			                            </div>
			                        </div>
			                    </div>
			                    <!--/span-->
			                </div>
			                <!--/row-->
			                <div class="row">
			                    <div class="col-md-12">
			                        <div class="form-group row">
			                            <label class="control-label text-left col-md-3">Kode Pos:</label>
			                            <div class="col-md-9">
			                                <p class="form-control-static"> <?php echo $model->kodepos; ?> </p>
			                            </div>
			                        </div>
			                    </div>
			                    <!--/span-->
			                    <div class="col-md-12">
			                        <div class="form-group row">
			                            <label class="control-label text-left col-md-3">E-mail:</label>
			                            <div class="col-md-9">
			                                <p class="form-control-static"> <?php echo $model->email; ?> </p>
			                            </div>
			                        </div>
			                    </div>
			                    <!--/span-->
			                </div>
			                <!--/row-->
			                <div class="row">
			                    <div class="col-md-12">
			                        <div class="form-group row">
			                            <label class="control-label text-left col-md-3">Telp:</label>
			                            <div class="col-md-9">
			                                <p class="form-control-static"> <?php echo $model->telp; ?> </p>
			                            </div>
			                        </div>
			                    </div>
			                    <!--/span-->
			                    <div class="col-md-12">
			                        <div class="form-group row">
			                            <label class="control-label text-left col-md-3">Fax:</label>
			                            <div class="col-md-9">
			                                <p class="form-control-static"> <?php echo $model->fax; ?> </p>
			                            </div>
			                        </div>
			                    </div>
			                    <!--/span-->
			                </div>
			                <!--/row-->
							<?php if (Yii::app()->user->getprivileges("edit", "1") || Yii::app()->user->isSuperadmin()) { ?>
			                <div class="row">
			                    <div class="col-md-12">
			                        <div class="row">
			                            <div class="col-md-offset-3 col-md-9">
			                                <a href="<?php echo $this->createUrl('instansi/update'); ?>" class="btn btn-primary">Update</a>
			                            </div>
			                        </div>
			                    </div>
			                </div>
							<?php } ?>
			            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<!-- Row -->
<!-- ============================================================== -->
<!-- End PAge Content -->
