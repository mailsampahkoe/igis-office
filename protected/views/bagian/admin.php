<?php
/* @var $this BagianController */
/* @var $model Bagian */

Yii::import('ext.select2.Select2');
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data Bagian</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Bagian</li>
            </ol>
			<?php if (Yii::app()->user->getprivileges("create", "5") || Yii::app()->user->isSuperadmin()) { ?>
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('bagian/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tambah Bagian</button>
			<?php } ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
        
		
    </div>
<div class="row">	
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
	<div class="form-group" id="" style="padding-right:0px;">
			<div> 
                <?php echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
        </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'bagian-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('bagian/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => 'Unit Kerja',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => '',
                'name' => 'jenisship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['kode'] . " " . $data['nama'];
		        },
            ),
            array(
                'template' => '{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/bagian/update", array("bagianid"=>$data["bagianid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "5") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/bagian/delete", array("bagianid"=>$data["bagianid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "5") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
    function tambahData() {
        js:document.location.href="<?php echo CController::createUrl('bagian/create') ?>";
    }
	$(document).ready(function () {
        $('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				$.fn.yiiGridView.update('bagian-grid', {
					data: {searchtext:$('#searchtext').val()},
				});
				return false;
			}
		});
    });
</script>