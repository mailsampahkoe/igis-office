<?php
/* @var $this PengumumanController */
/* @var $model Pengumuman */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
?>
<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'pengumuman-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
        <div class="form-group">
	        	<div class="col-lg-4">
				<?php echo $form->labelEx($model, 'tanggal pengumuman'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'language' => 'id',
                    'attribute' => 'tglpengumuman',
                    'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy', 'changeMonth'=>true, 'changeYear'=>true),
                    'htmlOptions' => array('readonly' => false, 'class' => 'form-control', 'maxlength' => 10)
                ));
                ?> 
				</div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'pengirim'); ?>
                <?php echo $form->textField($model, 'pengirim', array('size' => 60, 'maxlength' => 150, 'class' => 'form-control', 'placeholder' => 'Pengirim')); ?>
                <?php echo $form->error($model, 'pengirim'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'judul'); ?>
                <?php echo $form->textField($model, 'judul', array('size' => 60, 'maxlength' => 1200, 'class' => 'form-control', 'placeholder' => 'Judul')); ?>
                <?php echo $form->error($model, 'judul'); ?>
            </div>
        </div>
		<div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'keterangan'); ?>
                <?php echo $form->textArea($model, 'keterangan', array('size' => 60, 'maxlength' => 250, 'class' => 'form-control', 'placeholder' => 'Keterangan')); ?>
                <?php echo $form->error($model, 'keterangan'); ?>
            </div>
        </div>
		<div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'status'); ?> &nbsp;
				<?php echo $form->checkBox($model,'status',array('value' => '1', 'uncheckValue'=>'0')); ?>
                <?php echo $form->error($model, 'status'); ?>
            </div>
        </div>
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary')); ?>
            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('pengumuman/admin'); ?>';">Cancel</button>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div></div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    
	$(document).ready(function () {
		
    });

</script>