<?php
/* @var $this PengumumanController */
/* @var $model Pengumuman */

Yii::import('ext.select2.Select2');
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data Pengumuman</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Data Pengumuman</li>
            </ol>
			<?php if (Yii::app()->user->getprivileges("create", "304") || Yii::app()->user->isSuperadmin()) { ?>
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('pengumuman/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tambah Pengumuman</button>
			<?php } ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
        
		
    </div>
<div class="row">	
<div class="table-responsive">
	<div class="card"><div class="card-body">
	<div class="form-group" id="" style="padding-right:0px;">
			<div> 
                <?php echo CHtml::textField('searchtext','',array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
        </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'pengumuman-grid',
        'itemsCssClass' => 'table table-hover color-table info-table',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('pengumuman/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => 'Pengumuman',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Tanggal',
                'name' => 'tglpengumumanship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['tglpengumuman'];
		        },
            ),
            array(
                'header' => 'Pengirim',
                'name' => 'pengirimship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['pengirim'];
		        },
            ),
            array(
                'header' => 'Judul',
                'name' => 'judulship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return $data['judul'];
		        },
            ),
            array(
                'header' => 'Status',
                'name' => 'statusship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
		            return ($data['status'] == '1') ? 'Aktif' : 'Tidak Aktif';
		        },
            ),
            array(
                'template' => '{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/pengumuman/update", array("pengumumanid"=>$data["pengumumanid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "304") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/pengumuman/delete", array("pengumumanid"=>$data["pengumumanid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "304") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>
<script>
    function tambahData() {
        js:document.location.href="<?php echo CController::createUrl('pengumuman/create') ?>";
    }
	$(document).ready(function () {
        $('#searchtext').keyup(function(e){
			if(e.keyCode == 13)
		    {
				$.fn.yiiGridView.update('pengumuman-grid', {
					data: {searchtext:$('#searchtext').val()},
				});
				return false;
			}
		});
    });
</script>