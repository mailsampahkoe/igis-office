<?php
/* @var $this SubbagianController */
/* @var $model Subbagian */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
?>
<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'subbagian-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'bagian'); ?><br>
                <?php
                    echo Select2::activeDropDownList($model, 'bagianid', array('' => ''), array(
                        'prompt' => 'PILIH SUB BAGIAN',
                        'class' => 'col-lg-6', 'style' => 'padding:0px',
						'onchange' => 'getKode()',
						'selected' => 'selected'
                    ));
                ?>
                <?php echo $form->error($model, 'bagianid'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'kode'); ?>
                <?php echo $form->textField($model, 'kode', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Kode')); ?>
                <?php echo $form->error($model, 'kode'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'nama'); ?>
                <?php echo $form->textField($model, 'nama', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Nama Unit Kerja')); ?>
                <?php echo $form->error($model, 'nama'); ?>
            </div>
        </div>
		<div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'alamat'); ?>
                <?php echo $form->textArea($model, 'alamat', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Alamat')); ?>
                <?php echo $form->error($model, 'alamat'); ?>
            </div>
        </div>
		<div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'keterangan'); ?>
                <?php echo $form->textArea($model, 'keterangan', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Keterangan')); ?>
                <?php echo $form->error($model, 'keterangan'); ?>
            </div>
        </div>
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary')); ?>
            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('subbagian/admin'); ?>';">Cancel</button>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div></div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    
	$(document).ready(function () {
        loadBagian();
    });
	
	function loadBagian() {
        $.ajax({
            url: "<?php echo CController::createUrl('subbagian/loadbagian') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val', '<?php echo $model->bagianid; ?>');
				<?php if ($ismode == "add") { ?>
				    getKode();
				<?php } ?>
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
    
    function getKode() {
        $.ajax({
            url: "<?php echo CController::createUrl('subbagian/kode') ?>",
            type: 'POST',
            data: {bagianid: $('#' + '<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val')},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'kode') ?>').val(data);
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

</script>