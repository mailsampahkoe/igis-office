<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<?php
/* @var $this DisposisiController */
/* @var $model Suratmasuk */

$this->breadcrumbs = array(
    'Disposisi Surat Masuk' => array('admin'),
    'Disposisi Surat Masuk',
);
?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css"/>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>

<link type="text/css" href="<?php echo $baseUrl; ?>/assets/js/jquery-datatables-checkboxes-1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $baseUrl; ?>/assets/js/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/rg-1.0.2/sl-1.2.4/datatables.min.css"/>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/rg-1.0.2/sl-1.2.4/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"/>
<script type="text/javascript" language="javascript" src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Disposisi Kasubbag</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('disposisikasubbag/admin'); ?>">Disposisi Surat Masuk</a></li>
                <li class="breadcrumb-item active">Disposisi Kasubbag</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
			<div class="card">
			<?php
	        $form = $this->beginWidget('CActiveForm', array(
	            'id' => 'suratmasuk-form',
	            'method' => 'POST',
	            'enableAjaxValidation' => true,
	            'clientOptions' => array(
	                'validateOnSubmit' => true,
	                'validateOnChange' => true
	            ),
	            'htmlOptions' => array(
	                'enctype' => 'multipart/form-data',
                'onsubmit'=>'return validate()',
	            )
	        ));
	        ?>
	        <?php echo $form->errorSummary($model); ?>
	        <?php echo $form->hiddenField($model, 'status'); ?>
    			<div class="card-body">
		            <div class="form-body">
		                <h3 class="box-title">Agenda Surat Masuk</h3>
		                <hr class="m-t-0 m-b-40">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Indek:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->indexnomor; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Klasifikasi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->klasifikasi; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Kode:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->kode; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl. Penyerahan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglpenyerahan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl dan Nomor Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglsurat; ?> <?php echo $model->nosurat; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Asal Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->asal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Hal/Isi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->perihal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <hr class="m-t-0 m-b-40">
				        <div class="row">
				            <div class="col-md-12">
				                <h3 class="box-title">File yang sudah diupload</h3>
				                <hr class="m-t-0">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div id="d_files"></div>
				                    </div>
				                </div>
				            </div>
				            <!--/span-->
			            </div>
		                <!--/row-->
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">Penerima Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_jabatan"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
				                <h3 class="box-title">Perintah Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group row">
		                            <div class="col-md-12">
		                                <div id="d_perintah"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <div class="row">
		                    <div class="col-md-12">
				                <h3 class="box-title">Staf Penerima Disposisi</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <?php echo CHtml::hiddenField('pegawaidata' , '', array('id' => 'pegawaidata')); ?>
									<?php echo CHtml::hiddenField('pegawaidelete' , '', array('id' => 'pegawaidelete')); ?>
		                            <div class="col-md-12">
			                            <?php echo CHtml::button('Tambah Staf Penerima Disposisi', array('class' => 'btn btn-success', 'onclick' => 'displayFormPegawai(\'add\',\'\');')); ?>
		                            </div>
		                            <div class="col-md-12">
		                                <div id="d_pegawai"></div>
		                            </div>
		                        </div>
		                    </div>
			            </div>
		            </div>
		            <div class="form-actions">
		                <div class="row">
				            <?php if ($confirmed == '0') { ?>
		                    <div class="col-md-12">
								<p style="color: red; font-weight: bold;">Pastikan seluruh Kasubbag dan Kasie telah melakukan Konfirmasi Disposisi, untuk dapat melakukan melakukan Posting ke Proses Selanjutnya.</p>
							</div>
				            <?php } ?>
		                    <div class="col-md-12">
		                        <div class="row">
		                            <div class="col-md-offset-3 col-md-9">
							            <?php if ($confirmed == '0') { ?>
										<?php echo CHtml::submitButton('Update', array('class' => 'btn btn-primary', 'disabled' => 'true')); ?>
							            <?php } else { ?>
										<?php echo CHtml::submitButton('Update', array('class' => 'btn btn-primary')); ?>
							            <?php } ?>
		                                <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('disposisikasubbag/admin'); ?>';">Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		    	</div>
	        <?php $this->endWidget(); ?>
		    
		    </div>
		</div>
    </div>
</div>

<div class="modal fade" id="m_modifDataPerintah" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Pilih Perintah Disposisi</h4>
		</div>
		<div style="padding-left:20px;padding-right:20px;">
			<div class="table-responsive">
				<table id="tabelsuratmasukperintah" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="10%"></th>
                            <th width="20%">NAMA</th>
                            <th width="70%">KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
					</tbody>
                </table>
            </div>
	    </div>
	    <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" onclick="modifyDataPerintah();">SIMPAN</button>
       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
	    </div>
	  </div>
	</div>
</div>

<div class="modal fade" id="m_modifDataPegawai" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Pilih Pegawai Penerima Disposisi</h4>
		</div>
		
		<div class="modal-body">
			<div id="d_descmsg"></div>
			
			<form id="f_pegawai" class="form-horizontal">
				<?php echo CHtml::hiddenField('hid_det_mode' , '', array('id' => 'hid_det_mode')); ?>
				<?php echo CHtml::hiddenField('hid_det_id' , '', array('id' => 'hid_det_id')); ?>
				<?php echo CHtml::hiddenField('hid_det_model' , '', array('id' => 'hid_det_model')); ?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="slc_pegawaiid" class="col-lg-3 control-label">Nama Pegawai</label> 
							<div class="col-lg-9">
							<select name="slc_pegawaiid" id="slc_pegawaiid" class="form-control">
								<option value=""></option>
							</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="txt_disposisi" class="col-lg-3 control-label">Disposisi</label> 
							<div class="col-lg-9">
							<input type="text" name="txt_disposisi" class="form-control" id="txt_disposisi" placeholder="Disposisi" maxlength="250">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	    <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" onclick="modifyDataPegawai();">SIMPAN</button>
       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
	    </div>
	  </div>
	</div>
</div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    var v_detailTmpId = 0;
    var v_listDataPerintah = Array();
    var v_listDataJabatan = Array();
    var v_listDataPegawai = Array();
	var v_listDataFiles = Array();
    var v_listDetailDeleted = Array();
    var v_id = '';
    
	$(document).ready(function () {
        $(".select2").select2();
        
		loadDetailJabatan();
		loadDetailPerintah();
        loadDetailPegawai();
		loadDetailFiles();
    });
	
	function validate() {
		if (v_listDataPegawai.length <= 0) {
			alert('Pegawai penerima disposisi harus dipilih.');
			return false;
		}
		
		var v_temp = '';
		$.each( v_listDataPegawai, function( p_key, p_value ) {
			if (v_temp != '') {
				v_temp += '~#;#~';
			}
			v_temp += p_value.suratmasukpegawaiid + '~#|#~';
			v_temp += p_value.suratmasukid + '~#|#~';
			v_temp += p_value.pegawaiid + '~#|#~';
			v_temp += p_value.disposisi + '~#|#~';
		});
		$("#pegawaidata").val(v_temp);
		
		v_temp = '';
		var v_filter;
		v_filter = v_listDetailDeleted.filter(function (p_element) {
		  	return p_element.model == 'suratmasukpegawai';
		});
		if (v_filter.length > 0) {
			$.each( v_filter, function( p_key, p_value ) {
				if (v_temp != '') {
					v_temp += '~#;#~';
				}
				v_temp += p_value.id + '~#|#~';
			});
		}
		$("#pegawaidelete").val(v_temp);
	}
    
	function loadPegawai(){
		 $.ajax({
            url: "<?php echo CController::createUrl('disposisikasubbag/loadpegawai') ?>",
            type: 'POST',
            data: {isall:'false'},
            success: function (data) {
                $('#slc_pegawaiid').val('');
                $('#slc_pegawaiid').html(data);
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
	}

	function displayFormPegawai(p_mode, p_id) {
		loadPegawai();
		$("#hid_det_mode").val(p_mode);
		$("#hid_det_id").val(p_id);
		
		$("#formLabel").text('Pilih Pegawai Disposisi');
		$("#slc_pegawaiid").val('');
		$("#txt_disposisi").val('');
		if (p_mode.toLowerCase() == 'edit') {
			var v_filter = v_listData.filter(function (p_element) {
			  	return p_element.suratmasukpegawaiid === p_id;
			});
    		if (v_filter != null && v_filter != '') {
    			$.each( v_filter, function( p_key, p_value ) {
    				$("#slc_pegawaiid").val(p_value.pegawaiid);
    				$("#txt_disposisi").val(p_value.disposisi);
    			});
    		}
		}
		
		$('#m_modifDataPegawai').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifDataPegawai').modal('show');
	}
    
    function loadDetailPegawai() {
        v_listDataPegawai = [];
        $.ajax({
            url: "<?php echo CController::createUrl('disposisikasubbag/loadsuratmasukpegawai') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukpegawaiid": p_value.suratmasukpegawaiid, 
										"suratmasukid": p_value.suratmasukid, 
										"pegawaiid": p_value.pegawaiid, 
										"pegawaiview": p_value.pegawaiview,
										"disposisi": p_value.disposisi	};
					v_listDataPegawai.push(v_newData);
			    });
            	
            	loadDataPegawai('d_pegawai');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataPegawai(p_divName) {
		var v_filter = v_listDataPegawai;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukpegawai" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 45%;">Pegawai</th><th class="header" style="width: 40%;">Disposisi</th><th class="header" style="width:10%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukpegawai">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.pegawaiview+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.disposisi+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Edit" id="btn_edit" class="btn btn-sm btn-warning" onclick="displayFormPegawai(\'edit\',\''+p_value.suratmasukpegawaiid+'\')">&nbsp;';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataPegawai(\''+p_value.suratmasukpegawaiid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function modifyDataPegawai() {
		if ($("#slc_pegawaiid").val() == '') {
			alert('Pegawai harus dipilih...');
			return;
		}
		if ($("#txt_disposisi").val() == '') {
			alert('Disposisi harus diisi...');
			return;
		}
		var v_filter;
		v_filter = v_listDataPegawai.filter(function (p_element) {
		  	return p_element.pegawaiid == $('#slc_pegawaiid').val() && p_element.suratmasukpegawaiid != $('#hid_det_id').val();
		});
		if (v_filter.length > 0) {
			alert('Pegawai ini sudah pernah dipilih...');
			return;
		}
		if ($('#hid_det_mode').val().toLowerCase() == 'add') {
			v_detailTmpId ++;
			var v_newData = {	"suratmasukpegawaiid": "tmp__"+v_detailTmpId, 
								"suratmasukid": v_id,
								"pegawaiid": $('#slc_pegawaiid').val(),
								"pegawaiview": $('#slc_pegawaiid option:selected').text(),
								"disposisi": $('#txt_disposisi').val()	};
			v_listDataPegawai.push(v_newData);
		}
		else {
	    	$.each( v_listDataPegawai, function( p_key, p_value ) {
	    		if (p_value.suratmasukpegawaiid ==  $('#hid_det_id').val()) {
					p_value.pegawaiid = $('#slc_pegawaiid').val();
					p_value.pegawaiview = $('#slc_pegawaiid option:selected').text();
					p_value.disposisi = $('#txt_disposisi').val();
					
					return false;
				}	
	    	});
		}
		
		$('#m_modifDataPegawai').modal('hide');
		
		loadDataPegawai('d_pegawai');
	}

	function deleteDataPegawai(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratmasukpegawai"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataPegawai.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratmasukpegawaiid'] === p_id) {
			      v_listDataPegawai.splice(p_index, 1);
			    }    
			});
			
			loadDataPegawai('d_pegawai');
		}
	}
    
    function loadDetailPerintah() {
        v_listDataPerintah = [];
        $.ajax({
            url: "<?php echo CController::createUrl('disposisikasubbag/loadsuratmasukperintah') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukperintahid": p_value.suratmasukperintahid, 
										"suratmasukid": p_value.suratmasukid, 
										"perintahid": p_value.perintahid,
										"perintah": p_value.perintah	};
					v_listDataPerintah.push(v_newData);
			    });
            	
            	loadDataPerintah('d_perintah');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataPerintah(p_divName) {
		var v_filter = v_listDataPerintah;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukperintah" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 85%;">Perintah</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukperintah">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.perintah+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailJabatan() {
        v_listDataJabatan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('disposisikasubbag/loadsuratmasukjabatan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukjabatanid": p_value.suratmasukjabatanid, 
										"suratmasukid": p_value.suratmasukid, 
										"jabatanid": p_value.jabatanid,
										"jabatan": p_value.jabatan,
										"statusbaca": p_value.statusbaca,
										"ketstatusbaca": p_value.ketstatusbaca	};
					v_listDataJabatan.push(v_newData);
			    });
            	
            	loadDataJabatan('d_jabatan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataJabatan(p_divName) {
		var v_filter = v_listDataJabatan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukjabatan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 55%;">Jabatan</th><th class="header" style="width: 30%;">Status</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukjabatan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.jabatan+'</td>';
				if (p_value.statusbaca == '0' || p_value.statusbaca == null) {
		    		v_rows = v_rows + '<td style="color: red; font-weight: bold;">'+p_value.ketstatusbaca+'</td>';
				}
				else {
		    		v_rows = v_rows + '<td style="color: green; font-weight: bold;">'+p_value.ketstatusbaca+'</td>';
				}
		    	
				v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailFiles() {
        v_listDataFiles = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadsuratmasukfiles') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukfilesid": p_value.suratmasukfilesid, 
										"suratmasukid": p_value.suratmasukid, 
										"filename": p_value.filename,
										"files": p_value.files	};
					v_listDataFiles.push(v_newData);
			    });
            	
            	loadDataFiles('d_files');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataFiles(p_divName) {
		var v_filter = v_listDataFiles;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
        v_rows = v_rows + '        <div class="card-columns el-element-overlay">';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/big/img5.jpg"> <img src="../assets/images/big/img5.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/users/1.jpg"> <img src="../assets/images/users/1.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '        </div>';
		
		v_rows = '';
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukfiles" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 75%;">File</th><th class="header" style="width:20%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukfiles">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.filename+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Preview" id="btn_delete" class="btn btn-sm btn-primary" onclick="window.open(\'<?php echo Yii::app()->baseUrl; ?>/files/'+p_value.files+'\', \'_blank\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
</script>
