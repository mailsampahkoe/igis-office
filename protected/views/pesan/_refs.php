<?php
/* @var $this PesanController */
/* @var $model Pesan */

$baseUrl = Yii::app()->theme->baseUrl;

?><!-- wysihtml5 CSS -->
<link rel="stylesheet" href="<?php echo $baseUrl; ?>/assets/node_modules/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo $baseUrl; ?>/assets/node_modules/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $baseUrl; ?>/dist/css/pages/inbox.css" rel="stylesheet" />


<script src="<?php echo $baseUrl; ?>/assets/node_modules/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo $baseUrl; ?>/assets/node_modules/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="<?php echo $baseUrl; ?>/assets/node_modules/dropzone-master/dist/dropzone.js"></script>
<script>
$(document).ready(function() {

    $('.textarea_editor').wysihtml5();

});
</script>