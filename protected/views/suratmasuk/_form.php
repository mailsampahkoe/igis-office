<?php
/* @var $this SuratmasukController */
/* @var $model Suratmasuk */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
?>
<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'suratmasuk-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
	            'onsubmit'=>'return validate()',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
      	<div class="form-group">
	        <div class="row">
	        	<div class="col-md-12 col-sm-12">
					<?php echo $form->labelEx($model, 'kepala'); ?>
	                <?php
	                    echo Select2::activeDropDownList($model, 'pejabatid', array('' => ''), array(
	                        'prompt' => 'PILIH KEPALA',
	                        'class' => 'col-lg-12', 'style' => 'padding:0px',
							'selected' => 'selected'
	                    ));
	                ?>
	                <?php echo $form->error($model, 'pejabatid'); ?>
		        </div>
			</div>
      	</div>
    	<div class="form-group">
	        <div class="row">
				<div class="col-lg-6">
				<label>Indek</label>
				</div>
			</div>
			<div class="row">
	        	<div class="col-md-4 col-sm-4">
	                <?php
	                    echo Select2::activeDropDownList($model, 'jenisid', array('' => ''), array(
	                        'prompt' => 'PILIH JENIS SURAT',
	                        'class' => 'col-lg-12', 'style' => 'padding:0px',
							'onchange' => 'setIndexKode1(); getNextNo();',
							'selected' => 'selected'
	                    ));
	                ?>
	                <?php echo $form->error($model, 'jenisid'); ?>
		        </div>
                <?php echo $form->hiddenField($model, 'indexkode1'); ?>
	        	<div class="col-md-2 col-sm-2">
	                <?php echo $form->textField($model, 'indexkode2', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Indek Sub Jenis', 'required' => 'true')); ?>
	                <?php echo $form->error($model, 'indexkode2'); ?>
	            </div>
	        	<div class="col-md-2 col-sm-2">
	                <?php echo $form->textField($model, 'indexbulan', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Indek Kode', 'required' => 'true')); ?>
	                <?php echo $form->error($model, 'indexbulan'); ?>
	            </div>
	        	<div class="col-md-2 col-sm-2">
	                <?php echo $form->textField($model, 'indexnomor', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Indek Nomor', 'required' => 'true')); ?>
	                <?php echo $form->error($model, 'indexnomor'); ?>
	            </div>
	        	<div class="col-md-2 col-sm-2">
	                <?php echo $form->textField($model, 'indextahun', array('size' => 20, 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Indek Tahun', 'required' => 'true')); ?>
	                <?php echo $form->error($model, 'indextahun'); ?>
	            </div>
	        </div>
        </div>
      	<div class="form-group">
	        <div class="row">
	        	<div class="col-md-4 col-sm-4">
	                <?php echo $form->labelEx($model, 'kode'); ?><br>
	                <?php echo $form->textField($model, 'kode', array('size' => 60, 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Kode')); ?>
	                <?php echo $form->error($model, 'kode'); ?>
		        </div>
		    	<div class="col-md-4 col-sm-4">
	                <?php echo $form->labelEx($model, 'klasifikasi'); ?><br>
	                <?php
	                    echo Select2::activeDropDownList($model, 'klasifikasi', array('rahasia'=>'Rahasia', 'segera'=>'Segera', 'biasa'=>'Biasa'), array(
	                        'prompt' => 'PILIH KLASIFIKASI',
	                        'class' => 'col-lg-12', 'style' => 'padding:0px',
							'selected' => 'selected',
							'required' => 'true'
	                    ));
	                ?>
	                <?php echo $form->error($model, 'klasifikasi'); ?>
		        </div>
	        	<div class="col-lg-4">
				<?php echo $form->labelEx($model, 'tanggal penyerahan'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'language' => 'id',
                    'attribute' => 'tglpenyerahan',
                    'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy', 'changeMonth'=>true, 'changeYear'=>true),
                    'htmlOptions' => array('readonly' => false, 'class' => 'form-control', 'maxlength' => 10)
                ));
                ?> 
				</div>
	        </div>
        </div>
		<div class="form-group">
            <div class="row">
				<div class="col-lg-4">
				<?php echo $form->labelEx($model, 'nomor surat'); ?>
	            <?php echo $form->textField($model, 'nosurat', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'placeholder' => 'Nomor Surat', 'required' => 'true')); ?>
				</div>
	        	<div class="col-lg-2">
				<?php echo $form->labelEx($model, 'tanggal surat'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'language' => 'id',
                    'attribute' => 'tglsurat',
                    'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy', 'changeMonth'=>true, 'changeYear'=>true),
                    'htmlOptions' => array('readonly' => false, 'class' => 'form-control', 'maxlength' => 10, 'required' => 'true')
                ));
                ?> 
				</div>
		    	<div class="col-md-6 col-sm-6">
	                <?php echo $form->labelEx($model, 'asal surat'); ?>
	            	<?php echo $form->textField($model, 'asal', array('size' => 60, 'maxlength' => 254, 'class' => 'form-control', 'placeholder' => 'Asal Surat', 'required' => 'true')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
            <div class="row">
	        	<div class="col-md-4 col-sm-4">
	                <?php echo $form->labelEx($model, 'kembali pada'); ?><br>
	                <?php echo $form->textField($model, 'kembalipada', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'placeholder' => 'Kembali Pada')); ?>
	                <?php echo $form->error($model, 'kembalipada'); ?>
		        </div>
	        	<div class="col-lg-4">
				<?php echo $form->labelEx($model, 'tanggal kembali'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'language' => 'id',
                    'attribute' => 'tglkembali',
                    'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy', 'changeMonth'=>true, 'changeYear'=>true),
                    'htmlOptions' => array('readonly' => false, 'class' => 'form-control', 'maxlength' => 10)
                ));
                ?> 
				</div>
	        </div>
        </div>
		<div class="form-group">
			<div class="row">
	            <div class="col-lg-12">
	                <?php echo $form->labelEx($model, 'hal / isi'); ?>
	                <?php echo $form->textArea($model, 'perihal', array('size' => 60, 'maxlength' => 254, 'class' => 'form-control', 'placeholder' => 'Hal / Isi', 'required' => 'true')); ?>
	                <?php echo $form->error($model, 'perihal'); ?>
	            </div>
	        </div>
        </div>
		<div class="form-group">
			<div class="row">
	            <div class="col-lg-12">
	                <?php echo $form->labelEx($model, 'keterangan'); ?>
	                <?php echo $form->textArea($model, 'keterangan', array('size' => 60, 'maxlength' => 254, 'class' => 'form-control', 'placeholder' => 'Keterangan')); ?>
	                <?php echo $form->error($model, 'keterangan'); ?>
	            </div>
	        </div>
        </div>
        <?php echo CHtml::hiddenField('filedata' , '', array('id' => 'filedata')); ?>
        <?php echo CHtml::hiddenField('filedelete' , '', array('id' => 'filedelete')); ?>
        <div class="row">
            <div class="col-md-6">
                <h3 class="box-title">File yang sudah diupload</h3>
                <hr class="m-t-0">
                <div class="form-group">
                    <div class="col-md-12">
                        <div id="d_files"></div>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
                <h3 class="box-title">Upload File</h3>
                <hr class="m-t-0">
                <div class="form-group row">
                    <div class="col-md-12">
				<?php echo $form->labelEx($model, 'File Surat'); ?>
				<div class="dropzone" id="dropzoneForm"></div>
				<!-- <?php
					$this->widget('ext.dropzone.EDropzone', array(
					'name'=>'upload',
					'url' => $this->createUrl('controller/action'),
					    'mimeTypes' => array('image/png', 'image/jpg', 'image/jpeg'),
					    'onSuccess' => 'alert("ok");',
					'options' => array('autoProcessQueue'=>'false'),
					));
				?> -->
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary')); ?>
            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('suratmasuk/admin'); ?>';">Cancel</button>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div></div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    var v_detailTmpId = 0;
    var v_listDataPerintah = Array();
    var v_listDataJabatan = Array();
    var v_listDataPegawai = Array();
	var v_listDataFiles = Array();
	var v_listDetailDeleted = Array();
	var v_listJenis = Array();
    var v_id = '';
    var myDropzone;
	var v_uploadEnd = false;
	var v_uploadRes = false;
	
	$(document).ready(function () {
        Dropzone.options.dropzoneForm = {
			acceptedFiles:'.png,.jpg,.jpeg,.gif,.pdf,.bmp',
			autoProcessQueue:false,
			url:"<?php echo Yii::app()->createUrl('suratmasuk/upload1'); ?>",
			paramName:'upload',
			maxFiles: 10,
			//parallelUploads: 5,
			//uploadMultiple: true,
			addRemoveLinks: true,
			init:function(){
				this.on("maxfilesexceeded", function(file){
			        aler4t("No more files please!");
			    });
				this.on('success',function(file, response){
					var v_response = JSON.parse(response);
					var v_newData = {"id": "file", "files": v_response.files, "filename": v_response.filename};
					v_listDataFiles.push(v_newData);
				});
				myDropzone = this;
				/*var submitButton = document.querySelector('#btn_submit');
				submitButton.addEventListener("click", function() {
					myDropzone.processQueue();
				});*/
				v_uploadRes = false;
				this.on('complete',function(file, response){
					if (this.getUploadingFiles().length == 0 && this.getQueuedFiles().length > 0) {
						myDropzone.processQueue();
					}
					
					if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
						var _this = this;
						_this.removeAllFiles();
						v_uploadRes = true;
						v_uploadEnd = true;
						
						$("#suratmasuk-form").submit();
					}
				});
			},
		};
		
        loadPejabat();
        loadJenis();
        loadDetailFiles();
    });
	
	function validate() {
		var v_count = myDropzone.files.length;
		//alert(v_count);
		if (v_count <= 0) v_uploadRes = true;
		if (v_uploadRes == false) {
			myDropzone.processQueue();
			return false;
		}
		else {
			var v_temp = '';
			var v_filter;
			v_filter = v_listDataFiles.filter(function (p_element) {
			  	return p_element.id == 'file';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.files + '~#|#~';
					v_temp += p_value.filename + '~#|#~';
				});
			}
			$("#filedata").val(v_temp);
			
			v_temp = '';
			var v_filter;
			v_filter = v_listDetailDeleted.filter(function (p_element) {
			  	return p_element.model == 'suratmasukfiles';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.id + '~#|#~';
				});
			}
			$("#filedelete").val(v_temp);
			//alert('go');
			return true;
		}
	}
    
    function loadDetailFiles() {
        v_listDataFiles = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadsuratmasukfiles') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukfilesid": p_value.suratmasukfilesid, 
										"suratmasukid": p_value.suratmasukid, 
										"filename": p_value.filename,
										"files": p_value.files	};
					v_listDataFiles.push(v_newData);
			    });
            	
            	loadDataFiles('d_files');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataFiles(p_divName) {
		var v_filter = v_listDataFiles;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
        v_rows = v_rows + '        <div class="card-columns el-element-overlay">';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/big/img5.jpg"> <img src="../assets/images/big/img5.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/users/1.jpg"> <img src="../assets/images/users/1.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '        </div>';
		
		v_rows = '';
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukfiles" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 65%;">File</th><th class="header" style="width:30%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukfiles">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.filename+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Preview" id="btn_delete" class="btn btn-sm btn-primary" onclick="window.open(\'<?php echo Yii::app()->baseUrl; ?>/files/'+p_value.files+'\', \'_blank\')">';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataFiles(\''+p_value.suratmasukfilesid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}

	function deleteDataFiles(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratmasukfiles"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataFiles.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratmasukfilesid'] === p_id) {
			      v_listDataFiles.splice(p_index, 1);
			    }    
			});
			
			loadDataFiles('d_files');
		}
	}
	
	function loadPejabat() {
		$.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadpejabat') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
            	v_listJenis = JSON.parse(data);
				var v_data = '';
				$.each( v_listJenis, function( p_key, p_value ) {
					v_data += '<option value="'+p_value.id+'">'+p_value.text+'</option>';
			    });
				
				$('#<?php echo CHtml::activeId($model, 'pejabatid') ?>').html(v_data);
                $('#<?php echo CHtml::activeId($model, 'pejabatid') ?>').select2().select2('val', '');
                $('#<?php echo CHtml::activeId($model, 'pejabatid') ?>').select2().select2('val', '<?php echo $model->pejabatid; ?>');
            },
            error: function (jqXHR, status, err) {
                alert(err);
            }
        });
    }
	
	function loadJenis() {
		$.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadjenis') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
            	v_listJenis = JSON.parse(data);
				var v_data = '';
				$.each( v_listJenis, function( p_key, p_value ) {
					v_data += '<option value="'+p_value.id+'">'+p_value.text+'</option>';
			    });
				
				$('#<?php echo CHtml::activeId($model, 'jenisid') ?>').html(v_data);
                $('#<?php echo CHtml::activeId($model, 'jenisid') ?>').select2().select2('val', '');
                $('#<?php echo CHtml::activeId($model, 'jenisid') ?>').select2().select2('val', '<?php echo $model->jenisid; ?>');
            },
            error: function (jqXHR, status, err) {
                alert(err);
            }
        });
    }
	
	function setIndexKode1() {
		var v_filter;
		v_filter = v_listJenis.filter(function (p_element) {
		  	return p_element.id == $('#<?php echo CHtml::activeId($model, 'jenisid') ?>').select2().select2('val');
		});
		if (v_filter.length > 0) {
			$.each( v_filter, function( p_key, p_value ) {
				$('#<?php echo CHtml::activeId($model, 'indexkode1') ?>').val(p_value.value);
			});
		}
		else {
			$('#<?php echo CHtml::activeId($model, 'indexkode1') ?>').val('');
		}
	}
	
    function getNextNo() {
        $.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/getnextno') ?>",
            type: 'POST',
            data: {jenisid: $('#<?php echo CHtml::activeId($model, 'jenisid') ?>').val() },
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'indexnomor') ?>').val(data);
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
</script>