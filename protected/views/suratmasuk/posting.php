<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<?php
/* @var $this SuratmasukController */
/* @var $model Suratmasuk */

$this->breadcrumbs = array(
    'Posting Surat Masuk' => array('admin'),
    'Posting Surat Masuk',
);
?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css"/>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>

<link type="text/css" href="<?php echo $baseUrl; ?>/assets/js/jquery-datatables-checkboxes-1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $baseUrl; ?>/assets/js/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/rg-1.0.2/sl-1.2.4/datatables.min.css"/>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/rg-1.0.2/sl-1.2.4/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"/>
<script type="text/javascript" language="javascript" src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Disposisi Surat Masuk Kepala</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('disposisi/admin'); ?>">Disposisi Surat Masuk</a></li>
                <li class="breadcrumb-item active">Disposisi</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
			<div class="card">
			<?php
	        $form = $this->beginWidget('CActiveForm', array(
	            'id' => 'suratmasuk-form',
	            'method' => 'POST',
	            'enableAjaxValidation' => true,
	            'clientOptions' => array(
	                'validateOnSubmit' => true,
	                'validateOnChange' => true
	            ),
	            'htmlOptions' => array(
	                'enctype' => 'multipart/form-data',
					'onsubmit'=>'return validate()',
	            )
	        ));
	        ?>
	        <?php echo $form->errorSummary($model); ?>
	        <?php echo $form->hiddenField($model, 'status'); ?>
    			<div class="card-body">
		            <div class="form-body">
		                <h3 class="box-title">Agenda Surat Masuk</h3>
		                <hr class="m-t-0 m-b-40">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Indek:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->indexnomor; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Klasifikasi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->klasifikasi; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Kode:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->kode; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl. Penyerahan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglpenyerahan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl dan Nomor Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglsurat; ?> <?php echo $model->nosurat; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Asal Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->asal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <!--/span-->
		                    <div class="col-md-6">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Hal/Isi:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->perihal; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <hr class="m-t-0 m-b-40">
				        <div class="row">
				            <div class="col-md-12">
				                <h3 class="box-title">File yang sudah diupload</h3>
				                <hr class="m-t-0">
				                <div class="form-group">
				                    <div class="col-md-12">
				                        <div id="d_files"></div>
				                    </div>
				                </div>
				            </div>
				            <!--/span-->
			            </div>
		                <!--/row-->
		            </div>
		            <div class="form-actions">
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="row">
		                            <div class="col-md-offset-3 col-md-9">
							            <?php echo CHtml::submitButton('Posting', array('class' => 'btn btn-primary')); ?>
							            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('suratmasuk/admin'); ?>';">Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6"> </div>
		                </div>
		            </div>
		    	</div>
	        <?php $this->endWidget(); ?>
		    
		    </div>
		</div>
    </div>
</div>

<?php
    $ismode = "edit";
?>

<script type="text/javascript">
	var v_listDataFiles = Array();
	
	$(document).ready(function () {
        loadDetailFiles();
    });
	
	function validate() {
	
	}
    
    function loadDetailFiles() {
        v_listDataFiles = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratmasuk/loadsuratmasukfiles') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratmasukid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratmasukfilesid": p_value.suratmasukfilesid, 
										"suratmasukid": p_value.suratmasukid, 
										"filename": p_value.filename,
										"files": p_value.files	};
					v_listDataFiles.push(v_newData);
			    });
            	
            	loadDataFiles('d_files');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataFiles(p_divName) {
		var v_filter = v_listDataFiles;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
        v_rows = v_rows + '        <div class="card-columns el-element-overlay">';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/big/img5.jpg"> <img src="../assets/images/big/img5.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/users/1.jpg"> <img src="../assets/images/users/1.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '        </div>';
		
		v_rows = '';
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratmasukfiles" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 75%;">File</th><th class="header" style="width:20%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratmasukfiles">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.filename+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Preview" id="btn_delete" class="btn btn-sm btn-primary" onclick="window.open(\'<?php echo Yii::app()->baseUrl; ?>/files/'+p_value.files+'\', \'_blank\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
</script>
