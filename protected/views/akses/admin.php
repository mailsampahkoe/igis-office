<?php
/* @var $this AksesController */
/* @var $model Akses */


?>
<?php
/* @var $this CustomerController */
/* @var $model PersonalInfo */

Yii::app()->clientScript->registerScript('search', "
$('#searchtext').keyup(function(e){
	if(e.keyCode == 13)
    {
		$.fn.yiiGridView.update('akses-grid', {
			data: $(this).serialize()
		});
		return false;
	}
});

");
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Data Akses</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Dashboard</a></li>
                <li class="breadcrumb-item active">Data Akses</li>
            </ol>
			<button type="button" onclick='js:document.location.href="<?php echo $this->createUrl('akses/create'); ?>"' class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Tambah Akses</button>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
        <div class="form-group" id="">
            <div class="col-lg-6" style="margin-right:15px;"> 
                <?php echo CHtml::textField('searchtext',"",array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Pencarian')); ?>
            </div>
			
			
        </div>
    </div>
<div class="row">
<div class="table-responsive" style="">
	<div class="card"><div class="card-body">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'akses-grid',
        'itemsCssClass' => 'table table-hover',
        // 'filter' => $model,
        'dataProvider' => $dataProvider,
        'ajaxUrl' => $this->createUrl('akses/admin'),
        'pager' => array(
//            'prevPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-left')),
//            'nextPageLabel' => CHtml::tag('i', array('class' => 'fa fa-angle-double-right')),
            'header' => '',
            'cssFile' => false,
            'htmlOptions' => array(
                'class' => 'pagination pagination-lg',
            ),
        ),
//        'dataProvider' => $model->search(),
        'columns' => array(
            // 'id',
            array(
                'header' => 'Nama Akses',
                'name' => 'unitship_id',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'font-size:15px;'),
                'value' => function($data) {
            return
                    $data['kode'] . " " . $data['nama']	;
        },
            ),
            array(
                'template' => '{view}{update}{delete}',
                'class' => 'CButtonColumn',
                'htmlOptions' => array('width' => 90, 'style' => 'text-align: center;'),
                'buttons' => array(
                    'view' => array(
                        'url' => 'Yii::app()->createUrl("/urusan/admin", array("aksesid"=>$data["aksesid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("view", "3") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'update' => array(
                        'url' => 'Yii::app()->createUrl("/akses/update", array("aksesid"=>$data["aksesid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges("edit", "2") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                    'delete' => array(
                        'url' => 'Yii::app()->createUrl("/akses/delete", array("aksesid"=>$data["aksesid"]))',
                        'visible' => '(!Yii::app()->user->getprivileges ("del", "2") && !Yii::app()->user->isSuperadmin())?0:1',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div></div></div></div>