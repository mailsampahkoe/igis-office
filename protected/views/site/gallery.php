<?php
	$baseUrl = Yii::app()->theme->baseUrl;
?>
<link href="<?php echo $baseUrl; ?>/assets/node_modules/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
    <!-- Custom CSS -->
<link href="<?php echo $baseUrl; ?>/dist/css/style.min.css" rel="stylesheet">
<!-- page css -->
<link href="<?php echo $baseUrl; ?>/dist/css/pages/user-card.css" rel="stylesheet">
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">GALERI</h4>
    </div>
</div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    
	<div class="row el-element-overlay">
        
		<?php foreach($dataProvider->getData() as $data) { ?>
			<div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="el-card-item">
                        <div class="el-card-avatar el-overlay-1"> 
							<?php echo CHtml::image(Yii::app()->request->baseUrl.'/'.$data['lokasifoto'],null,array('alt'=>'user')); ?>
						    <div class="el-overlay">
                                <ul class="el-info">
                                    <li><a class="btn default btn-outline image-popup-vertical-fit" href="<?php echo Yii::app()->request->baseUrl.'/'.$data['lokasifoto']; ?>"><i class="icon-magnifier"></i></a></li>
                                    <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="el-card-content">
                            <small><?php echo $data['deskripsi']; ?></small>
                            <br/> </div>
                    </div>
                </div>
            </div>
		<?php } ?>
    </div>
<script src="<?php echo $baseUrl; ?>/assets/node_modules/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $baseUrl; ?>/assets/node_modules/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>