<?php
/* @var $this CustomerController */
/* @var $model PersonalInfo */
$baseUrl = Yii::app()->theme->baseUrl;
$judul = "EVALUASI KINERJA";
Yii::app()->clientScript->registerScript('search', "
$('#searchtext').keyup(function(e){
	if(e.keyCode == 13)
    {
		$.fn.yiiGridView.update('uploaddok-grid', {
			data: $(this).serialize()
		});
		return false;
	}
});

");
?>
<script src="<?php echo $baseUrl; ?>/assets/node_modules/datatables/jquery.dataTables.min.js"></script>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">DATA <?php echo $judul; ?></h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Dashboard</a></li>
                <li class="breadcrumb-item active">DATA <?php echo $judul; ?></li>
            </ol>
		</div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
<div class="table-responsive">
 <div class="card">
  	<form class="card-body" method="post" action="<?php echo Yii::app()->createUrl("/site/dashboard4"); ?>">
		<h4 class="card-title">Evaluasi</h4>
		<h4 class="m-b-0">TAHUN <?php 
			$sql="select tahun from tbmtahun where dlt='0' order by tahun";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();
			$arrtahun = array();
			foreach($rows AS $row){
				$arrtahun += array("$row[tahun]"=>"$row[tahun]"); 
            }
			echo CHtml::dropDownList('tahun',$tahun,$arrtahun,array('class' => 'form-control col-3', "onchange" => "this.form.submit()")); 
		?></h4>
        
			<table id="tableEvaluasi" class="table table-bordered table-striped">
				<thead>
                    <tr bgcolor="#01c0c8">
                        <th>SKPD</th>
                        <th width="50px">Evaluasi</th>
                    </tr>
                </thead>
				<tbody>
					<?php 
						foreach($dataProvider->getData() as $data) {
							echo "<tr>";
						 	echo "<td>".$data["unitkode"]." ".$data["unitnama"]."</td>";
							echo "<td align='center'>";
								if ($data["pathevaluasi"]!=""){
									echo '<a title="Download" href="'.Yii::app()->createUrl("/uploaddok/download", array("dokid"=>$data["dokid"], "jenis"=>"3")).'"><img src="'.Yii::app()->request->baseUrl.'/assets/icons/download.png" alt="Download" /></a>';
								}
							echo "</td>";
							echo "</tr>";
						}
					?>
				</tbody>
			</table>
	</form>
 </div>	
 </div>
</div>
<script>
	$(document).ready(function() {
		$('#tableEvaluasi').DataTable();	
	});
</script>