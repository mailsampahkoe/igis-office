<?php
$this->pageTitle = Yii::app()->name . ' - Error';
$this->breadcrumbs = array(
    'Error',
);
?>

<h2>Error <?php echo $code; ?></h2>

<div class="error">
    <?php
    if ($code == "403") {
        echo CHtml::encode("Anda Tidak Memiliki Hak Akses");
    } else {
        echo CHtml::encode($message);
    }
    ?>
</div>