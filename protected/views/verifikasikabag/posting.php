<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<?php
/* @var $this VerifikasiController */
/* @var $model Suratkeluar */

$this->breadcrumbs = array(
    'Verifikasi Surat Keluar' => array('admin'),
    'Verifikasi Surat Keluar',
);
?>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Verifikasi Kabag</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('verifikasikabag/admin'); ?>">Verifikasi Surat Keluar</a></li>
                <li class="breadcrumb-item active">Verifikasi Kabag</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
			<div class="card">
			<?php
	        $form = $this->beginWidget('CActiveForm', array(
	            'id' => 'suratkeluar-form',
	            'method' => 'POST',
	            'enableAjaxValidation' => true,
	            'clientOptions' => array(
	                'validateOnSubmit' => true,
	                'validateOnChange' => true
	            ),
	            'htmlOptions' => array(
	                'enctype' => 'multipart/form-data',
                'onsubmit'=>'return validate()',
	            )
	        ));
	        ?>
	        <?php echo $form->errorSummary($model); ?>
	        <?php echo $form->hiddenField($model, 'status'); ?>
    			<div class="card-body">
		            <div class="form-body">
		                <h3 class="box-title">Agenda Surat Keluar</h3>
		                <hr class="m-t-0 m-b-40">
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl dan Nomor Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglsurat; ?> <?php echo $model->nosurat; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tujuan Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->kepada; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Keterangan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->keterangan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-12">
				                <h3 class="box-title">Konfirmasi Kasubbag / Kasie</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_jabatan"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">Lampiran</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_lampiran"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
				                <h3 class="box-title">Tembusan</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_tembusan"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		            </div>
		            <div class="form-actions">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="row">
		                            <div class="col-md-offset-3 col-md-9">
							            <?php echo CHtml::submitButton('Verifikasi', array('class' => 'btn btn-primary')); ?>
		                                <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('verifikasikabag/admin'); ?>';">Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6"> </div>
		                </div>
		            </div>
		    	</div>
	        <?php $this->endWidget(); ?>
		    
		    </div>
		</div>
    </div>
</div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    var v_detailTmpId = 0;
    var v_listDataPerintah = Array();
    var v_listDataJabatan = Array();
    var v_listDetailDeleted = Array();
    var v_id = '';
    
	$(document).ready(function () {
        $(".select2").select2();
        
		loadDetailJabatan();
		loadDetailLampiran();
		loadDetailTembusan();
    });
	
	function validate() {
	}
    
    function loadDetailJabatan() {
        v_listDataJabatan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('verifikasikabag/loadsuratkeluarjabatan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluarjabatanid": p_value.suratkeluarjabatanid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"jabatanid": p_value.jabatanid,
										"jabatan": p_value.jabatan	};
					v_listDataJabatan.push(v_newData);
			    });
            	
            	loadDataJabatan('d_jabatan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataJabatan(p_divName) {
		var v_filter = v_listDataJabatan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluarjabatan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 85%;">Jabatan</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluarjabatan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.jabatan+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailLampiran() {
        v_listDataLampiran = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluarlampiran') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluarlampiranid": p_value.suratkeluarlampiranid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"nourut": p_value.nourut,
										"lampiran": p_value.lampiran	};
					v_listDataLampiran.push(v_newData);
			    });
            	
            	loadDataLampiran('d_lampiran');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataLampiran(p_divName) {
		var v_filter = v_listDataLampiran;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluarlampiran" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 15%;">No Urut</th><th class="header" style="width: 65%;">Lampiran</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluarlampiran">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+p_value.nourut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.lampiran+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailTembusan() {
        v_listDataTembusan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluartembusan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluartembusanid": p_value.suratkeluartembusanid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"nourut": p_value.nourut,
										"tembusan": p_value.tembusan	};
					v_listDataTembusan.push(v_newData);
			    });
            	
            	loadDataTembusan('d_tembusan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataTembusan(p_divName) {
		var v_filter = v_listDataTembusan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluartembusan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 15%;">No Urut</th><th class="header" style="width: 65%;">Tembusan</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluartembusan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+p_value.nourut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.tembusan+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
</script>
