<?php
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<?php
/* @var $this VerifikasiController */
/* @var $model Suratkeluar */

$this->breadcrumbs = array(
    'Verifikasi Surat Keluar' => array('admin'),
    'Verifikasi Surat Keluar',
);
?>
<link href="<?php echo $baseUrl; ?>/dist/css/pages/user-card.css" rel="stylesheet">

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Upload File</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('verifikasikabag/admin'); ?>">Verifikasi Surat Keluar</a></li>
                <li class="breadcrumb-item active">Upload File</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
			<div class="card">
			<?php
	        $form = $this->beginWidget('CActiveForm', array(
	            'id' => 'suratkeluar-form',
	            'method' => 'POST',
	            'enableAjaxValidation' => true,
	            'clientOptions' => array(
	                'validateOnSubmit' => true,
	                'validateOnChange' => true
	            ),
	            'htmlOptions' => array(
	                'enctype' => 'multipart/form-data',
	                'onsubmit'=>'return validate()',
					
	            )
	        ));
	        ?>
	        <?php echo $form->errorSummary($model); ?>
	        <?php echo CHtml::hiddenField('filedata' , '', array('id' => 'filedata')); ?>
	        <?php echo CHtml::hiddenField('filedelete' , '', array('id' => 'filedelete')); ?>
	        <?php echo $form->hiddenField($model, 'status'); ?>
    			<div class="card-body">
		            <div class="form-body">
		                <h3 class="box-title">Agenda Surat Keluar</h3>
		                <hr class="m-t-0 m-b-40">
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tgl dan Nomor Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->tglsurat; ?> <?php echo $model->nosurat; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Tujuan Surat:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->kepada; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-group row">
		                            <label class="control-label text-right col-md-3">Keterangan:</label>
		                            <div class="col-md-9">
		                                <p class="form-control-static"> <?php echo $model->keterangan; ?> </p>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <!--/row-->
		                <div class="row">
		                    <div class="col-md-12">
				                <h3 class="box-title">Konfirmasi Kasubbag / Kasie</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_jabatan"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">Lampiran</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_lampiran"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
				                <h3 class="box-title">Tembusan</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_tembusan"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
						<hr>
		                <div class="row">
		                    <div class="col-md-6">
				                <h3 class="box-title">File yang sudah diupload</h3>
				                <hr class="m-t-0">
		                        <div class="form-group">
		                            <div class="col-md-12">
		                                <div id="d_files"></div>
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                    <div class="col-md-6">
				                <h3 class="box-title">Upload File</h3>
				                <hr class="m-t-0">
		                        <div class="form-group row">
		                            <div class="col-md-12">
								<?php echo $form->labelEx($model, 'File Surat'); ?>
								<div class="dropzone" id="dropzoneForm"></div>
								<!-- <?php
									$this->widget('ext.dropzone.EDropzone', array(
    									'name'=>'upload',
    									'url' => $this->createUrl('controller/action'),
									    'mimeTypes' => array('image/png', 'image/jpg', 'image/jpeg'),
									    'onSuccess' => 'alert("ok");',
    									'options' => array('autoProcessQueue'=>'false'),
									));
								?> -->
		                            </div>
		                        </div>
		                    </div>
		                    <!--/span-->
		                </div>
						
		                <!--/row-->
		            </div>
		            <div class="form-actions">
		                <div class="row">
		                    <div class="col-md-6">
		                        <div class="row">
		                            <div class="col-md-offset-3 col-md-9">
							            <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-primary')); ?>
		                                <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('verifikasikabag/admin'); ?>';">Cancel</button>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-md-6"> </div>
		                </div>
		            </div>
		    	</div>
	        <?php $this->endWidget(); ?>
		    
		    </div>
		</div>
    </div>
</div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    var v_detailTmpId = 0;
    var v_listDataPerintah = Array();
    var v_listDataJabatan = Array();
	var v_listDataFiles = Array();
    var v_listDetailDeleted = Array();
    var v_id = '';
    var myDropzone;
	var v_uploadEnd = false;
	var v_uploadRes = false;
    
	$(document).ready(function () {
        Dropzone.options.dropzoneForm = {
			acceptedFiles:'.png,.jpg,.jpeg,.gif,.pdf,.bmp',
			autoProcessQueue:false,
			url:"<?php echo Yii::app()->createUrl('suratkeluar/upload1'); ?>",
			paramName:'upload',
			maxFiles: 10,
			//parallelUploads: 5,
			//uploadMultiple: true,
			addRemoveLinks: true,
			init:function(){
				this.on("maxfilesexceeded", function(file){
			        alert("No more files please!");
			    });
				this.on('success',function(file, response){
					var v_response = JSON.parse(response);
					var v_newData = {"id": "file", "files": v_response.files, "filename": v_response.filename};
					v_listDataFiles.push(v_newData);
				});
				myDropzone = this;
				/*var submitButton = document.querySelector('#btn_submit');
				submitButton.addEventListener("click", function() {
					myDropzone.processQueue();
				});*/
				v_uploadRes = false;
				this.on('complete',function(file, response){
					if (this.getUploadingFiles().length == 0 && this.getQueuedFiles().length > 0) {
						myDropzone.processQueue();
					}
					
					if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
						var _this = this;
						_this.removeAllFiles();
						v_uploadRes = true;
						v_uploadEnd = true;
						
						alert('complete->'+v_listDataFiles);
						$("#suratkeluar-form").submit();
					}
				});
			},
		};
		
		$(".select2").select2();
        
		loadDetailJabatan();
		loadDetailLampiran();
		loadDetailTembusan();
        loadDetailFiles();
    });
	
	function validate() {
		var v_count = myDropzone.files.length;
		
		if (v_count <= 0) v_uploadRes = true;
		if (v_uploadRes == false) {
			myDropzone.processQueue();
			return false;
		}
		else {
			var v_temp = '';
			var v_filter;
			v_filter = v_listDataFiles.filter(function (p_element) {
			  	return p_element.id == 'file';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.files + '~#|#~';
					v_temp += p_value.filename + '~#|#~';
				});
			}
			$("#filedata").val(v_temp);
			
			v_temp = '';
			var v_filter;
			v_filter = v_listDetailDeleted.filter(function (p_element) {
			  	return p_element.model == 'suratkeluarfiles';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.id + '~#|#~';
				});
			}
			$("#filedelete").val(v_temp);
			//alert('go');
			return true;
		}
	}
    
    function loadDetailFiles() {
        v_listDataFiles = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluarfiles') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluarfilesid": p_value.suratkeluarfilesid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"filename": p_value.filename,
										"files": p_value.files	};
					v_listDataFiles.push(v_newData);
			    });
            	
            	loadDataFiles('d_files');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataFiles(p_divName) {
		var v_filter = v_listDataFiles;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
        v_rows = v_rows + '        <div class="card-columns el-element-overlay">';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/big/img5.jpg"> <img src="../assets/images/big/img5.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/users/1.jpg"> <img src="../assets/images/users/1.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '        </div>';
		
		v_rows = '';
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluarfiles" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 65%;">File</th><th class="header" style="width:30%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluarfiles">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.filename+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Preview" id="btn_delete" class="btn btn-sm btn-primary" onclick="window.open(\'<?php echo Yii::app()->baseUrl; ?>/files/'+p_value.files+'\', \'_blank\')">';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataFiles(\''+p_value.suratkeluarfilesid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}

	function deleteDataFiles(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratkeluarfiles"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataFiles.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratkeluarfilesid'] === p_id) {
			      v_listDataFiles.splice(p_index, 1);
			    }    
			});
			
			loadDataFiles('d_files');
		}
	}
    
    function loadDetailJabatan() {
        v_listDataJabatan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('verifikasikabag/loadsuratkeluarjabatan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluarjabatanid": p_value.suratkeluarjabatanid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"jabatanid": p_value.jabatanid,
										"jabatan": p_value.jabatan,
										"statusbaca": p_value.statusbaca,
										"ketstatusbaca": p_value.ketstatusbaca	};
					v_listDataJabatan.push(v_newData);
			    });
            	
            	loadDataJabatan('d_jabatan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataJabatan(p_divName) {
		var v_filter = v_listDataJabatan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluarjabatan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 55%;">Jabatan</th><th class="header" style="width: 30%;">Status</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluarjabatan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.jabatan+'</td>';
				if (p_value.statusbaca == '0' || p_value.statusbaca == null) {
		    		v_rows = v_rows + '<td style="color: red; font-weight: bold;">'+p_value.ketstatusbaca+'</td>';
				}
				else {
		    		v_rows = v_rows + '<td style="color: black; font-weight: bold;">'+p_value.ketstatusbaca+'</td>';
				}
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailLampiran() {
        v_listDataLampiran = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluarlampiran') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluarlampiranid": p_value.suratkeluarlampiranid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"nourut": p_value.nourut,
										"lampiran": p_value.lampiran	};
					v_listDataLampiran.push(v_newData);
			    });
            	
            	loadDataLampiran('d_lampiran');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataLampiran(p_divName) {
		var v_filter = v_listDataLampiran;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluarlampiran" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 15%;">No Urut</th><th class="header" style="width: 65%;">Lampiran</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluarlampiran">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+p_value.nourut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.lampiran+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function loadDetailTembusan() {
        v_listDataTembusan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluartembusan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluartembusanid": p_value.suratkeluartembusanid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"nourut": p_value.nourut,
										"tembusan": p_value.tembusan	};
					v_listDataTembusan.push(v_newData);
			    });
            	
            	loadDataTembusan('d_tembusan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataTembusan(p_divName) {
		var v_filter = v_listDataTembusan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluartembusan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 15%;">No Urut</th><th class="header" style="width: 65%;">Tembusan</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluartembusan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+p_value.nourut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.tembusan+'</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
</script>
