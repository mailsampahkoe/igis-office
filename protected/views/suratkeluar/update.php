<?php
/* @var $this SuratkeluarController */
/* @var $model Suratkeluar */

$this->breadcrumbs = array(
    'Surat Keluar' => array('admin'),
    'Ubah Surat Keluar',
);
?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ubah Surat Keluar</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('suratkeluar/admin'); ?>">Data Surat Keluar</a></li>
                <li class="breadcrumb-item active">Ubah Surat Keluar</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>