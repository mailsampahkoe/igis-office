<?php
/* @var $this SuratkeluarController */
/* @var $model Suratkeluar */

$this->breadcrumbs = array(
    'Surat Keluar' => array('admin'),
    'Ubah Surat Keluar',
);
$baseUrl = Yii::app()->theme->baseUrl;
Yii::import('ext.select2.Select2');
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css"/>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>

<link type="text/css" href="<?php echo $baseUrl; ?>/assets/js/jquery-datatables-checkboxes-1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $baseUrl; ?>/assets/js/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/rg-1.0.2/sl-1.2.4/datatables.min.css"/>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/rg-1.0.2/sl-1.2.4/datatables.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/css/dataTables.checkboxes.css"/>
<script type="text/javascript" language="javascript" src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.9/js/dataTables.checkboxes.min.js"></script>

<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ubah Surat Keluar</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('suratkeluar/admin'); ?>">Data Surat Keluar</a></li>
                <li class="breadcrumb-item active">Ubah Surat Keluar</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'suratkeluar-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
                'onsubmit'=>'return validate()',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
      	<div class="form-group">
	        <div class="row">
	        	<div class="col-md-12 col-sm-12">
					<?php echo $form->labelEx($model, 'kepala'); ?>
	                <?php
	                    echo Select2::activeDropDownList($model, 'pejabatid', array('' => ''), array(
	                        'prompt' => 'PILIH KEPALA',
	                        'class' => 'col-lg-12', 'style' => 'padding:0px',
							'selected' => 'selected'
	                    ));
	                ?>
	                <?php echo $form->error($model, 'pejabatid'); ?>
		        </div>
			</div>
      	</div>
		<div class="form-group">
	        <div class="row">
				<div class="col-lg-4">
				<?php echo $form->labelEx($model, 'nomor agenda'); ?>
	            <?php echo $form->textField($model, 'nosurat', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'placeholder' => 'Nomor Agenda')); ?>
				</div>
	        	<div class="col-lg-2">
				<?php echo $form->labelEx($model, 'tanggal agenda'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'language' => 'id',
                    'attribute' => 'tglsurat',
                    'options' => array('showAnim'=>'fold','dateFormat'=>'dd-mm-yy', 'changeMonth'=>true, 'changeYear'=>true),
                    'htmlOptions' => array('readonly' => false, 'class' => 'form-control', 'maxlength' => 10)
                ));
                ?> 
				</div>
		    	<div class="col-md-6 col-sm-6">
	                <?php echo $form->labelEx($model, 'tujuan surat'); ?>
	            	<?php echo $form->textField($model, 'kepada', array('size' => 60, 'maxlength' => 150, 'class' => 'form-control', 'placeholder' => 'Tujuan Surat')); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
	            <div class="col-lg-12">
	                <?php echo $form->labelEx($model, 'keterangan'); ?>
	                <?php echo $form->textArea($model, 'keterangan', array('size' => 60, 'maxlength' => 254, 'class' => 'form-control', 'placeholder' => 'Keterangan')); ?>
	                <?php echo $form->error($model, 'keterangan'); ?>
	            </div>
	        </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h3 class="box-title">Detail Surat Keluar</h3>
                <hr class="m-t-0">
                <div class="form-group row">
                    <?php echo CHtml::hiddenField('detaildata' , '', array('id' => 'detaildata')); ?>
					<?php echo CHtml::hiddenField('detaildelete' , '', array('id' => 'detaildelete')); ?>
                    <div class="col-md-12">
                        <?php echo CHtml::button('Tambah Detail Surat Keluar', array('class' => 'btn btn-success', 'onclick' => 'displayFormDetail(\'add\',\'\');')); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="d_detail"></div>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
                <h3 class="box-title">Pejabat Konfirmasi</h3>
                <hr class="m-t-0">
                <div class="form-group">
                    <?php echo CHtml::hiddenField('jabatandata' , '', array('id' => 'jabatandata')); ?>
					<?php echo CHtml::hiddenField('jabatandelete' , '', array('id' => 'jabatandelete')); ?>
                    <div class="col-md-12">
                        <?php echo CHtml::button('Tambah Pejabat Konfirmasi', array('class' => 'btn btn-success', 'onclick' => 'displayFormJabatan(\'add\',\'\');')); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="d_jabatan"></div>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <div class="row">
            <div class="col-md-6">
                <h3 class="box-title">Lampiran</h3>
                <hr class="m-t-0">
                <div class="form-group row">
                    <?php echo CHtml::hiddenField('lampirandata' , '', array('id' => 'lampirandata')); ?>
					<?php echo CHtml::hiddenField('lampirandelete' , '', array('id' => 'lampirandelete')); ?>
                    <div class="col-md-12">
                        <?php echo CHtml::button('Tambah Lampiran', array('class' => 'btn btn-success', 'onclick' => 'displayFormLampiran(\'add\',\'\');')); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="d_lampiran"></div>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
                <h3 class="box-title">Tembusan</h3>
                <hr class="m-t-0">
                <div class="form-group">
                    <?php echo CHtml::hiddenField('tembusandata' , '', array('id' => 'tembusandata')); ?>
					<?php echo CHtml::hiddenField('tembusandelete' , '', array('id' => 'tembusandelete')); ?>
                    <div class="col-md-12">
                        <?php echo CHtml::button('Tambah Tembusan', array('class' => 'btn btn-success', 'onclick' => 'displayFormTembusan(\'add\',\'\');')); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="d_tembusan"></div>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <?php echo CHtml::hiddenField('filedata' , '', array('id' => 'filedata')); ?>
        <?php echo CHtml::hiddenField('filedelete' , '', array('id' => 'filedelete')); ?>
        <div class="row">
            <div class="col-md-6">
                <h3 class="box-title">File yang sudah diupload</h3>
                <hr class="m-t-0">
                <div class="form-group">
                    <div class="col-md-12">
                        <div id="d_files"></div>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
                <h3 class="box-title">Upload File</h3>
                <hr class="m-t-0">
                <div class="form-group row">
                    <div class="col-md-12">
				<?php echo $form->labelEx($model, 'File Surat'); ?>
				<div class="dropzone" id="dropzoneForm"></div>
				<!-- <?php
					$this->widget('ext.dropzone.EDropzone', array(
					'name'=>'upload',
					'url' => $this->createUrl('controller/action'),
					    'mimeTypes' => array('image/png', 'image/jpg', 'image/jpeg'),
					    'onSuccess' => 'alert("ok");',
					'options' => array('autoProcessQueue'=>'false'),
					));
				?> -->
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary')); ?>
            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('suratkeluar/admin'); ?>';">Cancel</button>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div></div>

<div class="modal fade" id="m_modifDataJabatan" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Pilih Jabatan Disposisi</h4>
		</div>
		<div style="padding-left:20px;padding-right:20px;">
			<div class="table-responsive">
				<table id="tabelsuratkeluarjabatan" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="10%"></th>
                            <th width="20%">NAMA</th>
                            <th width="70%">KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
					</tbody>
                </table>
            </div>
	    </div>
	    <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" onclick="modifyDataJabatan();">SIMPAN</button>
       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
	    </div>
	  </div>
	</div>
</div>

<div class="modal fade" id="m_modifDataDetail" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Isi Detail Surat Keluar</h4>
		</div>
		
		<div class="modal-body">
			<div id="d_descmsg"></div>
			
			<form id="f_detail" class="form-horizontal">
				<?php echo CHtml::hiddenField('hid_det_mode' , '', array('id' => 'hid_det_mode')); ?>
				<?php echo CHtml::hiddenField('hid_det_id' , '', array('id' => 'hid_det_id')); ?>
				<?php echo CHtml::hiddenField('hid_det_model' , '', array('id' => 'hid_det_model')); ?>
				<?php echo CHtml::hiddenField('hid_indekkode1' , '', array('id' => 'hid_indekkode1')); ?>
				<div class="row">
					<div class="col-lg-12">
						<label for="slc_jenisid" class="col-lg-3 control-label">No Surat</label> 
						<div class="form-group row">
							<div class="col-lg-4">
							<select name="slc_jenisid" class="form-control" id="slc_jenisid" onchange="setIndexKode1(); getNextNo();">
							</select>
							</div>
							<div class="col-lg-2">
							<input type="text" name="txt_indekkode2" class="form-control" id="txt_indekkode2" placeholder="Indek Kode 1" maxlength="10">
							</div>
							<div class="col-lg-2">
							<input type="text" name="txt_indekbulan" class="form-control" id="txt_indekbulan" placeholder="Indek Kode 2" maxlength="10">
							</div>
							<div class="col-lg-2">
							<input type="text" name="txt_indeknomor" class="form-control" id="txt_indeknomor" placeholder="Indek Nomor" maxlength="10">
							</div>
							<div class="col-lg-2">
							<input type="text" name="txt_indektahun" class="form-control" id="txt_indektahun" placeholder="Indek Tahun" maxlength="4">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<label for="txt_perihal" class="col-lg-3 control-label">Hal</label> 
						<div class="form-group row">
							<div class="col-lg-12">
							<input type="text" name="txt_perihal" class="form-control" id="txt_perihal" placeholder="Perihal" maxlength="250">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
							<label for="slc_klasifikasi" class="col-lg-3 control-label">Klasifikasi</label> 
						<div class="form-group row">
							<div class="col-lg-12">
							<select name="slc_klasifikasi" class="form-control" id="slc_klasifikasi">
								<option value="rahasia">Rahasia</option>
								<option value="biasa">Biasa</option>
								<option value="segera">Segera</option>
							</select>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	    <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" onclick="modifyDataDetail();">SIMPAN</button>
       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
	    </div>
	  </div>
	</div>
</div>

<div class="modal fade" id="m_modifDataLampiran" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Isi Lampiran</h4>
		</div>
		
		<div class="modal-body">
			<div id="d_descmsg"></div>
			
			<form id="f_lampiran" class="form-horizontal">
				<?php echo CHtml::hiddenField('hid_lam_mode' , '', array('id' => 'hid_lam_mode')); ?>
				<?php echo CHtml::hiddenField('hid_lam_id' , '', array('id' => 'hid_lam_id')); ?>
				<?php echo CHtml::hiddenField('hid_lam_model' , '', array('id' => 'hid_lam_model')); ?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="txt_nourut_lampiran" class="col-lg-3 control-label">Nomor Urut</label> 
							<div class="col-lg-9">
							<input type="number" name="txt_nourut_lampiran" class="form-control" id="txt_nourut_lampiran" placeholder="Nomor Urut" maxlength="10">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="txt_lampiran" class="col-lg-3 control-label">Lampiran</label> 
							<div class="col-lg-12">
							<input type="text" name="txt_lampiran" class="form-control" id="txt_lampiran" placeholder="Lampiran" maxlength="250">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	    <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" onclick="modifyDataLampiran();">SIMPAN</button>
       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
	    </div>
	  </div>
	</div>
</div>

<div class="modal fade" id="m_modifDataTembusan" role="dialog">
	<div class="modal-dialog modal-lg">
	<!-- Modal content-->
	  <div class="modal-content">
	  	<div class="modal-header">
	      <h4 class="modal-title">Isi Tembusan</h4>
		</div>
		
		<div class="modal-body">
			<div id="d_descmsg"></div>
			
			<form id="f_tembusan" class="form-horizontal">
				<?php echo CHtml::hiddenField('hid_tem_mode' , '', array('id' => 'hid_tem_mode')); ?>
				<?php echo CHtml::hiddenField('hid_tem_id' , '', array('id' => 'hid_tem_id')); ?>
				<?php echo CHtml::hiddenField('hid_tem_model' , '', array('id' => 'hid_tem_model')); ?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="txt_nourut_tembusan" class="col-lg-3 control-label">Nomor Urut</label> 
							<div class="col-lg-9">
							<input type="number" name="txt_nourut_tembusan" class="form-control" id="txt_nourut_tembusan" placeholder="Nomor Urut" maxlength="10">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label for="txt_tembusan" class="col-lg-3 control-label">Tembusan</label> 
							<div class="col-lg-12">
							<input type="text" name="txt_tembusan" class="form-control" id="txt_tembusan" placeholder="Tembusan" maxlength="250">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	    <div class="modal-footer">
	      	<button type="button" class="btn btn-primary" onclick="modifyDataTembusan();">SIMPAN</button>
       	 	<button type="button" class="btn btn-secondary" data-dismiss="modal">TUTUP</button>
	    </div>
	  </div>
	</div>
</div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    var v_detailTmpId = 0;
    var v_listDataJabatan = Array();
    var v_listDataDetail = Array();
    var v_listDataLampiran = Array();
    var v_listDataTembusan = Array();
	var v_listDataFiles = Array();
    var v_listDetailDeleted = Array();
	var v_listJenis = Array();
    var v_id = '';
    
	$(document).ready(function () {
        Dropzone.options.dropzoneForm = {
			acceptedFiles:'.png,.jpg,.jpeg,.gif,.pdf,.bmp',
			autoProcessQueue:false,
			url:"<?php echo Yii::app()->createUrl('suratkeluar/upload1'); ?>",
			paramName:'upload',
			maxFiles: 10,
			//parallelUploads: 5,
			//uploadMultiple: true,
			init:function(){
				this.on("maxfilesexceeded", function(file){
			        aler4t("No more files please!");
			    });
				this.on('success',function(file, response){
					var v_response = JSON.parse(response);
					var v_newData = {"id": "file", "files": v_response.files, "filename": v_response.filename};
					v_listDataFiles.push(v_newData);
				});
				myDropzone = this;
				/*var submitButton = document.querySelector('#btn_submit');
				submitButton.addEventListener("click", function() {
					myDropzone.processQueue();
				});*/
				v_uploadRes = false;
				this.on('complete',function(file, response){
					if (this.getUploadingFiles().length == 0 && this.getQueuedFiles().length > 0) {
						myDropzone.processQueue();
					}
					
					if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
						var _this = this;
						_this.removeAllFiles();
						v_uploadRes = true;
						v_uploadEnd = true;
						
						$("#suratkeluar-form").submit();
					}
				});
			},
		};
		
        loadPejabat();
		loadJenis();
		loadDetailJabatan();
		loadDetailDetail();
		loadDetailLampiran();
		loadDetailTembusan();
        loadDetailFiles();
    });
	
	function validate() {
		var v_count = myDropzone.files.length;
		//alert(v_count);
		if (v_count <= 0) v_uploadRes = true;
		if (v_uploadRes == false) {
			myDropzone.processQueue();
			return false;
		}
		else {
			var v_temp = '';
			var v_filter;
			v_filter = v_listDataFiles.filter(function (p_element) {
			  	return p_element.id == 'file';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.files + '~#|#~';
					v_temp += p_value.filename + '~#|#~';
				});
			}
			$("#filedata").val(v_temp);
			
			v_temp = '';
			var v_filter;
			v_filter = v_listDetailDeleted.filter(function (p_element) {
			  	return p_element.model == 'suratkeluarfiles';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.id + '~#|#~';
				});
			}
			$("#filedelete").val(v_temp);
			
			v_temp = '';
			$.each( v_listDataJabatan, function( p_key, p_value ) {
				if (v_temp != '') {
					v_temp += '~#;#~';
				}
				v_temp += p_value.suratkeluarjabatanid + '~#|#~';
				v_temp += p_value.suratkeluarid + '~#|#~';
				v_temp += p_value.jabatanid + '~#|#~';
				v_temp += p_value.jabatan + '~#|#~';
			});
			$("#jabatandata").val(v_temp);
			
			v_temp = '';
			var v_filter;
			v_filter = v_listDetailDeleted.filter(function (p_element) {
			  	return p_element.model == 'suratkeluarjabatan';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.id + '~#|#~';
				});
			}
			$("#jabatandelete").val(v_temp);
			
			v_temp = '';
			$.each( v_listDataDetail, function( p_key, p_value ) {
				if (v_temp != '') {
					v_temp += '~#;#~';
				}
				v_temp += p_value.suratkeluardetailid + '~#|#~';
				v_temp += p_value.suratkeluarid + '~#|#~';
				v_temp += p_value.kode + '~#|#~';
				v_temp += p_value.indekkode1 + '~#|#~';
				v_temp += p_value.indekkode2 + '~#|#~';
				v_temp += p_value.indeknomor + '~#|#~';
				v_temp += p_value.indekbulan + '~#|#~';
				v_temp += p_value.indektahun + '~#|#~';
				v_temp += p_value.klasifikasi + '~#|#~';
				v_temp += p_value.perihal + '~#|#~';
				v_temp += $('#' + '<?php echo CHtml::activeId($model, 'tglsurat') ?>').val() + '~#|#~';
				v_temp += p_value.jenisid + '~#|#~';
			});
			$("#detaildata").val(v_temp);
			
			v_temp = '';
			var v_filter;
			v_filter = v_listDetailDeleted.filter(function (p_element) {
			  	return p_element.model == 'suratkeluardetail';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.id + '~#|#~';
				});
			}
			$("#detaildelete").val(v_temp);
			
			v_temp = '';
			$.each( v_listDataLampiran, function( p_key, p_value ) {
				if (v_temp != '') {
					v_temp += '~#;#~';
				}
				v_temp += p_value.suratkeluarlampiranid + '~#|#~';
				v_temp += p_value.suratkeluarid + '~#|#~';
				v_temp += p_value.nourut + '~#|#~';
				v_temp += p_value.lampiran + '~#|#~';
			});
			$("#lampirandata").val(v_temp);
			
			v_temp = '';
			var v_filter;
			v_filter = v_listDetailDeleted.filter(function (p_element) {
			  	return p_element.model == 'suratkeluarlampiran';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.id + '~#|#~';
				});
			}
			$("#lampirandelete").val(v_temp);
			
			v_temp = '';
			$.each( v_listDataTembusan, function( p_key, p_value ) {
				if (v_temp != '') {
					v_temp += '~#;#~';
				}
				v_temp += p_value.suratkeluartembusanid + '~#|#~';
				v_temp += p_value.suratkeluarid + '~#|#~';
				v_temp += p_value.nourut + '~#|#~';
				v_temp += p_value.tembusan + '~#|#~';
			});
			$("#tembusandata").val(v_temp);
			
			v_temp = '';
			var v_filter;
			v_filter = v_listDetailDeleted.filter(function (p_element) {
			  	return p_element.model == 'suratkeluartembusan';
			});
			if (v_filter.length > 0) {
				$.each( v_filter, function( p_key, p_value ) {
					if (v_temp != '') {
						v_temp += '~#;#~';
					}
					v_temp += p_value.id + '~#|#~';
				});
			}
			$("#tembusandelete").val(v_temp);
			
			if ($("#detaildata").val() == '') {
				alert("Detail Surat Keluar tidak boleh kosong");
				return false;
			}
			if ($("#jabatandata").val() == '') {
				alert("Pejabat Konfirmasi tidak boleh kosong");
				return false;
			}
			if ($("#lampirandata").val() == '') {
				alert("Lampiran tidak boleh kosong");
				return false;
			}
			if ($("#tembusandata").val() == '') {
				alert("Tembusan tidak boleh kosong");
				return false;
			}
			
			return true;
		}
	}
	
	function loadPejabat() {
		$.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadpejabat') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
            	v_listJenis = JSON.parse(data);
				var v_data = '';
				$.each( v_listJenis, function( p_key, p_value ) {
					v_data += '<option value="'+p_value.id+'">'+p_value.text+'</option>';
			    });
				
				$('#<?php echo CHtml::activeId($model, 'pejabatid') ?>').html(v_data);
                $('#<?php echo CHtml::activeId($model, 'pejabatid') ?>').select2().select2('val', '');
                $('#<?php echo CHtml::activeId($model, 'pejabatid') ?>').select2().select2('val', '<?php echo $model->pejabatid; ?>');
            },
            error: function (jqXHR, status, err) {
                alert(err);
            }
        });
    }
	
	function loadJenis() {
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadjenis') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
	//alert('ok'+data);
            	v_listJenis = JSON.parse(data);
				var v_data = '';
				$.each( v_listJenis, function( p_key, p_value ) {
					v_data += '<option value="'+p_value.id+'">'+p_value.text+'</option>';
			    });
				
                $('#slc_jenisid').html(v_data);
                $('#indexkode1').val('');
            },
            error: function (jqXHR, status, err) {
                alert(err);
            }
        });
    }
    
	function loadJabatan(){
		 $("#tabelsuratkeluarjabatan").dataTable({
	        "bPaginate": true,
			"ajax" : {
				url: "<?php echo CController::createUrl('suratkeluar/loadjabatan', array(''=>'')) ?>",
	            type: 'POST',
	            data: {
					
				}/*,
				"dataSrc": function (json) {
		           return $.parseJSON(json);
		        }	*/
			},
			//data: JSON.parse(data),
	        columns: [
	            
	            { title: "ID", data:"jabatanid" },
	            { title: "NAMA", data:"nama" },
	            { title: "KETERANGAN", data:"keterangan" }
	        ],
			'columnDefs': [
			 {
			    'targets': 0,
			    'checkboxes': {
			       'selectRow': true
			    }
			 }
			],
			'select': {
			 'style': 'multi'
			},
			"bDestroy": true,
			'order': [[1, 'asc']]
	    });
	}

	function displayFormJabatan(p_mode, p_id) {
		loadJabatan();
		$("#formLabel").text('Pilih Penerima Disposisi');
		
		$('#m_modifDataJabatan').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifDataJabatan').modal('show');
	}
    
    function loadDetailJabatan() {
        v_listDataJabatan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluarjabatan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluarjabatanid": p_value.suratkeluarjabatanid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"jabatanid": p_value.jabatanid,
										"jabatan": p_value.jabatan	};
					v_listDataJabatan.push(v_newData);
			    });
            	
            	loadDataJabatan('d_jabatan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataJabatan(p_divName) {
		var v_filter = v_listDataJabatan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluarjabatan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 85%;">Jabatan</th><th class="header" style="width:10%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluarjabatan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.jabatan+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataJabatan(\''+p_value.suratkeluarjabatanid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function modifyDataJabatan() {
		var v_tabelsuratkeluarjabatan = $('#tabelsuratkeluarjabatan').DataTable();
		var v_tblData = v_tabelsuratkeluarjabatan.rows('.selected').data();
		$.each(v_tblData, function( p_key, p_value ) {
			var v_filter;
			v_filter = v_listDataJabatan.filter(function (p_element) {
			  	return p_element.jabatanid == p_value.jabatanid;
			});
			if (v_filter.length > 0) {
				alert('Jabatan ('+p_value.nama+') sudah pernah dipilih...');
				return;
			}
			else {
				v_detailTmpId ++;
				var v_newData = {	"suratkeluarjabatanid": "tmp__"+v_detailTmpId, 
									"suratkeluarid": v_id,
									"jabatanid": p_value.jabatanid,
									"jabatan": p_value.nama	};
				v_listDataJabatan.push(v_newData);
			}
		});
		
		$('#m_modifDataJabatan').modal('hide');
		
		loadDataJabatan('d_jabatan');
	}

	function deleteDataJabatan(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratkeluarjabatan"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataJabatan.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratkeluarjabatanid'] === p_id) {
			      v_listDataJabatan.splice(p_index, 1);
			    }    
			});
			
			loadDataJabatan('d_jabatan');
		}
	}

	function displayFormDetail(p_mode, p_id) {
		$("#hid_det_mode").val(p_mode);
		$("#hid_det_id").val(p_id);
		
		$("#formLabel").text('Isi Detail Surat Keluar');
		$("#slc_jenisid").val('');
		$("#txt_indekkode2").val('');
		$("#txt_indekbulan").val('');
		$("#txt_indektahun").val('');
		$("#txt_indeknomor").val('');
		$("#slc_klasifikasi").val('');
		$("#txt_perihal").val('');
		if (p_mode.toLowerCase() == 'edit') {
			var v_filter = v_listDataDetail.filter(function (p_element) {
			  	return p_element.suratkeluardetailid === p_id;
			});
    		if (v_filter != null && v_filter != '') {
    			$.each( v_filter, function( p_key, p_value ) {
    				$("#slc_jenisid").val(p_value.indekkode1);
    				$("#txt_indekkode2").val(p_value.indekkode2);
    				$("#txt_indekbulan").val(p_value.indekbulan);
    				$("#txt_indektahun").val(p_value.indektahun);
    				$("#txt_indeknomor").val(p_value.indeknomor);
    				$("#slc_klasifikasi").val(p_value.klasifikasi);
    				$("#txt_perihal").val(p_value.perihal);
    			});
    		}
		}
		
		$('#m_modifDataDetail').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifDataDetail').modal('show');
	}
    
    function loadDetailDetail() {
        v_listDataDetail = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluardetail') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluardetailid": p_value.suratkeluardetailid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"kode": p_value.kode,
										"indekkode1": p_value.indekkode1,
										"indekkode2": p_value.indekkode2,
										"indekbulan": p_value.indekbulan,
										"indektahun": p_value.indektahun,
										"indeknomor": p_value.indeknomor,
										"klasifikasi": p_value.klasifikasi,
										"perihal": p_value.perihal	};
					v_listDataDetail.push(v_newData);
			    });
            	
            	loadDataDetail('d_detail');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataDetail(p_divName) {
		var v_filter = v_listDataDetail;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluardetail" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 30%;">Nomor Surat</th><th class="header" style="width: 45%;">Perihal</th><th class="header" style="width:20%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluardetail">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.indekkode1+'.'+p_value.indekkode2+'/'+p_value.indekbulan+'/'+p_value.indeknomor+'/'+p_value.indektahun+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.perihal+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Edit" id="btn_edit" class="btn btn-sm btn-warning" onclick="displayFormDetail(\'edit\',\''+p_value.suratkeluardetailid+'\')">';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataDetail(\''+p_value.suratkeluardetailid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function modifyDataDetail() {
		if ($("#slc_jenisid").val() == '') {
			alert('Jenis Surat harus dipilih...');
			return;
		}
		if ($("#txt_indekkode2").val() == '') {
			alert('Indek harus diisi...');
			return;
		}
		if ($("#txt_indekbulan").val() == '') {
			alert('Indek harus diisi...');
			return;
		}
		if ($("#txt_indeknomor").val() == '') {
			alert('indek harus diisi...');
			return;
		}
		if ($("#txt_indektahun").val() == '') {
			alert('Indek harus diisi...');
			return;
		}
		if ($("#slc_klasifikasi").val() == '') {
			alert('Klasifikasi harus dipilih...');
			return;
		}
		if ($("#txt_perihal").val() == '') {
			alert('Perihal harus diisi...');
			return;
		}
		var v_filter;
		v_filter = v_listDataDetail.filter(function (p_element) {
		  	return p_element.indeknomor == $('#slc_indeknomor').val() && p_element.suratkeluardetailid != $('#hid_det_id').val();
		});
		if (v_filter.length > 0) {
			alert('Detail ini sudah pernah dipilih...');
			return;
		}
		if ($('#hid_det_mode').val().toLowerCase() == 'add') {
			v_detailTmpId ++;
			var v_newData = {	"suratkeluardetailid": "tmp__"+v_detailTmpId, 
								"suratkeluarid": v_id,
								"jenisid": $('#slc_jenisid').val(),
								"kode": "-", //$('#txt_kode').val(),
								"indekkode2": $('#txt_indekkode2').val(),
								"indeknomor": $('#txt_indeknomor').val(),
								"indekbulan": $('#txt_indekbulan').val(),
								"indektahun": $('#txt_indektahun').val(),
								"indekkode1": $('#hid_indekkode1').val(),
								"klasifikasi": $('#slc_klasifikasi').val(),
								"perihal": $('#txt_perihal').val()	};
			v_listDataDetail.push(v_newData);
		}
		else {
	    	$.each( v_listDataDetail, function( p_key, p_value ) {
	    		if (p_value.suratkeluardetailid ==  $('#hid_det_id').val()) {
					p_value.jenisid = $('#slc_jenisid').val();
					p_value.kode = ""; //$('#slc_indeknomor').val();
					p_value.indekkode1 = $('#hid_indekkode1').val();
					p_value.indekkode2 = $('#txt_indekkode2').val();
					p_value.indeknomor = $('#txt_indeknomor').val();
					p_value.indekbulan = $('#txt_indekbulan').val();
					p_value.indektahun = $('#txt_indektahun').val();
					p_value.klasifikasi = $('#slc_klasifikasi').val();
					p_value.perihal = $('#txt_perihal').val();
					
					return false;
				}	
	    	});
		}
		
		$('#m_modifDataDetail').modal('hide');
		
		loadDataDetail('d_detail');
	}

	function deleteDataDetail(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratkeluardetail"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataDetail.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratkeluardetailid'] === p_id) {
			      v_listDataDetail.splice(p_index, 1);
			    }    
			});
			
			loadDataDetail('d_detail');
		}
	}

	function displayFormLampiran(p_mode, p_id) {
		$("#hid_lam_mode").val(p_mode);
		$("#hid_lam_id").val(p_id);
		
		$("#formLabel").text('Isi Lampiran Surat Keluar');
		$("#txt_nourut_lampiran").val('');
		$("#txt_lampiran").val('');
		if (p_mode.toLowerCase() == 'edit') {
			var v_filter = v_listDataLampiran.filter(function (p_element) {
			  	return p_element.suratkeluarlampiranid === p_id;
			});
    		if (v_filter != null && v_filter != '') {
    			$.each( v_filter, function( p_key, p_value ) {
    				$("#txt_nourut_lampiran").val(p_value.nourut);
    				$("#txt_lampiran").val(p_value.lampiran);
    			});
    		}
		}
		
		$('#m_modifDataLampiran').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifDataLampiran').modal('show');
	}
    
    function loadDetailLampiran() {
        v_listDataLampiran = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluarlampiran') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluarlampiranid": p_value.suratkeluarlampiranid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"nourut": p_value.nourut,
										"lampiran": p_value.lampiran	};
					v_listDataLampiran.push(v_newData);
			    });
            	
            	loadDataLampiran('d_lampiran');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataLampiran(p_divName) {
		var v_filter = v_listDataLampiran;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluarlampiran" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 15%;">No Urut</th><th class="header" style="width: 65%;">Lampiran</th><th class="header" style="width:20%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluarlampiran">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+p_value.nourut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.lampiran+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Edit" id="btn_edit" class="btn btn-sm btn-warning" onclick="displayFormLampiran(\'edit\',\''+p_value.suratkeluarlampiranid+'\')">';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataLampiran(\''+p_value.suratkeluarlampiranid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function modifyDataLampiran() {
		if ($("#txt_nourut_lampiran").val() == '') {
			alert('Nomor Urut harus dipilih...');
			return;
		}
		if ($("#txt_lampiran").val() == '') {
			alert('Lampiran harus diisi...');
			return;
		}
		var v_filter;
		v_filter = v_listDataLampiran.filter(function (p_element) {
		  	return p_element.lampiran == $('#txt_lampiran').val() && p_element.suratkeluarlampiranid != $('#hid_lam_id').val();
		});
		if (v_filter.length > 0) {
			alert('Lampiran ini sudah pernah diisi...');
			return;
		}
		if ($('#hid_lam_mode').val().toLowerCase() == 'add') {
			v_detailTmpId ++;
			var v_newData = {	"suratkeluarlampiranid": "tmp__"+v_detailTmpId, 
								"suratkeluarid": v_id,
								"nourut": $('#txt_nourut_lampiran').val(),
								"lampiran": $('#txt_lampiran').val()	};
			v_listDataLampiran.push(v_newData);
		}
		else {
	    	$.each( v_listDataLampiran, function( p_key, p_value ) {
	    		if (p_value.suratkeluarlampiranid ==  $('#hid_lam_id').val()) {
					p_value.nourut = $('#txt_nourut_lampiran').val();
					p_value.lampiran = $('#txt_lampiran').val();
					
					return false;
				}	
	    	});
		}
		
		$('#m_modifDataLampiran').modal('hide');
		
		loadDataLampiran('d_lampiran');
	}

	function deleteDataLampiran(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratkeluarlampiran"};
				v_listLampiranDeleted.push(v_newData);
			}
			v_listDataLampiran.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratkeluarlampiranid'] === p_id) {
			      v_listDataLampiran.splice(p_index, 1);
			    }    
			});
			
			loadDataLampiran('d_lampiran');
		}
	}

	function displayFormTembusan(p_mode, p_id) {
		$("#hid_tem_mode").val(p_mode);
		$("#hid_tem_id").val(p_id);
		
		$("#formLabel").text('Isi Tembusan Surat Keluar');
		$("#txt_nourut_tembusan").val('');
		$("#txt_tembusan").val('');
		if (p_mode.toLowerCase() == 'edit') {
			var v_filter = v_listDataTembusan.filter(function (p_element) {
			  	return p_element.suratkeluartembusanid === p_id;
			});
    		if (v_filter != null && v_filter != '') {
    			$.each( v_filter, function( p_key, p_value ) {
    				$("#txt_nourut_tembusan").val(p_value.nourut);
    				$("#txt_tembusan").val(p_value.tembusan);
    			});
    		}
		}
		
		$('#m_modifDataTembusan').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifDataTembusan').modal('show');
	}
    
    function loadDetailTembusan() {
        v_listDataTembusan = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluartembusan') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluartembusanid": p_value.suratkeluartembusanid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"nourut": p_value.nourut,
										"tembusan": p_value.tembusan	};
					v_listDataTembusan.push(v_newData);
			    });
            	
            	loadDataTembusan('d_tembusan');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataTembusan(p_divName) {
		var v_filter = v_listDataTembusan;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluartembusan" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 15%;">No Urut</th><th class="header" style="width: 65%;">Tembusan</th><th class="header" style="width:20%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluartembusan">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+p_value.nourut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.tembusan+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Edit" id="btn_edit" class="btn btn-sm btn-warning" onclick="displayFormTembusan(\'edit\',\''+p_value.suratkeluartembusanid+'\')">';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataTembusan(\''+p_value.suratkeluartembusanid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
	    	v_rows = v_rows + '<tr>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '<td></td>';
	    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}
    
    function modifyDataTembusan() {
		if ($("#txt_nourut_tembusan").val() == '') {
			alert('Nomor Urut harus diisi...');
			return;
		}
		if ($("#txt_tembusan").val() == '') {
			alert('Tembusan harus diisi...');
			return;
		}
		var v_filter;
		v_filter = v_listDataTembusan.filter(function (p_element) {
		  	return p_element.tembusan == $('#txt_tembusan').val() && p_element.suratkeluartembusanid != $('#hid_tem_id').val();
		});
		if (v_filter.length > 0) {
			alert('Tembusan ini sudah pernah diisi...');
			return;
		}
		if ($('#hid_tem_mode').val().toLowerCase() == 'add') {
			v_detailTmpId ++;
			var v_newData = {	"suratkeluartembusanid": "tmp__"+v_detailTmpId, 
								"suratkeluarid": v_id,
								"nourut": $('#txt_nourut_tembusan').val(),
								"tembusan": $('#txt_tembusan').val()	};
			v_listDataTembusan.push(v_newData);
		}
		else {
	    	$.each( v_listDataTembusan, function( p_key, p_value ) {
	    		if (p_value.suratkeluartembusanid ==  $('#hid_tem_id').val()) {
					p_value.nourut = $('#txt_nourut_tembusan').val();
					p_value.tembusan = $('#txt_tembusan').val();
					
					return false;
				}	
	    	});
		}
		
		$('#m_modifDataTembusan').modal('hide');
		
		loadDataTembusan('d_tembusan');
	}

	function deleteDataTembusan(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratkeluartembusan"};
				v_listTembusanDeleted.push(v_newData);
			}
			v_listDataTembusan.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratkeluartembusanid'] === p_id) {
			      v_listDataTembusan.splice(p_index, 1);
			    }    
			});
			
			loadDataTembusan('d_tembusan');
		}
	}
    
    function loadDetailFiles() {
        v_listDataFiles = [];
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/loadsuratkeluarfiles') ?>",
            type: 'POST',
            data: {id: "<?php echo $model->suratkeluarid; ?>"},
            success: function (data) {
            	var v_data = JSON.parse(data);
				$.each( v_data, function( p_key, p_value ) {
					var v_newData = {	"suratkeluarfilesid": p_value.suratkeluarfilesid, 
										"suratkeluarid": p_value.suratkeluarid, 
										"filename": p_value.filename,
										"files": p_value.files	};
					v_listDataFiles.push(v_newData);
			    });
            	
            	loadDataFiles('d_files');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

	function loadDataFiles(p_divName) {
		var v_filter = v_listDataFiles;
	    var v_rows = '';
    	var v_noUrut = 1;
    	
        v_rows = v_rows + '        <div class="card-columns el-element-overlay">';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/big/img5.jpg"> <img src="../assets/images/big/img5.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '            <div class="card">';
        v_rows = v_rows + '                <div class="el-card-item">';
        v_rows = v_rows + '                    <div class="el-card-avatar el-overlay-1">';
        v_rows = v_rows + '                        <a class="image-popup-vertical-fit" href="../assets/images/users/1.jpg"> <img src="../assets/images/users/1.jpg" alt="user" /> </a>';
        v_rows = v_rows + '                    </div>';
        v_rows = v_rows + '                    <div class="el-card-content">';
        v_rows = v_rows + '                        <h3 class="box-title">Project title</h3> <small>subtitle of project</small>';
        v_rows = v_rows + '                        <br/> </div>';
        v_rows = v_rows + '                </div>';
        v_rows = v_rows + '            </div>';
        v_rows = v_rows + '        </div>';
		
		v_rows = '';
    	v_rows = v_rows + '<div class="table-responsive"><table id="t_suratkeluarfiles" class="table table-hover color-table muted-table" width="100%"><thead><tr>';
    	v_rows = v_rows + '<th class="header" style="width: 5%;">No</th><th class="header" style="width: 65%;">File</th><th class="header" style="width:30%;">Action</th>';
    	v_rows = v_rows + '</tr></thead><tbody id="tbody_suratkeluarfiles">';
    	
	    if (v_filter != null && v_filter != '') {
			$.each( v_filter, function( p_key, p_value ) {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td>'+v_noUrut+'</td>';
		    	v_rows = v_rows + '<td>'+p_value.filename+'</td>';
		    	v_rows = v_rows + '<td>';
		    	v_rows = v_rows + '<input type="button" value="Preview" id="btn_delete" class="btn btn-sm btn-primary" onclick="window.open(\'<?php echo Yii::app()->baseUrl; ?>/files/'+p_value.files+'\', \'_blank\')">';
		    	v_rows = v_rows + '<input type="button" value="Delete" id="btn_delete" class="btn btn-sm btn-danger" onclick="deleteDataFiles(\''+p_value.suratkeluarfilesid+'\')">';
		    	v_rows = v_rows + '</td>';
		    	v_rows = v_rows + '</tr>';
		    	
		    	v_noUrut ++;
		    });
		}
		else {
		    	v_rows = v_rows + '<tr>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '<td></td>';
		    	v_rows = v_rows + '</tr>';
		}
    	v_rows = v_rows + '</tbody></table></div></div></div>';
		
		$('#'+p_divName).html(v_rows);
	}

	function deleteDataFiles(p_id) {
		var v_konfirmasi = confirm("Anda yakin ingin menghapus data ini?");
	    if (v_konfirmasi == true) {
	    	if (p_id != '' && p_id != null) {
				var v_newData = {"id": p_id, "model": "suratkeluarfiles"};
				v_listDetailDeleted.push(v_newData);
			}
			v_listDataFiles.forEach(function(p_hasil, p_index) {
			    if(p_hasil['suratkeluarfilesid'] === p_id) {
			      v_listDataFiles.splice(p_index, 1);
			    }    
			});
			
			loadDataFiles('d_files');
		}
	}
	
	function setIndexKode1() {
		var v_filter;
		v_filter = v_listJenis.filter(function (p_element) {
		  	return p_element.id == $('#slc_jenisid').val();
		});
		if (v_filter.length > 0) {
			$.each( v_filter, function( p_key, p_value ) {
				$('#hid_indekkode1').val(p_value.value);
			});
		}
		else {
			$('#hid_indekkode1').val('');
		}
	}
	
    function getNextNo() {
        $.ajax({
            url: "<?php echo CController::createUrl('suratkeluar/getnextno') ?>",
            type: 'POST',
            data: {jenisid: $('#slc_jenisid').val() },
            success: function (data) {
                $('#txt_indeknomor').val(data);
				$('#txt_indektahun').val('<?php echo Yii::app()->user->getTahun(); ?>');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
</script>