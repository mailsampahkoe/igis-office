<?php
/* @var $this TahunController */
/* @var $model Tahun */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
?>
<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'tahun-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'kode'); ?>
                <?php echo $form->textField($model, 'kode', array('size' => 60, 'maxlength' => 4, 'class' => 'form-control', 'placeholder' => 'Kode')); ?>
                <?php echo $form->error($model, 'kode'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'nama'); ?>
                <?php echo $form->textField($model, 'nama', array('size' => 60, 'maxlength' => 20, 'class' => 'form-control', 'placeholder' => 'Nama Tahun')); ?>
                <?php echo $form->error($model, 'nama'); ?>
            </div>
        </div>
		<div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'keterangan'); ?>
                <?php echo $form->textArea($model, 'keterangan', array('size' => 60, 'maxlength' => 250, 'class' => 'form-control', 'placeholder' => 'Keterangan')); ?>
                <?php echo $form->error($model, 'keterangan'); ?>
            </div>
        </div>
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary')); ?>
            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('tahun/admin'); ?>';">Cancel</button>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div></div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    
	$(document).ready(function () {
		
    });

</script>