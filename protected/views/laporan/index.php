<?php
/* @var $this RinciobyekController */
/* @var $model Rinciobyek */

Yii::import('ext.select2.Select2');
$baseUrl = Yii::app()->theme->baseUrl;
?>

<script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap-treeview-master/dist/bootstrap-treeview.min.js"></script>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Laporan Rekap</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('site/index'); ?>">Home</a></li>
                <li class="breadcrumb-item active">Laporan Rekap</li>
            </ol>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="search-form">
    
</div><!-- search-form -->
<div class="row">
</div>
<br/>
<div class="row">	
<div class="table-responsive" style="">
	<div class="card">
	<div class="card-body">
    <div class="row">
        <div class="col-md-12">
		    <div class="row">
		        <div class="col-md-12">
		            <h4 class="card-title">Jenis Laporan</h4>
		            <div id="tvwJenisRpt" class="" style=" overflow-y:auto; border: 0px solid #ccc ;"></div>
		        </div>
		    </div>
		    <div class="row">&nbsp;
		    </div>
		    <div class="row">
				<div class="col-md-2">
			        <div class="form-group" style="padding-right:0px;">
						<input type="button" name="btn_print" id="btn_print" value="Cetak" class="btn btn-primary" onclick="printDoc('1')"/>
					</div>
				</div>
				<div class="col-md-10">
			        <div class="form-group" style="padding-right:0px;">
						<div id="d_loaderprinting" style="text-align: right;font-weight: bold;" class="none animate-flicker">L o a d i n g &nbsp; P r e v i e w &nbsp; d a t a . . .</div>
						<iframe name="d_pdfView" id="d_pdfView" style="width:100%;height:0px;" frameborder="0" src="" onload="closeProgress()"></iframe>
					</div>
				</div>
		    </div>
	    </div>
        <!--<div class="col-md-7">
		    <div class="row">
		        <div class="col-md-12">
		            <h4 class="card-title">Filter Data</h4>
					<div class="form-group" id="" style="padding-right:0px;">
						<div> 
							<?php
								echo Select2::dropDownList('bagianid', "", array(), array(
				                    'prompt' => 'PILIH BAGIAN',
				                    'class' => 'col-lg-12 fontface', 'style' => 'padding:0px', 
									'onchange' => 'loadSubunit()',
									'selected' => 'selected'
				                )); 
							?>
			            </div>
			        </div>
					<div class="form-group" id="" style="padding-right:0px;">
						<div> 
							<?php
								echo Select2::dropDownList('subbagianid', "", array(), array(
				                    'prompt' => 'PILIH SUB BAGIAN',
				                    'class' => 'col-lg-12 fontface', 'style' => 'padding:0px', 
									'selected' => 'selected'
				                )); 
							?>
			            </div>
			        </div>
				</div>
				<div class="col-md-4">
			        <div class="form-group" style="padding-right:0px;">
						<input type="button" name="btn_print" id="btn_print" value="Cetak" class="btn btn-primary" onclick="printDoc('1')"/>
					</div>
				</div>
				<div class="col-md-8">
			        <div class="form-group" style="padding-right:0px;">
						<div id="d_loaderprinting" style="text-align: right;font-weight: bold;" class="none animate-flicker">L o a d i n g &nbsp; P r e v i e w &nbsp; d a t a . . .</div>
						<iframe name="d_pdfView" id="d_pdfView" style="width:100%;height:0px;" frameborder="0" src="" onload="closeProgress()"></iframe>
					</div>
				</div>
			</div>
	    </div>-->
	</div>
	</div>
</div></div></div>
<script>
    var v_token;
    
	$(document).ready(function () {
		closeProgress();
    });
    
	function printDoc(p_show){
		blockResubmit();
		
		var v_id = "1";
		var v_newLocation="";
		var selected = $('#tvwJenisRpt').treeview('getSelected', 'sm');
		if (selected.length>0){
			if (selected[0].id=="suratmasuk"){
				v_newLocation="<?php echo CController::createUrl('laporan/print', array('jenis' => 'suratmasuk', 'token'=>'"+v_token+"')); ?>";
			}
			else if (selected[0].id=="disposisikabag"){
				v_newLocation="<?php echo CController::createUrl('laporan/print', array('jenis' => 'disposisikabag', 'token'=>'"+v_token+"')); ?>";
			}
			else if (selected[0].id=="disposisikasubbag"){
				v_newLocation="<?php echo CController::createUrl('laporan/print', array('jenis' => 'disposisikasubbag', 'token'=>'"+v_token+"')); ?>";
			}
			else if (selected[0].id=="pelaksanaan"){
				v_newLocation="<?php echo CController::createUrl('laporan/print', array('jenis' => 'pelaksanaan', 'token'=>'"+v_token+"')); ?>";
			}
			else if (selected[0].id=="suratkeluar"){
				v_newLocation="<?php echo CController::createUrl('laporan/print', array('jenis' => 'suratkeluar', 'token'=>'"+v_token+"')); ?>";
			}
			else if (selected[0].id=="verifikasikabag"){
				v_newLocation="<?php echo CController::createUrl('laporan/print', array('jenis' => 'verifikasikabag', 'token'=>'"+v_token+"')); ?>";
			}
			else if (selected[0].id=="verifikasikasubbag"){
				v_newLocation="<?php echo CController::createUrl('laporan/print', array('jenis' => 'verifikasikasubbag', 'token'=>'"+v_token+"')); ?>";
			}
			else if (selected[0].id=="pengiriman"){
				v_newLocation="<?php echo CController::createUrl('laporan/print', array('jenis' => 'pengiriman', 'token'=>'"+v_token+"')); ?>";
			}
		}
		
		var v_frame=document.getElementById('d_pdfView');
		if (v_newLocation != "") {
			var v_frame=document.getElementById('d_pdfView');
			if(p_show==null || p_show==0){
				v_frame.src="";
				v_frame.style.visibility="hidden";
				v_frame.style.height="0px";
			}else{
				$("#d_loaderprinting").show();
				v_frame.style.height="1000px";
				v_frame.src=v_newLocation;
				v_frame.style.visibility="visible";
			}
		}
		else {
			alert("Pilih jenis laporan terlebih dahulu...");
		}
	}
	
  	function getCookie( name ) {
	  var parts = document.cookie.split(name + "=");
	  if (parts.length == 2) return parts.pop().split(";").shift();
	}

	function expireCookie( cName ) {
	    document.cookie = 
	        encodeURIComponent(cName) + "=deleted; expires=" + new Date( 0 ).toUTCString();
	}

	function setCursor( docStyle, buttonStyle ) {
	    document.getElementById( "btn_print" ).style.cursor = docStyle;
	    //document.getElementById( "button-id" ).style.cursor = buttonStyle;
	}
	
	function setButtonDisable(p_value) {
		document.getElementById("btn_print").disabled = p_value;
	}

	function setFormToken() {
	    var v_downloadToken = new Date().getTime();
	    return v_downloadToken;
	}

	var downloadTimer;
	var attempts = 3;

	// Prevents double-submits by waiting for a cookie from the server.
	function blockResubmit() {
	    v_token = setFormToken();
	    setCursor( "wait", "wait" );
	    setButtonDisable(true);

	    downloadTimer = window.setInterval( function() {
	        var token = getCookie( "downloadToken" );

	        if( (token == v_token) || (attempts == 0) ) {
	            unblockSubmit();
	        }

	        attempts--;
	    }, 1000 );
	}

	function unblockSubmit() {
	  setCursor( "auto", "pointer" );
	  setButtonDisable(false);
	  window.clearInterval( downloadTimer );
	  expireCookie( "downloadToken" );
	  attempts = 3;
	  closeProgress();
	}
	
	function closeProgress(){
		$("#d_loaderprinting").hide();
	}
	
    var v_dataJenisRpt = [
      {
        text: 'Surat Masuk',
        href: '#parent',
        id:'sm',
        tags: ['7'],
        nodes: [
	      {
	        text: 'Surat Masuk',
	        href: '#parent1',
	        id:'suratmasuk',
	        tags: ['0']
	      },
	      {
	        text: 'Disposisi Kepala',
	        href: '#parent2',
	        id:'disposisikabag',
	        tags: ['0']
	      },
	      {
	        text: 'Disposisi Kepala Sub Bagian',
	        href: '#parent3',
	        id:'disposisikasubbag',
	         tags: ['0']
	      },
	      {
	        text: 'Pelaksanaan',
	        href: '#parent3',
	        id:'pelaksanaan',
	         tags: ['0']
	      }
	    ]
	  },
	  {
        text: 'Surat Keluar',
        href: '#parent',
        id:'sk',
        tags: ['7'],
        nodes: [
	      {
	        text: 'Surat Keluar',
	        href: '#parent1',
	        id:'suratkeluar',
	        tags: ['0']
	      },
	      {
	        text: 'Verifikasi Kepala',
	        href: '#parent2',
	        id:'verifikasikabag',
	        tags: ['0']
	      },
	      {
	        text: 'Verifikasi Kepala Sub Bagian',
	        href: '#parent3',
	        id:'verifikasikasubbag',
	         tags: ['0']
	      },
	      {
	        text: 'Pengiriman',
	        href: '#parent3',
	        id:'pengiriman',
	         tags: ['0']
	      }
	    ]
	  }
    ];
    
    $('#tvwJenisRpt').treeview({
      color: "#428bca",
      selectedBackColor: "#03a9f3",
      onhoverColor: "rgba(0, 0, 0, 0.05)",
      expandIcon: 'ti-plus',
      collapseIcon: 'ti-minus',
      nodeIcon: 'fa fa-folder',
      data: v_dataJenisRpt
    });
    $('#tvwJenisRpt').treeview('collapseAll', { silent: true });
    
    function getTree() {
	  return v_dataJenisRpt;
	}
	$('#tvwJenisRpt').bind('mousewheel', function(e) {
	  $(this).scrollTop($(this).scrollTop() - e.originalEvent.wheelDeltaY);
	  return false;
	});
</script>