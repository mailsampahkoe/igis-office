<?php
/* @var $this AksesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Aksess',
);

$this->menu=array(
	array('label'=>'Create Akses', 'url'=>array('create')),
	array('label'=>'Manage Akses', 'url'=>array('admin')),
);
?>

<h1>Aksess</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
