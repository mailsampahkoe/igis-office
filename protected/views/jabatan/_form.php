<?php
/* @var $this JabatanController */
/* @var $model Jabatan */
/* @var $form CActiveForm */
Yii::import('ext.select2.Select2');
?>
<header class="panel_header">
	<div class="actions panel_actions pull-right"></div>

</header>

<div class="content-body">
    <div class="row">
		<div class="col-12">
           <div class="card"><div class="card-body">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'jabatan-form',
            'method' => 'POST',
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true
            ),
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
            )
        ));
        ?>
        <?php echo $form->errorSummary($model); ?>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'bagian'); ?><br>
                <?php
                    echo Select2::activeDropDownList($model, 'bagianid', array('' => ''), array(
                        'prompt' => 'PILIH BAGIAN',
                        'class' => 'col-lg-6', 'style' => 'padding:0px',
						'onchange' => 'loadSubbagian()',
						'selected' => 'selected'
                    ));
                ?>
                <?php echo $form->error($model, 'bagianid'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'sub bagian'); ?><br>
                <?php
                    echo Select2::activeDropDownList($model, 'subbagianid', array('' => ''), array(
                        'prompt' => 'PILIH SUB BAGIAN',
                        'class' => 'col-lg-6', 'style' => 'padding:0px',
						'selected' => 'selected'
                    ));
                ?>
                <?php echo $form->error($model, 'subbagianid'); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'nama jabatan'); ?>
                <?php echo $form->textField($model, 'nama', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Nama Jabatan')); ?>
                <?php echo $form->error($model, 'nama'); ?>
            </div>
        </div>
        <div class="form-group">
	    	<div class="col-md-4 col-sm-4">
                <?php echo $form->labelEx($model, 'level'); ?><br>
                <?php
                    echo Select2::activeDropDownList($model, 'level', array('4'=>'Kepala', '5'=>'Kasubbag/Kasie', '10'=>'Staf'), array(
                        'prompt' => 'PILIH LEVEL',
                        'class' => 'col-lg-12', 'style' => 'padding:0px',
						'selected' => 'selected'
                    ));
                ?>
                <?php echo $form->error($model, 'level'); ?>
	        </div>
        </div>
		<div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'terima disposisi'); ?> &nbsp;
				<?php echo $form->checkBox($model,'terimadisposisi',array('value' => '1', 'uncheckValue'=>'0')); ?>
                <?php echo $form->error($model, 'terimadisposisi'); ?>
            </div>
        </div>
		<div class="form-group">
            <div class="col-lg-12">
                <?php echo $form->labelEx($model, 'keterangan'); ?>
                <?php echo $form->textArea($model, 'keterangan', array('size' => 60, 'maxlength' => 90, 'class' => 'form-control', 'placeholder' => 'Keterangan')); ?>
                <?php echo $form->error($model, 'keterangan'); ?>
            </div>
        </div>
		<div class="col-lg-12">
            <br/><br/>
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambah' : 'Update', array('class' => 'btn btn-primary')); ?>
            <button type="button" class="btn btn-inverse" onclick="window.location.href='<?php echo CController::createUrl('jabatan/admin'); ?>';">Cancel</button>
        </div>
        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div></div></div></div>

<?php
$ismode = "add";
if (!$model->isNewRecord) {
    $ismode = "edit";
}
?>

<script type="text/javascript">
    
	$(document).ready(function () {
        loadBagian();
        loadSubbagian();
    });
	
	function loadBagian() {
        $.ajax({
            url: "<?php echo CController::createUrl('jabatan/loadbagian') ?>",
            type: 'POST',
            data: {},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'bagianid') ?>').select2().select2('val', '<?php echo $model->bagianid; ?>');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }
	
	function loadSubbagian() {
        $.ajax({
            url: "<?php echo CController::createUrl('jabatan/loadsubbagian') ?>",
            type: 'POST',
            data: {bagianid: $('#<?php echo CHtml::activeId($model, 'bagianid') ?>').val()},
            success: function (data) {
                $('#<?php echo CHtml::activeId($model, 'subbagianid') ?>').html(data);
                $('#' + '<?php echo CHtml::activeId($model, 'subbagianid') ?>').select2().select2('val', '');
                $('#' + '<?php echo CHtml::activeId($model, 'subbagianid') ?>').select2().select2('val', '<?php echo $model->subbagianid; ?>');
            },
            error: function (jqXHR, status, err) {
                //alert(err);
            }
        });
    }

</script>