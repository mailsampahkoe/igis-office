<?php
/* @var $this JabatanController */
/* @var $model Jabatan */

$this->breadcrumbs = array(
    'Jabatan' => array('admin'),
    'Ubah Jabatan',
);
?>


<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Ubah Jabatan</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $this->createUrl('jabatan/admin'); ?>">Data Jabatan</a></li>
                <li class="breadcrumb-item active">Ubah Jabatan</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>