-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 11:37 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `igis-office`
--

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1539636091),
('m130524_201442_init', 1539636097);

-- --------------------------------------------------------

--
-- Table structure for table `tmakses`
--

CREATE TABLE `tmakses` (
  `aksesid` varchar(20) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `superadmin` bit(1) NOT NULL DEFAULT b'0',
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmakses`
--

INSERT INTO `tmakses` (`aksesid`, `kode`, `nama`, `superadmin`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('0', 'superadmin', 'Superadmin', b'1', 'admin', '2019-11-01 00:00:00', 'admin', 'superadmin', '2019-11-01 00:00:00', 'admin', b'1'),
('1', 'admin', 'Admin', b'1', 'admin', '2018-10-15 00:00:00', 'admin', 'admin', '2018-10-15 00:00:00', 'admin', b'0'),
('10', 'staf', 'Staf', b'0', 'admin', '2019-11-01 00:00:00', 'admin', 'admin', '2019-11-01 00:00:00', 'admin', b'0'),
('2', 'kaunit', 'Kepala Unit', b'1', 'admin', '2018-10-15 00:00:00', 'admin', 'admin', '2018-10-15 00:00:00', 'admin', b'1'),
('3', 'kasubunit', 'Kepala Sub Unit', b'1', 'admin', '2018-10-15 00:00:00', 'admin', 'admin', '2018-10-15 00:00:00', 'admin', b'1'),
('4', 'kabid', 'Kepala Bidang', b'1', 'admin', '2018-10-15 00:00:00', 'admin', 'admin', '2018-10-15 00:00:00', 'admin', b'1'),
('5', 'kasubbid', 'Kepala Sub Bidang', b'1', 'admin', '2018-10-15 00:00:00', 'admin', 'admin', '2018-10-15 00:00:00', 'admin', b'1'),
('6', 'kabag', 'Kepala Bagian / UPT', b'0', 'admin', '2018-10-15 00:00:00', 'admin', 'admin', '2018-10-15 00:00:00', 'admin', b'0'),
('7', 'kasubbag', 'Kepala Sub Bagian / Seksie', b'0', 'admin', '2019-11-01 00:00:00', 'admin', 'admin', '2019-11-01 00:00:00', 'admin', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tmbagian`
--

CREATE TABLE `tmbagian` (
  `bagianid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmbagian`
--

INSERT INTO `tmbagian` (`bagianid`, `unitid`, `kode`, `nama`, `alamat`, `keterangan`, `tahun`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('A73C39EAD9D0548DDAE2', NULL, '01.', 'Balai Teknik Kesehatan Lingkungan dan Pengendalian Penyakit (BTKLPP)', '123', '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tminstansi`
--

CREATE TABLE `tminstansi` (
  `instansiid` varchar(20) NOT NULL,
  `namainstansi` varchar(150) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `kodepos` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `jenisinstansi` varchar(50) DEFAULT NULL,
  `jenisdaerah` varchar(1) DEFAULT NULL,
  `kepda` varchar(50) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `namakepda` varchar(50) DEFAULT NULL,
  `logo` varchar(1000) DEFAULT NULL,
  `picpemda` varchar(1000) DEFAULT NULL,
  `namakadis` varchar(100) DEFAULT NULL,
  `nipkadis` varchar(50) DEFAULT NULL,
  `namakasi` varchar(100) DEFAULT NULL,
  `nipkasi` varchar(50) DEFAULT NULL,
  `namakabid` varchar(100) DEFAULT NULL,
  `nipkabid` varchar(50) DEFAULT NULL,
  `penomoran` varchar(50) DEFAULT NULL,
  `penomoran1` varchar(50) DEFAULT NULL,
  `penomoran2` varchar(50) DEFAULT NULL,
  `opadd` varchar(20) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(20) DEFAULT NULL,
  `opedit` varchar(20) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(20) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tminstansi`
--

INSERT INTO `tminstansi` (`instansiid`, `namainstansi`, `alamat`, `telp`, `fax`, `kodepos`, `email`, `jenisinstansi`, `jenisdaerah`, `kepda`, `kota`, `namakepda`, `logo`, `picpemda`, `namakadis`, `nipkadis`, `namakasi`, `nipkasi`, `namakabid`, `nipkabid`, `penomoran`, `penomoran1`, `penomoran2`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('', 'Balai Teknik Kesehatan Lingkungan dan Pengendalian Penyakit - Kelas 1 Batam', 'Kelurahan Sei Binti Kecamatan Sagulung Kota Batam', '0778-8075096', '0778-8075097', '29434', 'btklbatam@yahoo.co.id', '1', '1', NULL, 'Batam', NULL, 'logo-kemenkes-png.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '2018-10-26 22:10:52', '::1', 'admin', '2018-11-04 20:52:03', '127.0.0.1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tmjabatan`
--

CREATE TABLE `tmjabatan` (
  `jabatanid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `subunitid` varchar(20) DEFAULT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `subbagianid` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `terimadisposisi` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmjabatan`
--

INSERT INTO `tmjabatan` (`jabatanid`, `unitid`, `subunitid`, `bagianid`, `subbagianid`, `nama`, `level`, `keterangan`, `terimadisposisi`, `tahun`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('5380303FA54C3B4F0FF0', NULL, NULL, 'A73C39EAD9D0548DDAE2', 'AD4D71EAF74CFF613E98', 'Kepala Sub Bagian TU', 5, 'Ok', b'1', '2018', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '2018-10-31 07:03:26', '::1', b'0'),
('893C493AA48B557CF999', NULL, NULL, 'A73C39EAD9D0548DDAE2', 'AD4D71EAF74CFF613EA1', 'Staf', 10, '', b'0', '2018', 'admin', '2018-11-05 02:16:11', '127.0.0.1', 'admin', '2018-11-05 02:16:21', '127.0.0.1', b'0'),
('B2611EFDDC196804D239', NULL, NULL, 'A73C39EAD9D0548DDAE2', NULL, 'Kepala UPT', 4, 'Ket....', b'0', '2018', 'admin', '2018-10-23 15:41:22', '202.154.185.250', 'admin', '2018-10-31 07:03:32', '::1', b'0'),
('D3BD701257A7063571A1', NULL, NULL, 'A73C39EAD9D0548DDAE2', 'AD4D71EAF74CFF613E98', 'Staf', 10, '', b'0', '2018', 'admin', '2018-11-05 02:15:46', '127.0.0.1', 'admin', '2018-11-05 02:15:46', '127.0.0.1', b'0'),
('F8265B4659313AC9044F', NULL, NULL, 'A73C39EAD9D0548DDAE2', 'AD4D71EAF74CFF613EA1', 'Kepala Seksi SE', 5, '', b'1', '2018', 'admin', '2018-11-05 02:05:18', '127.0.0.1', 'admin', '2018-11-05 02:05:18', '127.0.0.1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tmjenis`
--

CREATE TABLE `tmjenis` (
  `jenisid` varchar(20) NOT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmjenis`
--

INSERT INTO `tmjenis` (`jenisid`, `kode`, `nama`, `alamat`, `keterangan`, `tahun`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('A42FE1083EE3A6359DFC', 'AA', '1233', NULL, '-', '', 'admin', '2018-10-29 06:16:05', '::1', '1', '2018-10-29 06:16:09', '::1', b'1'),
('A73C39EAD9D0548DDAA0', 'AR', 'Kearsipan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA1', 'HK', 'Hukum', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA2', 'IR', 'Informatika', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA3', 'KH', 'Kemahasiswaan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA4', 'KM', 'Komunikasi Publik', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA5', 'KN', 'Kekayaan Negara', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA6', 'KP', 'Kepegawaian', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA7', 'KR', 'Kerumahtanggaan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA8', 'KS', 'Kerja Sama', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAA9', 'KU', 'Keuangan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAB1', 'OT', 'Organisasi dan Tatalaksana', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAB2', 'PP', 'Pendidikan dan Pengajaran', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAB3', 'PR', 'Perencanaan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAB4', 'PS', 'Pengawasan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAB5', 'UM', 'Umum', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC0', 'AD', 'Analisis Determinan Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC1', 'DG', 'Perencanaan dan Pendayagunaan SDM Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC2', 'DL', 'Pelatihan Sumber Daya Manusia Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC3', 'DM', 'Peningkatan Mutu Sumber Daya Manusia Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC4', 'DP', 'Pendidikan Sumber Daya Manusia Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC5', 'FK', 'Pengawasan Alat Kesehatan dan Perbekalan Rumah Tangga', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC6', 'FO', 'Tata Kelola Obat Publik dan Perbekalan Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC7', 'FP', 'Produksi dan Distribusi Kefarmasian', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC8', 'FR', 'Penilaian Alat Kesehatan dan Perbekalan Rumah Tangga', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAC9', 'FY', 'Pelayanan Kefarmasian', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD0', 'GM', 'Gizi Masyarakat', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD1', 'HJ', 'Kesehatan Haji', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD2', 'JP', 'Pembiayaan dan Jaminan Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD3', 'KG', 'Kesehatan Keluarga', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD4', 'KI', 'Konsil Kedokteran Indonesia', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD5', 'KJ', 'Kesehatan Jiwa', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD6', 'KK', 'Penanggulangan Krisis', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD7', 'KL', 'Kesehatan Lingkungan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD8', 'KO', 'Kesehatan Kerja dan Olahraga', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAD9', 'LB', 'Penelitian dan Pengembangan Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE0', 'PK', 'Promosi Kesehatan dan Pemberdayaan Masyarakat', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE1', 'PM', 'Pencegahan dan Pengendalian Penyakit Menular Langsung', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE2', 'PV', 'Pencegahan dan Pengendalian Penyakit Tular Vektor Zoonotik', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE3', 'SR', 'Surveilans dan Karantina Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE4', 'TL', 'Pengembangan Teknologi Laboratorium', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE5', 'TM', 'Pencegahan dan Pengendalian Penyakit Tidak Menular', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE6', 'YK', 'Fasilitas Pelayanan Kesehatan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE7', 'YM', 'Mutu dan Akreditasi Pelayanan Masyarakat', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE8', 'YP', 'Pelayanan Kesehatan Primer', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAE9', 'YR', 'Pelayanan Kesehatan Rujukan', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('A73C39EAD9D0548DDAF0', 'YT', 'Pelayanan Kesehatan Tradisional', NULL, '-', '', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tmmenu`
--

CREATE TABLE `tmmenu` (
  `menuid` varchar(20) NOT NULL,
  `modulid` varchar(20) DEFAULT NULL,
  `parent` varchar(50) DEFAULT NULL,
  `menu` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `ishide` varchar(50) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmmenu`
--

INSERT INTO `tmmenu` (`menuid`, `modulid`, `parent`, `menu`, `url`, `urutan`, `ishide`, `jenis`) VALUES
('1', NULL, 'Instansi', 'Instansi', 'instansi', 1, '0', 1),
('101', NULL, 'Surat Masuk', 'Surat Masuk', 'suratmasuk/admin', 101, '0', 2),
('102', NULL, 'Surat Masuk', 'Disposisi Kepala', 'disposisikabag/admin', 102, '0', 2),
('103', NULL, 'Surat Masuk', 'Disposisi Kasubbag', 'disposisikasubbag/admin', 103, '0', 2),
('104', NULL, 'Surat Masuk', 'Pelaksanaan', 'pelaksanaan/admin', 104, '0', 2),
('105', NULL, 'Surat Keluar', 'Surat Keluar', 'suratkeluar/admin', 105, '0', 2),
('106', NULL, 'Surat Keluar', 'Verifikasi Kasubbag', 'verifikasikasubbag/admin', 106, '0', 2),
('107', NULL, 'Surat Keluar', 'Verifikasi Kepala', 'verifikasikabag/admin', 107, '0', 2),
('108', NULL, 'Surat Keluar', 'Pengiriman', 'pengiriman/admin', 108, '0', 2),
('2', NULL, 'Tahun', 'Tahun', 'tahun/admin', 2, '0', 1),
('201', NULL, 'Laporan Rekap', 'Laporan Rekapitulasi', 'laporan', 201, '0', 3),
('3', NULL, 'Jenis', 'Jenis', 'jenis/admin', 3, '0', 1),
('301', NULL, 'Pengkodean', 'Pengkodean', 'kode/admin', 301, '1', 4),
('302', NULL, 'User', 'Pengaturan User', 'user/admin', 302, '0', 4),
('303', NULL, 'User', 'Ubah Password', 'user/password', 303, '0', 4),
('304', NULL, 'Pengumuman', 'Pengumuman', 'pengumuman/admin', 4, '0', 4),
('5', NULL, 'Bagian', 'Bagian', 'bagian/admin', 5, '0', 1),
('6', NULL, 'Bagian', 'Sub Bagian', 'subbagian/admin', 6, '0', 1),
('7', NULL, 'Pegawai', 'Pegawai', 'pegawai/admin', 7, '0', 1),
('8', NULL, 'Jabatan', 'Jabatan', 'jabatan/admin', 8, '0', 1),
('9', NULL, 'Perintah', 'Perintah', 'perintah/admin', 9, '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tmmodul`
--

CREATE TABLE `tmmodul` (
  `modulid` varchar(20) NOT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL,
  `nourut` int(11) DEFAULT NULL,
  `hidden` smallint(6) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmpegawai`
--

CREATE TABLE `tmpegawai` (
  `pegawaiid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `subunitid` varchar(20) DEFAULT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `subbagianid` varchar(20) DEFAULT NULL,
  `jabatanid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `gelardepan` varchar(30) DEFAULT NULL,
  `gelarbelakang1` varchar(30) DEFAULT NULL,
  `gelarbelakang2` varchar(30) DEFAULT NULL,
  `tempatlahir` varchar(50) DEFAULT NULL,
  `tanggallahir` date DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `jeniskelamin` smallint(6) DEFAULT NULL,
  `jeniskepegawaian` smallint(6) DEFAULT NULL,
  `statusaktif` smallint(6) DEFAULT NULL,
  `statuspernikahan` smallint(6) DEFAULT NULL,
  `nohp` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmpegawai`
--

INSERT INTO `tmpegawai` (`pegawaiid`, `unitid`, `subunitid`, `bagianid`, `subbagianid`, `jabatanid`, `kode`, `nama`, `gelardepan`, `gelarbelakang1`, `gelarbelakang2`, `tempatlahir`, `tanggallahir`, `alamat`, `jeniskelamin`, `jeniskepegawaian`, `statusaktif`, `statuspernikahan`, `nohp`, `email`, `keterangan`, `tahun`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('7C7691F2A499DB759F9B', NULL, NULL, 'A73C39EAD9D0548DDAE2', 'AD4D71EAF74CFF613EA1', '893C493AA48B557CF999', '14', 'uyiyiy', 'iuyui', 'uy', 'iuyu', 'iyuiy', '2018-11-07', 'kjgh', 0, NULL, 1, 0, '67687', 'hkjhhk@jhghj.com', NULL, NULL, 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '2018-11-05 02:05:58', '127.0.0.1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tmpegawaijabatan`
--

CREATE TABLE `tmpegawaijabatan` (
  `pegawaijabatanid` varchar(20) NOT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `jabatan` smallint(6) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusaktif` smallint(6) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmperintah`
--

CREATE TABLE `tmperintah` (
  `perintahid` varchar(20) NOT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmperintah`
--

INSERT INTO `tmperintah` (`perintahid`, `nourut`, `nama`, `keterangan`, `status`, `tahun`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('10', 10, 'Arsip', '-', b'1', '2018', 'admin', '2018-10-29 00:00:00', 'admin', 'admin', '2018-10-29 00:00:00', 'admin', b'0'),
('201EF2C49B036ECCB91E', 1, 'Mohon Saran / Tanggapan', 'test ...', b'1', '2018', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('508E4BBAB1F73B7E28BC', 2, 'Mohon Konsep Jawaban', '--', b'1', '2018', 'admin', '2018-10-25 05:53:55', '::1', 'admin', '2018-10-25 05:53:55', '::1', b'0'),
('51CC713A295451D15C62', 3, 'Untuk Diketahui', 'test ...', b'1', '2018', 'admin', '0000-00-00 00:00:00', '127.0.0.1', '1', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('6', 6, 'Untuk Dipergunakan', '-', b'1', '2018', 'admin', '2018-10-29 00:00:00', 'admin', 'admin', '2018-10-29 00:00:00', 'admin', b'0'),
('7', 7, 'Dibicarakan Dengan Saya', '-', b'1', '2018', 'admin', '2018-10-29 00:00:00', 'admin', 'admin', '2018-10-29 00:00:00', 'admin', b'0'),
('7F897739A13968C8A889', 4, 'Untuk Tindak Lanjut', 'test ...', b'1', '2018', 'admin', '0000-00-00 00:00:00', '127.0.0.1', '1', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('8', 8, 'Teruskan Ke Staf', '-', b'1', '2018', 'admin', '2018-10-29 00:00:00', 'admin', 'admin', '2018-10-29 00:00:00', 'admin', b'0'),
('9', 9, 'Fotocopy', '-', b'1', '2018', 'admin', '2018-10-29 00:00:00', 'admin', 'admin', '2018-10-29 00:00:00', 'admin', b'0'),
('F8EC291302EB9FE6B658', 5, 'Untuk Dipertimbangkan', '-', b'1', '2018', 'admin', '2018-10-25 05:54:05', '::1', 'admin', '2018-10-25 05:54:05', '::1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tmsubbagian`
--

CREATE TABLE `tmsubbagian` (
  `subbagianid` varchar(20) NOT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmsubbagian`
--

INSERT INTO `tmsubbagian` (`subbagianid`, `bagianid`, `kode`, `nama`, `alamat`, `keterangan`, `tahun`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('AD4D71EAF74CFF613E98', 'A73C39EAD9D0548DDAE2', '01.', 'Sub Bagian TU', 'alamat - ', 'keterangan - ', '2018', 'admin', '0000-00-00 00:00:00', '127.0.0.1', 'admin', '0000-00-00 00:00:00', '127.0.0.1', b'0'),
('AD4D71EAF74CFF613EA1', 'A73C39EAD9D0548DDAE2', '02.', 'Seksi SE', '-', '-', '2018', 'Admin', '2018-10-29 00:00:00', 'Admin', 'Admin', '2018-10-29 00:00:00', 'Admin', b'0'),
('AD4D71EAF74CFF613EA2', 'A73C39EAD9D0548DDAE2', '03.', 'Seksi PTL', '-', '-', '2018', 'Admin', '2018-10-29 00:00:00', 'Admin', 'Admin', '2018-10-29 00:00:00', 'Admin', b'0'),
('AD4D71EAF74CFF613EA3', 'A73C39EAD9D0548DDAE2', '04.', 'Seksi ADKL', '-', '-', '2018', 'Admin', '2018-10-29 00:00:00', 'Admin', 'Admin', '2018-10-29 00:00:00', 'Admin', b'0'),
('AD4D71EAF74CFF613EA4', 'A73C39EAD9D0548DDAE2', '05.', 'Instalasi Lab', '-', '-', '2018', 'Admin', '2018-10-29 00:00:00', 'Admin', 'Admin', '2018-10-29 00:00:00', 'Admin', b'0'),
('AD4D71EAF74CFF613EA5', 'A73C39EAD9D0548DDAE2', '06.', 'PK Dipa', '-', '-', '2018', 'Admin', '2018-10-29 00:00:00', 'Admin', 'Admin', '2018-10-29 00:00:00', 'Admin', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tmsubunit`
--

CREATE TABLE `tmsubunit` (
  `subunitid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmtahun`
--

CREATE TABLE `tmtahun` (
  `tahunid` varchar(20) NOT NULL,
  `kode` varchar(4) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmtahun`
--

INSERT INTO `tmtahun` (`tahunid`, `kode`, `nama`, `keterangan`, `status`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('8B88B7FECCA39CA6957B', '2018', '2018', 'Tahun 2018', NULL, 'admin', '2018-11-04 21:18:56', '127.0.0.1', 'admin', '2018-11-04 21:18:56', '127.0.0.1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tmunit`
--

CREATE TABLE `tmunit` (
  `unitid` varchar(20) NOT NULL,
  `urusanid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `nama` varchar(254) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL,
  `alamat` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmunit`
--

INSERT INTO `tmunit` (`unitid`, `urusanid`, `kode`, `nama`, `alias`, `jenis`, `alamat`, `keterangan`, `tahun`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('1', NULL, '1', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `tmuser`
--

CREATE TABLE `tmuser` (
  `userid` varchar(20) NOT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(254) DEFAULT NULL,
  `password_reset_token` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `grup` smallint(6) DEFAULT NULL,
  `aksesid` varchar(20) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `nohp` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  `jeniskelamin` smallint(6) DEFAULT '0',
  `alamat` varchar(254) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `filenik` varchar(250) DEFAULT NULL,
  `npwp` varchar(30) DEFAULT NULL,
  `filenpwp` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmuser`
--

INSERT INTO `tmuser` (`userid`, `pegawaiid`, `username`, `password`, `password_reset_token`, `auth_key`, `grup`, `aksesid`, `nama`, `nohp`, `email`, `picture`, `status`, `tahun`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`, `jeniskelamin`, `alamat`, `nik`, `filenik`, `npwp`, `filenpwp`) VALUES
('1', '7C7691F2A499DB759F9B', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '202cb962ac59075b964b07152d234b70', '', 1, '1', 'Administrator', '0811', 'kuclukb@gmail.com', 'admin.jpg', 1, '2018', 'admin', '2018-10-15 00:00:00', 'admin', 'admin', '2018-10-15 00:00:00', 'admin', b'0', 1, 'Batam', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `traksesmenu`
--

CREATE TABLE `traksesmenu` (
  `aksesmenuid` varchar(20) NOT NULL,
  `aksesid` varchar(20) DEFAULT NULL,
  `menuid` varchar(20) DEFAULT NULL,
  `accessview` bit(1) DEFAULT NULL,
  `accessadd` bit(1) DEFAULT NULL,
  `accessedit` bit(1) DEFAULT NULL,
  `accessdel` bit(1) DEFAULT NULL,
  `accessprint` bit(1) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `traksesmenu`
--

INSERT INTO `traksesmenu` (`aksesmenuid`, `aksesid`, `menuid`, `accessview`, `accessadd`, `accessedit`, `accessdel`, `accessprint`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('00000000000000060001', '6', '1', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060002', '6', '2', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060003', '6', '3', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060005', '6', '5', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060006', '6', '6', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060007', '6', '7', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060008', '6', '8', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060009', '6', '9', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060101', '6', '101', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060102', '6', '102', b'1', b'1', b'1', b'1', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060103', '6', '103', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060104', '6', '104', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060105', '6', '105', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060106', '6', '106', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060107', '6', '107', b'1', b'1', b'1', b'1', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060108', '6', '108', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060201', '6', '201', b'1', b'0', b'0', b'0', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060301', '6', '301', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060302', '6', '302', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060303', '6', '303', b'1', b'0', b'1', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000060304', '6', '304', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070001', '7', '1', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070002', '7', '2', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070003', '7', '3', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070005', '7', '5', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070006', '7', '6', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070007', '7', '7', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070008', '7', '8', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070009', '7', '9', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070101', '7', '101', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070102', '7', '102', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070103', '7', '103', b'1', b'1', b'1', b'1', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070104', '7', '104', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070105', '7', '105', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070106', '7', '106', b'1', b'1', b'1', b'1', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070107', '7', '107', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070108', '7', '108', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070201', '7', '201', b'1', b'0', b'0', b'0', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070301', '7', '301', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070302', '7', '302', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070303', '7', '303', b'1', b'0', b'1', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000070304', '7', '304', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100001', '10', '1', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100002', '10', '2', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100003', '10', '3', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100005', '10', '5', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100006', '10', '6', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100007', '10', '7', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100008', '10', '8', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100009', '10', '9', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100101', '10', '101', b'1', b'1', b'1', b'1', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100102', '10', '102', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100103', '10', '103', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100104', '10', '104', b'1', b'1', b'1', b'1', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100105', '10', '105', b'1', b'1', b'1', b'1', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100106', '10', '106', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100107', '10', '107', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100108', '10', '108', b'1', b'1', b'1', b'1', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100201', '10', '201', b'1', b'0', b'0', b'0', b'1', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100301', '10', '301', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100302', '10', '302', b'1', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100303', '10', '303', b'1', b'0', b'1', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0'),
('00000000000000100304', '10', '304', b'0', b'0', b'0', b'0', b'0', 'admin', '2018-11-03 00:00:00', 'admin', 'admin', '2018-11-03 00:00:00', 'admin', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `traksesmodul`
--

CREATE TABLE `traksesmodul` (
  `aksesmodulid` varchar(20) NOT NULL,
  `aksesid` varchar(20) DEFAULT NULL,
  `modulid` varchar(20) DEFAULT NULL,
  `accessview` bit(1) DEFAULT NULL,
  `accessadd` bit(1) DEFAULT NULL,
  `accessedit` bit(1) DEFAULT NULL,
  `accessdel` bit(1) DEFAULT NULL,
  `accessprint` bit(1) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tsberita`
--

CREATE TABLE `tsberita` (
  `beritaid` varchar(20) NOT NULL,
  `judul` varchar(150) DEFAULT NULL,
  `keterangan` text,
  `tglberita` datetime DEFAULT NULL,
  `flag` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `img` varchar(30) DEFAULT NULL,
  `jenis` smallint(6) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tsberita`
--

INSERT INTO `tsberita` (`beritaid`, `judul`, `keterangan`, `tglberita`, `flag`, `status`, `img`, `jenis`, `url`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('1', 'Menkes: Persiapkan Riskesdas 2018 Secara Matang', '-', '2018-01-29 00:00:00', 1, 1, 'slide_101.jpg', 2, 'http://www.depkes.go.id/article/view/18013000002/menkes-persiapkan-riskesdas-2018-secara-matang.html', 'admin', '2018-09-18 00:00:00', 'pc', 'admin', '2018-09-18 00:00:00', 'pc', b'0'),
('2', 'Pembiayaan Mikro Jadi Solusi Mudah Permodalan Nelayan', '', '2018-06-06 00:00:00', 1, 1, 'slide_102.jpg', 2, 'http://www.depkes.go.id/article/view/18060700001/pembiayaan-mikro-jadi-solusi-mudah-permodalan-nelayan.html', 'admin', '2018-09-18 00:00:00', 'pc', 'admin', '2018-09-18 00:00:00', 'pc', b'0'),
('3', 'Rakerkesnas 2018, Kemenkes Percepat Atasi 3 Masalah Kesehatan', '-', '2018-03-05 00:00:00', 1, 1, 'slide_103.jpg', 2, 'http://www.depkes.go.id/article/view/18030700005/rakerkesnas-2018-kemenkes-percepat-atasi-3-masalah-kesehatan.html', 'admin', '2018-09-18 00:00:00', 'pc', 'admin', '2018-09-18 00:00:00', 'pc', b'0'),
('4', 'Indonesia Jadi Center of Excelent: Momentum Baru Bagi Negara-negara Islam dalam Pengembangan Vaksin dan Produk Bioteknologi', '-', '2018-05-14 00:00:00', 1, 1, 'slide_104.jpg', 2, 'http://www.depkes.go.id/article/view/18051500002/indonesia-jadi-center-of-excelent-momentum-baru-bagi-negara-negara-islam-dalam-pengembangan-vaksin-d.html', 'admin', '2018-09-18 00:00:00', 'pc', 'admin', '2018-09-18 00:00:00', 'pc', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tsemailverifikasi`
--

CREATE TABLE `tsemailverifikasi` (
  `emailverifikasiid` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `tglgenerate` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tsnotifikasi`
--

CREATE TABLE `tsnotifikasi` (
  `notifikasiid` varchar(20) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `tglnotifikasi` datetime DEFAULT NULL,
  `controller` varchar(30) DEFAULT NULL,
  `action` varchar(30) DEFAULT NULL,
  `primarykey` varchar(20) DEFAULT NULL,
  `transaksiid` varchar(20) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL,
  `jenistransaksi` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tsnotifikasiuser`
--

CREATE TABLE `tsnotifikasiuser` (
  `notifikasiuserid` varchar(20) NOT NULL,
  `notifikasiid` varchar(20) DEFAULT NULL,
  `userid` varchar(20) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tspengumuman`
--

CREATE TABLE `tspengumuman` (
  `pengumumanid` varchar(20) NOT NULL,
  `pengirim` varchar(150) DEFAULT NULL,
  `judul` varchar(150) DEFAULT NULL,
  `keterangan` text,
  `tglpengumuman` datetime DEFAULT NULL,
  `flag` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `img` varchar(30) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tspengumuman`
--

INSERT INTO `tspengumuman` (`pengumumanid`, `pengirim`, `judul`, `keterangan`, `tglpengumuman`, `flag`, `status`, `img`, `opadd`, `tgladd`, `pcadd`, `opedit`, `tgledit`, `pcedit`, `dlt`) VALUES
('1', 'Kemenkes RI', 'Test Sistem', 'Kepada seluruh Staf untuk melakukan test Sistem', '2018-09-18 00:00:00', 1, 1, 'slide_11.jpg', 'admin', '2018-09-18 00:00:00', 'pc', 'admin', '2018-09-18 00:00:00', 'pc', b'0'),
('1E5044CF679DCA0D4C24', 'Kemenkes RI', 'Pengumuman', 'Pengumuman pengumuman pengumuman pengumuman pengumuman pengumuman pengumuman pengumuman', '2018-11-07 00:00:00', 1, 1, NULL, 'admin', '2018-11-06 21:58:35', '127.0.0.1', 'admin', '2018-11-06 21:58:35', '127.0.0.1', b'0'),
('2', 'Kemenkes', 'Penginputan Sistem', 'Diharapkan kepada seluruh Staf untuk melakukan pengarsipan surat melalui sistem. Ok..', '2018-09-18 00:00:00', 1, 1, 'slide_12.jpg', 'admin', '2018-09-18 00:00:00', 'pc', 'admin', '2018-11-05 00:54:22', '127.0.0.1', b'0'),
('6564D681362360C17E8A', 'Kemenkes', 'Test', 'Test Test Test Test Test Test Test Test Test Test Test Test ', '2018-11-05 00:00:00', 1, 1, NULL, 'admin', '2018-11-05 00:55:31', '127.0.0.1', 'admin', '2018-11-05 01:00:55', '127.0.0.1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tspesan`
--

CREATE TABLE `tspesan` (
  `pesanid` varchar(20) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `tglpesan` datetime DEFAULT NULL,
  `flag` smallint(6) DEFAULT NULL,
  `controller` varchar(30) DEFAULT NULL,
  `action` varchar(30) DEFAULT NULL,
  `transaksiid` varchar(20) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tspesanuser`
--

CREATE TABLE `tspesanuser` (
  `pesanuserid` varchar(20) NOT NULL,
  `pesanid` varchar(20) DEFAULT NULL,
  `userid` varchar(20) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratkeluar`
--

CREATE TABLE `ttsuratkeluar` (
  `suratkeluarid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `subunitid` varchar(20) DEFAULT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `subbagianid` varchar(20) DEFAULT NULL,
  `pejabatid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `indexkode1` varchar(20) DEFAULT NULL,
  `indexkode2` varchar(20) DEFAULT NULL,
  `indexbulan` varchar(20) DEFAULT NULL,
  `indexnomor` varchar(20) DEFAULT NULL,
  `indextahun` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `klasifikasi` varchar(20) DEFAULT NULL,
  `tglpenyerahan` date DEFAULT NULL,
  `nosurat` varchar(100) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `kepada` varchar(254) DEFAULT NULL,
  `perihal` varchar(254) DEFAULT NULL,
  `kirimvia` varchar(100) DEFAULT NULL,
  `tglkirim` date DEFAULT NULL,
  `jamkirim` varchar(20) DEFAULT NULL,
  `paraf` bit(1) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratkeluardetail`
--

CREATE TABLE `ttsuratkeluardetail` (
  `suratkeluardetailid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `jenisid` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `indekkode1` varchar(20) DEFAULT NULL,
  `indekkode2` varchar(20) DEFAULT NULL,
  `indeknomor` varchar(20) DEFAULT NULL,
  `indekbulan` varchar(20) DEFAULT NULL,
  `indektahun` varchar(20) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `klasifikasi` varchar(20) DEFAULT NULL,
  `perihal` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `paraf` bit(1) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratkeluarfiles`
--

CREATE TABLE `ttsuratkeluarfiles` (
  `suratkeluarfilesid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `files` varchar(254) DEFAULT NULL,
  `filename` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratkeluarhistory`
--

CREATE TABLE `ttsuratkeluarhistory` (
  `suratkeluarhistoryid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `tglverifikasi` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `catatan` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratkeluarjabatan`
--

CREATE TABLE `ttsuratkeluarjabatan` (
  `suratkeluarjabatanid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `jabatanid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratkeluarlampiran`
--

CREATE TABLE `ttsuratkeluarlampiran` (
  `suratkeluarlampiranid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `nourut` int(11) DEFAULT NULL,
  `lampiran` varchar(150) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratkeluarpegawai`
--

CREATE TABLE `ttsuratkeluarpegawai` (
  `suratkeluarpegawaiid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `catatan` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratkeluartembusan`
--

CREATE TABLE `ttsuratkeluartembusan` (
  `suratkeluartembusanid` varchar(20) NOT NULL,
  `suratkeluarid` varchar(20) DEFAULT NULL,
  `nourut` int(11) DEFAULT NULL,
  `tembusan` varchar(150) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratmasuk`
--

CREATE TABLE `ttsuratmasuk` (
  `suratmasukid` varchar(20) NOT NULL,
  `unitid` varchar(20) DEFAULT NULL,
  `subunitid` varchar(20) DEFAULT NULL,
  `bagianid` varchar(20) DEFAULT NULL,
  `subbagianid` varchar(20) DEFAULT NULL,
  `pejabatid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `jenisid` varchar(20) DEFAULT NULL,
  `indexkode1` varchar(20) DEFAULT NULL,
  `indexkode2` varchar(20) DEFAULT NULL,
  `indexbulan` varchar(20) DEFAULT NULL,
  `indexnomor` varchar(20) DEFAULT NULL,
  `indextahun` varchar(20) DEFAULT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `klasifikasi` varchar(20) DEFAULT NULL,
  `tglpenyerahan` date DEFAULT NULL,
  `nosurat` varchar(100) DEFAULT NULL,
  `tglsurat` date DEFAULT NULL,
  `asal` varchar(254) DEFAULT NULL,
  `perihal` varchar(254) DEFAULT NULL,
  `tglpelaksanaan` date DEFAULT NULL,
  `catatanpelaksanaan` varchar(254) DEFAULT NULL,
  `kembalipada` varchar(100) DEFAULT NULL,
  `tglkembali` date DEFAULT NULL,
  `tglterima` date DEFAULT NULL,
  `jamkembali` varchar(20) DEFAULT NULL,
  `jamterima` varchar(20) DEFAULT NULL,
  `paraf` bit(1) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratmasukfiles`
--

CREATE TABLE `ttsuratmasukfiles` (
  `suratmasukfilesid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `files` varchar(254) DEFAULT NULL,
  `filename` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratmasukhistory`
--

CREATE TABLE `ttsuratmasukhistory` (
  `suratmasukhistoryid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `tglverifikasi` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `catatan` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratmasukjabatan`
--

CREATE TABLE `ttsuratmasukjabatan` (
  `suratmasukjabatanid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `jabatanid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratmasukpegawai`
--

CREATE TABLE `ttsuratmasukpegawai` (
  `suratmasukpegawaiid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `pegawaiid` varchar(20) DEFAULT NULL,
  `disposisi` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `statusbaca` bit(1) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ttsuratmasukperintah`
--

CREATE TABLE `ttsuratmasukperintah` (
  `suratmasukperintahid` varchar(20) NOT NULL,
  `suratmasukid` varchar(20) DEFAULT NULL,
  `perintahid` varchar(20) DEFAULT NULL,
  `nourut` smallint(6) DEFAULT NULL,
  `perintah` varchar(254) DEFAULT NULL,
  `keterangan` varchar(254) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `opadd` varchar(40) DEFAULT NULL,
  `tgladd` datetime DEFAULT NULL,
  `pcadd` varchar(40) DEFAULT NULL,
  `opedit` varchar(40) DEFAULT NULL,
  `tgledit` datetime DEFAULT NULL,
  `pcedit` varchar(40) DEFAULT NULL,
  `dlt` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `password`) VALUES
(1, 'admin', '123456', '$2y$13$9m68xruYj3Rxq3MJ/KEL5ezE9RaSwA9Dq6baFSa2cUeKDXBsxRC6q', '123456', 'kuclukb@gmail.com', 1, 0, 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `tmakses`
--
ALTER TABLE `tmakses`
  ADD PRIMARY KEY (`aksesid`);

--
-- Indexes for table `tmbagian`
--
ALTER TABLE `tmbagian`
  ADD PRIMARY KEY (`bagianid`),
  ADD KEY `fk_tmbagian_reference_tmunit` (`unitid`);

--
-- Indexes for table `tminstansi`
--
ALTER TABLE `tminstansi`
  ADD PRIMARY KEY (`instansiid`);

--
-- Indexes for table `tmjabatan`
--
ALTER TABLE `tmjabatan`
  ADD PRIMARY KEY (`jabatanid`),
  ADD KEY `fk_tmjabatan_reference_tmunit` (`unitid`),
  ADD KEY `fk_tmjabatan_reference_tmsubunit` (`subunitid`),
  ADD KEY `fk_tmjabatan_reference_tmbagian` (`bagianid`),
  ADD KEY `fk_tmjabatan_reference_tmsubbagian` (`subbagianid`);

--
-- Indexes for table `tmjenis`
--
ALTER TABLE `tmjenis`
  ADD PRIMARY KEY (`jenisid`);

--
-- Indexes for table `tmmenu`
--
ALTER TABLE `tmmenu`
  ADD PRIMARY KEY (`menuid`),
  ADD KEY `tmmenu_modulid_fkey` (`modulid`);

--
-- Indexes for table `tmmodul`
--
ALTER TABLE `tmmodul`
  ADD PRIMARY KEY (`modulid`);

--
-- Indexes for table `tmpegawai`
--
ALTER TABLE `tmpegawai`
  ADD PRIMARY KEY (`pegawaiid`);

--
-- Indexes for table `tmpegawaijabatan`
--
ALTER TABLE `tmpegawaijabatan`
  ADD PRIMARY KEY (`pegawaijabatanid`),
  ADD KEY `fk_tmpegawa_reference_tmpegawa` (`pegawaiid`);

--
-- Indexes for table `tmperintah`
--
ALTER TABLE `tmperintah`
  ADD PRIMARY KEY (`perintahid`);

--
-- Indexes for table `tmsubbagian`
--
ALTER TABLE `tmsubbagian`
  ADD PRIMARY KEY (`subbagianid`),
  ADD KEY `fk_tmsubbag_reference_tmbagian` (`bagianid`);

--
-- Indexes for table `tmsubunit`
--
ALTER TABLE `tmsubunit`
  ADD PRIMARY KEY (`subunitid`),
  ADD KEY `fk_tmsubuni_reference_tmunit` (`unitid`);

--
-- Indexes for table `tmtahun`
--
ALTER TABLE `tmtahun`
  ADD PRIMARY KEY (`tahunid`);

--
-- Indexes for table `tmunit`
--
ALTER TABLE `tmunit`
  ADD PRIMARY KEY (`unitid`);

--
-- Indexes for table `tmuser`
--
ALTER TABLE `tmuser`
  ADD PRIMARY KEY (`userid`),
  ADD KEY `fk_tmakses_reference_tmuser` (`aksesid`),
  ADD KEY `fk_tmuser_reference_tmpegawa` (`pegawaiid`);

--
-- Indexes for table `traksesmenu`
--
ALTER TABLE `traksesmenu`
  ADD PRIMARY KEY (`aksesmenuid`),
  ADD KEY `fk_tmakses_reference_traksesm` (`aksesid`),
  ADD KEY `fk_tmmenu_reference_traksesm` (`menuid`);

--
-- Indexes for table `traksesmodul`
--
ALTER TABLE `traksesmodul`
  ADD PRIMARY KEY (`aksesmodulid`),
  ADD KEY `fk_tmmodul_reference_traksesm` (`modulid`);

--
-- Indexes for table `tsberita`
--
ALTER TABLE `tsberita`
  ADD PRIMARY KEY (`beritaid`);

--
-- Indexes for table `tsemailverifikasi`
--
ALTER TABLE `tsemailverifikasi`
  ADD PRIMARY KEY (`emailverifikasiid`);

--
-- Indexes for table `tsnotifikasi`
--
ALTER TABLE `tsnotifikasi`
  ADD PRIMARY KEY (`notifikasiid`);

--
-- Indexes for table `tsnotifikasiuser`
--
ALTER TABLE `tsnotifikasiuser`
  ADD PRIMARY KEY (`notifikasiuserid`),
  ADD KEY `fk_tsnotifi_reference_tmuser` (`userid`),
  ADD KEY `fk_tsnotifi_reference_tsnotifi` (`notifikasiid`);

--
-- Indexes for table `tspengumuman`
--
ALTER TABLE `tspengumuman`
  ADD PRIMARY KEY (`pengumumanid`);

--
-- Indexes for table `tspesan`
--
ALTER TABLE `tspesan`
  ADD PRIMARY KEY (`pesanid`);

--
-- Indexes for table `tspesanuser`
--
ALTER TABLE `tspesanuser`
  ADD PRIMARY KEY (`pesanuserid`),
  ADD KEY `fk_tspesanu_reference_tmuser` (`userid`),
  ADD KEY `fk_tspesanu_reference_tspesan` (`pesanid`);

--
-- Indexes for table `ttsuratkeluar`
--
ALTER TABLE `ttsuratkeluar`
  ADD PRIMARY KEY (`suratkeluarid`),
  ADD KEY `fk_ttsuratkeluar_reference_tmunit` (`unitid`),
  ADD KEY `fk_ttsuratkeluar_reference_tmsubunit` (`subunitid`),
  ADD KEY `fk_ttsuratkeluar_reference_tmbagian` (`bagianid`),
  ADD KEY `fk_ttsuratkeluar_reference_tmsubbagian` (`subbagianid`);

--
-- Indexes for table `ttsuratkeluardetail`
--
ALTER TABLE `ttsuratkeluardetail`
  ADD PRIMARY KEY (`suratkeluardetailid`),
  ADD KEY `fk_ttsuratkeluardetail_reference_ttsuratkeluar` (`suratkeluarid`),
  ADD KEY `fk_ttsuratkeluardetail_reference_tmjenis` (`jenisid`);

--
-- Indexes for table `ttsuratkeluarfiles`
--
ALTER TABLE `ttsuratkeluarfiles`
  ADD PRIMARY KEY (`suratkeluarfilesid`),
  ADD KEY `fk_ttsuratkeluarfiles_reference_ttsuratkeluar` (`suratkeluarid`);

--
-- Indexes for table `ttsuratkeluarhistory`
--
ALTER TABLE `ttsuratkeluarhistory`
  ADD PRIMARY KEY (`suratkeluarhistoryid`),
  ADD KEY `fk_ttsuratkeluarhistory_reference_ttsuratkeluar` (`suratkeluarid`),
  ADD KEY `fk_ttsuratkeluarpegawai_reference_tmpegawai` (`pegawaiid`);

--
-- Indexes for table `ttsuratkeluarjabatan`
--
ALTER TABLE `ttsuratkeluarjabatan`
  ADD PRIMARY KEY (`suratkeluarjabatanid`),
  ADD KEY `fk_ttsuratkeluarjabatan_reference_ttsuratkeluar` (`suratkeluarid`),
  ADD KEY `fk_ttsuratkeluarjabatan_reference_tmjabatan` (`jabatanid`);

--
-- Indexes for table `ttsuratkeluarlampiran`
--
ALTER TABLE `ttsuratkeluarlampiran`
  ADD PRIMARY KEY (`suratkeluarlampiranid`),
  ADD KEY `fk_ttsuratkeluarlampiran_reference_ttsuratkeluar` (`suratkeluarid`);

--
-- Indexes for table `ttsuratkeluarpegawai`
--
ALTER TABLE `ttsuratkeluarpegawai`
  ADD PRIMARY KEY (`suratkeluarpegawaiid`),
  ADD KEY `fk_ttsuratkeluarpegawai_reference_ttsuratkeluar` (`suratkeluarid`),
  ADD KEY `fk_ttsuratkeluarpegawai_reference_tmpegawai` (`pegawaiid`);

--
-- Indexes for table `ttsuratkeluartembusan`
--
ALTER TABLE `ttsuratkeluartembusan`
  ADD PRIMARY KEY (`suratkeluartembusanid`),
  ADD KEY `fk_ttsuratkeluartembusan_reference_ttsuratkeluar` (`suratkeluarid`);

--
-- Indexes for table `ttsuratmasuk`
--
ALTER TABLE `ttsuratmasuk`
  ADD PRIMARY KEY (`suratmasukid`),
  ADD KEY `fk_ttsuratmasuk_reference_tmunit` (`unitid`),
  ADD KEY `fk_ttsuratmasuk_reference_tmsubunit` (`subunitid`),
  ADD KEY `fk_ttsuratmasuk_reference_tmbagian` (`bagianid`),
  ADD KEY `fk_ttsuratmasuk_reference_tmsubbagian` (`subbagianid`),
  ADD KEY `fk_ttsuratmasuk_reference_tmjenis` (`jenisid`);

--
-- Indexes for table `ttsuratmasukfiles`
--
ALTER TABLE `ttsuratmasukfiles`
  ADD PRIMARY KEY (`suratmasukfilesid`),
  ADD KEY `fk_ttsuratmasukfiles_reference_ttsuratmasuk` (`suratmasukid`);

--
-- Indexes for table `ttsuratmasukhistory`
--
ALTER TABLE `ttsuratmasukhistory`
  ADD PRIMARY KEY (`suratmasukhistoryid`),
  ADD KEY `fk_ttsuratmasukhistory_reference_ttsuratmasuk` (`suratmasukid`),
  ADD KEY `fk_ttsuratmasukpegawai_reference_tmpegawai` (`pegawaiid`);

--
-- Indexes for table `ttsuratmasukjabatan`
--
ALTER TABLE `ttsuratmasukjabatan`
  ADD PRIMARY KEY (`suratmasukjabatanid`),
  ADD KEY `fk_ttsuratmasukjabatan_reference_ttsuratmasuk` (`suratmasukid`),
  ADD KEY `fk_ttsuratmasukjabatan_reference_tmjabatan` (`jabatanid`);

--
-- Indexes for table `ttsuratmasukpegawai`
--
ALTER TABLE `ttsuratmasukpegawai`
  ADD PRIMARY KEY (`suratmasukpegawaiid`),
  ADD KEY `fk_ttsuratmasukpegawai_reference_ttsuratmasuk` (`suratmasukid`),
  ADD KEY `fk_ttsuratmasukpegawai_reference_tmpegawai` (`pegawaiid`);

--
-- Indexes for table `ttsuratmasukperintah`
--
ALTER TABLE `ttsuratmasukperintah`
  ADD PRIMARY KEY (`suratmasukperintahid`),
  ADD KEY `fk_ttsuratmasukperintah_reference_ttsuratmasuk` (`suratmasukid`),
  ADD KEY `fk_ttsuratmasukperintah_reference_tmperintah` (`perintahid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tmbagian`
--
ALTER TABLE `tmbagian`
  ADD CONSTRAINT `fk_tmbagian_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`);

--
-- Constraints for table `tmjabatan`
--
ALTER TABLE `tmjabatan`
  ADD CONSTRAINT `fk_tmjabatan_reference_tmbagian` FOREIGN KEY (`bagianid`) REFERENCES `tmbagian` (`bagianid`),
  ADD CONSTRAINT `fk_tmjabatan_reference_tmsubbagian` FOREIGN KEY (`subbagianid`) REFERENCES `tmsubbagian` (`subbagianid`),
  ADD CONSTRAINT `fk_tmjabatan_reference_tmsubunit` FOREIGN KEY (`subunitid`) REFERENCES `tmsubunit` (`subunitid`),
  ADD CONSTRAINT `fk_tmjabatan_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`);

--
-- Constraints for table `tmmenu`
--
ALTER TABLE `tmmenu`
  ADD CONSTRAINT `tmmenu_modulid_fkey` FOREIGN KEY (`modulid`) REFERENCES `tmmodul` (`modulid`);

--
-- Constraints for table `tmpegawaijabatan`
--
ALTER TABLE `tmpegawaijabatan`
  ADD CONSTRAINT `fk_tmpegawa_reference_tmpegawa` FOREIGN KEY (`pegawaiid`) REFERENCES `tmpegawai` (`pegawaiid`);

--
-- Constraints for table `tmsubbagian`
--
ALTER TABLE `tmsubbagian`
  ADD CONSTRAINT `fk_tmsubbag_reference_tmbagian` FOREIGN KEY (`bagianid`) REFERENCES `tmbagian` (`bagianid`);

--
-- Constraints for table `tmsubunit`
--
ALTER TABLE `tmsubunit`
  ADD CONSTRAINT `fk_tmsubuni_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`);

--
-- Constraints for table `tmuser`
--
ALTER TABLE `tmuser`
  ADD CONSTRAINT `fk_tmakses_reference_tmuser` FOREIGN KEY (`aksesid`) REFERENCES `tmakses` (`aksesid`),
  ADD CONSTRAINT `fk_tmuser_reference_tmpegawa` FOREIGN KEY (`pegawaiid`) REFERENCES `tmpegawai` (`pegawaiid`);

--
-- Constraints for table `traksesmenu`
--
ALTER TABLE `traksesmenu`
  ADD CONSTRAINT `fk_tmakses_reference_traksesm` FOREIGN KEY (`aksesid`) REFERENCES `tmakses` (`aksesid`),
  ADD CONSTRAINT `fk_tmmenu_reference_traksesm` FOREIGN KEY (`menuid`) REFERENCES `tmmenu` (`menuid`);

--
-- Constraints for table `traksesmodul`
--
ALTER TABLE `traksesmodul`
  ADD CONSTRAINT `fk_tmmodul_reference_traksesm` FOREIGN KEY (`modulid`) REFERENCES `tmmodul` (`modulid`);

--
-- Constraints for table `tsnotifikasiuser`
--
ALTER TABLE `tsnotifikasiuser`
  ADD CONSTRAINT `fk_tsnotifi_reference_tmuser` FOREIGN KEY (`userid`) REFERENCES `tmuser` (`userid`),
  ADD CONSTRAINT `fk_tsnotifi_reference_tsnotifi` FOREIGN KEY (`notifikasiid`) REFERENCES `tsnotifikasi` (`notifikasiid`);

--
-- Constraints for table `tspesanuser`
--
ALTER TABLE `tspesanuser`
  ADD CONSTRAINT `fk_tspesanu_reference_tmuser` FOREIGN KEY (`userid`) REFERENCES `tmuser` (`userid`),
  ADD CONSTRAINT `fk_tspesanu_reference_tspesan` FOREIGN KEY (`pesanid`) REFERENCES `tspesan` (`pesanid`);

--
-- Constraints for table `ttsuratkeluar`
--
ALTER TABLE `ttsuratkeluar`
  ADD CONSTRAINT `fk_ttsuratkeluar_reference_tmbagian` FOREIGN KEY (`bagianid`) REFERENCES `tmbagian` (`bagianid`),
  ADD CONSTRAINT `fk_ttsuratkeluar_reference_tmsubbagian` FOREIGN KEY (`subbagianid`) REFERENCES `tmsubbagian` (`subbagianid`),
  ADD CONSTRAINT `fk_ttsuratkeluar_reference_tmsubunit` FOREIGN KEY (`subunitid`) REFERENCES `tmsubunit` (`subunitid`),
  ADD CONSTRAINT `fk_ttsuratkeluar_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`);

--
-- Constraints for table `ttsuratkeluardetail`
--
ALTER TABLE `ttsuratkeluardetail`
  ADD CONSTRAINT `fk_ttsuratkeluardetail_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`);

--
-- Constraints for table `ttsuratkeluarfiles`
--
ALTER TABLE `ttsuratkeluarfiles`
  ADD CONSTRAINT `fk_ttsuratkeluarfiles_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`);

--
-- Constraints for table `ttsuratkeluarjabatan`
--
ALTER TABLE `ttsuratkeluarjabatan`
  ADD CONSTRAINT `fk_ttsuratkeluarjabatan_reference_tmjabatan` FOREIGN KEY (`jabatanid`) REFERENCES `tmjabatan` (`jabatanid`),
  ADD CONSTRAINT `fk_ttsuratkeluarjabatan_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`);

--
-- Constraints for table `ttsuratkeluarlampiran`
--
ALTER TABLE `ttsuratkeluarlampiran`
  ADD CONSTRAINT `fk_ttsuratkeluarlampiran_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`);

--
-- Constraints for table `ttsuratkeluarpegawai`
--
ALTER TABLE `ttsuratkeluarpegawai`
  ADD CONSTRAINT `fk_ttsuratkeluarpegawai_reference_tmpegawai` FOREIGN KEY (`pegawaiid`) REFERENCES `tmpegawai` (`pegawaiid`),
  ADD CONSTRAINT `fk_ttsuratkeluarpegawai_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`);

--
-- Constraints for table `ttsuratkeluartembusan`
--
ALTER TABLE `ttsuratkeluartembusan`
  ADD CONSTRAINT `fk_ttsuratkeluartembusan_reference_ttsuratkeluar` FOREIGN KEY (`suratkeluarid`) REFERENCES `ttsuratkeluar` (`suratkeluarid`);

--
-- Constraints for table `ttsuratmasuk`
--
ALTER TABLE `ttsuratmasuk`
  ADD CONSTRAINT `fk_ttsuratmasuk_reference_tmbagian` FOREIGN KEY (`bagianid`) REFERENCES `tmbagian` (`bagianid`),
  ADD CONSTRAINT `fk_ttsuratmasuk_reference_tmsubbagian` FOREIGN KEY (`subbagianid`) REFERENCES `tmsubbagian` (`subbagianid`),
  ADD CONSTRAINT `fk_ttsuratmasuk_reference_tmsubunit` FOREIGN KEY (`subunitid`) REFERENCES `tmsubunit` (`subunitid`),
  ADD CONSTRAINT `fk_ttsuratmasuk_reference_tmunit` FOREIGN KEY (`unitid`) REFERENCES `tmunit` (`unitid`);

--
-- Constraints for table `ttsuratmasukfiles`
--
ALTER TABLE `ttsuratmasukfiles`
  ADD CONSTRAINT `fk_ttsuratmasukfiles_reference_ttsuratmasuk` FOREIGN KEY (`suratmasukid`) REFERENCES `ttsuratmasuk` (`suratmasukid`);

--
-- Constraints for table `ttsuratmasukjabatan`
--
ALTER TABLE `ttsuratmasukjabatan`
  ADD CONSTRAINT `fk_ttsuratmasukjabatan_reference_tmjabatan` FOREIGN KEY (`jabatanid`) REFERENCES `tmjabatan` (`jabatanid`),
  ADD CONSTRAINT `fk_ttsuratmasukjabatan_reference_ttsuratmasuk` FOREIGN KEY (`suratmasukid`) REFERENCES `ttsuratmasuk` (`suratmasukid`);

--
-- Constraints for table `ttsuratmasukpegawai`
--
ALTER TABLE `ttsuratmasukpegawai`
  ADD CONSTRAINT `fk_ttsuratmasukpegawai_reference_tmpegawai` FOREIGN KEY (`pegawaiid`) REFERENCES `tmpegawai` (`pegawaiid`),
  ADD CONSTRAINT `fk_ttsuratmasukpegawai_reference_ttsuratmasuk` FOREIGN KEY (`suratmasukid`) REFERENCES `ttsuratmasuk` (`suratmasukid`);

--
-- Constraints for table `ttsuratmasukperintah`
--
ALTER TABLE `ttsuratmasukperintah`
  ADD CONSTRAINT `fk_ttsuratmasukperintah_reference_tmperintah` FOREIGN KEY (`perintahid`) REFERENCES `tmperintah` (`perintahid`),
  ADD CONSTRAINT `fk_ttsuratmasukperintah_reference_ttsuratmasuk` FOREIGN KEY (`suratmasukid`) REFERENCES `ttsuratmasuk` (`suratmasukid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
