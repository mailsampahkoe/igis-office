<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php require_once('tpl_navigation.php') ?>	

<div class="page-wrapper">
    <div class="container-fluid">
        <?php echo $content; ?>
	</div>
</div>

<?php $this->endContent(); ?>