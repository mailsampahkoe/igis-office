<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<section id="navigation-main">   
    <!-- Require the navigation -->
    <?php require_once('tpl_navigation.php') ?>
</section>
<!-- /#navigation-main -->
<!-- Include content pages -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>
        <!-- Include content pages -->
        <?php echo $content; ?>
    </section><!--/row-->
</section>><!--/row-->

<?php $this->endContent(); ?>