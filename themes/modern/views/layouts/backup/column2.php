<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php require_once('tpl_navigation.php') ?>	

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h1>Dashboard</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo $this->breadcrumbs[1]; ?>"><?php echo $this->breadcrumbs[0]; ?></a></li>
        <li><i class="fa fa-angle-right"></i> <?php echo $this->breadcrumbs[1]; ?></li>
      </ol>
    </div>
    
    <!-- Main content -->
    <div class="content">
        <?php echo $content; ?>
    </div>
    <!-- /.content --> 
  </div>
  <!-- /.content-wrapper -->

<?php $this->endContent(); ?>