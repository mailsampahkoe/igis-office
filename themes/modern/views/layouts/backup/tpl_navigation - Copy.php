<?php
$baseUrl = Yii::app()->theme->baseUrl;
?>
   <!-- SIDEBAR - START -->
        <div class="side-mini-panel">
            <ul class="mini-nav">
                <div class="togglediv"><a href="javascript:void(0)" id="togglebtn"><i class="ti-menu"></i></a></div>
                <!-- .Dashboard -->
                <li class="selected">
                    <a href="javascript:void(0)"><i class="icon-speedometer"></i></a>
                    <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                        <h3 class="menu-title">Menu Utama</h3>
                        <ul class="sidebar-menu">
                            <li><a href="<?php echo Yii::app()->createUrl('site'); ?>">Home </a></li>
							<?php  if(!Yii::app()->user->isGuest) { ?>
	                            <li><a href="<?php echo Yii::app()->createUrl('dashboard'); ?>">Dashboard </a></li>
			                <?php } ?>
							<?php  if(Yii::app()->user->isGuest) { ?>
								<li><a class="waves-effect waves-dark" href="<?php echo Yii::app()->createUrl('site/login'); ?>"></i>Login</a></li>
								<li><a class="waves-effect waves-dark" href="<?php echo Yii::app()->createUrl('site/recoverpass'); ?>"></i>Lupa Password</a></li>
			                <?php } ?>
							<?php  if(!Yii::app()->user->isGuest) { ?>
								<li><a href="<?php echo Yii::app()->createUrl('site/logout'); ?>"></i>Logout</a></li>
			                <?php } ?>
                        </ul>
                        <!-- Left navbar-header end -->
                    </div>
                </li>
                <!-- /.Dashboard -->
				<?php
					$iconmenu = array (
						"Master" =>  "ti-layout-grid2",
						"Transaksi" =>  "ti-email",
						"Laporan" =>  "ti-layout-media-right-alt",
						"Utilitas" =>  "ti-layout-accordion-merged",
					);
					
					$join = '';
					$filter = '';
					if (!Yii::app()->user->isSuperadmin()) {
						$filter = " and D.userid='".Yii::app()->user->getuser('id')."' AND B.view='1' ";
						$join = "left join tbraksesmenu B ON B.menuid=A.menuid  and B.dlt = '0'
						left join tbmakses C ON C.aksesid=B.aksesid and C.dlt = '0' 
						left join tbmuser D ON D.aksesid=C.aksesid and D.dlt = '0'";
					}
					$sql = "SELECT A.parent,A.menu,A.url, case a.jenis when 1 then 'Master' when 2 then 'Transaksi' when 3 then 'Laporan' when 4 then 'Utilitas' end as jenisvw FROM tbmmenu A 
						$join
						WHERE a.jenis<>'0'  and a.ishide='0' 
						$filter
						GROUP BY A.parent,A.menu,A.url,a.jenis,A.urutan ORDER BY a.jenis::integer, A.urutan::integer";
						//echo $sql;
					$rows = Yii::app()->db->createCommand($sql)->queryAll();
		            $parent = "";
		            $subparent = "";
					$countitem = 0;
					$headeritem = "";
		            $listitem = "";
		            foreach ($rows as $row) {
						if ($parent=="" || $parent!=$row["jenisvw"]){
							if ($subparent!="") {
								if ($countitem<=1){
									$lst = str_replace("a href", 'a class="waves-effect waves-dark" href', $listitem);
									$icn = "";
									if (isset($iconmenu[$subparent]) && !is_null($iconmenu[$subparent])) {
										$icn = '<i class="'.$iconmenu[$subparent].'"></i>';
										$lst = str_replace('">','">'.$icn,$lst);
									}
									echo $lst; 
								}else{
									echo $headeritem.$listitem.'</ul></li>';
								}
								$countitem = 0;
								$listitem = "";
							}
							if ($parent != "") {
								echo '</ul></div></li>';
							}
							echo '
							<li><a href="javascript:void(0)"><i class="'.$iconmenu[$row["jenisvw"]].'"></i></a>
                    <div class="sidebarmenu">
                        <!-- Left navbar-header -->
                        <h3 class="menu-title">'.$row["jenisvw"].'</h3>
                        <ul class="sidebar-menu">
							';
						}
						if ($subparent=="" || $subparent!=$row["parent"]){
							if ($subparent!="") {
								if ($countitem<=1){
									$lst = str_replace("a href", 'a class="waves-effect waves-dark" href', $listitem);
									$icn = "";
									if (isset($iconmenu[$subparent]) && !is_null($iconmenu[$subparent])) {
										$icn = '<i class="'.$iconmenu[$subparent].'"></i>';
										$lst = str_replace('">','">'.$icn,$lst);
									}
									echo $lst; 
								}else{
									echo $headeritem.$listitem.'</ul></li>';
								}
								$countitem = 0;
								$listitem = "";
							}
							$icn = "";
							if (isset($iconmenu[$row["parent"]]) && !is_null($iconmenu[$row["parent"]])) $icn = '<i class="'.$iconmenu[$row["parent"]].'"></i>';
							$headeritem = '<li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">'.$icn.'<span class="hide-menu">'.$row["parent"].'</span></a><ul class="sub-menu">';
						}
 						$countitem++;
						$listitem .= '<li><a href="'.Yii::app()->createUrl($row["url"]).'">'.$row["menu"].'</a></li>';
						$parent = $row["jenisvw"];
						$subparent = $row["parent"];
					}
					if ($parent!=""){
						if ($countitem<=1){
							$lst = str_replace("a href", 'a class="waves-effect waves-dark" href', $listitem);
							$icn = "";
							if (isset($iconmenu[$subparent]) && !is_null($iconmenu[$subparent])) {
								$icn = '<i class="'.$iconmenu[$subparent].'"></i>';
								$lst = str_replace('">','">'.$icn,$lst);
							}
							echo $lst; 
						}else{
							echo $headeritem;
							echo $listitem;
							echo '</ul></li>';
						}
						
						echo '</ul></div></li>';
					}
				?>

            </ul>
        </div>
