<?php
$baseUrl = Yii::app()->theme->baseUrl;
$datanotif = $this->getNotification();
$datanotifikasi = $datanotif['datanotifikasi'];
$datapesan = $datanotif['datapesan'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IGIS-Office adalah Sistem Manajemen Surat Keluar dan Surat Masuk di sebuah Instansi / Perusahaan">
    <meta name="author" content="moeir">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl; ?>/assets/images/igis-office-light.png">
    <title>IGIS-Office :: Sistem Manajemen Surat</title>
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="<?php echo $baseUrl; ?>/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="<?php echo $baseUrl; ?>/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php echo $baseUrl; ?>/assets/node_modules/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?php echo $baseUrl; ?>/dist/css/style.min.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="<?php echo $baseUrl; ?>/dist/css/pages/dashboard1.css" rel="stylesheet">
	
	<link href="<?php echo $baseUrl; ?>/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo $baseUrl; ?>/dist/css/custom.css" rel="stylesheet">
    <?php //echo Yii::app()->bootstrap->init();?>
    
    <style>
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-blue fixed-layout sidebar-collapse">
<!--<body class="skin-megna-dark fixed-layout">-->
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">IGIS-Office</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('site'); ?>">
                        <!-- Logo icon -->
						<b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?php echo $baseUrl; ?>/assets/images/igis-office-light.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo $baseUrl; ?>/assets/images/igis-office-light.png" height="40" width="40" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                         </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <li class="d-none d-md-block d-lg-block">
                            <span style="color: #fff"><h4>&nbsp;&nbsp;&nbsp;Sistem Manajemen Surat</h4></span>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
						<?php  if(!Yii::app()->user->isGuest) { ?>
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icon-note"></i>
                            <?php if (count($datanotifikasi) > 0) { ?>
							    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            <?php } ?>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifikasi</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
		                                    <?php
		                                    if ($datanotifikasi == null || count($datanotifikasi) <= 0) {
											?>	
											
		                                    <?php
											}
											else {
		                                    	$icounter = 1;
												foreach ($datanotifikasi as $row) {
													if ($icounter > 4) {
														break;
													}
		                                    ?>
                                            <a href="<?php echo Yii::app()->createUrl('site/notifikasi', array('id'=>$row['notifikasiid'])); ?>">
                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                	<?php if ($row['status'] == '1') { ?>
                                                    <h5><?php echo $row['judul']; ?></h5> <span class="mail-desc"><?php echo substr($row['keterangan'], 0, 50); ?></span> <span class="time"><?php echo $row['tglnotifikasi']; ?></span>
                                                	<?php } else { ?>
                                                    <h5><?php echo $row['judul']; ?></h5> <span class="mail-desc"><?php echo substr($row['keterangan'], 0, 50); ?></span> <span class="time"><?php echo $row['tglnotifikasi']; ?></span>
                                                	<?php } ?>
                                                </div>
                                            </a>
		                                    <?php
													$icounter++;
		                                    	}
		                                    }
		                                    ?>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center link" href="<?php echo Yii::app()->createUrl('notifikasi/admin'); ?>"> <strong>Cek semua notifikasi</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- User Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo Yii::app()->user->getuser('picture'); ?>" alt="user" class=""> <span class="hidden-md-down"><?php echo Yii::app()->user->getuser('nama'); ?> &nbsp;<i class="fa fa-angle-down"></i></span> </a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <!-- text-->
                                <a href="<?php echo Yii::app()->createUrl('user/profil'); ?>" class="dropdown-item"><i class="ti-user"></i> Profil</a>
                                <!-- text-->
                                <div class="dropdown-divider"></div>
                                <!-- text-->
                                <a href="<?php echo Yii::app()->createUrl('user/akun'); ?>" class="dropdown-item"><i class="ti-settings"></i> Akun</a>
                                <!-- text-->
                                <div class="dropdown-divider"></div>
                                <!-- text-->
                                <a href="<?php echo Yii::app()->createUrl('site/logout'); ?>" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                                <!-- text-->
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End User Profile -->
                        <!-- ============================================================== -->
		                <?php } else { ?>
                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="<?php echo Yii::app()->createUrl('site/login'); ?>"><span>Login</span> </a>
                        </li>
		                <?php } ?>
                    </ul>
                </div>
            </nav>
        </header>
        
       	<?php echo $content; ?>
		
        <?php require_once('tpl_footer.php') ?>
        
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- <script src="<?php echo $baseUrl; ?>/assets/node_modules/jquery/jquery-3.2.1.min.js"></script> -->
	<!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo $baseUrl; ?>/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo $baseUrl; ?>/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo $baseUrl; ?>/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo $baseUrl; ?>/dist/js/custom.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/dist/js/pages/mask.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/morrisjs/morris.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Popup message jquery -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/toast-master/js/jquery.toast.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/sweetalert/sweetalert.min.js"></script>
    
	<script src="<?php echo $baseUrl; ?>/assets/node_modules/select2/dist/js/select2.full.min.js" type="text/javascript"></script>    <!-- Custom Page CSS -->
	
    <script>
    	
    </script>
</body>

</html>