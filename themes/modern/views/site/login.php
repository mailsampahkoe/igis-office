<?php
$baseUrl = Yii::app()->theme->baseUrl;
?>  
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl; ?>/assets/images/igis-office-light.png">
    <title>Login</title>
    
    <!-- page css -->
    <link href="<?php echo $baseUrl; ?>/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo $baseUrl; ?>/dist/css/style.min.css" rel="stylesheet">
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-blue card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url(<?php echo $baseUrl; ?>/assets/images/background/img00.jpg); overflow-y:auto;">
            <div class="login-box card text-center" style="margin-top:-20px; margin-bottom:5px;">
                <div>&nbsp;</div>
				<div class="row col-md-12">
                    <div class="col-md-12 text-center">
						<a href="javascript:void(0)" class="text-center m-b-40"><img src="<?php echo $baseUrl; ?>/assets/images/kemenkes.png" width="70px" height="70px" alt="Home" /></a>
						<a href="javascript:void(0)" class="text-center m-b-40"><img src="<?php echo $baseUrl; ?>/assets/images/igis-office-light.png" alt="Home" /></a>
					</div>
				</div><br/>&nbsp;<br/>
                <div class="text-center" style="font-weight: bold;">Sistem Manajemen Surat<br>Kementrian Kesehatan Republik Indonesia</div><br/>
                <div>&nbsp;</div>
            </div>
            <div class="login-box card" style="margin-bottom:5px;">
                <div class="card-body">
                    <?php
	                $form = $this->beginWidget('CActiveForm', array(
	                    'id' => 'loginform',
	                    'enableClientValidation' => true,
	                    'clientOptions' => array(
	                        'validateOnSubmit' => true,
							'enableAjaxValidation' => true,
	                        'enableClientValidation' => true,
	                        'focus' => array($model, 'firstName'),
	                    ),
						'htmlOptions'=>array(
							'class' => "form-horizontal form-material"
						)
	                ));
	                ?>
                        <h3 class="box-title m-b-20">Login</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <?php echo $form->textField($model, 'username', array("class" => "form-control", "required" => "required", "placeholder" => "Username")); ?>
								<?php echo $form->error($model, 'username', array("style" => "color:red;font-weight:bold;")); ?>
							</div>
						</div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php echo $form->passwordField($model, 'password', array("class" => "form-control", "required" => "required", "placeholder" => "Password")); ?>
								<?php echo $form->error($model, 'password', array("style" => "color:red;font-weight:bold;")); ?>								
							</div>
                        </div>
						<div class="form-group">
				          <div class="col-xs-12">
				            <select name="tahun" class="form-control">
							<?php
								foreach($tahun AS $row){
									echo "<option value='$row[nama]' ".( date("Y")==$row["nama"]? "selected='selected'" : "").">$row[nama]</option>";
					            } 
							?>
							</select>
				          </div>
				        </div>
                        <div class="form-group text-center" style="margin-bottom:10px;">
                            <div class="col-xs-12">
                                <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-block btn-lg btn-info')); ?>
                            </div>
                        </div>
                        
                    <?php $this->endWidget(); ?>
                </div>
            </div>
            <div class="login-box card text-left" style="margin-bottom:20px;padding:20px;">
				<div>Copyright © 2018 3Digit. All rights reserved.</div>
                <div>Kementrian Kesehatan Republik Indonesia</div>
            </div>
        </div>
    </section>
    
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
    </script>
    
</body>

</html>