<?php
$baseUrl = Yii::app()->theme->baseUrl;
?>  
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl; ?>/assets/images/favicon.png">
    <title>Login</title>
    
    <!-- page css -->
    <link href="<?php echo $baseUrl; ?>/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo $baseUrl; ?>/dist/css/style.min.css" rel="stylesheet">
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-blue card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url(<?php echo $baseUrl; ?>/assets/images/background/website_background_23.jpg);">
            <div class="login-box card">
                <div class="card-body">
                    <?php
	                $form = $this->beginWidget('CActiveForm', array(
	                    'id' => 'loginform',
	                    'enableClientValidation' => true,
	                    'clientOptions' => array(
	                        'validateOnSubmit' => true,
							'enableAjaxValidation' => true,
	                        'enableClientValidation' => true,
	                        'focus' => array($model, 'firstName'),
	                    ),
						'htmlOptions'=>array(
							'class' => "form-horizontal form-material"
						)
	                ));
	                ?>
                        <h3 class="box-title m-b-20">Login</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <?php echo $form->textField($model, 'username', array("class" => "form-control", "required" => "required", "placeholder" => "Username")); ?>
								<?php echo $form->error($model, 'username', array("style" => "color:red;font-weight:bold;")); ?>
							</div>
						</div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php echo $form->passwordField($model, 'password', array("class" => "form-control", "required" => "required", "placeholder" => "Password")); ?>
								<?php echo $form->error($model, 'password', array("style" => "color:red;font-weight:bold;")); ?>								
							</div>
                        </div>
						<div class="form-group">
				          <div class="col-xs-12">
				            <select name="tahun" class="form-control">
							<?php
								$sql="select nama from tmtahun where dlt='0' order by nama";
								$rows = Yii::app()->db->createCommand($sql)->queryAll();
								foreach($rows AS $row){
					                    echo "<option value='$row[nama]' ".( date("Y")==$row["nama"]? "selected='selected'" : "").">$row[nama]</option>";
					            } 
							?>
							</select>
				          </div>
				        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <?php echo $form->checkBox($model, 'rememberMe', array('class'=>'custom-control-input', "id"=>"customCheck1")); ?>
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                </div> 
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-block btn-lg btn-info btn-rounded')); ?>
                            </div>
                        </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </section>
    
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
    </script>
    
</body>

</html>