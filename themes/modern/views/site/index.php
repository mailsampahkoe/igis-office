<?php
$baseUrl = Yii::app()->theme->baseUrl;

$this->breadcrumbs = array(
    'Home',
);
?>

<?php
if (!enumVar::BCINMAIN) {
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Home</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Welcome</li>
            </ol>
        </div>
    </div>
	
</div>
<?php
}
?>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Info box -->
<!-- ============================================================== -->
<!-- End Info box -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Over Visitor, Our income , slaes different and  sales prediction -->
<!-- ============================================================== -->
    <!-- Column -->
<div class="row">
                    <!-- Column -->
                    <div class="col-lg-8 col-md-12">
                        <div class="blogs-slide m-b-15">
                            <div class="carousel slide" data-ride="carousel">
                                <!-- Carousel items -->
                                <div class="carousel-inner">
                                    <!-- <?php
                                    $icounter = 0;
                                    foreach ($databerita as $row) {
                                    ?>
                                    <?php
	                                    if ($icounter == 0) {
                                    ?>
	                                    <div class="active carousel-item">
                                    <?php
	                                    } else {
                                    ?>
	                                    <div class="carousel-item">
                                    <?php
	                                    }
                                    ?>
                                        <div class="overlaybg" style="background:url(<?php echo $baseUrl.'/assets/images/news/'.$row['img']; ?>) no-repeat center center /cover"></div>
                                        <div class="blogs-content carousel-caption"><span class="label label-danger label-rounded">Primary</span>
                                            <h4><?php echo $row['judul']; ?></h4> 
                                            <?php
                                            if ($row['jenis'] == '2') {
												echo '<a href="'.$row['url'].'" target="_blank">Read More</a>';
											}
											else {
												echo '<a href="'.Yii::app()->createUrl($row["url"]).'" target="_blank">Read More</a>';
											}
                                            ?>
                                            
                                        </div>
                                    </div>
                                    <?php
                                    	$icounter++;
							        }
                                    ?>-->
                                    <div class="active carousel-item">
                                        <!--<div class="overlaybg" style="background:url(/demo/igis-office/themes/modern/assets/images/background/bg02.jpg) no-repeat center center /cover; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;"></div>-->
                                        <div class="overlaybg" style="background:url(<?php echo $baseUrl; ?>/assets/images/news/bg02.png) no-repeat center center /cover;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-lg-4 col-md-12">
                        <div class="row">
                            <!-- Column -->
                            <div class="col-md-12">
                                <div class="card text-white" style="background-color:#060a57; padding-top:30px;">
                                    <div class="card-body ">
                                        <div class="row weather">
                                            <div class="col-8 m-t-40">
                                                <h3>&nbsp;</h3>
                                                <div class="display-4" style="font-size: 30px" id="d_time">73<sup>°F</sup></div>
                                                <p class="text-white">Kemenkes RI</p>
                                            </div>
                                            <div class="col-4 text-right">
                                                <h1 class="m-b-"><i class="fa fa-clock-o"></i></h1>
                                                <b class="text-white" id="d_day">SUNNEY DAY</b>
                                                <p class="op-5" id="d_fulldate">April 14</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <div class="col-md-12">
                                <div class="card bg-orange text-white">
                                    <div class="card-body">
                                        <div id="myCarouse2" class="carousel slide" data-ride="carousel">
                                            <!-- Carousel items -->
                                            <div class="carousel-inner">
			                                    <?php
			                                    //var_dump($datapengumuman);
			                                    ?>
			                                    
			                                    <?php
			                                    $icounter = 0;
			                                    foreach ($datapengumuman as $row) {
			                                    ?>
			                                    <?php
				                                    if ($icounter == 0) {
			                                    ?>
				                                    <div class="active carousel-item">
			                                    <?php
				                                    } else {
			                                    ?>
				                                    <div class="carousel-item">
			                                    <?php
				                                    }
			                                    ?>
                                                    <h5 class="cmin-height text-white"><?php echo $row['keterangan']; ?></h5>
                                                    <div class="d-flex no-block">
                                                        
                                                        <span class="m-l-10">
                                                    <h4 class="text-white m-b-0"><?php echo $row['pengirim']; ?></h4>
                                                    <p class="text-white"><?php echo $row['tglpengumuman']; ?></p>
                                                    </span>
                                                    </div>
                                                </div>
			                                    <?php
			                                    	$icounter++;
										        }
			                                    ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                        </div>
                    </div>
                </div>
    <!-- Column -->
    
<!-- ============================================================== -->
<!-- Comment - table -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Comment - chats -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Over Visitor, Our income , slaes different and  sales prediction -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Todo, chat, notification -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->
<div class="right-sidebar">
    <script src="<?php echo $baseUrl; ?>/dist/js/dashboard1.js"></script>
	<script src="<?php echo $baseUrl; ?>/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
	<script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
	
</div>

<script>
	startTime();
	
	function startTime() {
	    var today = new Date();
	    var h = today.getHours();
	    var m = today.getMinutes();
	    var s = today.getSeconds();
	    h = checkTime(h);
	    m = checkTime(m);
	    s = checkTime(s);
	    document.getElementById('d_time').innerHTML =
	    h + ":" + m + ":" + s;
	    var days = ["Ahad", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"];
		document.getElementById("d_day").innerHTML = days[today.getDay()];
	    var months = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"];
		document.getElementById("d_fulldate").innerHTML = today.getDate() +' '+ months[today.getMonth()] +' '+ today.getFullYear();
	    var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
	    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	    return i;
	}
</script>