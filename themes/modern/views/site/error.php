<?php
/* @var $this SiteController */
/* @var $error array */
$baseUrl = Yii::app()->theme->baseUrl;

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<link href="<?php echo $baseUrl; ?>/dist/css/pages/error-pages.css" rel="stylesheet">
<style>
	.error-box {
	    position: relative;
		background: url(<?php echo $baseUrl; ?>/assets/images/background/error-bg.jpg) no-repeat center center #fff;
		height: 100%;
		width: 100%;
		color: #151764;
	}
</style>
<section id="wrapper" class="error-page">
    <div class="error-box">
        <div class="error-body text-center">
            <h1><?php echo $code; ?></h1>
            <h3 class="text-uppercase"><?php echo CHtml::encode($message); ?></h3>
            <p class="text-muted m-t-20 m-b-20"><br><br><br><br></p>
            <a href="<?php echo Yii::app()->createUrl('site'); ?>" class="btn btn-lg btn-info waves-effect waves-light m-b-40">Back to home</a> </div>
        
    </div>
</section>
<script src="<?php echo $baseUrl; ?>/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
