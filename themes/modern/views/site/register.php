<?php
Yii::import('ext.select2.Select2');
$baseUrl = Yii::app()->theme->baseUrl;
$datas = new enumVar;
$dataids = $datas->listJenisKel();
$datanames = $datas->listJenisKel("name");
$options = '';
for($i = 0; $i<count($dataids); $i++) {
    $options .='<option value="'.$dataids[$i].'">'.$datanames[$i].'</option>';
}
?>  
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl; ?>/assets/images/esptpd-icon.png">
    <title>Register e-SPTPD - BPPRD Kota Batam</title>
    <!-- page css -->
    <link href="<?php echo $baseUrl; ?>/assets/node_modules/register-steps/register-steps.css" rel="stylesheet">
    <link href="<?php echo $baseUrl; ?>/dist/css/pages/register3.css" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php echo $baseUrl; ?>/assets/node_modules/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?php echo $baseUrl; ?>/dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">e-SPTPD</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="step-register">
        <div class="register-box">
            <div class="">
                <a href="javascript:void(0)" class="text-center m-b-40"><img src="<?php echo $baseUrl; ?>/assets/images/esptpd.png" alt="Home" /></a>
                <div class="text-center" style="font-weight: bold;">Sistem Manajemen SPTPD - BPPRD Kota Batam</div>
                <div class="text-center" style="font-style: italic;">Sudah memiliki akun, silakan Login <a href="<?php echo CController::createUrl('site/login') ?>">DI SINI</a></div>
                <!-- multistep form -->
                <?php
	                $form = $this->beginWidget('CActiveForm', array(
	                    'id' => 'registerform',
						'method' => 'POST',
	                    'enableClientValidation' => true,
						'enableAjaxValidation' => false,
	                    'clientOptions' => array(
	                        'validateOnSubmit' => true,
							'validateOnChange' => true,
							'enableAjaxValidation' => true,
	                        'enableClientValidation' => true,
	                        'focus' => array($model, 'npwp'),
	                    ),
						'htmlOptions'=>array(
							'enctype' => 'multipart/form-data',
							'class' => "msform"
						)
	                ));
	            ?>
					<?php echo $form->errorSummary($model); ?>
                    <!-- progressbar -->
                    <ul class="eliteregister">
                        <li class="active">Data Akun</li>
                        <li>Data Profil</li>
                        <li>Data Identitas</li>
                    </ul>
                    <!-- fieldsets -->
                    <fieldset>
                        <h2 class="fs-title">Buat akun Anda</h2>
                        <h3 class="fs-subtitle">Akun yang akan digunakan untuk login</h3>
                        <?php echo $form->textField($model, 'npwp', array("class" => "form-control", "required" => "required", "placeholder" => "NPWPD")); ?>
						<?php echo $form->error($model, 'npwp', array("style" => "color:red;font-weight:bold;")); ?>
						<div style="color:red;" id="errornpwp"></div>

                        <?php echo $form->textField($model, 'username', array("class" => "form-control", "required" => "required", "placeholder" => "Username diambil dari NPWPD", "readonly" => "true")); ?>
						<?php echo $form->error($model, 'username', array("style" => "color:red;font-weight:bold;")); ?>

                        <?php echo $form->passwordField($model, 'password', array("class" => "form-control", "required" => "required", "placeholder" => "Password")); ?>
						<?php echo $form->error($model, 'password', array("style" => "color:red;font-weight:bold;")); ?>
						
                        <?php echo $form->passwordField($model, 'repeat_password', array("class" => "form-control", "required" => "required", "placeholder" => "Re-password")); ?>
						<?php echo $form->error($model, 'repeat_password', array("style" => "color:red;font-weight:bold;")); ?>
						
                		<?php echo $form->labelEx($model, 'Klik tombol validasi e-mail untuk pengecekan apakah e-mail valid atau tidak'); ?>
						<div class="input-group mb-3 has-success has-feedback">
							<?php echo $form->textField($model, 'email', array("class" => "form-control", "required" => "required", "placeholder" => "E-mail")); ?>
							<!--<span class="input-group-append"><div class="input-group-text bg-transparent"><i class="fa fa-search"></i></div></span>-->
							<div class="input-group-append">
								<button class="btn btn-outline-warning" type="button" id="btnvalidate" onclick="displayFormVerifyEmail('<?php echo CHtml::activeId($model, 'email') ?>');">Validasi E-mail</button>
							</div>
						</div>
						<?php echo $form->error($model, 'email', array("style" => "color:red;font-weight:bold;")); ?>
						<?php 
							if ($model->userid == '') {
								echo $form->labelEx($model, 'status : E-mail belum divalidasi', array("id" => "lblstatus", "style" => "color:red;font-weight:bold;")); 
							}
							else {
								echo $form->labelEx($model, 'status : E-mail valid', array("id" => "lblstatus", "style" => "color:green;font-weight:bold;")); 
							}
						?><br>
						
                        <input type="button" name="next" class="next action-button" value="Next" />
                    </fieldset>
                    <fieldset>
                        <h2 class="fs-title">Profil Pengguna</h2>
                        <h3 class="fs-subtitle">Data Profil yang akan ditampilkan</h3>
                        
                        <?php echo $form->textField($model, 'nik', array("class" => "form-control", "required" => "required", "placeholder" => "NIK (Nomor KTP)")); ?>
						<?php echo $form->error($model, 'nik', array("style" => "color:red;font-weight:bold;")); ?>

                        <?php echo $form->textField($model, 'nama', array("class" => "form-control", "required" => "required", "placeholder" => "Nama Lengkap")); ?>
						<?php echo $form->error($model, 'nama', array("style" => "color:red;font-weight:bold;")); ?>
						
						<?php
		                    echo Select2::activeDropDownList($model, 'jeniskelamin', array('' => ''), array(
		                        'prompt' => 'PILIH JENIS KELAMIN',
		                        'class' => 'form-control', 'style' => 'height:55px;',
								'selected' => 'selected'
		                    ));
		                ?>
						<?php echo $form->error($model, 'jeniskelamin', array("style" => "color:red;font-weight:bold;")); ?>
						
                        <?php echo $form->textField($model, 'alamat', array("class" => "form-control", "required" => "required", "placeholder" => "Alamat")); ?>
						<?php echo $form->error($model, 'alamat', array("style" => "color:red;font-weight:bold;")); ?>

                        <?php echo $form->textField($model, 'nohp', array("class" => "form-control", "required" => "required", "placeholder" => "Nomor HP")); ?>
						<?php echo $form->error($model, 'nohp', array("style" => "color:red;font-weight:bold;")); ?>
						
                        <?php echo $form->textField($model, 'nolegal', array("class" => "form-control", "placeholder" => "Nomor Akta / Legal")); ?>
						<?php echo $form->error($model, 'nolegal', array("style" => "color:red;font-weight:bold;")); ?>
						
                        <input type="button" name="previous" class="previous action-button" value="Previous" />
                        <input type="button" name="next" class="next action-button" value="Next" />
                    </fieldset>
                    <fieldset>
                        <h2 class="fs-title">Upload File</h2>
                        <h3 class="fs-subtitle">Upload file identitas</h3>
                        
                		<?php echo $form->labelEx($model, 'Foto Profil'); ?>
						<?php echo $form->fileField($model, 'picture'); ?>
						<?php echo $form->error($model, 'picture'); ?>
						
                		<?php echo $form->labelEx($model, 'Scan KTP'); ?>
						<?php echo $form->fileField($model, 'filenik'); ?>
						<?php echo $form->error($model, 'filenik'); ?>
						
                		<?php echo $form->labelEx($model, 'Scan NPWP'); ?>
						<?php echo $form->fileField($model, 'filenpwp'); ?>
						<?php echo $form->error($model, 'filenpwp'); ?>
						
                		<?php echo $form->labelEx($model, 'Scan Legal', array("id" => "lbl_legal")); ?>
						<?php echo $form->fileField($model, 'filelegal'); ?>
						<?php echo $form->error($model, 'filelegal'); ?>
						
						<?php echo $form->hiddenField($model, 'emailstatus'); ?>
                        <input type="button" name="previous" class="previous action-button" value="Previous" />
                        <?php echo CHtml::submitButton('Register', array('id' => 'btnsubmit', 'class' => 'submit action-button')); ?>
                    </fieldset>
                    <?php $this->endWidget(); ?>
                <div class="clear"></div>
            </div>
        </div>
		<!-- Modal Verifikasi Email -->
		<div class="modal fade" id="m_modifVerifyEmail" tabindex="-1" role="dialog" aria-labelledby="m_modifVerifyEmail">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="formLabelVerifyEmail">Verifikasi E-mail</h4>
					</div>

					<div class="modal-body">
						<div id="d_descmsg"></div>
						<div id="d_verifyemailForm">
							<form id="f_verifyemail">
								<label for="txt_email_address" class="col-sm-12 control-label" style="text-align: left;">Klik tombol Kirim Kode untuk mengirimkan kode verifikasi ke e-mail anda. Setelah itu, silakan cek e-mail, lalu masukkan kode yang telah dikirim ke e-mail anda ke kolom Kode Verifikasi di bawah.</label> 
								<div class="rows">&nbsp;</div>
								<div class="form-group">
									<label for="txt_email_address" class="col-sm-12 control-label">E-mail</label>
									<div class="col-sm-12">
										<div class="input-group mb-3">
											<input type="text" name="txt_email_address" class="form-control" id="txt_email_address" placeholder="E-mail" style="height: 50px" disabled>
											<div class="input-group-append">
												<button id="btn_send_code" name="btn_send_code" class="btn btn-outline-success" type="button" onclick="sendVerifyEmailCode();">Kirim Kode</button>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="txt_verifyemail_code" class="col-sm-12 control-label">Kode Verifikasi</label> 
									<div class="col-sm-12">
									<input type="text" name="txt_verifyemail_code" class="form-control" id="txt_verifyemail_code" placeholder="Kode Verifikasi" style="height: 60px">
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div class="modal-footer">
						<div id="d_verifyemailFormBtn">
							<div class="form-group">
								<input type="button" id="btn_verifyemail" name="btn_verifyemail" value="Verifikasi" class="btn btn-primary" onclick="modifyVerifyEmail()">
								<input type="button" value="Tutup" class="btn btn-warning" onclick="$('#m_modifVerifyEmail').modal('hide');">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Modal VerifyEmail -->

    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
	
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/register-steps/jquery.easing.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/register-steps/register-init.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php echo $baseUrl; ?>/assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
    var v_jenisusaha = '';
	
    $(function() {
        $(".preloader").fadeOut();
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
    
    $('#<?php echo CHtml::activeId($model, 'jeniskelamin') ?>').html('<?php echo $options; ?>');
    
    $('#<?php echo CHtml::activeId($model, 'npwp') ?>').change(function() {
		$('#<?php echo CHtml::activeId($model, 'username') ?>').val($('#<?php echo CHtml::activeId($model, 'npwp') ?>').val());
	});
	
    $('#<?php echo CHtml::activeId($model, 'npwp') ?>').blur(function() {
		$.ajax({
			type: 'post',
			url: '<?php echo CController::createUrl('site/confirmdatawpexist') ?>',
			data: {
				key: '',
				npwpd: $('#<?php echo CHtml::activeId($model, 'npwp') ?>').val(),
			},
			success: function (p_data) {
				v_result = 'succeed';
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				v_result = 'error';
			},
			complete: function (p_data) {
				var v_responseText = JSON.parse(p_data.responseText);
				var v_data = v_responseText.data;
				//alert(v_data[0].npwpd +'-'+v_data[0].jenisusaha);
				if (v_responseText.r == "0" && v_responseText.e == "1" && v_responseText.message != undefined) {
			   		$("#errornpwp").show();
					$("#errornpwp").html(v_responseText.message);
				}
				else if (v_data.length <= 0 || (v_data.length > 0 && v_data[0].npwpd == "")) {
			   		$("#errornpwp").show();
					$("#errornpwp").html("NPWPD tidak terdaftar...");
				}
				else {
			   		$("#errornpwp").hide();
			   		v_jenisusaha = v_data[0].jenisusaha;
			   		if (v_data[0].jenisusaha == '6') {
						$('#<?php echo CHtml::activeId($model, 'nolegal') ?>').hide();
						$('#<?php echo CHtml::activeId($model, 'filelegal') ?>').hide();
						$('#lbl_legal').hide();
					}
					else {
						$('#<?php echo CHtml::activeId($model, 'nolegal') ?>').show();
						$('#<?php echo CHtml::activeId($model, 'filelegal') ?>').show();
						$('#lbl_legal').show();
					}
				}
			}
		});
	});
	
	function verifyEmail(p_email) {
		alert('valid');
		$("#<?php echo CHtml::activeId($model, 'emailstatus') ?>").val('valid');
		$("#lblstatus").text('Status : E-mail Valid');
		$("#lblstatus").attr('style', 'color:green;font-weight:bold;');
		v_emailvalid = true;
	}
	
	function displayFormVerifyEmail(p_email) {
		if ($("#"+p_email).val() == '') {
			swal("Validasi E-Mail", "Masukkan alamat e-mail yang akan anda gunakan untuk melakukan registrasi akun.");
			return;
		}
		$("#txt_email_address").val($("#"+p_email).val());
		$("#txt_verifyemail_code").val('');

		$('#m_modifVerifyEmail').modal({ backdrop: 'static', keyboard: false });
		$('#m_modifVerifyEmail').modal('show');
	}
	
	function sendVerifyEmailCode() {
		if ($("#txt_email_address").val() === '') {
			alert('E-mail address tidak boleh kosong...');
			return;
		}
		
		$("#btn_send_code").prop('disabled', true);
		$("#btn_send_code").html('<span class="glyphicon glyphicon-refresh spinning"></span>Kirim Kode &nbsp; <i class="icon-chevron-right"></i>');
		
		$.ajax({
			type: 'post',
			url: '<?php echo CController::createUrl('site/getemailtoken') ?>',
			data: {
				key: '',
				email: $("#txt_email_address").val(),
			},
			//dataType: 'text',
			//context: this,
			success: function (p_data) {
				v_result = 'succeed';
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				v_result = 'error';
			},
			complete: function (p_data) {
				/*if (v_result === 'succeed' && p_data.responseText.substr(0, 7).toLowerCase() === 'succeed') {
					alert(p_data.responseText.substr(8));
				}
				else if (p_data.responseText.substr(0, 6).toLowerCase() === 'failed') {
					alert(p_data.responseText.substr(7));
				}
				else {
					alert(p_data.responseText);
				}*/
				//alert(JSON.stringify(p_data.responseText));
				var v_data = JSON.parse(p_data.responseText);
				alert(v_data.message);
				
				$("#btn_send_code").html('Kirim Kode');
				$("#btn_send_code").prop('disabled', false);
			}
		});
		
	}
	
	function modifyVerifyEmail() {
		if ($("#txt_verifyemail_code").val() === '') {
			//alert('Kode Verifikasi harus diisi...');
			alert('Kode Verifikasi harus diisi...');
			return;
		}
		
		$("#btn_verifyemail").prop('disabled', true);
		$("#btn_verifyemail").html('<span class="glyphicon glyphicon-refresh spinning"></span>Verifikasi &nbsp; <i class="icon-chevron-right"></i>');
		
		$.ajax({
			type: 'post',
			url: '<?php echo CController::createUrl('site/validateemailtoken') ?>',
			data: {
				key: '',
				email: $("#txt_email_address").val(),
				token: $("#txt_verifyemail_code").val()
			},
			dataType: 'text',
			context: this,
			success: function (p_data) {
				v_result = 'succeed';
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				v_result = 'error';
			},
			complete: function (p_data) {
				var v_data = JSON.parse(p_data.responseText);
				if (v_data.r == '1') {
					alert(v_data.message);
					v_emailvalid = true;
					$("#<?php echo CHtml::activeId($model, 'emailstatus') ?>").val('valid');
					$("#lblstatus").text('Status : E-mail valid');
					$("#lblstatus").attr('style', 'color:green;font-weight:bold;');
					$('#m_modifVerifyEmail').modal('hide');
				}
				else if (v_data.r == '0') {
					alert(v_data.message);
					//$("#<?php echo CHtml::activeId($model, 'emailstatus') ?>").val('valid');
					//$("#lblstatus").text('Status : E-mail belum divalidasi');
					//$("#lblstatus").attr('style', 'color:red;font-weight:bold;');
				}
				else {
					alert(v_data.message);
				}
				$("#btn_verifyemail").html('Verifikasi');
				$("#btn_verifyemail").prop('disabled', false);
			}
		});
		
	}
	var v_emailvalid = <?php echo ($model->userid == '' ? 'false' : 'true'); ?>;
	$(".next").click(function(){
		if (!v_emailvalid) {
			alert('Pastikan anda telah melakukan validasi e-mail dan e-mail yang anda gunakan valid...!');
		}
   		if (v_jenisusaha == '6') {
			$('#<?php echo CHtml::activeId($model, 'nolegal') ?>').hide();
			$('#<?php echo CHtml::activeId($model, 'filelegal') ?>').hide();
		}
		else {
			$('#<?php echo CHtml::activeId($model, 'nolegal') ?>').show();
			$('#<?php echo CHtml::activeId($model, 'filelegal') ?>').show();
		}
	});
    </script>
    
    <?
    if ($result != null && count($result) > 0) {
    	if ($result['r'] == '0') {
			echo '<script>swal("'.$result['message'].'");</script>';
		}
    	else {
			echo '<script>swal("Sukses melakukan Registrasi", "'.$result['message'].'");setTimeout(function(){ window.location.href="'.CController::createUrl('site/login').'"; }, 30000);</script>';
		}
	}
    ?>
</body>

</html>