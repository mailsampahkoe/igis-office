//Variabel global
var gVarPosisiAng;
var gVarJenisOrg;
var aktifForm;

/**
 * Mengecek jumlah index karakter di sebuah kata
 * e.g : strpos('saya','a')
 * result : 2
 */
function strpos(haystack, needle, offset) {
    var i = (haystack + '').indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

/**
 * Mengecek apakah bilangan tersebut INTEGER
 * e.g : isInt('233.5')
 * result : false
 **/
function isInt(v) {
    var isPass;
    if (v == null)
        return false;
    for (i = 0; i < v.length; i++) {
        isPass = false;
        for (j = 0; j <= 9; j++) {
            if (parseInt(v.substr(i, 1)) == j) {
                isPass = true;
                break;
            }
        }
        if (!isPass) {
            break;
        }
    }
    return isPass;
}

/**
 *Merubah nilai menjadi format uang
 * e.g : MoneyFormat(1232400)
 * result : 1,232,400
 **/
function MoneyFormat(v, maxDesimal) {
    if (v == null)
        return '0';
    if (maxDesimal == null)
        maxDesimal = 2;
    v = parseStr(v);
    var isNegatif = false;
    if (v.substr(0, 1) == '-') {
        isNegatif = true;

    }
    var hasil = '';
    var desimal = '.00';
    if ((!strpos(v, '.') ? false : true)) {
        desimal = '';
        var i = v.length - 1;
        var jalan = true;
        while (jalan) {
            desimal = v.substr(i, 1) + desimal;
            if (v.substr(i, 1) == '.')
                jalan = false;
            i--;
        }
        v = v.replace(desimal, '');
    }
    var ind = v.length - 1;
    for (i = 1; i <= v.length; i++) {
        hasil = v.substr(ind, 1) + hasil;
        if (i != v.length && i % 3 == 0)
            hasil = ',' + hasil;
        ind--;
    }
    des = roundNumber(('0' + desimal), maxDesimal);
    desimal = (des == 0 ? desimal : parseStr(des).replace('0.', '.'));

    while ((desimal.length - 1) < maxDesimal) {
        desimal += '0';
    }
    var result = hasil + desimal;
    result = result.replace('-,', '-');
    return result;
}

/**
 *Merubah type data nilai menjadi String
 **/
function parseStr(v) {
    return v + '';
}

/**
 *Pembulatan bilangan
 **/
function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}


//Mendapatakan panjang sebuah string
function strlen(string) {
    var str = string + '';
    var i = 0, chr = '',
            lgth = 0;

    if (!this.php_js || !this.php_js.ini || !this.php_js.ini['unicode.semantics'] || this.php_js.ini['unicode.semantics'].local_value.toLowerCase() !== 'on') {
        return string.length;
    }

    var getWholeChar = function(str, i) {
        var code = str.charCodeAt(i);
        var next = '', prev = '';
        if (0xD800 <= code && code <= 0xDBFF) { // High surrogate (could change last hex to 0xDB7F to treat high private surrogates as single characters)
            if (str.length <= (i + 1)) {
                throw 'High surrogate without following low surrogate';
            }
            next = str.charCodeAt(i + 1);
            if (0xDC00 > next || next > 0xDFFF) {
                throw 'High surrogate without following low surrogate';
            }
            return str.charAt(i) + str.charAt(i + 1);
        } else if (0xDC00 <= code && code <= 0xDFFF) { // Low surrogate
            if (i === 0) {
                throw 'Low surrogate without preceding high surrogate';
            }
            prev = str.charCodeAt(i - 1);
            if (0xD800 > prev || prev > 0xDBFF) { //(could change last hex to 0xDB7F to treat high private surrogates as single characters)
                throw 'Low surrogate without preceding high surrogate';
            }
            return false; // We can pass over low surrogates now as the second component in a pair which we have already processed
        }
        return str.charAt(i);
    };

    for (i = 0, lgth = 0; i < str.length; i++) {
        if ((chr = getWholeChar(str, i)) === false) {
            continue;
        } // Adapt this line at the top of any loop, passing in the whole string and the current iteration and returning a variable to represent the individual character; purpose is to treat the first part of a surrogate pair as the whole character and then ignore the second part
        lgth++;
    }
    return lgth;
}

/**
 merubah format tanggal menjadi bilangan:
 e.g : DateFormat('17-12-2012')
 result : 17 Desember 2012
 **/
function DateFormat(tanggal) {
    if (tanggal === null)
        return '';
    arTanggal = tanggal.split('-');
    var strBulan = '';
    if (arTanggal[1] == '01') {
        strBulan = 'Januari';
    } else if (arTanggal[1] == '02') {
        strBulan = 'Februari';
    } else if (arTanggal[1] == '03') {
        strBulan = 'Maret';
    } else if (arTanggal[1] == '04') {
        strBulan = 'April';
    } else if (arTanggal[1] == '05') {
        strBulan = 'Mei';
    } else if (arTanggal[1] == '06') {
        strBulan = 'Juni';
    } else if (arTanggal[1] == '07') {
        strBulan = 'Juli';
    } else if (arTanggal[1] == '08') {
        strBulan = 'Agustus';
    } else if (arTanggal[1] == '09') {
        strBulan = 'September';
    } else if (arTanggal[1] == '10') {
        strBulan = 'Oktober';
    } else if (arTanggal[1] == '11') {
        strBulan = 'November';
    } else if (arTanggal[1] == '12') {
        strBulan = 'Desember';
    }
    return arTanggal[0] + ' ' + strBulan + ' ' + arTanggal[2];
}

function gbMoney(value, metaData, record, rowIdx, colIdx, store, view) {
    return '' + MoneyFormat(Number(value)) + '';
    this.setEditable(false);
}

function gbMoneyRight(value, metaData, record, rowIdx, colIdx, store, view) {
    return '<span style="float:right;">' + MoneyFormat(Number(value)) + '</style>';
    this.setEditable(false);
}

function gbInputMoney(value, metaData, record, rowIdx, colIdx, store, view) {
    metaData.style = "background-color:#E8FCEE;";
    metaData.attr = 'style="background-color: #E8FCEE;"';
    return '<span style="color:black;float:right;"><b>' + MoneyFormat(value) + '</b></span>';

}

function gbSum(val) {
    var value = parseInt(val);
    return '<span style="color:black;float:left;font-weight:bold;font-size:12px;"></span> <span style="color:black;float:right;font-weight:bold;font-size:12px;">' + MoneyFormat(value) + '</span>';
}

function eTextArea(v) {
    return v.replace(/\n/g, '<br>')
}

var eGrafik = function(item) {
    console.log(item);
    if (Number(item) < 0) {
        return "<font color=red> " + gbMoney(Number(item)) + "</font> ";
    } else {
        return "<font color=blue> " + gbMoney(Number(item)) + "</font> ";
    }
}

var ePersen = function(item) {
    if (Number(item) < 0) {
        return "<font color=red> " + Number(item) + "</font> (%)";
    } else {
        return "<font color=blue> " + Number(item) + "</font> (%)";
    }
}


function showLoadingMask()
{
    var loadText = "Loading... Please wait";
    if (Ext.isEmpty(Ext.getBody()))
        loadText = 'Loading... Please wait';
//Use the mask function on the Ext.getBody() element to mask the body element during Ajax calls
    Ext.Ajax.on('beforerequest', function() {
        Ext.getBody().mask(loadText, 'loading')
    }, Ext.getBody());
    Ext.Ajax.on('requestcomplete', Ext.getBody().unmask, Ext.getBody());
    Ext.Ajax.on('requestexception', Ext.getBody().unmask, Ext.getBody());
}
